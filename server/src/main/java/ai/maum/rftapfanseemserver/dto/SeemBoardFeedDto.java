package ai.maum.rftapfanseemserver.dto;

public class SeemBoardFeedDto {
    private int feedNo;
    private int feedUserNo;
    private int createUserNo;
    private String feedType;
    private String text;
    private String imageUrl;
    private String createDate;
    private String deleteDate;
    private String userName;
    private String profileImageUrl;
    private int likeCnt;
    private int commentCnt;
    private int seemCnt;
    private Boolean isLiked;
    private Boolean isSeemed;
    private String feedContentType;
    private String feedContentUrl;

    public SeemBoardFeedDto() {
    }

    public SeemBoardFeedDto(int feedNo, int feedUserNo, int createUserNo, String feedType, String text, String imageUrl, String createDate, String deleteDate, String userName, String profileImageUrl, int likeCnt, int commentCnt, int seemCnt, Boolean isLiked, Boolean isSeemed, String feedContentType, String feedContentUrl) {
        this.feedNo = feedNo;
        this.feedUserNo = feedUserNo;
        this.createUserNo = createUserNo;
        this.feedType = feedType;
        this.text = text;
        this.imageUrl = imageUrl;
        this.createDate = createDate;
        this.deleteDate = deleteDate;
        this.userName = userName;
        this.profileImageUrl = profileImageUrl;
        this.likeCnt = likeCnt;
        this.commentCnt = commentCnt;
        this.seemCnt = seemCnt;
        this.isLiked = isLiked;
        this.isSeemed = isSeemed;
        this.feedContentType = feedContentType;
        this.feedContentUrl = feedContentUrl;
    }

    public int getFeedNo() {
        return feedNo;
    }

    public void setFeedNo(int feedNo) {
        this.feedNo = feedNo;
    }

    public int getFeedUserNo() {
        return feedUserNo;
    }

    public void setFeedUserNo(int feedUserNo) {
        this.feedUserNo = feedUserNo;
    }

    public int getCreateUserNo() {
        return createUserNo;
    }

    public void setCreateUserNo(int createUserNo) {
        this.createUserNo = createUserNo;
    }

    public String getFeedType() {
        return feedType;
    }

    public void setFeedType(String feedType) {
        this.feedType = feedType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(String deleteDate) {
        this.deleteDate = deleteDate;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public int getLikeCnt() {
        return likeCnt;
    }

    public void setLikeCnt(int likeCnt) {
        this.likeCnt = likeCnt;
    }

    public int getCommentCnt() {
        return commentCnt;
    }

    public void setCommentCnt(int commentCnt) {
        this.commentCnt = commentCnt;
    }

    public int getSeemCnt() {
        return seemCnt;
    }

    public void setSeemCnt(int seemCnt) {
        this.seemCnt = seemCnt;
    }

    public Boolean getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(Boolean liked) {
        isLiked = liked;
    }

    public Boolean getIsSeemed() {
        return isSeemed;
    }

    public void setIsSeemed(Boolean seemed) {
        isSeemed = seemed;
    }

    public String getFeedContentType() {
        return feedContentType;
    }

    public void setFeedContentType(String feedContentType) {
        this.feedContentType = feedContentType;
    }

    public String getFeedContentUrl() {
        return feedContentUrl;
    }

    public void setFeedContentUrl(String feedContentUrl) {
        this.feedContentUrl = feedContentUrl;
    }

    @Override
    public String toString() {
        return "SeemBoardFeedDto{" +
                "feedNo=" + feedNo +
                ", feedUserNo=" + feedUserNo +
                ", createUserNo=" + createUserNo +
                ", feedType='" + feedType + '\'' +
                ", text='" + text + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", createDate='" + createDate + '\'' +
                ", deleteDate='" + deleteDate + '\'' +
                ", userName='" + userName + '\'' +
                ", profileImageUrl='" + profileImageUrl + '\'' +
                ", likeCnt=" + likeCnt +
                ", commentCnt=" + commentCnt +
                ", seemCnt=" + seemCnt +
                ", isLiked=" + isLiked +
                ", isSeemed=" + isSeemed +
                '}';
    }
}
