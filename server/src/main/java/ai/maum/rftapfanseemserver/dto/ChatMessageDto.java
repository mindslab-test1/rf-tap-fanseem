package ai.maum.rftapfanseemserver.dto;

import ai.maum.rftapfanseemserver.ChatMessage;
import ai.maum.rftapfanseemserver.ChatMessageList;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ChatMessageDto implements Serializable {

    @JsonProperty("CREATE_TIME")
    private String createTime;

    @JsonProperty("DELETE_TIME")
    private String deleteTime;

    @JsonProperty("USER_NO")
    private Integer userNo;

    @JsonProperty("CHAT_MESSAGE_NO")
    private Integer chatMessageNo;

    @JsonProperty("CONTENT_TYPE")
    private String contentType;

    @JsonProperty("CONTENT_STRING")
    private String contentString;

    @JsonProperty("ACTIVE")
    private Character active;

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(String deleteTime) {
        this.deleteTime = deleteTime;
    }

    public Integer getUserNo() {
        return userNo;
    }

    public void setUserNo(Integer userNo) {
        this.userNo = userNo;
    }

    public Integer getChatMessageNo() {
        return chatMessageNo;
    }

    public void setChatMessageNo(Integer chatMessageNo) {
        this.chatMessageNo = chatMessageNo;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public String getContentString() {
        return contentString;
    }

    public void setContentString(String contentString) {
        this.contentString = contentString;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "ChatMessageDto{" +
                "createTime='" + createTime + '\'' +
                ", deleteTime='" + deleteTime + '\'' +
                ", userNo=" + userNo +
                ", chatMessageNo=" + chatMessageNo +
                ", contentType='" + contentType + '\'' +
                ", contentString='" + contentString + '\'' +
                ", active=" + active +
                '}';
    }

    public static ChatMessage buildChatMessageFromChatMessageDto(ChatMessageDto chatMessageDto) {
        ChatMessage.Builder chatMessageBuilder = ChatMessage.newBuilder();

        if (chatMessageDto == null) {
            return chatMessageBuilder.build();
        }

        chatMessageBuilder
                .setCreateTime(chatMessageDto.getCreateTime())
                .setUserNo(chatMessageDto.getUserNo())
                .setContentType(chatMessageDto.getContentType())
                .setChatMessageNo(chatMessageDto.getChatMessageNo())
                .setContentString(chatMessageDto.getContentString());

        return chatMessageBuilder.build();
    }

    public static ChatMessageList buildChatMessageListFromChatMessageDtoList(List<ChatMessageDto> chatMessageDtoList) {
        ChatMessageList.Builder responseBuilder = ChatMessageList.newBuilder();

        for (ChatMessageDto iter : chatMessageDtoList) {
            ChatMessage chatMessage = buildChatMessageFromChatMessageDto(iter);
            responseBuilder.addChatMessage(chatMessage);
        }

        ChatMessageList chatMessageList = responseBuilder.build();

        return chatMessageList;
    }
}
