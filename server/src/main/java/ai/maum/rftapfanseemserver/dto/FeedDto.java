package ai.maum.rftapfanseemserver.dto;

import ai.maum.rftapfanseemserver.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FeedDto implements Serializable {

    @JsonProperty("FEED_NO")
    private Integer feedNo;

    @JsonProperty("FEED_USER_NO")
    private Integer feedUserNo;

    @JsonProperty("CREATE_USER_NO")
    private Integer createUserNo;

    @JsonProperty("FEED_TYPE")
    private String feedType;

    @JsonProperty("TEXT")
    private String text;

    @JsonProperty("IMAGE_URL")
    private String imageUrl;

    @JsonProperty("ACTIVE")
    private Character active;

    @JsonProperty("CREATE_DATE")
    private String createDate;

    @JsonProperty("UPDATE_DATE")
    private String updateDate;

    private Integer likeCnt;

    private Integer commentCnt;

    private Integer seemCnt;

    private Boolean isLiked;

    private Boolean isSeemed;

    private List<FeedCommentDto> comments;

    public Integer getFeedNo() {
        return feedNo;
    }

    public void setFeedNo(Integer feedNo) {
        this.feedNo = feedNo;
    }

    public Integer getFeedUserNo() {
        return feedUserNo;
    }

    public void setFeedUserNo(Integer feedUserNo) {
        this.feedUserNo = feedUserNo;
    }

    public Integer getCreateUserNo() {
        return createUserNo;
    }

    public void setCreateUserNo(Integer createUserNo) {
        this.createUserNo = createUserNo;
    }

    public String getFeedType() {
        return feedType;
    }

    public void setFeedType(String feedType) {
        this.feedType = feedType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public Integer getLikeCnt() {
        return likeCnt;
    }

    public void setLikeCnt(Integer likeCnt) {
        this.likeCnt = likeCnt;
    }

    public Integer getCommentCnt() {
        return commentCnt;
    }

    public void setCommentCnt(Integer commentCnt) {
        this.commentCnt = commentCnt;
    }

    public Integer getSeemCnt() {
        return seemCnt;
    }

    public void setSeemCnt(Integer seemCnt) {
        this.seemCnt = seemCnt;
    }

    public List<FeedCommentDto> getComments() {
        return comments;
    }

    public void setComments(List<FeedCommentDto> comments) {
        this.comments = comments;
    }

    public Boolean getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(Boolean isLiked) {
        this.isLiked = isLiked;
    }

    public Boolean getIsSeemed() {
        return isSeemed;
    }

    public void setIsSeemed(Boolean isSeemed) {
        this.isSeemed = isSeemed;
    }

    @Override
    public String toString() {
        return "FeedDto{" +
                "feedNo=" + feedNo +
                ", feedUserNo=" + feedUserNo +
                ", createUserNo=" + createUserNo +
                ", feedType='" + feedType + '\'' +
                ", text='" + text + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", active=" + active +
                ", createDate='" + createDate + '\'' +
                ", updateDate='" + updateDate + '\'' +
                ", likeCnt=" + likeCnt +
                ", commentCnt=" + commentCnt +
                ", seemCnt=" + seemCnt +
                ", comments=" + comments +
                ", isSeemed=" + isSeemed +
                ", isLiked=" + isLiked +
                '}';
    }

    public static Feed buildFeedFromFeedDto(FeedDto feedDto) {
        Feed.Builder feedBuilder = Feed.newBuilder();

        if (feedDto == null) {
            return feedBuilder.build();
        }

        if (feedDto.getFeedNo() != null) {
            feedBuilder.setFeedNo(feedDto.getFeedNo());
        }
        if (feedDto.getFeedUserNo() != null) {
            feedBuilder.setFeedUserNo(feedDto.getFeedUserNo());
        }
        if (feedDto.getCreateUserNo() != null) {
            feedBuilder.setCreateUserNo(feedDto.getCreateUserNo());
        }
        if (feedDto.getFeedType() != null) {
            feedBuilder.setFeedType(feedDto.getFeedType());
        }
        if (feedDto.getText() != null) {
            feedBuilder.setText(feedDto.getText());
        }
        if (feedDto.getImageUrl() != null) {
            feedBuilder.setImageUrl(feedDto.getImageUrl());
        }
        if (feedDto.getCreateDate() != null) {
            feedBuilder.setCreateDate(feedDto.getCreateDate());
        }
        if (feedDto.getUpdateDate() != null) {
            feedBuilder.setUpdateDate(feedDto.getUpdateDate());
        }
        if (feedDto.getCommentCnt() != null) {
            feedBuilder.setCommentCnt(feedDto.getCommentCnt());
        }
        if (feedDto.getLikeCnt() != null) {
            feedBuilder.setLikeCnt(feedDto.getLikeCnt());
        }
        if (feedDto.getSeemCnt() != null) {
            feedBuilder.setSeemCnt(feedDto.getSeemCnt());
        }
        if (feedDto.getComments() != null) {
            FeedCommentList feedCommentList = FeedCommentDto.buildFeedCommentListFromFeedCommentDtoList(feedDto.getComments());
            feedBuilder.setComments(feedCommentList);
        }
        if (feedDto.getIsLiked() != null) {
            feedBuilder.setIsLiked(feedDto.getIsLiked());
        }
        if (feedDto.getIsSeemed() != null) {
            feedBuilder.setIsSeemed(feedDto.getIsSeemed());
        }

        return feedBuilder.build();
    }

    public static FeedList buildFeedListFromFeedDtoList(List<FeedDto> feedDtoList) {
        FeedList.Builder responseBuilder = FeedList.newBuilder();

        for (FeedDto iter : feedDtoList) {
            Feed feed = buildFeedFromFeedDto(iter);
            responseBuilder.addFeed(feed);
        }

        FeedList feedList = responseBuilder.build();

        return feedList;
    }
}
