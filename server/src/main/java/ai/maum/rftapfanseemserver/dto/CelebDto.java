package ai.maum.rftapfanseemserver.dto;

import ai.maum.rftapfanseemserver.Celeb;
import ai.maum.rftapfanseemserver.CelebList;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class CelebDto implements Serializable {

    @JsonProperty("CREATE_TIME")
    private String createTime;

    @JsonProperty("UPDATE_TIME")
    private String updateTime;

    @JsonProperty("USER_NO")
    private Integer userNo;

    @JsonProperty("NAME")
    private String name;

    @JsonProperty("PROFILE_IMAGE_URL")
    private String profileImageUrl;

    @JsonProperty("RELATED_CATEGORY_LIST")
    private String relatedCategoryList;

    @JsonProperty("INTRODUCTION")
    private String introduction;

    @JsonProperty("SUBSCRIBER_COUNT")
    private Integer subscriberCount;

    @JsonProperty("IS_SERVICE_MORNING_CALL")
    private Character isServiceMorningCall;

    @JsonProperty("IS_SERVICE_CHEER")
    private Character isServiceCheer;

    @JsonProperty("IS_SERVICE_TTS")
    private Character isServiceTts;

    @JsonProperty("EMAIL")
    private String email;

    @JsonProperty("COMPANY")
    private String company;

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getUserNo() {
        return userNo;
    }

    public void setUserNo(Integer userNo) {
        this.userNo = userNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getRelatedCategoryList() {
        return relatedCategoryList;
    }

    public void setRelatedCategoryList(String relatedCategoryList) {
        this.relatedCategoryList = relatedCategoryList;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public Integer getSubscriberCount() {
        return subscriberCount;
    }

    public void setSubscriberCount(Integer subscriberCount) {
        this.subscriberCount = subscriberCount;
    }

    public Character getIsServiceMorningCall() {
        return isServiceMorningCall;
    }

    public void setIsServiceMorningCall(Character isServiceMorningCall) {
        this.isServiceMorningCall = isServiceMorningCall;
    }

    public Character getIsServiceCheer() {
        return isServiceCheer;
    }

    public void setIsServiceCheer(Character isServiceCheer) {
        this.isServiceCheer = isServiceCheer;
    }

    public Character getIsServiceTts() {
        return isServiceTts;
    }

    public void setIsServiceTts(Character isServiceTts) {
        this.isServiceTts = isServiceTts;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    @Override
    public String toString() {
        return "CelebDto{" +
                "createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", userNo='" + userNo + '\'' +
                ", name='" + name + '\'' +
                ", profileImageUrl='" + profileImageUrl + '\'' +
                ", relatedCategoryList='" + relatedCategoryList + '\'' +
                ", introduction='" + introduction + '\'' +
                ", subscriberCount=" + subscriberCount +
                ", isServiceMorningCall=" + isServiceMorningCall +
                ", isServiceCheer=" + isServiceCheer +
                ", isServiceTts=" + isServiceTts +
                ", email='" + email + '\'' +
                ", company='" + company + '\'' +
                '}';
    }

    public static Celeb buildCelebFromCelebDto(CelebDto celebDto) {
        Celeb.Builder celebBuilder = Celeb.newBuilder();

        if (celebDto == null) {
            return celebBuilder.build();
        }

        if (celebDto.getCreateTime() != null) {
            celebBuilder.setCreateTime(celebDto.getCreateTime());
        }
        if (celebDto.getUpdateTime() != null) {
            celebBuilder.setUpdateTime(celebDto.getUpdateTime());
        }
        if (celebDto.getUserNo() != null) {
            celebBuilder.setUserNo(celebDto.getUserNo());
        }
        if (celebDto.getName() != null) {
            celebBuilder.setName(celebDto.getName());
        }
        if (celebDto.getSubscriberCount() != null) {
            celebBuilder.setSubscriberCount(celebDto.getSubscriberCount());
        }
        if (celebDto.getIsServiceMorningCall() != null) {
            celebBuilder.setIsServiceMorningCall(celebDto.getIsServiceMorningCall() == 'Y');
        }
        if (celebDto.getIsServiceCheer() != null) {
            celebBuilder.setIsServiceCheer(celebDto.getIsServiceCheer() == 'Y');
        }
        if (celebDto.getIsServiceTts() != null) {
            celebBuilder.setIsServiceTts(celebDto.getIsServiceTts() == 'Y');
        }
        if (celebDto.getProfileImageUrl() != null) {
            celebBuilder.setProfileImageUrl(celebDto.getProfileImageUrl());
        }
        if (celebDto.getIntroduction() != null) {
            celebBuilder.setIntroduction(celebDto.getIntroduction());
        }
        if (celebDto.getCompany() != null) {
            celebBuilder.setCompany(celebDto.getCompany());
        }
        if (celebDto.getEmail() != null) {
            celebBuilder.setEmail(celebDto.getEmail());
        }

        if (celebDto.getRelatedCategoryList() != null) {
            String[] categoryArray = celebDto.getRelatedCategoryList().split("\\$");
            for (String iter : categoryArray) {
                if (iter.equals("")) continue;
                celebBuilder.addRelatedCategoryList(iter);
            }
        }

        return celebBuilder.build();
    }

    public static CelebList buildCelebListFromCelebDtoList(List<CelebDto> celebDtoList) {
        CelebList.Builder responseBuilder = CelebList.newBuilder();

        for (CelebDto iter : celebDtoList) {
            Celeb celeb = buildCelebFromCelebDto(iter);
            responseBuilder.addCeleb(celeb);
        }

        CelebList celebList = responseBuilder.build();

        return celebList;
    }
}
