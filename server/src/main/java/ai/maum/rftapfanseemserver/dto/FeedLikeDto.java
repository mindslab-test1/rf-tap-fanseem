package ai.maum.rftapfanseemserver.dto;

import ai.maum.rftapfanseemserver.FeedComment;
import ai.maum.rftapfanseemserver.FeedCommentList;
import ai.maum.rftapfanseemserver.FeedLike;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FeedLikeDto implements Serializable {

    @JsonProperty("LIKE_NO")
    private Integer likeNo;

    @JsonProperty("FEED_NO")
    private Integer feedNo;

    @JsonProperty("LIKE_USER_NO")
    private Integer likeUserNo;

    @JsonProperty("ACTIVE")
    private Character active;

    @JsonProperty("CREATE_DATE")
    private String createDate;

    @JsonProperty("ACTIVE_DATE")
    private String activeDate;

    public Integer getLikeNo() {
        return likeNo;
    }

    public void setLikeNo(Integer likeNo) {
        this.likeNo = likeNo;
    }

    public Integer getFeedNo() {
        return feedNo;
    }

    public void setFeedNo(Integer feedNo) {
        this.feedNo = feedNo;
    }

    public Integer getLikeUserNo() {
        return likeUserNo;
    }

    public void setLikeUserNo(Integer likeUserNo) {
        this.likeUserNo = likeUserNo;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getActiveDate() {
        return activeDate;
    }

    public void setActiveDate(String activeDate) {
        this.activeDate = activeDate;
    }

    @Override
    public String toString() {
        return "FeedCommentDto{" +
                "likeNo=" + likeNo +
                ", feedNo=" + feedNo +
                ", likeUserNo=" + likeUserNo +
                ", active=" + active +
                ", createDate='" + createDate + '\'' +
                ", activeDate='" + activeDate + '\'' +
                '}';
    }

    public static FeedLike buildFeedLikeFromFeedLikeDto(FeedLikeDto feedLikeDto) {
        FeedLike.Builder feedLikeBuilder = FeedLike.newBuilder();

        if (feedLikeDto == null) {
            return feedLikeBuilder.build();
        }

        if (feedLikeDto.getFeedNo() != null) {
            feedLikeBuilder.setFeedNo(feedLikeDto.getFeedNo());
        }
        if (feedLikeDto.getLikeUserNo() != null) {
            feedLikeBuilder.setLikeUserNo(feedLikeDto.getLikeUserNo());
        }
        if (feedLikeDto.getActive() != null) {
            feedLikeBuilder.setActive(feedLikeDto.getActive() == 'Y');
        }

        return feedLikeBuilder.build();
    }
}
