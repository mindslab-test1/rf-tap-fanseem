package ai.maum.rftapfanseemserver.dto;

import ai.maum.rftapfanseemserver.FeedComment;
import ai.maum.rftapfanseemserver.FeedCommentList;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FeedCommentDto implements Serializable {

    @JsonProperty("COMMENT_NO")
    private Integer commentNo;

    @JsonProperty("FEED_NO")
    private Integer feedNo;

    @JsonProperty("COMMENT_USER_NO")
    private Integer commentUserNo;

    @JsonProperty("COMMENT_TEXT")
    private String commentText;

    @JsonProperty("ACTIVE")
    private Character active;

    @JsonProperty("CREATE_DATE")
    private String createDate;

    @JsonProperty("DELETE_DATE")
    private String deleteDate;

    public Integer getCommentNo() {
        return commentNo;
    }

    public void setCommentNo(Integer commentNo) {
        this.commentNo = commentNo;
    }

    public Integer getFeedNo() {
        return feedNo;
    }

    public void setFeedNo(Integer feedNo) {
        this.feedNo = feedNo;
    }

    public Integer getCommentUserNo() {
        return commentUserNo;
    }

    public void setCommentUserNo(Integer commentUserNo) {
        this.commentUserNo = commentUserNo;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getDeleteDate() {
        return deleteDate;
    }

    public void setDeleteDate(String deleteDate) {
        this.deleteDate = deleteDate;
    }

    @Override
    public String toString() {
        return "FeedCommentDto{" +
                "commentNo=" + commentNo +
                ", feedNo=" + feedNo +
                ", commentUserNo=" + commentUserNo +
                ", commentText='" + commentText + '\'' +
                ", active=" + active +
                ", createDate='" + createDate + '\'' +
                ", deleteDate='" + deleteDate + '\'' +
                '}';
    }

    public static FeedComment buildFeedCommentFromFeedCommentDto(FeedCommentDto feedCommentDto) {
        FeedComment.Builder feedCommentBuilder = FeedComment.newBuilder();

        if (feedCommentDto == null) {
            return feedCommentBuilder.build();
        }

        if (feedCommentDto.getCommentNo() != null) {
            feedCommentBuilder.setCommentNo(feedCommentDto.getCommentNo());
        }
        if (feedCommentDto.getFeedNo() != null) {
            feedCommentBuilder.setFeedNo(feedCommentDto.getFeedNo());
        }
        if (feedCommentDto.getCommentUserNo() != null) {
            feedCommentBuilder.setCommentUserNo(feedCommentDto.getCommentUserNo());
        }
        if (feedCommentDto.getCommentText() != null) {
            feedCommentBuilder.setCommentText(feedCommentDto.getCommentText());
        }
        if (feedCommentDto.getCreateDate() != null) {
            feedCommentBuilder.setCreateDate(feedCommentDto.getCreateDate());
        }

        return feedCommentBuilder.build();
    }

    public static FeedCommentList buildFeedCommentListFromFeedCommentDtoList(List<FeedCommentDto> feedCommentDtoList) {
        FeedCommentList.Builder responseBuilder = FeedCommentList.newBuilder();

        for (FeedCommentDto iter : feedCommentDtoList) {
            FeedComment feedComment = buildFeedCommentFromFeedCommentDto(iter);
            responseBuilder.addFeedComment(feedComment);
        }

        FeedCommentList feedCommentList = responseBuilder.build();

        return feedCommentList;
    }
}
