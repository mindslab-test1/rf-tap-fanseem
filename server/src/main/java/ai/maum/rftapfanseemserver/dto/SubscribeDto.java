package ai.maum.rftapfanseemserver.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SubscribeDto implements Serializable {
    @JsonProperty("ID")
    private int id;

    @JsonProperty("CELEB_NO")
    private int celebNo;

    @JsonProperty("USER_NO")
    private int userNo;

    @JsonProperty("ACTIVE")
    private String active;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCelebNo() {
        return celebNo;
    }

    public void setCelebNo(int celebNo) {
        this.celebNo = celebNo;
    }

    public int getUserNo() {
        return userNo;
    }

    public void setUserNo(int userNo) {
        this.userNo = userNo;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    @Override
    public String toString() {
        return "SubscribeDto{" +
                "id=" + id +
                ", celebNo=" + celebNo +
                ", userNo=" + userNo +
                ", active='" + active + '\'' +
                '}';
    }
}
