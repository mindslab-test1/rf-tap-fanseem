package ai.maum.rftapfanseemserver.dto;

public class PushSettingsDto {
    private int id;
    private int userNo;
    private boolean pushMessage;
    private boolean pushFeed;
    private boolean pushActivity;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserNo() {
        return userNo;
    }

    public void setUserNo(int userNo) {
        this.userNo = userNo;
    }

    public boolean getPushMessage() {
        return pushMessage;
    }

    public void setPushMessage(boolean pushMessage) {
        this.pushMessage = pushMessage;
    }

    public boolean getPushFeed() {
        return pushFeed;
    }

    public void setPushFeed(boolean pushFeed) {
        this.pushFeed = pushFeed;
    }

    public boolean getPushActivity() {
        return pushActivity;
    }

    public void setPushActivity(boolean pushActivity) {
        this.pushActivity = pushActivity;
    }

    @Override
    public String toString() {
        return "PushSettingsDto{" +
                "id=" + id +
                ", userNo=" + userNo +
                ", pushMessage=" + pushMessage +
                ", pushFeed=" + pushFeed +
                ", pushActivity=" + pushActivity +
                '}';
    }
}
