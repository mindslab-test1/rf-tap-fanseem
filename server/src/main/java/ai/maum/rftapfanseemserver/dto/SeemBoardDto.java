package ai.maum.rftapfanseemserver.dto;

public class SeemBoardDto {
    private int boardNo;
    private String boardName;
    private int seemedCnt;

    public SeemBoardDto() {
    }

    public SeemBoardDto(int boardNo, String boardName, int seemedCnt) {
        this.boardNo = boardNo;
        this.boardName = boardName;
        this.seemedCnt = seemedCnt;
    }

    public int getBoardNo() {
        return boardNo;
    }

    public void setBoardNo(int boardNo) {
        this.boardNo = boardNo;
    }

    public String getBoardName() {
        return boardName;
    }

    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }

    public int getSeemedCnt() {
        return seemedCnt;
    }

    public void setSeemedCnt(int seemedCnt) {
        this.seemedCnt = seemedCnt;
    }

    @Override
    public String toString() {
        return "SeemBoardDto{" +
                "boardNo=" + boardNo +
                ", boardName='" + boardName + '\'' +
                ", seemedCnt=" + seemedCnt +
                '}';
    }
}
