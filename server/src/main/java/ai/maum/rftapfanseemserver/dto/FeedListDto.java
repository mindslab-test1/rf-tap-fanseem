package ai.maum.rftapfanseemserver.dto;

public class FeedListDto {
    private int feedNo;
    private int feedUserNo;
    private String feedType;
    private String text;
    private String imageUrl;
    private String createDate;
    private int commentCnt;
    private int seemCnt;
    private int likeCnt;
    private Boolean isSeemed;
    private Boolean isLiked;
    private int userNo;
    private String name;
    private String profileImageUrl;
    private String social;
    private String gender;
    private int age;
    private Boolean isCeleb;
    private String email;
    private String feedContentType;
    private String feedContentUrl;

    public FeedListDto() {
    }

    public FeedListDto(int feedNo, int feedUserNo, String feedType, String text, String imageUrl, String createDate, int commentCnt, int seemCnt, int likeCnt, Boolean isSeemed, Boolean isLiked, int userNo, String name, String profileImageUrl, String social, String gender, int age, Boolean isCeleb, String email, String feedContentType, String feedContentUrl) {
        this.feedNo = feedNo;
        this.feedUserNo = feedUserNo;
        this.feedType = feedType;
        this.text = text;
        this.imageUrl = imageUrl;
        this.createDate = createDate;
        this.commentCnt = commentCnt;
        this.seemCnt = seemCnt;
        this.likeCnt = likeCnt;
        this.isSeemed = isSeemed;
        this.isLiked = isLiked;
        this.userNo = userNo;
        this.name = name;
        this.profileImageUrl = profileImageUrl;
        this.social = social;
        this.gender = gender;
        this.age = age;
        this.isCeleb = isCeleb;
        this.email = email;
        this.feedContentType = feedContentType;
        this.feedContentUrl = feedContentUrl;
    }

    public int getFeedNo() {
        return feedNo;
    }

    public void setFeedNo(int feedNo) {
        this.feedNo = feedNo;
    }

    public int getFeedUserNo() {
        return feedUserNo;
    }

    public void setFeedUserNo(int feedUserNo) {
        this.feedUserNo = feedUserNo;
    }

    public String getFeedType() {
        return feedType;
    }

    public void setFeedType(String feedType) {
        this.feedType = feedType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getCommentCnt() {
        return commentCnt;
    }

    public void setCommentCnt(int commentCnt) {
        this.commentCnt = commentCnt;
    }

    public int getSeemCnt() {
        return seemCnt;
    }

    public void setSeemCnt(int seemCnt) {
        this.seemCnt = seemCnt;
    }

    public int getLikeCnt() {
        return likeCnt;
    }

    public void setLikeCnt(int likeCnt) {
        this.likeCnt = likeCnt;
    }

    public Boolean getIsSeemed() {
        return isSeemed;
    }

    public void setIsSeemed(Boolean seemed) {
        isSeemed = seemed;
    }

    public Boolean getIsLiked() {
        return isLiked;
    }

    public void setIsLiked(Boolean liked) {
        isLiked = liked;
    }

    public int getUserNo() {
        return userNo;
    }

    public void setUserNo(int userNo) {
        this.userNo = userNo;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getSocial() {
        return social;
    }

    public void setSocial(String social) {
        this.social = social;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Boolean getIsCeleb() {
        return isCeleb;
    }

    public void setIsCeleb(Boolean celeb) {
        isCeleb = celeb;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFeedContentType() {
        return feedContentType;
    }

    public void setFeedContentType(String feedContentType) {
        this.feedContentType = feedContentType;
    }

    public String getFeedContentUrl() {
        return feedContentUrl;
    }

    public void setFeedContentUrl(String feedContentUrl) {
        this.feedContentUrl = feedContentUrl;
    }

    @Override
    public String toString() {
        return "FeedListDto{" +
                "feedNo=" + feedNo +
                ", feedUserNo=" + feedUserNo +
                ", feedType='" + feedType + '\'' +
                ", text='" + text + '\'' +
                ", imageUrl='" + imageUrl + '\'' +
                ", createDate='" + createDate + '\'' +
                ", commentCnt=" + commentCnt +
                ", seemCnt=" + seemCnt +
                ", likeCnt=" + likeCnt +
                ", isSeemed=" + isSeemed +
                ", isLiked=" + isLiked +
                ", userNo=" + userNo +
                ", name='" + name + '\'' +
                ", profileImageUrl='" + profileImageUrl + '\'' +
                ", social='" + social + '\'' +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                ", isCeleb=" + isCeleb +
                ", email='" + email + '\'' +
                ", feedContentType='" + feedContentType + '\'' +
                ", feedContentUrl='" + feedContentUrl + '\'' +
                '}';
    }
}
