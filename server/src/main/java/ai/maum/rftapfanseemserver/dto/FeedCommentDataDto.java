package ai.maum.rftapfanseemserver.dto;

public class FeedCommentDataDto {
    private int commentNo;
    private int feedNo;
    private int commentUserNo;
    private String commentText;
    private String createDate;
    private String name;
    private String profileImageUrl;
    private String gender;
    private int age;

    public FeedCommentDataDto() {
    }

    public FeedCommentDataDto(int commentNo, int feedNo, int commentUserNo, String commentText, String createDate, String name, String profileImageUrl, String gender, int age) {
        this.commentNo = commentNo;
        this.feedNo = feedNo;
        this.commentUserNo = commentUserNo;
        this.commentText = commentText;
        this.createDate = createDate;
        this.name = name;
        this.profileImageUrl = profileImageUrl;
        this.gender = gender;
        this.age = age;
    }

    public int getCommentNo() {
        return commentNo;
    }

    public void setCommentNo(int commentNo) {
        this.commentNo = commentNo;
    }

    public int getFeedNo() {
        return feedNo;
    }

    public void setFeedNo(int feedNo) {
        this.feedNo = feedNo;
    }

    public int getCommentUserNo() {
        return commentUserNo;
    }

    public void setCommentUserNo(int commentUserNo) {
        this.commentUserNo = commentUserNo;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "FeedCommentDataDto{" +
                "commentNo=" + commentNo +
                ", feedNo=" + feedNo +
                ", commentUserNo=" + commentUserNo +
                ", commentText='" + commentText + '\'' +
                ", createDate='" + createDate + '\'' +
                ", name='" + name + '\'' +
                ", profileImageUrl='" + profileImageUrl + '\'' +
                ", gender='" + gender + '\'' +
                ", age=" + age +
                '}';
    }
}
