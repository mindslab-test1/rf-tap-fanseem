package ai.maum.rftapfanseemserver.dto;

import ai.maum.rftapfanseemserver.User;
import ai.maum.rftapfanseemserver.UserList;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.protobuf.Descriptors;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDto implements Serializable {

    @JsonProperty("CREATE_TIME")
    private String createTime;

    @JsonProperty("UPDATE_TIME")
    private String updateTime;

    @JsonProperty("DEVICE_TOKEN_ID")
    private String deviceTokenId;

    @JsonProperty("USER_NO")
    private Integer userNo;

    @JsonProperty("ID")
    private String id;

    @JsonProperty("PW")
    private String pw;

    @JsonProperty("NAME")
    private String name;

    @JsonProperty("PROFILE_IMAGE_URL")
    private String profileImageUrl;

    @JsonProperty("POINT")
    private Integer point;

    @JsonProperty("SUBSCRIBING_USER_NO_LIST")
    private String subscribingUserNoList;

    @JsonProperty("INTERESTED_CATEGORY_LIST")
    private String interestedCategoryList;

    @JsonProperty("SOCIAL")
    private String social;

    @JsonProperty("GENDER")
    private Character gender;

    @JsonProperty("AGE")
    private Integer age;

    @JsonProperty("BIRTHDAY")
    private String birthday;

    @JsonProperty("IS_CELEB")
    private Character isCeleb;

    public String getSocial() {
        return social;
    }

    public void setSocial(String social) {
        this.social = social;
    }

    public Character getGender() {
        return gender;
    }

    public void setGender(Character gender) {
        this.gender = gender;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getDeviceTokenId() {
        return deviceTokenId;
    }

    public void setDeviceTokenId(String deviceTokenId) {
        this.deviceTokenId = deviceTokenId;
    }

    public Integer getUserNo() {
        return userNo;
    }

    public void setUserNo(Integer userNo) {
        this.userNo = userNo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPw() {
        return pw;
    }

    public void setPw(String pw) {
        this.pw = pw;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public Integer getPoint() {
        return point;
    }

    public void setPoint(Integer point) {
        this.point = point;
    }

    public String getSubscribingUserNoList() {
        return subscribingUserNoList;
    }

    public void setSubscribingUserNoList(String subscribingUserNoList) {
        this.subscribingUserNoList = subscribingUserNoList;
    }

    public String getInterestedCategoryList() {
        return interestedCategoryList;
    }

    public void setInterestedCategoryList(String interestedCategoryList) {
        this.interestedCategoryList = interestedCategoryList;
    }

    public Character getIsCeleb() {
        return isCeleb;
    }

    public void setIsCeleb(Character isCeleb) {
        this.isCeleb = isCeleb;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "createTime='" + createTime + '\'' +
                ", updateTime='" + updateTime + '\'' +
                ", deviceTokenId='" + deviceTokenId + '\'' +
                ", userNo=" + userNo +
                ", id='" + id + '\'' +
                ", pw='" + pw + '\'' +
                ", name='" + name + '\'' +
                ", profileImageUrl='" + profileImageUrl + '\'' +
                ", point=" + point +
                ", subscribingUserNoList='" + subscribingUserNoList + '\'' +
                ", interestedCategoryList='" + interestedCategoryList + '\'' +
                ", social='" + social + '\'' +
                ", gender=" + gender +
                ", age=" + age +
                ", birthday='" + birthday + '\'' +
                ", isCeleb='" + isCeleb + '\'' +
                '}';
    }

    public static List<String> getNotNullColumnList(User user) {
        List<String> result = new ArrayList<>();

        Map<Descriptors.FieldDescriptor, Object> map = user.getAllFields();
        for (Descriptors.FieldDescriptor iter : map.keySet()) {
            result.add(iter.getName().toUpperCase());
        }

        return result;
    }

    public static User buildUserFromUserDto(UserDto userDto) {
        User.Builder userBuilder = User.newBuilder();

        if (userDto == null) {
            return userBuilder.build();
        }

        if (userDto.getCreateTime() != null) {
            userBuilder.setCreateTime(userDto.getCreateTime());
        }
        if (userDto.getUpdateTime() != null) {
            userBuilder.setUpdateTime(userDto.getUpdateTime());
        }
        if (userDto.getDeviceTokenId() != null) {
            userBuilder.setDeviceTokenId(userDto.getDeviceTokenId());
        }
        if (userDto.getUserNo() != null) {
            userBuilder.setUserNo(userDto.getUserNo());
        }
        if (userDto.getId() != null) {
            userBuilder.setId(userDto.getId());
        }
        if (userDto.getName() != null) {
            userBuilder.setName(userDto.getName());
        }
        if (userDto.getPoint() != null) {
            userBuilder.setPoint(userDto.getPoint());
        }
        if (userDto.getSocial() != null) {
            userBuilder.setSocial(userDto.getSocial());
        }
        if (userDto.getGender() != null) {
            userBuilder.setGender(userDto.getGender().toString());
        }
        if (userDto.getAge() != null) {
            userBuilder.setAge(userDto.getAge());
        }
        if (userDto.getBirthday() != null) {
            userBuilder.setBirthday(userDto.getBirthday());
        }
        if (userDto.getPw() != null) {
            userBuilder.setPw(userDto.getPw());
        }
        if (userDto.getProfileImageUrl() != null) {
            userBuilder.setProfileImageUrl(userDto.getProfileImageUrl());
        }

        if (userDto.getSubscribingUserNoList() != null) {
            String[] userNoArray = userDto.getSubscribingUserNoList().split("\\$");
            for (String iter : userNoArray) {
                if (iter.equals("")) continue;
                userBuilder.addSubscribingUserNoList(Integer.parseInt(iter));
            }
        }
        if (userDto.getInterestedCategoryList() != null) {
            String[] categoryArray = userDto.getInterestedCategoryList().split("\\$");
            for (String iter : categoryArray) {
                if (iter.equals("")) continue;
                userBuilder.addInterestedCategoryList(iter);
            }
        }
        if (userDto.getPw() != null) {
            userBuilder.setPw(userDto.getPw());
        }
        if (userDto.getIsCeleb() != null) {
            userBuilder.setIsCeleb(userDto.getIsCeleb() == 'Y');
        }

        return userBuilder.build();
    }

    public static UserList buildUserListFromUserDtoList(List<UserDto> userDtoList) {
        UserList.Builder responseBuilder = UserList.newBuilder();

        for (UserDto iter : userDtoList) {
            User user = buildUserFromUserDto(iter);
            responseBuilder.addUser(user);
        }

        UserList userList = responseBuilder.build();

        return userList;
    }

}
