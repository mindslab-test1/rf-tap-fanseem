package ai.maum.rftapfanseemserver.dto;

import ai.maum.rftapfanseemserver.FeedLike;
import ai.maum.rftapfanseemserver.FeedSeemed;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
@JsonIgnoreProperties(ignoreUnknown = true)
public class FeedSeemedDto implements Serializable {

    @JsonProperty("SEEM_NO")
    private Integer seemNo;

    @JsonProperty("FEED_NO")
    private Integer feedNo;

    @JsonProperty("SEEM_USER_NO")
    private Integer seemUserNo;

    @JsonProperty("ACTIVE")
    private Character active;

    @JsonProperty("ACTIVE_DATE")
    private String activeDate;

    @JsonProperty("SEEM_DATE")
    private String seemDate;

    public Integer getSeemNo() {
        return seemNo;
    }

    public void setSeemNo(Integer seemNo) {
        this.seemNo = seemNo;
    }

    public Integer getFeedNo() {
        return feedNo;
    }

    public void setFeedNo(Integer feedNo) {
        this.feedNo = feedNo;
    }

    public Integer getSeemUserNo() {
        return seemUserNo;
    }

    public void setSeemUserNo(Integer seemUserNo) {
        this.seemUserNo = seemUserNo;
    }

    public Character getActive() {
        return active;
    }

    public void setActive(Character active) {
        this.active = active;
    }

    public String getActiveDate() {
        return activeDate;
    }

    public void setActiveDate(String activeDate) {
        this.activeDate = activeDate;
    }

    public String getSeemDate() {
        return seemDate;
    }

    public void setSeemDate(String seemDate) {
        this.seemDate = seemDate;
    }

    @Override
    public String toString() {
        return "FeedSeemedDto{" +
                "seemNo=" + seemNo +
                ", feedNo=" + feedNo +
                ", seemUserNo=" + seemUserNo +
                ", active=" + active +
                ", activeDate='" + activeDate + '\'' +
                ", seemDate='" + seemDate + '\'' +
                '}';
    }

    public static FeedSeemed buildFeedSeemedFromFeedSeemedDto(FeedSeemedDto feedSeemedDto) {
        FeedSeemed.Builder feedSeemedBuilder = FeedSeemed.newBuilder();

        if (feedSeemedDto == null) {
            return feedSeemedBuilder.build();
        }

        if (feedSeemedDto.getFeedNo() != null) {
            feedSeemedBuilder.setFeedNo(feedSeemedDto.getFeedNo());
        }
        if (feedSeemedDto.getSeemUserNo() != null) {
            feedSeemedBuilder.setSeemUserNo(feedSeemedDto.getSeemUserNo());
        }
        if (feedSeemedDto.getActive() != null) {
            feedSeemedBuilder.setActive(feedSeemedDto.getActive() == 'Y');
        }
        if (feedSeemedDto.getActiveDate() != null) {
            feedSeemedBuilder.setActiveDate(feedSeemedDto.getActiveDate());
        }

        return feedSeemedBuilder.build();
    }
}
