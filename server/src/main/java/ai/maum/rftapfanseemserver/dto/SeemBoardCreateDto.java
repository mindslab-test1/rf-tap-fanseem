package ai.maum.rftapfanseemserver.dto;

public class SeemBoardCreateDto {
    private int boardNo;
    private int boardUserNo;
    private String boardName;
    private String active;
    private String createDate;
    private String updateDate;

    public SeemBoardCreateDto() {
    }

    public SeemBoardCreateDto(int boardNo, int boardUserNo, String boardName, String active, String createDate, String updateDate) {
        this.boardNo = boardNo;
        this.boardUserNo = boardUserNo;
        this.boardName = boardName;
        this.active = active;
        this.createDate = createDate;
        this.updateDate = updateDate;
    }

    public int getBoardNo() {
        return boardNo;
    }

    public void setBoardNo(int boardNo) {
        this.boardNo = boardNo;
    }

    public int getBoardUserNo() {
        return boardUserNo;
    }

    public void setBoardUserNo(int boardUserNo) {
        this.boardUserNo = boardUserNo;
    }

    public String getBoardName() {
        return boardName;
    }

    public void setBoardName(String boardName) {
        this.boardName = boardName;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    @Override
    public String toString() {
        return "SeemBoardCreateDto{" +
                "boardNo=" + boardNo +
                ", boardUserNo=" + boardUserNo +
                ", boardName='" + boardName + '\'' +
                ", active='" + active + '\'' +
                ", createDate='" + createDate + '\'' +
                ", updateDate='" + updateDate + '\'' +
                '}';
    }
}
