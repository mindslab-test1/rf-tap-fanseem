package ai.maum.rftapfanseemserver.rest;

import ai.maum.rftapfanseemserver.mybatis.MybatisConnectionFactory;
import ai.maum.rftapfanseemserver.mybatis.mappers.FeedSeemedMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(
        "/scrap"
)
public class SeemBoardController {
    private static final Logger logger = LoggerFactory.getLogger(SeemBoardController.class);

    SqlSessionFactory sqlSessionFactory = MybatisConnectionFactory.getSqlSessionFactory();

    @PostMapping(
            value = "makeBoard",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity makeBoard(@RequestBody Map<String, Object> requestParams){
        Map<String, Object> response = new HashMap<>();
        try (SqlSession session = sqlSessionFactory.openSession()){
            FeedSeemedMapper mapper = session.getMapper(FeedSeemedMapper.class);
            int i = mapper.insertNewBoard(requestParams);
            // TODO check for insertion failures
            session.commit();
            int boardId = mapper.selectBoardId(requestParams).getBoardNo();
            response.put("boardId", boardId);
        } catch (Exception e){
            logger.error(e.toString());
            return ResponseEntity.status(500).build();
        }

        return ResponseEntity.ok(response);
    }

    @PostMapping(
            value = "getBoards",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity getBoards(@RequestBody Map<String, Object> requestParams){
        try (SqlSession session = sqlSessionFactory.openSession()){
            FeedSeemedMapper mapper = session.getMapper(FeedSeemedMapper.class);
            Map<String, Object> response = new HashMap<>();
            response.put("boards", mapper.getUserBoardInfo(requestParams));
            return ResponseEntity.ok(response);
        } catch (Exception e){
            logger.error(e.toString());
            return ResponseEntity.status(500).build();
        }
    }

    @PostMapping(
            value = "getBoardFeed",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity getBoardFeed(@RequestBody Map<String, Object> requestParams){
        try (SqlSession session = sqlSessionFactory.openSession()){
            FeedSeemedMapper mapper = session.getMapper(FeedSeemedMapper.class);
            return ResponseEntity.ok(mapper.getFeedFromBoard(requestParams));
        } catch (Exception e){
            logger.error(e.toString());
            return ResponseEntity.status(500).build();
        }
    }

    @PostMapping(
            value = "deleteBoard",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity deleteBoard(@RequestBody Map<String, Object> requestParams){
        try (SqlSession session = sqlSessionFactory.openSession()){
            FeedSeemedMapper mapper = session.getMapper(FeedSeemedMapper.class);
            int i = mapper.deleteBoard(requestParams);
            session.commit();
            return ResponseEntity.ok().build();
        } catch (Exception e){
            logger.error(e.toString());
            return ResponseEntity.status(500).build();
        }
    }

    @PostMapping(
            value = "deleteBoardFeed",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity deleteBoardFeed(@RequestBody Map<String, Object> requestParams){
        try (SqlSession session = sqlSessionFactory.openSession()){
            FeedSeemedMapper mapper = session.getMapper(FeedSeemedMapper.class);
            int i = mapper.deleteBoardFeed(requestParams);
            session.commit();
            return ResponseEntity.ok().build();
        } catch (Exception e){
            logger.error(e.toString());
            return ResponseEntity.status(500).build();
        }
    }

    @PostMapping(
            value = "insertFeed",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity insertBoardFeed(@RequestBody Map<String, Object> requestParams){
        try (SqlSession session = sqlSessionFactory.openSession()){
            FeedSeemedMapper mapper = session.getMapper(FeedSeemedMapper.class);
            if(mapper.checkInactiveFeed(requestParams) > 0)
                mapper.updateBoardFeed(requestParams);
            else mapper.insertFeedToBoard(requestParams);
            session.commit();
            return ResponseEntity.ok().build();
        } catch (Exception e){
            logger.error(e.toString());
            return ResponseEntity.status(500).build();
        }
    }

    @PostMapping(
            value = "updateBoardName",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity updateBoardName(@RequestBody Map<String, Object> requestParams){
        try (SqlSession session = sqlSessionFactory.openSession()){
            FeedSeemedMapper mapper = session.getMapper(FeedSeemedMapper.class);
            int i = mapper.updateBoardName(requestParams);
            session.commit();
            return ResponseEntity.ok().build();
        } catch (Exception e){
            logger.error(e.toString());
            return ResponseEntity.status(500).build();
        }
    }
}
