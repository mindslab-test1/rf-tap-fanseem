package ai.maum.rftapfanseemserver.rest;

import ai.maum.rftapfanseemserver.dto.PushSettingsDto;
import ai.maum.rftapfanseemserver.firebase.FirebaseFcmNotifyServer;
import ai.maum.rftapfanseemserver.mybatis.MybatisConnectionFactory;
import ai.maum.rftapfanseemserver.mybatis.mappers.AlertMapper;
import ai.maum.rftapfanseemserver.mybatis.mappers.UserMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(
        "push"
)
public class AlertController {
    public static Logger logger = LoggerFactory.getLogger(AlertController.class);

    @Autowired
    FirebaseFcmNotifyServer firebaseFcmNotifyServer;

    SqlSessionFactory sqlSessionFactory = MybatisConnectionFactory.getSqlSessionFactory();

    @PostMapping(
            "test"
    )
    public ResponseEntity testPush(){
        String testTokenId = "evbpVgEX770:APA91bEsEUYHWSdE5azr2Na_41r7lh0o-A80jU3FkgKibSZVYVsrc8BxFpP_ACg9IQgFABGs1CBDn9JKL2ldz1flHgSOdIArNqcqJd8QI0PhtFTSSdcj-OTfbFAzPOyz8M_TWRKf9f6q";

        firebaseFcmNotifyServer.sendTestMessage(testTokenId);

        return ResponseEntity.ok().build();
    }

    @PostMapping(
            value = "getSettings",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity getSettings(@RequestBody Map<String, Object> params){
        Map<String, Object> response = new HashMap<>();
        try (SqlSession session = sqlSessionFactory.openSession()){
            UserMapper mapper = session.getMapper(UserMapper.class);
            List<PushSettingsDto> result = mapper.selectUserSettings(params);
            if(result.size() == 0){
                mapper.initUserSettings(params);
                session.commit();
            }

            result = mapper.selectUserSettings(params);
            response.put("settings", result.get(0));

            return ResponseEntity.ok(response);
        } catch (Exception e){
            logger.error(e.getMessage());
            logger.error("Failed to get settings!");
            return ResponseEntity.status(500).build();
        }
    }

    @PostMapping(
            value = "updatePushSettings",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity updatePushSettings(@RequestBody Map<String, Object> params){
        try (SqlSession session = sqlSessionFactory.openSession()){
            UserMapper mapper = session.getMapper(UserMapper.class);
            int result = mapper.selectUserSettings(params).size();
            if(result == 0) {
                mapper.initUserSettings(params);
                session.commit();
            }
            result = mapper.updatePushSettings(params);
            if(result == 0) throw new Exception("failed to update!");
            else session.commit();
        } catch (Exception e){
            logger.error(e.getMessage());
            return ResponseEntity.status(500).build();
        }

        return ResponseEntity.ok().build();
    }

    @PostMapping(
            value = "messageAlert",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity messageAlert(@RequestBody Map<String, Object> params){
        try (SqlSession session = sqlSessionFactory.openSession()){
            AlertMapper mapper = session.getMapper(AlertMapper.class);
            List<String> deviceIds = mapper.selectMessageDevices(params);
            firebaseFcmNotifyServer.sendCelebMessage(deviceIds, (String) params.get("celebName"), "message");
        }

        return ResponseEntity.ok().build();
    }

    @PostMapping(
            value = "feedAlert",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity feedAlert(@RequestBody Map<String, Object> params){
        try (SqlSession session = sqlSessionFactory.openSession()){
            AlertMapper mapper = session.getMapper(AlertMapper.class);
            List<String> deviceIds = mapper.selectFeedDevices(params);
            firebaseFcmNotifyServer.sendCelebMessage(deviceIds, (String) params.get("celebName"), "feed");
        }

        return ResponseEntity.ok().build();
    }

    @PostMapping(
            value = "activityAlert",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity actvityAlert(@RequestBody Map<String, Object> params){
        try (SqlSession session = sqlSessionFactory.openSession()){
            AlertMapper mapper = session.getMapper(AlertMapper.class);
            String deviceId = mapper.selectActivityDevice(params);
            firebaseFcmNotifyServer.sendActivityMessage(deviceId, (String) params.get("messageType"));
        }

        return ResponseEntity.ok().build();
    }
}
