package ai.maum.rftapfanseemserver.rest;

import ai.maum.Media;
import ai.maum.rftapfanseemserver.mybatis.MybatisConnectionFactory;
import ai.maum.rftapfanseemserver.mybatis.mappers.FeedMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping(
        "feed"
)
public class FeedController {
    private static final Logger logger = LoggerFactory.getLogger(FeedController.class);

    SqlSessionFactory sqlSessionFactory = MybatisConnectionFactory.getSqlSessionFactory();

    @PostMapping(
            value = "postYoutube",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity postYoutubeFeed(@RequestBody Map<String, Object> params){
        Map<String, Object> response = new HashMap<>();
        try(SqlSession session = sqlSessionFactory.openSession()){
            FeedMapper mapper = session.getMapper(FeedMapper.class);
            mapper.insertFeedYoutube(params);
            session.commit();
        } catch (Exception e){
            logger.error(e.toString());
            return ResponseEntity.status(500).build();
        }

        return ResponseEntity.ok(response);
    }

    @PostMapping(
            value = "updateFeed",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity updateFeed(@RequestBody Map<String, Object> params){
        Map<String, Object> response = new HashMap<>();
        try (SqlSession session = sqlSessionFactory.openSession()){
            FeedMapper mapper = session.getMapper(FeedMapper.class);
            mapper.updateFeed(params);
            session.commit();
        } catch (Exception e){
            logger.error(e.toString());
            return ResponseEntity.status(500).build();
        }

        return ResponseEntity.ok(response);
    }
}
