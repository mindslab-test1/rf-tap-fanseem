package ai.maum.rftapfanseemserver.rest;

import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import maum.brain.tts.NgTtsServiceGrpc;
import maum.brain.tts.TtsMediaResponse;
import maum.brain.tts.TtsRequest;
import maum.common.LangOuterClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.WritableByteChannel;
import java.util.Iterator;

@RestController
@RequestMapping(
        "tts"
)
public class TtsRequestController {
    private static final Logger logger = LoggerFactory.getLogger(TtsRequestController.class);

    @Value("${grpc.tts.server.ip}")
    private String ttsIp;

    @Value("${grpc.tts.server.port}")
    private int ttsPort;

    @GetMapping(
            "doTts"
    )
    public StreamingResponseBody getSampleTts(
            @RequestParam("text") String sampleText, @RequestParam("appKey") String appKey,
            HttpServletResponse response
    ){
        logger.info(sampleText);
        if (!appKey.equals("20be1df8bfa246558d9bc83ad8ef1d4fb8e571f8dc624d0ba821ca913c4")){
            response.setStatus(403);
            return null;
        }
        return out -> {
            try (WritableByteChannel outChannel = Channels.newChannel(out)){
                Iterator<TtsMediaResponse> iter = this.getResponseIter("test", sampleText);
                while (iter.hasNext()){
                    ByteString tmpBytes = iter.next().getMediaData();
                    outChannel.write(tmpBytes.asReadOnlyByteBuffer());
                }
                out.close();
            } catch (IOException e){
                logger.error(e.getMessage());
            }
        };
    }

    private Iterator<TtsMediaResponse> getResponseIter(String voiceName, String text) {
        return this.getStub(voiceName)
                .speakWav(TtsRequest.newBuilder()
                        .setLang(LangOuterClass.Lang.ko_KR)
                        .setSampleRate(22050)
                        .setSpeaker(0)
                        .setText(text)
                        .build());
    }

    private NgTtsServiceGrpc.NgTtsServiceBlockingStub getStub(String voiceName){
        // TODO get ip port from voiceName
        return NgTtsServiceGrpc.newBlockingStub(
                ManagedChannelBuilder.forAddress(
                        ttsIp, ttsPort
                ).usePlaintext().build()
        );
    }

}
