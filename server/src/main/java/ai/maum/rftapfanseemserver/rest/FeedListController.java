package ai.maum.rftapfanseemserver.rest;

import ai.maum.rftapfanseemserver.dto.FeedCommentDataDto;
import ai.maum.rftapfanseemserver.dto.FeedListDto;
import ai.maum.rftapfanseemserver.mybatis.MybatisConnectionFactory;
import ai.maum.rftapfanseemserver.mybatis.mappers.FeedCommentMapper;
import ai.maum.rftapfanseemserver.mybatis.mappers.FeedMapper;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(
        "feedList"
)
public class FeedListController {
    private static final Logger logger = LoggerFactory.getLogger(FeedListController.class);

    SqlSessionFactory sqlSessionFactory = MybatisConnectionFactory.getSqlSessionFactory();

    @PostMapping(
            value = "getAll",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity getFeedList(@RequestBody Map<String, Object> params){
        Map<String, Object> response = new HashMap<>();
        try (SqlSession session = sqlSessionFactory.openSession()){
            List<FeedListDto> feedList = session.getMapper(FeedMapper.class).selectFeedList(params);
            response.put("status", 0);
            response.put("payload", feedList);
            return ResponseEntity.ok(response);
        } catch (Exception e){
            logger.error(e.toString());
            return ResponseEntity.status(500).build();
        }
    }

    @PostMapping(
            value = "getComment",
            consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity getCommentList(@RequestBody Map<String, Object> params){
        Map<String, Object> response = new HashMap<>();
        try (SqlSession session = sqlSessionFactory.openSession()){
            List<FeedCommentDataDto> commentList = session.getMapper(FeedCommentMapper.class).selectCommentList(params);
            response.put("status", 0);
            response.put("payload", commentList);
            return ResponseEntity.ok(response);
        } catch (Exception e){
            logger.error(e.toString());
            return ResponseEntity.status(500).build();
        }
    }
}
