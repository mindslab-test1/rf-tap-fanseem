package ai.maum.rftapfanseemserver.rest;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SeemRequest implements Serializable {
    private int userId;
    private int celebId;

    public SeemRequest() {
    }

    public SeemRequest(int userId, int celebId) {
        this.userId = userId;
        this.celebId = celebId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getCelebId() {
        return celebId;
    }

    public void setCelebId(int celebId) {
        this.celebId = celebId;
    }

    @Override
    public String toString() {
        return "SeemRequest{" +
                "userId=" + userId +
                ", celebId=" + celebId +
                '}';
    }
}
