package ai.maum.rftapfanseemserver.rest;

import ai.maum.rftapfanseemserver.db.MyBatisRepository;
import ai.maum.rftapfanseemserver.dto.SubscribeDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(
        "/api"
)
public class UserController {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private MyBatisRepository myBatisRepository;

    @RequestMapping(
            value = "/updateSeem",
            method = RequestMethod.POST
    )
    public ResponseEntity updateSeem(@RequestBody SeemRequest params, HttpServletRequest request){
        int userId = params.getUserId();
        int celebId = params.getCelebId();

        List<SubscribeDto> subscribeDtos = myBatisRepository.getSubscribeOne(userId, celebId);
        if (subscribeDtos.isEmpty()){
            myBatisRepository.insertSeem(userId, celebId);
        } else if(subscribeDtos.get(0).getActive().equals("Y")){
            myBatisRepository.updateSeemNo(userId, celebId);
        } else {
            myBatisRepository.updateSeemYes(userId, celebId);
        }

        return ResponseEntity.ok().build();
    }

    @RequestMapping(
            value = "/getSeemers",
            method = RequestMethod.POST
    )
    public ResponseEntity getSeemers(@RequestBody Map<String, Integer> params){
        Map<String, Object> response = new HashMap<>();
        response.put("celebNo", params.get("celebId"));
        response.put("seemers", myBatisRepository.getSeemers(params.get("celebId")));

        return ResponseEntity.ok(response);
    }

    @GetMapping(
            value = "getVersion",
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE
    )
    public ResponseEntity getVersion(){
        return ResponseEntity.ok(myBatisRepository.getVersion());
    }

}
