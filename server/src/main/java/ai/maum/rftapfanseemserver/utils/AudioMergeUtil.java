package ai.maum.rftapfanseemserver.utils;

import com.google.common.io.Files;
import com.google.protobuf.ByteString;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class AudioMergeUtil {
    List<ByteString> payloadList1;
    List<ByteString> payloadList2;
    int audioSize1;
    int audioSize2;
    Byte[] audioBytes1;
    Byte[] audioBytes2;

    AudioMergeUtil(List<ByteString> payloadList1, List<ByteString> payloadList2) {
        this.payloadList1 = payloadList1;
        this.payloadList2 = payloadList2;

        audioSize1 = 0;
        audioSize2 = 0;
        for (ByteString iter : payloadList1) {
            audioSize1 += iter.size();
        }
        for (ByteString iter : payloadList2) {
            audioSize2 += iter.size();
        }

        audioBytes1 = new Byte[audioSize1];
        audioBytes2 = new Byte[audioSize2];

        int index1 = 0;
        for (ByteString iter : payloadList1) {
            for (Byte jter : iter) {
                audioBytes1[index1] = jter;
                index1++;
            }
        }
        int index2 = 0;
        for (ByteString iter : payloadList2) {
            for (Byte jter : iter) {
                audioBytes2[index2] = jter;
                index2++;
            }
        }

        String classpathStr = System.getProperty("java.class.path")+"temp";

        File audioFile1;

    }


}
