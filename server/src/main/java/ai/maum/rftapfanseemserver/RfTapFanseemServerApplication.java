package ai.maum.rftapfanseemserver;

import ai.maum.rftapfanseemserver.firebase.FirebaseFcmNotifyServer;
import ai.maum.rftapfanseemserver.grpc.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RfTapFanseemServerApplication implements CommandLineRunner {
    private static final Logger logger = LoggerFactory.getLogger(RfTapFanseemServerApplication.class);

    @Autowired
    private GrpcUpdateServer grpcUpdateServer;
    @Autowired
    private GrpcGetServer grpcGetServer;
    @Autowired
    private FirebaseFcmNotifyServer firebaseFcmNotifyServer;

    public static void main(String[] args) {
        SpringApplication.run(RfTapFanseemServerApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        grpcUpdateServer.start();
        grpcGetServer.start();
        firebaseFcmNotifyServer.initialize();

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                // Use stderr here since the logger may have been grpcNotifyServerreset by its JVM shutdown hook.
                System.err.println("*** shutting down gRPC server since JVM is shutting down");
                try {
                    grpcUpdateServer.stop();
                    grpcGetServer.stop();
                } catch (InterruptedException e) {
                    logger.error("shutdown interrupted");
                }
            }
        });
    }
}

// mvn install:install-file -Dfile=ojdbc7-12.1.0.2.jar -DgroupId=com.oracle -DartifactId=ojdbc7 -Dversion=12.1.0.2 -Dpackaging=jar -DgeneratePom=true
// scp /Users/wookje/Desktop/project/rf-tap-fanseem/server/target/rf-tap-fanseem-server-1.0.1-SNAPSHOT.jar minds@114.108.173.102:/home/minds/rf-tap/rf-tap-app-server