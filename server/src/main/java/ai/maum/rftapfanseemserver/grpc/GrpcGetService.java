package ai.maum.rftapfanseemserver.grpc;

import ai.maum.rftapfanseemserver.*;
import ai.maum.rftapfanseemserver.db.*;
import ai.maum.rftapfanseemserver.dto.*;
import ai.maum.rftapfanseemserver.mybatis.MybatisConnectionFactory;
import ai.maum.rftapfanseemserver.mybatis.mappers.*;
import com.google.protobuf.Descriptors;
import io.grpc.stub.StreamObserver;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
class GrpcGetService extends GetServiceGrpc.GetServiceImplBase {
    private static final Logger logger = LoggerFactory.getLogger(GrpcGetService.class);

    @Autowired
    private MyBatisRepository myBatisRepository;

    SqlSessionFactory sqlSessionFactory = MybatisConnectionFactory.getSqlSessionFactory();

    private Map<String, Object> getOrdinaryMapFromProtoMap(Map<Descriptors.FieldDescriptor, Object> map) {
        Map<String, Object> result = new HashMap<>();
        for (Map.Entry<Descriptors.FieldDescriptor, Object> iter : map.entrySet()) {
            result.put(iter.getKey().getName().toUpperCase(), iter.getValue());
        }
        return result;
    }

    GrpcGetService() {
    }

    @Override
    public void getCelebByUserNo(Celeb request, StreamObserver<Celeb> responseObserver) {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            CelebMapper mapper = session.getMapper(CelebMapper.class);
            CelebDto celebDto = mapper.getCelebByUserNo(request.getUserNo());

            Celeb celeb = CelebDto.buildCelebFromCelebDto(celebDto);

            responseObserver.onNext(celeb);
            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onError(e);
            return;
        }
    }

    @Override
    public void getCelebListByCategoryList(User request, StreamObserver<CelebList> responseObserver) {
        List<CelebDto> celebDtoList;
        List<String> payloadList = request.getInterestedCategoryListList();

        try (SqlSession session = sqlSessionFactory.openSession()) {
            CelebMapper mapper = session.getMapper(CelebMapper.class);
            responseObserver.onNext(
                    CelebDto.buildCelebListFromCelebDtoList(
                            mapper.getCelebListByInterestedCategoryList(payloadList)
                    )
            );
//            if (!payloadList.isEmpty()) {
//                celebDtoList = mapper.getCelebListByInterestedCategoryList(payloadList);
//                System.out.println(celebDtoList);
//                CelebList celebList = CelebDto.buildCelebListFromCelebDtoList(celebDtoList);
//                responseObserver.onNext(celebList);
//            }
//            else {
//                responseObserver.onNext(CelebList.newBuilder().build());
//            }
            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onError(e);
            return;
        }
    }

    @Override
    public void getCelebListBySubscribingList(User request, StreamObserver<CelebList> responseObserver) {
        List<CelebDto> celebDtoList;
        List<Integer> payloadList = request.getSubscribingUserNoListList();

        try (SqlSession session = sqlSessionFactory.openSession()) {
            CelebMapper mapper = session.getMapper(CelebMapper.class);
            if (!payloadList.isEmpty()) {
                celebDtoList = mapper.getCelebListBySubscribingUserNoList(payloadList);
                CelebList celebList = CelebDto.buildCelebListFromCelebDtoList(celebDtoList);
                responseObserver.onNext(celebList);
            }
            else {
                responseObserver.onNext(CelebList.newBuilder().build());
            }
            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onError(e);
            return;
        }
    }

    @Override
    public void getUserByUserNo(User request, StreamObserver<User> responseObserver) {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            UserMapper mapper = session.getMapper(UserMapper.class);
            UserDto userDto = mapper.getUserByUserNo(request.getUserNo());
            User user = UserDto.buildUserFromUserDto(userDto);
            responseObserver.onNext(user);
            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onError(e);
            return;
        }
    }

    @Override
    public void getLastFeedByUserNo(Celeb request, StreamObserver<Feed> responseObserver) {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            FeedMapper mapper = session.getMapper(FeedMapper.class);
            FeedDto feedDto = mapper.getLastFeedByFeedUserNo(request.getUserNo());
            Feed feed = FeedDto.buildFeedFromFeedDto(feedDto);
            responseObserver.onNext(feed);
            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onError(e);
            return;
        }
    }

    @Override
    public void getFeedListByUserNo(Feed request, StreamObserver<FeedList> responseObserver) {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            FeedMapper mapper = session.getMapper(FeedMapper.class);
            FeedCommentMapper commentMapper = session.getMapper(FeedCommentMapper.class);
            FeedLikeMapper likeMapper = session.getMapper(FeedLikeMapper.class);
            FeedSeemedMapper seemedMapper = session.getMapper(FeedSeemedMapper.class);

            List<FeedDto> feedDtoList = mapper.getFeedListByFeedUserNo(request.getFeedUserNo());
            for (FeedDto feedDto : feedDtoList) {
                feedDto.setCommentCnt(commentMapper.getFeedCommentCountByFeedNo(feedDto.getFeedNo()));
                feedDto.setLikeCnt(likeMapper.getFeedLikeCountByFeedNo(feedDto.getFeedNo()));
                feedDto.setSeemCnt(seemedMapper.getFeedSeemedCountByFeedNo(feedDto.getFeedNo()));
                feedDto.setIsLiked(likeMapper.getIsFeedLikedByUser(feedDto.getFeedNo(), request.getCreateUserNo()) == 1);
                feedDto.setIsSeemed(seemedMapper.getIsFeedSeemedByUser(feedDto.getFeedNo(), request.getCreateUserNo()) == 1);
            }
            FeedList feedList = FeedDto.buildFeedListFromFeedDtoList(feedDtoList);

            responseObserver.onNext(feedList);
            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onError(e);
            return;
        }
    }

    @Override
    public void getFeedCommentListByFeedNo(Feed request, StreamObserver<FeedCommentList> responseObserver) {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            FeedCommentMapper mapper = session.getMapper(FeedCommentMapper.class);

            List<FeedCommentDto> feedCommentDtoList = mapper.getFeedCommentListByFeedNo(request.getFeedNo());
            FeedCommentList feedCommentList = FeedCommentDto.buildFeedCommentListFromFeedCommentDtoList(feedCommentDtoList);

            responseObserver.onNext(feedCommentList);
            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onError(e);
            return;
        }
    }

    @Override
    public void getChatMessageListByUserNo(Celeb request, StreamObserver<ChatMessageList> responseObserver) {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            ChatMessageMapper mapper = session.getMapper(ChatMessageMapper.class);
            List<ChatMessageDto> chatMessageDtoList = mapper.getChatMessageListByUserNo(request.getUserNo());

            ChatMessageList chatMessageList = ChatMessageDto.buildChatMessageListFromChatMessageDtoList(chatMessageDtoList);

            responseObserver.onNext(chatMessageList);
            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onError(e);
            return;
        }
    }

    @Override
    public void getCelebListBySearchQuery(SearchQuery request, StreamObserver<CelebList> responseObserver) {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            CelebMapper mapper = session.getMapper(CelebMapper.class);
            List<CelebDto> celebDtoList = mapper.getCelebListBySearchQuery(request.getQuery());

            CelebList celebList = CelebDto.buildCelebListFromCelebDtoList(celebDtoList);
            responseObserver.onNext(celebList);
            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onError(e);
            return;
        }
    }

    @Override
    public void getPopularCelebListByUserNo(User request, StreamObserver<CelebList> responseObserver) {
        List<CelebDto> celebDtoList = myBatisRepository.getEveryCelebList();
        CelebList celebList = CelebDto.buildCelebListFromCelebDtoList(celebDtoList);

        logger.info(celebDtoList.toString());
        responseObserver.onNext(celebList);
        responseObserver.onCompleted();
    }

    @Override
    public void getFeedDetailByFeedNo(Feed request, StreamObserver<Feed> responseObserver) {
        try (SqlSession session = sqlSessionFactory.openSession()) {
            FeedMapper mapper = session.getMapper(FeedMapper.class);
            FeedCommentMapper commentMapper = session.getMapper(FeedCommentMapper.class);
            FeedLikeMapper likeMapper = session.getMapper(FeedLikeMapper.class);
            FeedSeemedMapper seemedMapper = session.getMapper(FeedSeemedMapper.class);

            FeedDto feedDto = mapper.getFeedByFeedFeedNo(request.getFeedNo());
            feedDto.setComments(commentMapper.getFeedCommentListByFeedNo(request.getFeedNo()));
            feedDto.setCommentCnt(feedDto.getComments().size());
            feedDto.setLikeCnt(likeMapper.getFeedLikeCountByFeedNo(request.getFeedNo()));
            feedDto.setSeemCnt(seemedMapper.getFeedSeemedCountByFeedNo(request.getFeedNo()));

            Feed feed = FeedDto.buildFeedFromFeedDto(feedDto);

            responseObserver.onNext(feed);
            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onError(e);
            return;
        }
    }

    @Override
    public void getFeedSeemedListByUserNo(User request, StreamObserver<FeedList> responseObserver) {
//        try (SqlSession session = sqlSessionFactory.openSession()) {
//            FeedMapper mapper = session.getMapper(FeedMapper.class);
//            FeedCommentMapper commentMapper = session.getMapper(FeedCommentMapper.class);
//            FeedLikeMapper likeMapper = session.getMapper(FeedLikeMapper.class);
//            FeedSeemedMapper seemedMapper = session.getMapper(FeedSeemedMapper.class);
//
//            List<FeedDto> feedDtoList = mapper.getFeedListByFeedUserNo(request.getUserNo());
//            for (FeedDto feedDto : feedDtoList) {
//                feedDto.setCommentCnt(commentMapper.getFeedCommentCountByFeedNo(feedDto.getFeedNo()));
//                feedDto.setLikeCnt(likeMapper.getFeedLikeCountByFeedNo(feedDto.getFeedNo()));
//                feedDto.setSeemCnt(seemedMapper.getFeedSeemedCountByFeedNo(feedDto.getFeedNo()));
//            }
//            FeedList feedList = FeedDto.buildFeedListFromFeedDtoList(feedDtoList);
//
//            responseObserver.onNext(feedList);
//            responseObserver.onCompleted();
//        } catch (Exception e) {
//            logger.error(e.toString());
//            responseObserver.onError(e);
//            return;
//        }
    }
}