package ai.maum.rftapfanseemserver.grpc;


import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Component
public class GrpcUpdateServer {
    private static final Logger logger = LoggerFactory.getLogger(GrpcUpdateServer.class);

    @Value("${grpc.update.server.port}")
    private int port;
    private Server server;

    @Autowired
    private GrpcUpdateService grpcUpdateService;

    public void start() throws IOException {
        server = ServerBuilder.forPort(this.port).addService(grpcUpdateService).build();
        server.start();
        logger.info(String.format("update server start on port: %s", this.port));
    }

    public void stop() throws InterruptedException {
        if (server != null) {
            server.shutdown().awaitTermination(30, TimeUnit.SECONDS);
        }
    }
}

