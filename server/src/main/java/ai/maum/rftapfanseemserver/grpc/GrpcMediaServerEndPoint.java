package ai.maum.rftapfanseemserver.grpc;


import ai.maum.*;
import ai.maum.InternalMediaServerServiceGrpc.InternalMediaServerServiceStub;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Component
public class GrpcMediaServerEndPoint {
    private static final Logger logger = LoggerFactory.getLogger(GrpcMediaServerEndPoint.class);

    private final ManagedChannel channel;
    private final InternalMediaServerServiceStub asyncStub;

    @Autowired
    GrpcMediaServerEndPoint(@Value("${grpc.media.server.ip}") final String mediaServerIp,
                            @Value("${grpc.media.server.port}") final int mediaServerPort) {
        ManagedChannelBuilder channelBuilder = ManagedChannelBuilder.forAddress(mediaServerIp, mediaServerPort).usePlaintext();
        channel = channelBuilder.build();
        asyncStub = InternalMediaServerServiceGrpc.newStub(channel);
    }

    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    public String uploadImageToMediaServer(List<ByteString> payloadList, String fileExtension) {
        final CountDownLatch finishLatch = new CountDownLatch(1);
        final String[] resultUrl = new String[1];
        logger.info("try image upload media server: " + fileExtension);
        StreamObserver<MSUploadResult> responseObserver = new StreamObserver<MSUploadResult>() {
            @Override
            public void onNext(MSUploadResult msUploadResult) {
                resultUrl[0] = msUploadResult.getUrl();
            }

            @Override
            public void onError(Throwable throwable) {
                logger.error(throwable.toString());
            }

            @Override
            public void onCompleted() {
                finishLatch.countDown();
            }
        };
        StreamObserver<MSImagePayload> requestObserver = asyncStub.uploadImage(responseObserver);

        for (ByteString iter : payloadList) {
            requestObserver.onNext(
                    MSImagePayload
                            .newBuilder()
                            .setFileExtension(fileExtension)
                            .setImagePayload(iter)
                            .build());
        }
        requestObserver.onCompleted();
        try {
            finishLatch.await();
        } catch (InterruptedException e) {
            logger.error(e.toString());
        }
        logger.info(resultUrl[0]);

        return resultUrl[0];
    }

    public String uploadAudioToMediaServer(List<ByteString> payloadList, String fileExtension) {
        final CountDownLatch finishLatch = new CountDownLatch(1);
        final String[] resultUrl = new String[1];
        logger.info("try audio upload to media server: " + fileExtension);
        StreamObserver<MSUploadResult> responseObserver = new StreamObserver<MSUploadResult>() {
            @Override
            public void onNext(MSUploadResult msUploadResult) {
                resultUrl[0] = msUploadResult.getUrl();
            }

            @Override
            public void onError(Throwable throwable) {
                logger.error(throwable.toString());
            }

            @Override
            public void onCompleted() {
                finishLatch.countDown();
            }
        };
        StreamObserver<MSAudioPayload> requestObserver = asyncStub.uploadAudio(responseObserver);

        for (ByteString iter : payloadList) {
            requestObserver.onNext(
                    MSAudioPayload
                            .newBuilder()
                            .setFileExtension(fileExtension)
                            .setAudioPayload(iter)
                            .build());
        }
        requestObserver.onCompleted();
        try {
            finishLatch.await();
        } catch (InterruptedException e) {
            logger.error(e.toString());
        }
        logger.info(resultUrl[0]);

        return resultUrl[0];
    }
}

