package ai.maum.rftapfanseemserver.grpc;


import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Component
public class GrpcGetServer {
    private static final Logger logger = LoggerFactory.getLogger(GrpcGetServer.class);

    @Value("${grpc.get.server.port}")
    private int port;
    private Server server;

    @Autowired
    private GrpcGetService grpcGetService;

    public void start() throws IOException {
        server = ServerBuilder.forPort(this.port).addService(grpcGetService).build();
        server.start();
        logger.info(String.format("get server start on port: %s", this.port));
    }

    public void stop() throws InterruptedException {
        if (server != null) {
            server.shutdown().awaitTermination(30, TimeUnit.SECONDS);
        }
    }
}

