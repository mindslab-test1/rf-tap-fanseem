package ai.maum.rftapfanseemserver.grpc;

import ai.maum.InternalMediaServerServiceGrpc;
import ai.maum.MSImagePayload;
import ai.maum.MSUploadResult;
import com.google.protobuf.ByteString;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.stub.StreamObserver;
import maum.brain.tts.NgTtsServiceGrpc;
import maum.brain.tts.TtsMediaFileRequest;
import maum.brain.tts.TtsMediaResponse;
import maum.brain.tts.TtsRequest;
import maum.common.LangOuterClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

@Component
public class GrpcTtsEndpoint {
    private static final Logger logger = LoggerFactory.getLogger(GrpcMediaServerEndPoint.class);

    private final ManagedChannel channel;
    private final NgTtsServiceGrpc.NgTtsServiceBlockingStub blockingStub;
    private final NgTtsServiceGrpc.NgTtsServiceStub asyncStub;

    @Autowired
    GrpcTtsEndpoint(@Value("${grpc.tts.server.ip}") final String ttsServerIp,
                            @Value("${grpc.tts.server.port}") final int ttsServerPort) {
        ManagedChannelBuilder channelBuilder = ManagedChannelBuilder.forAddress(ttsServerIp, ttsServerPort).usePlaintext();
        channel = channelBuilder.build();
        blockingStub = NgTtsServiceGrpc.newBlockingStub(channel);
        asyncStub = NgTtsServiceGrpc.newStub(channel);
    }

    public void shutdown() throws InterruptedException {
        channel.shutdown().awaitTermination(5, TimeUnit.SECONDS);
    }

    public List<ByteString> getTtsWaveBytes(String ttsText) throws Exception {
        final CountDownLatch finishLatch = new CountDownLatch(1);
        final String[] resultUrl = new String[1];
        logger.info("try to get tts: " + ttsText);

        TtsRequest ttsRequest = TtsRequest.newBuilder()
                .setLang(LangOuterClass.Lang.ko_KR)
                .setSampleRate(22050)
                .setText(ttsText)
                .setSpeaker(1)
                .build();

        Iterator<TtsMediaResponse> waveBytes;
        List<ByteString> byteStringList = new ArrayList<>();
        byte[] byteArray;
        try {
            waveBytes = blockingStub.speakWav(ttsRequest);
            for (int i = 0; waveBytes.hasNext(); i++) {
                TtsMediaResponse ttsMediaResponse = waveBytes.next();
                ByteString byteString = ttsMediaResponse.getMediaData();
                //logger.info("tts wave bytes #" + i + ": " + byteString.size());
                byteStringList.add(byteString);
            }
        } catch (Exception e) {
            logger.error(e.toString());
            throw new Exception("error cause at getTtsWaveByte()");
        }

        return byteStringList;
    }
}
