package ai.maum.rftapfanseemserver.grpc;

import ai.maum.rftapfanseemserver.*;
import ai.maum.rftapfanseemserver.db.MyBatisRepository;
import ai.maum.rftapfanseemserver.dto.FeedLikeDto;
import ai.maum.rftapfanseemserver.dto.FeedSeemedDto;
import ai.maum.rftapfanseemserver.dto.UserDto;
import ai.maum.rftapfanseemserver.firebase.FirebaseFcmNotifyServer;
import ai.maum.rftapfanseemserver.mybatis.MybatisConnectionFactory;
import ai.maum.rftapfanseemserver.mybatis.mappers.*;
import com.google.protobuf.ByteString;
import com.google.protobuf.Descriptors;
import io.grpc.stub.StreamObserver;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class GrpcUpdateService extends UpdateServiceGrpc.UpdateServiceImplBase {
    private static final Logger logger = LoggerFactory.getLogger(GrpcUpdateServer.class);

    @Autowired
    private MyBatisRepository myBatisRepository;

    @Autowired
    GrpcMediaServerEndPoint grpcMediaServerEndPoint;

    @Autowired
    GrpcTtsEndpoint grpcTtsEndpoint;

    @Autowired
    FirebaseFcmNotifyServer firebaseFcmNotifyServer;

    SqlSessionFactory sqlSessionFactory = MybatisConnectionFactory.getSqlSessionFactory();

    private Map<String, Object> getOrdinaryMapFromProtoMap(Map<Descriptors.FieldDescriptor, Object> map) {
        Map<String, Object> result = new HashMap<>();
        for (Map.Entry<Descriptors.FieldDescriptor, Object> iter : map.entrySet()) {
            result.put(iter.getKey().getName().toUpperCase(), iter.getValue());
        }
        return result;
    }

    GrpcUpdateService() {
    }

    @Override
    public StreamObserver<AudioPayload> uploadAudio(StreamObserver<ContentUrl> responseObserver) {
        int payloadSize = 1024 * 64;
        List<ByteString> payloadList = new ArrayList<ByteString>();

        return new StreamObserver<AudioPayload>() {
            @Override
            public void onNext(AudioPayload audioPayload) {
                payloadList.add(audioPayload.getAudioPayload());
            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onCompleted() {
                responseObserver.onNext(ContentUrl.newBuilder()
                        .setUrl("\"https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3\"").build());
                responseObserver.onCompleted();
            }
        };
    }

    @Override
    public StreamObserver<ImagePayload> uploadImage(StreamObserver<ContentUrl> responseObserver) {
        int payloadSize = 1024 * 64;
        List<ByteString> payloadList = new ArrayList<>();
        final String[] fileExtension = new String[1];

        return new StreamObserver<ImagePayload>() {
            @Override
            public void onNext(ImagePayload imagePayload) {
                payloadList.add(imagePayload.getImagePayload());
                fileExtension[0] = imagePayload.getFileExtension();
            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onCompleted() {
                String resultUrl = grpcMediaServerEndPoint.uploadImageToMediaServer(payloadList, fileExtension[0]);
                responseObserver.onNext(ContentUrl.newBuilder()
                        .setUrl(resultUrl).build());
                responseObserver.onCompleted();
            }
        };
    }

    @Override
    public void sendChatMessage(ChatMessage request, StreamObserver<Status> responseObserver) {
        logger.info("got new message!");

        try (SqlSession session = sqlSessionFactory.openSession()) {
            ChatMessageMapper mapper = session.getMapper(ChatMessageMapper.class);

            Map<String, Object> map = getOrdinaryMapFromProtoMap(request.getAllFields());
            int result = mapper.insertChatMessage(map);

            if (result == 1) {
                session.commit();
                FirebaseFcmNotifyServer firebaseFcmNotifyServer = new FirebaseFcmNotifyServer();
                firebaseFcmNotifyServer.sendNewMessage(Integer.toString(request.getUserNo()), request);
                responseObserver.onNext(Status.newBuilder().setStatus(StatusType.OK).build());
            }
            else {
                throw new Exception("failed to insert message to DB");
            }

            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onNext(Status.newBuilder()
                    .setStatus(StatusType.ERROR)
                    .build()
            );
            responseObserver.onCompleted();
        }
    }

    @Override
    public void deleteChatMessage(ChatMessage request, StreamObserver<Status> responseObserver) {
        logger.info("got new delete chatMessage request: chatMessageNo " + request.getChatMessageNo());

        try (SqlSession session = sqlSessionFactory.openSession()) {
            ChatMessageMapper mapper = session.getMapper(ChatMessageMapper.class);

            int result = mapper.deleteChatMessage(request.getChatMessageNo());
            if (result == 1) {
                session.commit();
                responseObserver.onNext(Status.newBuilder().setStatus(StatusType.OK).build());
            }
            else {
                throw new Exception("failed to delete chatMessage in DB");
            }
            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onNext(Status.newBuilder()
                    .setStatus(StatusType.ERROR)
                    .build()
            );
            responseObserver.onCompleted();
        }
    }

    @Override
    public void loginRequest(User request, StreamObserver<LoginResult> responseObserver) {
        logger.info("got new login request!");

        UserDto userDto = myBatisRepository.getUserByIdPw(request.getId(), request.getPw());

        if (userDto == null) {
            responseObserver.onNext(LoginResult.newBuilder()
                    .setStatus(StatusType.LOGIN_FAIL)
                    .build()
            );
            responseObserver.onCompleted();
            return;
        }

        try (SqlSession session = sqlSessionFactory.openSession()) {
            UserMapper mapper = session.getMapper(UserMapper.class);

            Map<String, Object> map = new HashMap<>();
            if (!userDto.getDeviceTokenId().equals(request.getDeviceTokenId())) {
                map.put("DEVICE_TOKEN_ID", request.getDeviceTokenId());
                firebaseFcmNotifyServer.removeTokenIdFromList(userDto.getDeviceTokenId());
                firebaseFcmNotifyServer.addTokenIdToList(request.getDeviceTokenId());
                userDto.setDeviceTokenId(request.getDeviceTokenId());
            }
            map.put("UPDATE_TIME", request.getUpdateTime());
            map.put("USER_NO", userDto.getUserNo());
            if (mapper.updateUserByUserNo(map) != 1) {
                throw new Exception("failed to update UPDATE_TIME while login (maybe with DEVICE_TOKEN_ID)");
            }

            session.commit();
        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onNext(LoginResult.newBuilder()
                    .setStatus(StatusType.LOGIN_FAIL)
                    .build()
            );
            responseObserver.onCompleted();
            return;
        }

        User user = UserDto.buildUserFromUserDto(userDto);

        responseObserver.onNext(LoginResult.newBuilder()
                .setStatus(StatusType.OK)
                .setUser(user)
                .build()
        );
        responseObserver.onCompleted();
    }

    @Override
    public void snsLoginRequest(User request, StreamObserver<LoginResult> responseObserver) {
        logger.info("got new sns login request! " + request.getSocial() + ": " + request.getId());

        UserDto userDto;

        try (SqlSession session = sqlSessionFactory.openSession()) {
            UserMapper mapper = session.getMapper(UserMapper.class);
            userDto = mapper.getUserBySocialId(request.getSocial(), request.getId());

            if (userDto == null) {
                Map<String, Object> map = getOrdinaryMapFromProtoMap(request.getAllFields());
                int result = mapper.insertUser(map);
                if (result == 0) {
                    throw new Exception("failed to insert user");
                }
                userDto = mapper.getUserBySocialId(request.getSocial(), request.getId());
            }

            Map<String, Object> map = new HashMap<>();

            if (!userDto.getDeviceTokenId().equals(request.getDeviceTokenId())) {
                map.put("DEVICE_TOKEN_ID", request.getDeviceTokenId());
                firebaseFcmNotifyServer.removeTokenIdFromList(userDto.getDeviceTokenId());
                firebaseFcmNotifyServer.addTokenIdToList(request.getDeviceTokenId());
                userDto.setDeviceTokenId(request.getDeviceTokenId());
            }

            map.put("UPDATE_TIME", request.getUpdateTime());
            map.put("USER_NO", userDto.getUserNo());
            if (mapper.updateUserByUserNo(map) != 1) {
                throw new Exception("failed to update UPDATE_TIME while snsLogin (maybe with DEVICE_TOKEN_ID)");
            }
            session.commit();

            User user = UserDto.buildUserFromUserDto(userDto);
            responseObserver.onNext(LoginResult.newBuilder()
                    .setStatus(StatusType.OK)
                    .setUser(user)
                    .build()
            );
            responseObserver.onCompleted();

        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onNext(LoginResult.newBuilder()
                    .setStatus(StatusType.ERROR)
                    .build());
            responseObserver.onCompleted();
        }
    }

    @Override
    public void uploadTts(TtsRequest request, StreamObserver<ContentUrl> responseObserver) {
        logger.info("got new tts request");

        try {
            List<ByteString> ttsWaveByteStringList = grpcTtsEndpoint.getTtsWaveBytes(request.getTtsText());
            String resultUrl = grpcMediaServerEndPoint.uploadAudioToMediaServer(ttsWaveByteStringList, "wav");
            ContentUrl contentUrl =
                    ContentUrl.newBuilder()
                            .setUrl(resultUrl)
                            .build();
            responseObserver.onNext(contentUrl);
            responseObserver.onCompleted();
        } catch (Exception e) {
            responseObserver.onError(e);
            return;
        }
    }

    @Override
    public void updateCeleb(Celeb request, StreamObserver<Status> responseObserver) {
        logger.info("got new update celeb request!");

        try (SqlSession session = sqlSessionFactory.openSession()) {
            CelebMapper mapper = session.getMapper(CelebMapper.class);
            UserMapper userMapper = session.getMapper(UserMapper.class);

            Map<String, Object> map = getOrdinaryMapFromProtoMap(request.getAllFields());
            int result = mapper.updateCelebByUserNo(map);
            result += userMapper.updateUserByUserNo(map);

            if (result == 2) {
                session.commit();
                responseObserver.onNext(Status.newBuilder().setStatus(StatusType.OK).build());
            }
            else {
                throw new Exception("failed to update celeb to DB");
            }
            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onNext(Status.newBuilder()
                    .setStatus(StatusType.ERROR)
                    .build()
            );
            responseObserver.onCompleted();
        }
    }

    @Override
    public void updateUser(User request, StreamObserver<Status> responseObserver) {
        logger.info("got new update user request!");

        try (SqlSession session = sqlSessionFactory.openSession()) {
            UserMapper mapper = session.getMapper(UserMapper.class);
            CelebMapper celebMapper = session.getMapper(CelebMapper.class);

            Map<String, Object> map = getOrdinaryMapFromProtoMap(request.getAllFields());
            int result = mapper.updateUserByUserNo(map);
            if (request.getIsCeleb()) {
                result += celebMapper.updateCelebByUserNo(map);
            }
            else {
                result++;
            }

            if (result == 2) {
                session.commit();
                responseObserver.onNext(Status.newBuilder().setStatus(StatusType.OK).build());
            }
            else {
                throw new Exception("failed to update user to DB");
            }
            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onNext(Status.newBuilder()
                    .setStatus(StatusType.ERROR)
                    .build()
            );
            responseObserver.onCompleted();
        }
    }

    @Override
    public void uploadFeed(Feed request, StreamObserver<Status> responseObserver) {
        logger.info("got new upload feed request!");

        try (SqlSession session = sqlSessionFactory.openSession()) {
            FeedMapper mapper = session.getMapper(FeedMapper.class);

            Map<String, Object> map = getOrdinaryMapFromProtoMap(request.getAllFields());
            int result = mapper.insertFeed(map);
            if (result == 1) {
                session.commit();
                responseObserver.onNext(Status.newBuilder().setStatus(StatusType.OK).build());
            }
            else {
                throw new Exception("failed to update user to DB");
            }
            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onNext(Status.newBuilder()
                    .setStatus(StatusType.ERROR)
                    .build()
            );
            responseObserver.onCompleted();
        }
    }

    @Override
    public void uploadComment(FeedComment request, StreamObserver<Status> responseObserver) {
        logger.info("got new upload comment request!");

        try (SqlSession session = sqlSessionFactory.openSession()) {
            FeedCommentMapper mapper = session.getMapper(FeedCommentMapper.class);

            Map<String, Object> map = getOrdinaryMapFromProtoMap(request.getAllFields());
            int result = mapper.insertFeedComment(map);
            if (result == 1) {
                session.commit();
                responseObserver.onNext(Status.newBuilder().setStatus(StatusType.OK).build());
            }
            else {
                throw new Exception("failed to insert comment to DB");
            }
            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onNext(Status.newBuilder()
                    .setStatus(StatusType.ERROR)
                    .build()
            );
            responseObserver.onCompleted();
        }
    }

    @Override
    public void updateFeedLike(FeedLike request, StreamObserver<Status> responseObserver) {
        logger.info("got update feed like request!");

        try (SqlSession session = sqlSessionFactory.openSession()) {
            FeedLikeMapper mapper = session.getMapper(FeedLikeMapper.class);

            FeedLikeDto feedLikeDto = mapper.getFeedLikeByFeedNoUserNo(request.getFeedNo(), request.getLikeUserNo());

            if (feedLikeDto == null) {
                Map<String, Object> feedLikeMap = getOrdinaryMapFromProtoMap(request.getAllFields());
                if (mapper.insertFeedLike(feedLikeMap) == 0) {
                    throw new Exception("failed to insert new like into DB");
                }
            }
            else {
                Map<String, Object> feedLikeMap = new HashMap<>();
                feedLikeMap.put("LIKE_NO", feedLikeDto.getLikeNo());
                feedLikeMap.put("ACTIVE", feedLikeDto.getActive() == 'Y' ? 'N' : 'Y');
                if (mapper.updateFeedLike(feedLikeMap) == 0) {
                    throw new Exception("failed to update like into DB");
                }
            }

            session.commit();
            responseObserver.onNext(Status.newBuilder()
                    .setStatus(StatusType.OK)
                    .build());
            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onNext(Status.newBuilder()
                    .setStatus(StatusType.ERROR)
                    .build()
            );
            responseObserver.onCompleted();
        }
    }

    @Override
    public void updateFeedSemmed(FeedSeemed request, StreamObserver<Status> responseObserver) {
        logger.info("got update feed seemed request!");

        try (SqlSession session = sqlSessionFactory.openSession()) {
            FeedSeemedMapper mapper = session.getMapper(FeedSeemedMapper.class);

            FeedSeemedDto feedSeemedDto = mapper.getFeedSeemedByFeedNoUserNo(request.getFeedNo(), request.getSeemUserNo());

            if (feedSeemedDto == null) {
                Map<String, Object> feedSeemedMap = getOrdinaryMapFromProtoMap(request.getAllFields());
                if (mapper.insertFeedSeemed(feedSeemedMap) == 0) {
                    throw new Exception("failed to insert new seemed into DB");
                }
            }
            else {
                Map<String, Object> feedSeemedMap = new HashMap<>();
                feedSeemedMap.put("SEEM_NO", feedSeemedDto.getSeemNo());
                feedSeemedMap.put("ACTIVE", feedSeemedDto.getActive() == 'Y' ? 'N' : 'Y');
                if (mapper.updateFeedSeemed(feedSeemedMap) == 0) {
                    throw new Exception("failed to update seemed into DB");
                }
            }

            session.commit();
            responseObserver.onNext(Status.newBuilder()
                    .setStatus(StatusType.OK)
                    .build());
            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onNext(Status.newBuilder()
                    .setStatus(StatusType.ERROR)
                    .build()
            );
            responseObserver.onCompleted();
        }
    }

    @Override
    public void deleteFeed(Feed request, StreamObserver<Status> responseObserver) {
        logger.info("got new delete feed request: feedNo " + request.getFeedNo());

        try (SqlSession session = sqlSessionFactory.openSession()) {
            FeedMapper mapper = session.getMapper(FeedMapper.class);

            int result = mapper.deleteFeed(request.getFeedNo());
            if (result == 1) {
                session.commit();
                responseObserver.onNext(Status.newBuilder().setStatus(StatusType.OK).build());
            }
            else {
                throw new Exception("failed to delete feed in DB");
            }
            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onNext(Status.newBuilder()
                    .setStatus(StatusType.ERROR)
                    .build()
            );
            responseObserver.onCompleted();
        }
    }

    @Override
    public void deleteComment(FeedComment request, StreamObserver<Status> responseObserver) {
        logger.info("got new delete comment request: commentNo " + request.getCommentNo());

        try (SqlSession session = sqlSessionFactory.openSession()) {
            FeedCommentMapper mapper = session.getMapper(FeedCommentMapper.class);

            int result = mapper.deleteFeedComment(request.getCommentNo());
            if (result == 1) {
                session.commit();
                responseObserver.onNext(Status.newBuilder().setStatus(StatusType.OK).build());
            }
            else {
                throw new Exception("failed to delete comment in DB");
            }
            responseObserver.onCompleted();
        } catch (Exception e) {
            logger.error(e.toString());
            responseObserver.onNext(Status.newBuilder()
                    .setStatus(StatusType.ERROR)
                    .build()
            );
            responseObserver.onCompleted();
        }
    }
}
