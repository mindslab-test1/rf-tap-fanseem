package ai.maum.rftapfanseemserver.mybatis.mappers;

import ai.maum.rftapfanseemserver.dto.FeedSeemedDto;
import ai.maum.rftapfanseemserver.dto.SeemBoardCreateDto;
import ai.maum.rftapfanseemserver.dto.SeemBoardDto;
import ai.maum.rftapfanseemserver.dto.SeemBoardFeedDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface FeedSeemedMapper {

    int getFeedSeemedCountByFeedNo(int feedNo);

    FeedSeemedDto getFeedSeemedBySeemNo(int seemNo);

    FeedSeemedDto getFeedSeemedByFeedNoUserNo(int feedNo, int seemUserNo);

    List<Integer> getFeedSeemedListBySeemUserNo(int userNo);

    int insertFeedSeemed(Map<String, Object> feedSeemedMap);

    int updateFeedSeemed(Map<String, Object> feedSeemedMap);

    int getIsFeedSeemedByUser(int feedNo, int seemUserNo);

    int insertNewBoard(Map<String, Object> newBoardMap);

    SeemBoardCreateDto selectBoardId(Map<String, Object> newBoardMap);

    List<SeemBoardDto> getUserBoardInfo(Map<String, Object> params);

    int deleteBoard(Map<String, Object> params);

    int deleteBoardFeed(Map<String, Object> params);

    List<SeemBoardFeedDto> getFeedFromBoard(Map<String, Object> params);

    int checkInactiveFeed(Map<String, Object> params);

    int insertFeedToBoard(Map<String, Object> params);

    int updateBoardFeed(Map<String, Object> params);

    int updateBoardName(Map<String, Object> params);
}
