package ai.maum.rftapfanseemserver.mybatis.mappers;

import ai.maum.rftapfanseemserver.dto.FeedCommentDataDto;
import ai.maum.rftapfanseemserver.dto.FeedCommentDto;
import ai.maum.rftapfanseemserver.dto.FeedDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface FeedCommentMapper {

    int getFeedCommentCountByFeedNo(int feedNo);

    List<FeedCommentDto> getFeedCommentListByFeedNo(int feedNo);

    int insertFeedComment(Map<String, Object> feedCommentMap);

    int updateFeedComment(Map<String, Object> feedCommentMap);

    int deleteFeedComment(int commentNo);

    List<FeedCommentDataDto> selectCommentList(Map<String, Object> params);
}
