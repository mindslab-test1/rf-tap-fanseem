package ai.maum.rftapfanseemserver.mybatis.mappers;

import ai.maum.rftapfanseemserver.dto.CelebDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface CelebMapper {

    CelebDto getCelebByUserNo(int userNo);

    List<CelebDto> getCelebListBySubscribingUserNoList(List<Integer> subscribingUserNoList);

    List<CelebDto> getCelebListByInterestedCategoryList(List<String> interestedCategoryList);

    List<CelebDto> getCelebListBySearchQuery(String query);

    int updateCelebByUserNo(Map<String, Object> celebMap);
}
