package ai.maum.rftapfanseemserver.mybatis.mappers;

import ai.maum.rftapfanseemserver.dto.FeedDto;
import ai.maum.rftapfanseemserver.dto.FeedListDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface FeedMapper {

    FeedDto getFeedByFeedFeedNo(int feedNo);

    List<FeedDto> getFeedListByFeedUserNo(int feedUserNo);

    FeedDto getLastFeedByFeedUserNo(int feedUserNo);

    int insertFeed(Map<String, Object> feedMap);

    int updateFeed(Map<String, Object> feedMap);

    int deleteFeed(int feedNo);

    List<FeedListDto> selectFeedList(Map<String, Object> params);

    int insertFeedYoutube(Map<String, Object> feedMap);
}
