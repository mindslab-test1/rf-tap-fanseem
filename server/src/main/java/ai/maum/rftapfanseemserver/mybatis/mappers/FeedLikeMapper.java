package ai.maum.rftapfanseemserver.mybatis.mappers;

import ai.maum.rftapfanseemserver.dto.FeedLikeDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface FeedLikeMapper {

    int getFeedLikeCountByFeedNo(int feedNo);

    FeedLikeDto getFeedLikeByLikeNo(int likeNo);

    FeedLikeDto getFeedLikeByFeedNoUserNo(int feedNo, int likeUserNo);

    int insertFeedLike(Map<String, Object> feedLikeMap);

    int updateFeedLike(Map<String, Object> feedLikeMap);

    int getIsFeedLikedByUser(int feedNo, int likeUserNo);

}
