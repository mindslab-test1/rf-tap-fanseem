package ai.maum.rftapfanseemserver.mybatis.mappers;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface AlertMapper {
    List<String> selectMessageDevices(Map<String, Object> params);
    List<String> selectFeedDevices(Map<String, Object> params);
    String selectActivityDevice(Map<String, Object> params);
}
