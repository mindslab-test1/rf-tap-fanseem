package ai.maum.rftapfanseemserver.mybatis.mappers;

import ai.maum.rftapfanseemserver.dto.ChatMessageDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ChatMessageMapper {

    List<ChatMessageDto> getChatMessageListByUserNo(int userNo);

    int insertChatMessage(Map<String, Object> chatMessageMap);

    int deleteChatMessage(int chatMessageNo);
}
