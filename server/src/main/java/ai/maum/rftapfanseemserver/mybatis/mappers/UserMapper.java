package ai.maum.rftapfanseemserver.mybatis.mappers;

import ai.maum.rftapfanseemserver.dto.PushSettingsDto;
import ai.maum.rftapfanseemserver.dto.UserDto;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface UserMapper {

    UserDto getUserByUserNo(int userNo);

    UserDto getUserBySocialId(String social, String id);

    int insertUser(Map<String, Object> userMap);

    int updateUserByUserNo(Map<String, Object> userMap);

    int updatePushSettings(Map<String, Object> params);

    int initUserSettings(Map<String, Object> params);

    List<PushSettingsDto> selectUserSettings(Map<String, Object> params);
}
