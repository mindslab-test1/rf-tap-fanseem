package ai.maum.rftapfanseemserver.mybatis;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Reader;

public class MybatisConnectionFactory {
    private static final Logger logger = LoggerFactory.getLogger(MybatisConnectionFactory.class);

    public static SqlSessionFactory sqlSession;

    static {
        try {
            String resource = "mybatis/mybatis-config.xml";
            Reader reader;
            reader = Resources.getResourceAsReader( resource );
            sqlSession = new SqlSessionFactoryBuilder().build( reader );
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public static SqlSessionFactory getSqlSessionFactory() {
        return sqlSession;
    }
}
