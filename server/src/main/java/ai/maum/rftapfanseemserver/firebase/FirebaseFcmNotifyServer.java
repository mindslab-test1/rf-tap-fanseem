package ai.maum.rftapfanseemserver.firebase;

import ai.maum.rftapfanseemserver.ChatMessage;
import ai.maum.rftapfanseemserver.db.MyBatisRepository;
import ai.maum.rftapfanseemserver.dto.UserDto;
import ai.maum.rftapfanseemserver.grpc.GrpcUpdateServer;
import ai.maum.rftapfanseemserver.mybatis.MybatisConnectionFactory;
import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.*;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.*;

@Component
public class FirebaseFcmNotifyServer {
    private static final Logger logger = LoggerFactory.getLogger(GrpcUpdateServer.class);
    private static Set<String> tokenIdSet = new HashSet<>();
    private static List<String> tokenIdList = new ArrayList<>();

    @Autowired
    private MyBatisRepository myBatisRepository;

    @Value("${firebase.token.path}")
    private String tokenPath;

    public void removeTokenIdFromList(String tokenId) {
        if (!tokenIdSet.contains(tokenId)) return;
        tokenIdSet.remove(tokenId);
        tokenIdList.remove(tokenId);
    }

    public void addTokenIdToList(String tokenId) {
        if (tokenIdSet.contains(tokenId)) return;
        tokenIdSet.add(tokenId);
        tokenIdList.add(tokenId);
    }

    public void initialize() {
        try {
            FileInputStream token = new FileInputStream(tokenPath);

            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(token))
                    .setDatabaseUrl("https://rf-tap-fanseem.firebaseio.com")
                    .build();

            if (FirebaseApp.getApps().isEmpty()) {
                FirebaseApp.initializeApp(options);
                logger.info("initialize firebase app");
            }

            if (tokenIdList.isEmpty()) {
                List<UserDto> userDtoList = myBatisRepository.getEveryUsers();
                for (UserDto userDto : userDtoList) {
                    addTokenIdToList(userDto.getDeviceTokenId());
                }
                logger.info("Got Every DeviceTokenId from RF_USER table: " + tokenIdList.size());
            }
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public void sendNewMessage(String sender, ChatMessage chatMessage) {
        try {
            Map<String, String> data = new HashMap<String, String>();
            data.put("firebaseMessageType", "chatMessage");
            data.put("userNo", Integer.toString(chatMessage.getUserNo()));
            data.put("createTime", chatMessage.getCreateTime());
            data.put("contentType", chatMessage.getContentType());
            data.put("contentString", chatMessage.getContentString());

            List<String> deviceIds = myBatisRepository.selectDeviceIds(chatMessage.getUserNo());

            MulticastMessage multicastMessage = MulticastMessage.builder()
                    .setAndroidConfig(AndroidConfig.builder()
                            .setPriority(AndroidConfig.Priority.NORMAL)
                            .setNotification(AndroidNotification.builder()
                                    .setTitle("팬밋")
                                    .setBody("메시지가 도착하였습니다.")
                                    .setColor("#fc660c")
                                    .build()
                            ).build())
                    .putAllData(data)
                    .addAllTokens(deviceIds)
                    .build();

            logger.info(multicastMessage.toString());
            BatchResponse response = FirebaseMessaging.getInstance().sendMulticast(multicastMessage);

            List<SendResponse> responses = response.getResponses();
        } catch (Exception e) {
            logger.error(e.toString());
        }
    }

    public void sendCelebMessage(List<String> deviceIdList, String celebName, String messageType){
        try {
            String message;
            switch (messageType){
                case "feed":
                    message = "새 피드를 작성했습니다!";
                    break;
                case "message":
                    message = "메시지를 보내셨습니다!";
                    break;
                default:
                    throw new Exception("Unknown message type");
            }

            MulticastMessage multicastMessage = MulticastMessage.builder()
                    .setAndroidConfig(
                            AndroidConfig.builder()
                                    .setPriority(AndroidConfig.Priority.NORMAL)
                                    .setNotification(
                                            AndroidNotification.builder()
                                                    .setTitle("팬밋")
                                                    .setBody(String.format("%s님이 %s", celebName, message))
                                                    .setColor("#fc668c")
                                                    .build()
                                    )
                                    .build()
                    )
                    .addAllTokens(deviceIdList)
                    .build();
            BatchResponse response = FirebaseMessaging.getInstance().sendMulticast(multicastMessage);
            logger.info(String.format("success count: %d", response.getSuccessCount()));
            if(response.getFailureCount() > 0) logger.warn(String.format("failure count: %d", response.getFailureCount()));
        } catch (Exception e){
            logger.error(e.toString());
        }
    }

    public void sendActivityMessage(String deviceTokenId, String messageType){
        String message;
        try {
            switch (messageType){
                case "like":
                    message = "자신의 피드가 좋아요를 받았어요!";
                    break;
                case "scrap":
                    message = "누군가 피드를 스크랩 했어요!";
                    break;
                case "comment":
                    message = "자신의 피드에 댓글이 달렸어요!";
                    break;
                default:
                    throw new Exception("Unknown message type");
            }

            MulticastMessage multicastMessage = MulticastMessage.builder()
                    .setAndroidConfig(
                            AndroidConfig.builder()
                                    .setPriority(AndroidConfig.Priority.NORMAL)
                                    .setNotification(
                                            AndroidNotification.builder()
                                                    .setTitle("팬밋")
                                                    .setBody(message)
                                                    .setColor("#fc668c")
                                                    .build()
                                    )
                                    .build()
                    )
                    .addToken(deviceTokenId)
                    .build();

            BatchResponse response = FirebaseMessaging.getInstance().sendMulticast(multicastMessage);
            logger.info(String.format("success count: %d", response.getSuccessCount()));
            if(response.getFailureCount() > 0) logger.warn(String.format("failure count: %d", response.getFailureCount()));
        } catch (Exception e){
            logger.error(e.getMessage());
        }
    }

    public void sendTestMessage(String deviceTokenId){
        try {
            Map<String, String> data = new HashMap<String, String>();
            data.put("firebaseMessageType", "chatMessage");
            MulticastMessage multicastMessage = MulticastMessage.builder()
                    .setAndroidConfig(AndroidConfig.builder()
                            .setPriority(AndroidConfig.Priority.NORMAL)
                            .setNotification(AndroidNotification.builder()
                                    .setTitle("팬밋")
                                    .setBody("테스트 메시지 입니다.")
                                    .setColor("#fc668c")
                                    .build())
                            .build())
                    .putAllData(data)
                    .addToken(deviceTokenId)
                    .build();

            logger.info(multicastMessage.toString());
            BatchResponse response = FirebaseMessaging.getInstance().sendMulticast(multicastMessage);

            List<SendResponse> responses = response.getResponses();
        } catch (Exception e){
            logger.error(e.toString());
        }
    }
}
