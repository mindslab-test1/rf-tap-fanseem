package ai.maum.rftapfanseemserver.db;

import ai.maum.rftapfanseemserver.dto.*;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Mapper
@Component
public interface MyBatisRepository {
    /*
    Celeb Stuff
     */
    @Select("select * from MINDS.RF_CELEB")
    public List<CelebDto> getEveryCelebList();

    /*
    User Stuff
     */
    @Select("select * from MINDS.RF_USER") // 일부러 놔둠
    public List<UserDto> getEveryUsers();

    @Select("select * from MINDS.RF_USER where ID = #{id} and PW = #{pw}") // 일부러 놔둠
    public UserDto getUserByIdPw(String id, String pw);


    /*
    Feed Stuff
     */
    @Select("select * from MINDS.RF_FEED where CELEB_NO = ${celebNo}")
    public List<FeedDto> getFeedByCelebNo(int celebNo);

    @Select("select * from (select t.*, row_number() over (order by t.UPDATE_TIME desc) as rn from MINDS.RF_FEED t) where CELEB_NO = ${celebNo} and rn <= 1")
    public FeedDto getLastFeedByCelebNo(int celebNo);

    @Insert("INSERT INTO MINDS.RF_FEED ${col} VALUES ${exp}")
    public int insertFeed(String col, String exp);

    @Select("select * from MINDS.RF_SUBSCRIBE_TRACK where USER_NO = ${userId}")
    public List<SubscribeDto> getCurrentSubscribe(int userId);

    @Select("select * from MINDS.RF_SUBSCRIBE_TRACK where celeb_no = ${celebId}")
    public List<SubscribeDto> getCurrentSeemer(int celebId);

    @Select("select * from MINDS.RF_SUBSCRIBE_TRACK where celeb_no = ${celebId} and user_no = ${userId}")
    public List<SubscribeDto> getSubscribeOne(int userId, int celebId);

    @Insert("insert into MINDS.RF_SUBSCRIBE_TRACK(id, user_no, celeb_no, active) values (MINDS.SEQ_RF_SUBSCRIBE_ID.nextval, ${userId}, ${celebId}, 'Y')")
    public int insertSeem(int userId, int celebId);

    @Update("update MINDS.RF_SUBSCRIBE_TRACK set ACTIVE='Y' where USER_NO = ${userId} and CELEB_NO = ${celebId}")
    public int updateSeemYes(int userId, int celebId);

    @Update("update MINDS.RF_SUBSCRIBE_TRACK set ACTIVE='N' where USER_NO = ${userId} and CELEB_NO = ${celebId}")
    public int updateSeemNo(int userId, int celebId);

    @Select("select DEVICE_TOKEN_ID from MINDS.RF_USER where subscribing_user_no_list like '%${celebId}%'")
    public List<String> selectDeviceIds(int celebId);

    @Select("select count(*) from MINDS.RF_SUBSCRIBE_TRACK where CELEB_NO=${celebId} and ACTIVE='Y'")
    public int getSeemers(int celebId);

    @Select("select CURRENT_VERSION, COMPATIBLE_VERSION from (select * from RF_LATEST_VERSION order by CREATE_DATE desc) where ROWNUM<2")
    public Map<String, String> getVersion();
}
