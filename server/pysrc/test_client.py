import sys
import os
import grpc
import io

from rf_tap_fanseem_pb2 import
from rf_tap_fanseem_pb2_grpc import GetServiceStub


if __name__ == "__main__":
    with grpc.insecure_channel("0.0.0.0:12302") as channel:
        stub = EnhancedSuperResolutionStub(channel)
        res_ = stub.GetAds(img2str())
        print(res)
