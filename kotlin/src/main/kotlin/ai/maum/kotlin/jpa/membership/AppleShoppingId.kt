//package ai.maum.kotlin.jpa.membership
//
//import ai.maum.kotlin.jpa.AllOpen
//import java.util.*
//import javax.persistence.*
//
//@AllOpen
//@Entity
//class AppleShoppingId {
//
//    @Id
//    @SequenceGenerator(
//            name = "iOS_SHOPPING_ID_SEQ_GEN",
//            sequenceName = "iOS_SHOPPING_ID_SEQ",
//            initialValue = 1,
//            allocationSize = 1
//    )
//    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "iOS_SHOPPING_ID_SEQ_GEN")
//    var iosShoppingId: Int? = null
//
//    @Column(nullable = false)
//    var userId: Int? = null
//
//    @Column(nullable = false)
//    var orgTranId: String? = null
//
//    @Column(nullable = false)
//    var receiptFilePath: String? = null
//
//    @Column(nullable = false)
//    var crtDate: Date? = null
//
//    @Column(nullable = false)
//    var active: Int? = null
//
//}