package ai.maum.kotlin.jpa.ttsgen

import ai.maum.kotlin.jpa.AllOpen
import javax.persistence.*

@AllOpen
@Entity
class TtsAddress {
    @Id
    @SequenceGenerator(
            name = "TTS_ADDRESS_SEQ_GEN",
            sequenceName = "TTS_ADDRESS_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TTS_ADDRESS_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var celeb: Long? = null

    @Column(nullable = false)
    var ip: String? = null

    @Column(nullable = false)
    var port: Int? = null
}