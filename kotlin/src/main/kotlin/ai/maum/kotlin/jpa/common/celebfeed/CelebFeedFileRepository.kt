package ai.maum.kotlin.jpa.common.celebfeed

import org.springframework.data.repository.CrudRepository

interface CelebFeedFileRepository : CrudRepository<CelebFeedFile, Long> {
    fun findByActiveIsTrue() : List<CelebFeedFile>
}