package ai.maum.kotlin.jpa.common.service

import org.springframework.data.repository.CrudRepository

interface TtsRepository : CrudRepository<Tts, Long> {
    fun findByIdAndActiveIsTrue(id: Long): Tts?

    fun findByCelebAndStatusAndActiveIsTrue(celeb: Long, status: Tts.Status): List<Tts>

    fun findBySubscriberAndActiveIsTrue(subscriber: Long): List<Tts>?

    fun findBySubscriberAndCelebAndStatusAndActiveIsTrue(subscriber: Long, celeb: Long, status: Tts.Status): List<Tts>?

    fun findBySubscriberAndCelebAndStatusIsNotInAndActiveIsTrue(
            subscriber: Long,
            celeb: Long,
            status: Collection<Tts.Status>
    ): List<Tts>

}