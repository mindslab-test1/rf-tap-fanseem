package ai.maum.kotlin.jpa.membership

import org.springframework.data.repository.CrudRepository

interface UserMembershipHistoryRepository : CrudRepository<UserMembershipHistory, Long> {
    fun findByIdAndActiveIsTrue(id: Long): UserMembershipHistory?
    fun findTopByUserIdAndMembershipCelebAndActiveIsTrueOrderByExpirationDateDesc(userId: Long, membershipCeleb: Long): UserMembershipHistory?
    fun findTopByUserIdAndMembershipCelebAndStatusIsNotAndActiveIsTrueOrderByExpirationDate(userId: Long, membershipCeleb: Long, status: UserMembershipHistory.Status): UserMembershipHistory?
    fun findTopByPurchaseTokenAndActiveIsTrueOrderByExpirationDateDesc(purchaseToken: String): UserMembershipHistory?
    fun findByUserIdAndActiveIsTrue(userId: Long): List<UserMembershipHistory>
    fun findByUserIdAndOrgTranIdAndActiveIsTrueOrderByCreatedDesc(userId: Long, orgTranId: String):List<UserMembershipHistory>
    fun findByOrgTranIdAndActiveIsTrueOrderByCreatedDesc(orgTranId: String):List<UserMembershipHistory>
    fun findByOrgTranIdAndUserIdAndActiveIsFalseOrderByCreatedDesc(orgTranId: String, userId: Long): List<UserMembershipHistory>
    fun countAllById(orgTranId: String): Int
}