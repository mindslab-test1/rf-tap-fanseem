package ai.maum.kotlin.jpa.common.block

import org.springframework.data.repository.CrudRepository

interface BlockRepository : CrudRepository<Block, Long> {
    fun existsByWhoAndWhomAndActiveIsTrue(who: Long, whom: Long) : Boolean

    fun findByWhoAndActiveIsTrue(who: Long) : List<Block>
    fun findByWhoAndActiveIsTrueOrderByUpdatedDesc(who: Long) : List<Block>
    fun findByWhoAndActiveIsTrueOrderByUpdatedAsc(who: Long) : List<Block>

    fun findByWhomAndActiveIsTrue(whom: Long) : List<Block>
}