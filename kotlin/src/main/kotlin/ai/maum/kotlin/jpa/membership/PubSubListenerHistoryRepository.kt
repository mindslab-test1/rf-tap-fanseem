package ai.maum.kotlin.jpa.membership

import org.springframework.data.repository.CrudRepository

interface PubSubListenerHistoryRepository : CrudRepository<PubSubListenerHistory, Long> {
}