package ai.maum.kotlin.jpa.common.subscribe

import org.springframework.data.repository.CrudRepository

interface SubscribeRepository : CrudRepository<Subscribe, Long> {
    fun findAllByWho(who : Long) : List<Subscribe>?
    fun findAllByWhoAndActiveIsTrue(who: Long): List<Subscribe>
    fun findAllByWhomAndActiveIsTrue(whom : Long) : List<Subscribe>
    fun findTop10ByWhomAndActiveIsTrue(whom: Long) : List<Subscribe>
    fun findByWhoAndWhomAndActiveIsTrue(who: Long, whom: Long) : Subscribe?
    fun existsByWhoAndWhomAndActiveIsTrue(who: Long, whom: Long) : Boolean
    fun findByWhoAndWhomAndActiveIsFalse(who: Long, whom: Long) : Subscribe?
    fun countByWhomAndActiveIsTrue(whom: Long) : Long
}