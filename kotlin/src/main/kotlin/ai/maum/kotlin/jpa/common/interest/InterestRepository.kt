package ai.maum.kotlin.jpa.common.interest

import org.springframework.data.repository.CrudRepository

interface InterestRepository : CrudRepository<Interest, Long> {
    fun findAllByActiveIsTrue(): List<Interest>
    fun findByIdAndActiveIsTrue(id: Long): Interest?
}