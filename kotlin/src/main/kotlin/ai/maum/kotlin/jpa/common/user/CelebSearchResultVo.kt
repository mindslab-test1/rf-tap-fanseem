package ai.maum.kotlin.jpa.common.user

data class CelebSearchResultVo(
        val id: Long,
        val profileImageUrl: String?,
        val name: String,
        val introduction: String?
) {
    constructor(user: User) : this(
            id = user.id!!,
            profileImageUrl = user.profileImageUrl,
            name = user.name!!,
            introduction = user.celeb!!.hello
    )
}