package ai.maum.kotlin.jpa.authentication.kakao

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.authentication.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class KakaoUserToken () : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "KAKAO_USER_TOKEN_SEQ_GEN",
            sequenceName = "KAKAO_USER_TOKEN_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "KAKAO_USER_TOKEN_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var accessToken: String? = null

    @Column(nullable = false)
    var kakaoUserId: Long? = null

    @Column(nullable = false)
    var userId: Long? = null

    @Column(nullable = false)
    var deviceId: String? = null
}