package ai.maum.kotlin.jpa.common.feed

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import ai.maum.kotlin.jpa.common.service.Tts
import javax.persistence.*

@AllOpen
@Entity
class Feed : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "FEED_SEQ_GEN",
            sequenceName = "FEED_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FEED_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var celeb: Long? = null

    @Column(nullable = false)
    var who: Long? = null

    @Column(length = 1000)
    var text: String? = null

    @Column(nullable = false)
    var category: Long? = null

    @Column(nullable = false)
    var likeCount: Long? = 0

    @Column(nullable = false)
    var scrapCount: Long? = 0

    @Column(nullable = false)
    var commentCount: Long? = 0

    @OneToMany(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    @JoinColumn(name = "feed")
    var pictureList: MutableList<FeedFile>? = null

    @ManyToOne
    var tts: Tts? = null

    @OneToOne(cascade = [CascadeType.ALL])
    var video: FeedFile? = null

    @OneToOne(cascade = [CascadeType.ALL])
    var thumbnail: FeedFile? = null

    var youtube: String? = null
}