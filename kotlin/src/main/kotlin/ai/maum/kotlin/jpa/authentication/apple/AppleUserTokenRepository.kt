package ai.maum.kotlin.jpa.authentication.apple

import org.springframework.data.repository.CrudRepository
import javax.transaction.Transactional

interface AppleUserTokenRepository : CrudRepository<AppleUserToken, Long>{
    fun findByAccessToken(accessToken: String): List<AppleUserToken>?
    fun findByAppleUserId(appleUserId: String): List<AppleUserToken>?
    fun findByUserId(userId: Long): List<AppleUserToken>?
    fun findAllByUserId(userId: Long): List<AppleUserToken>?
    fun findAllByUserIdIn(userIds: List<Long>): List<AppleUserToken>?
    fun findByDeviceId(deviceId: String): List<AppleUserToken>?

    @Transactional
    fun deleteByDeviceId(deviceId: String): Long?
}