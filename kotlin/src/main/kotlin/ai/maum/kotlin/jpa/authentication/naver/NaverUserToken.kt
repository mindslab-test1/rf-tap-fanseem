package ai.maum.kotlin.jpa.authentication.naver

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.authentication.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class NaverUserToken () : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "NAVER_USER_TOKEN_SEQ_GEN",
            sequenceName = "NAVER_USER_TOKEN_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NAVER_USER_TOKEN_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var accessToken: String? = null

    @Column(nullable = false)
    var naverUserId: String? = null

    @Column(nullable = false)
    var userId: Long? = null

    @Column(nullable = false)
    var deviceId: String? = null
}