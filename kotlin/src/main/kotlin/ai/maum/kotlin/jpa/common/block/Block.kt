package ai.maum.kotlin.jpa.common.block

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class Block : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "BLOCK_SEQ_GEN",
            sequenceName = "BLOCK_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "BLOCK_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var who: Long? = null

    @Column(nullable = false)
    var whom: Long? = null
}