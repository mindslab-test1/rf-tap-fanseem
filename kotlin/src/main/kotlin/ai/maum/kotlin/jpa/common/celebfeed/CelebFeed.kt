package ai.maum.kotlin.jpa.common.celebfeed

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import ai.maum.kotlin.jpa.common.feed.FeedFile
import ai.maum.kotlin.jpa.common.service.Tts
import javax.persistence.*

@AllOpen
@Entity
class CelebFeed : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "CELEB_FEED_SEQ_GEN",
            sequenceName = "CELEB_FEED_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CELEB_FEED_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var who: Long? = null

    @Column(columnDefinition = "CLOB NOT NULL")
    @Lob
    var text: String? = null

    @Column(nullable = false)
    var category: Long? = null

    @Column(nullable = false)
    var likeCount: Long? = 0

    @Column(nullable = false)
    var scrapCount: Long? = 0

    @Column(nullable = false)
    var commentCount: Long? = 0

    @Column(nullable = false)
    var accessLevel: Long? = 0

    @OneToMany(fetch = FetchType.LAZY, cascade = [CascadeType.ALL])
    @JoinColumn(name = "celebFeed")
    var pictureList: MutableList<CelebFeedFile>? = null

    @ManyToOne
    var tts: Tts? = null

    @OneToOne(cascade = [CascadeType.ALL])
    var video: CelebFeedFile? = null

    @OneToOne(cascade = [CascadeType.ALL])
    var thumbnail: CelebFeedFile? = null

    var youtube: String? = null
}