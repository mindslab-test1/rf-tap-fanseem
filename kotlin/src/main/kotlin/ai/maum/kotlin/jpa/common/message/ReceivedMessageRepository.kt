package ai.maum.kotlin.jpa.common.message

import org.springframework.data.repository.CrudRepository
import java.time.Instant

interface ReceivedMessageRepository : CrudRepository<ReceivedMessage, Long> {
    fun findTopByWhomAndWhatWhoAndActiveIsTrueOrderByCreatedDesc(whom: Long, whatWho: Long): ReceivedMessage?

    fun findTop20ByWhomAndWhatWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(whom: Long, whatWho: Long, created: Instant): List<ReceivedMessage>
}