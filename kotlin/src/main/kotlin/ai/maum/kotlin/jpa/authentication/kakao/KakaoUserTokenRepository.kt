package ai.maum.kotlin.jpa.authentication.kakao

import org.springframework.data.repository.CrudRepository
import org.springframework.transaction.annotation.Transactional

interface KakaoUserTokenRepository : CrudRepository<KakaoUserToken, Long> {
    fun findByAccessToken(accessToken: String): List<KakaoUserToken>?
    fun findByKakaoUserId(kakaoUserId: Long): List<KakaoUserToken>?
    fun findByUserId(userId: Long): List<KakaoUserToken>?
    fun findAllByUserId(userId: Long): List<KakaoUserToken>?
    fun findAllByUserIdIn(userIds: List<Long>): List<KakaoUserToken>?
    fun findByDeviceId(deviceId: String): List<KakaoUserToken>?

    @Transactional
    fun deleteByDeviceId(deviceId: String): Long?
}