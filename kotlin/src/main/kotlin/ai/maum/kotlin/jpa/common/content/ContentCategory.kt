package ai.maum.kotlin.jpa.common.content

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
class ContentCategory: BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "CONTENT_CATEGORY_SEQ_GEN",
            sequenceName = "CONTENT_CATEGORY_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CONTENT_CATEGORY_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var celeb: Long? = null

    @Column(nullable = false)
    var name: String? = null
}