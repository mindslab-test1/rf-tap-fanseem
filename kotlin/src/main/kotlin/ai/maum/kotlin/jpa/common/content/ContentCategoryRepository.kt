package ai.maum.kotlin.jpa.common.content

import org.springframework.data.repository.CrudRepository

interface ContentCategoryRepository : CrudRepository<ContentCategory, Long> {
    fun findByIdAndActiveIsTrue(id: Long): ContentCategory?
    fun findByActiveIsTrue(): List<ContentCategory>

    fun findByCelebAndActiveIsTrueOrderByCreatedAsc(celeb: Long): List<ContentCategory>
    fun findByCelebAndActiveIsTrueOrderByCreatedDesc(celeb: Long): List<ContentCategory>
}