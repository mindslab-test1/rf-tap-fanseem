package ai.maum.kotlin.jpa.common.content

import org.springframework.data.repository.CrudRepository

interface ContentFileRepository : CrudRepository<ContentFile, Long> {
    fun findByActiveIsTrue(): List<ContentFile>
}