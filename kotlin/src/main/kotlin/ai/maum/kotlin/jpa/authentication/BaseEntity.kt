package ai.maum.kotlin.jpa.authentication

import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.LastModifiedDate
import org.springframework.data.jpa.domain.support.AuditingEntityListener
import java.time.Instant
import javax.persistence.*

@MappedSuperclass
@EntityListeners(AuditingEntityListener::class)
open class BaseEntity () {
    @CreatedDate
    @Column(nullable = false, updatable = false)
    private var created: Instant? = null

    @LastModifiedDate
    @Column(nullable = false, updatable = true)
    private var updated: Instant? = null
}