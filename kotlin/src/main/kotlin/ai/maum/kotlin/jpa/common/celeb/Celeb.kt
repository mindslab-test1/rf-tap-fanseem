package ai.maum.kotlin.jpa.common.celeb

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import ai.maum.kotlin.jpa.common.user.User
import javax.persistence.*

@AllOpen
@Entity
class Celeb : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "CELEB_SEQ_GEN",
            sequenceName = "CELEB_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "CELEB_SEQ_GEN")
    @Column(nullable = false)
    var id: Long? = null

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userId")
    var user: User? = null

    var hello: String? = null

    var instagramUrl: String? = null
    var facebookUrl: String? = null
    var lineUrl: String? = null
    var twitterUrl: String? = null

    var introductionTitle0: String? = null
    var introductionTitle1: String? = null
    var introductionTitle2: String? = null
    var introductionTitle3: String? = null
    var introductionDescription0: String? = null
    var introductionDescription1: String? = null
    var introductionDescription2: String? = null
    var introductionDescription3: String? = null
}