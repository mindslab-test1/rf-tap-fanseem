package ai.maum.kotlin.jpa.common.content

import org.springframework.data.domain.Sort
import org.springframework.data.repository.CrudRepository
import java.time.Instant

interface ContentRepository : CrudRepository<Content, Long> {
    fun findByIdAndWhoAndActiveIsTrue(id: Long, who: Long): Content?
    fun findByIdAndActiveIsTrue(id: Long): Content?
    fun findByWhoAndActiveIsTrue(who: Long): List<Content>
    fun findTopByWhoAndAccessLevelLessThanEqualAndActiveIsTrueOrderByCreatedDesc(who: Long, accessLevel: Long): Content?

    fun findTop1ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop2ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop3ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop4ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop5ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop6ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop7ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop8ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop9ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop10ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop11ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop12ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop13ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop14ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop15ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop16ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop17ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop18ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop19ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop20ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop21ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop22ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop23ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop24ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop25ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop26ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop27ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop28ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop29ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop30ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop1ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop2ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop3ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop4ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop5ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop6ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop7ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop8ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop9ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop10ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop11ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop12ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop13ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop14ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop15ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop16ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop17ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop18ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop19ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop20ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop21ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop22ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop23ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop24ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop25ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop26ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop27ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop28ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop29ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
    fun findTop30ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(who: Long, category: Long, created: Instant, sort: Sort): List<Content>?
}