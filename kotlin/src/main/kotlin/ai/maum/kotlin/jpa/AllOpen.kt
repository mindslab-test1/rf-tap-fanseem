package ai.maum.kotlin.jpa

@Target(AnnotationTarget.CLASS, AnnotationTarget.FUNCTION)
@Retention(AnnotationRetention.SOURCE)
annotation class AllOpen {
    // in pom.xml,
    // <option>all-open:annotation=ai.maum.kotlin.repository.meta.AllOpen</option>
}