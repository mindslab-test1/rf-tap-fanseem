package ai.maum.kotlin.jpa.common.ignore

import org.springframework.data.repository.CrudRepository

interface IgnoreRepository : CrudRepository<Ignore, Long> {
    fun existsByWhoAndWhomAndActiveIsTrue(who: Long, whom: Long) : Boolean
}