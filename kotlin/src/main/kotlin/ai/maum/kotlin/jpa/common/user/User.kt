package ai.maum.kotlin.jpa.common.user

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.celeb.Celeb
import ai.maum.kotlin.jpa.common.BaseEntity
import ai.maum.kotlin.jpa.common.interest.UserInterested
import javax.persistence.*

@AllOpen
@Entity
@Table(name = "\"USER\"")
class User : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "USER_SEQ_GEN",
            sequenceName = "USER_SEQ",
            initialValue = 1000,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "USER_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var name: String? = null

    @Column(nullable = false)
    var subscriberCount: Long? = 0

    var profileImageUrl: String? = null
    var bannerImageUrl: String? = null

    var gender: String? = null
    var age: Long? = null
    var birthday: String? = null

    var googleUserId: String? = null
    var kakaoUserId: Long? = null
    var naverUserId: String? = null
    var appleUserId: String? =null

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "celebId")
    var celeb: Celeb? = null

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "who")
    var interestedList : MutableList<UserInterested>? = null
}