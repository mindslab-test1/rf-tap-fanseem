package ai.maum.kotlin.jpa.common.report

import org.springframework.data.repository.CrudRepository

interface ReportRepository : CrudRepository<Report, Long> {

    fun findAllByWhomAndActiveIsTrue(whom: Long) : List<Report>?
    fun findAllByJudgeAndActiveIsTrue(who: Long) : List<Report>?
    fun countByWhomAndJudgeAndActiveIsTrue(whom: Long, judge: Long) : Long
}