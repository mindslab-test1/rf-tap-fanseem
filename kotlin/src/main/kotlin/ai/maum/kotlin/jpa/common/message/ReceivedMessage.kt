package ai.maum.kotlin.jpa.common.message

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import ai.maum.kotlin.jpa.common.service.Tts
import javax.persistence.*

@AllOpen
@Entity
class ReceivedMessage : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "RECEIVED_MESSAGE_SEQ_GEN",
            sequenceName = "RECEIVED_MESSAGE_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "RECEIVED_MESSAGE_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var whom: Long? = null

    @ManyToOne
    var what: Message? = null
}