package ai.maum.kotlin.jpa.authentication.naver

import org.springframework.data.repository.CrudRepository
import org.springframework.transaction.annotation.Transactional

interface NaverUserTokenRepository : CrudRepository<NaverUserToken, Long> {
    fun findByAccessToken(accessToken: String): List<NaverUserToken>?
    fun findByNaverUserId(naverUserId: String): List<NaverUserToken>?
    fun findByUserId(userId: Long): List<NaverUserToken>?
    fun findAllByUserId(userId: Long): List<NaverUserToken>?
    fun findAllByUserIdIn(userIds: List<Long>): List<NaverUserToken>?
    fun findByDeviceId(deviceId: String): List<NaverUserToken>?

    @Transactional
    fun deleteByDeviceId(deviceId: String): Long?
}