package ai.maum.kotlin.jpa.common.feed

import org.springframework.data.repository.CrudRepository

interface FeedCategoryRepository : CrudRepository<FeedCategory, Long> {
    fun findByIdAndActiveIsTrue(id: Long): FeedCategory?
    fun findByActiveIsTrueOrderByCreatedAsc(): List<FeedCategory>
}