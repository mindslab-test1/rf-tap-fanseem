package ai.maum.kotlin.jpa.common.like

import ai.maum.kotlin.jpa.AllOpen
import ai.maum.kotlin.jpa.common.BaseEntity
import javax.persistence.*

@AllOpen
@Entity
@Table(name = "\"LIKE\"")
class Like : BaseEntity() {
    @Id
    @SequenceGenerator(
            name = "LIKE_SEQ_GEN",
            sequenceName = "LIKE_SEQ",
            initialValue = 1,
            allocationSize = 1
    )
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "LIKE_SEQ_GEN")
    var id: Long? = null

    @Column(nullable = false)
    var userId: Long? = null

    @Column(nullable = false)
    var targetId: Long? = null

    @Column(nullable = false)
    var targetType: TargetType? = null

    enum class TargetType {
        NONE,
        USER_FEED,
        USER_FEED_COMMENT,
        CELEB_FEED,
        CELEB_FEED_COMMENT,
        CONTENT,
        CONTENT_COMMENT
    }
}