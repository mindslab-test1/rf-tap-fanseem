package ai.maum.kotlin.jpa.common.feed

import org.springframework.data.repository.CrudRepository

interface FeedFileRepository : CrudRepository<FeedFile, Long> {
    fun findByActiveIsTrue(): List<FeedFile>
}