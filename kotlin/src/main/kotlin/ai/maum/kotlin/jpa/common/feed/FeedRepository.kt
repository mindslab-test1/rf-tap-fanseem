package ai.maum.kotlin.jpa.common.feed

import org.springframework.data.domain.Sort
import org.springframework.data.repository.PagingAndSortingRepository
import java.time.Instant

interface FeedRepository : PagingAndSortingRepository<Feed, Long> {
    fun findByIdAndWhoAndActiveIsTrue(id: Long, who: Long) : Feed?
    fun findByIdAndActiveIsTrue(id: Long) : Feed?
    fun existsByIdAndActiveIsTrue(id: Long) : Boolean
    fun countByCelebAndActiveIsTrue(celeb: Long) : Long

    fun findTop1ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop2ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop3ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop4ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop5ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop6ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop7ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop8ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop9ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop10ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop11ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop12ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop13ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop14ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop15ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop16ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop17ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop18ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop19ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop20ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop21ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop22ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop23ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop24ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop25ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop26ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop27ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop28ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop29ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop30ByCelebAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop1ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop2ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop3ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop4ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop5ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop6ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop7ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop8ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop9ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop10ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop11ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop12ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop13ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop14ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop15ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop16ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop17ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop18ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop19ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop20ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop21ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop22ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop23ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop24ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop25ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop26ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop27ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop28ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop29ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
    fun findTop30ByCelebAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb: Long, category: Long, created: Instant, sort: Sort) : List<Feed>?
}