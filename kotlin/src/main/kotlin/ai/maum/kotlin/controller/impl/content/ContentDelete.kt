package ai.maum.kotlin.controller.impl.content

import ai.maum.kotlin.controller.ContentController
import ai.maum.kotlin.jpa.common.scrap.Scrap
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.content.ContentDeleteRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ContentDelete : HandlerType {
    var contentController: ContentController? = null
    var contentDeleteRequestDto: ContentDeleteRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = contentController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val contentRepository = fanmeet.contentRepository
        val scrapRepository = fanmeet.scrapRepository
        val dto = contentDeleteRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val content = contentRepository.findByIdAndActiveIsTrue(dto.content!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Content")

        content.active = false

        val scraps = scrapRepository.findByTargetIdAndTargetTypeAndActiveIsTrue(content.id!!, Scrap.TargetType.CONTENT)
        for (scrap in scraps) {
            scrap.active = false
        }

        contentRepository.save(content)
        scrapRepository.saveAll(scraps)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        contentController = controller as ContentController
    }

    override fun setDto(dto: Any) {
        contentDeleteRequestDto = dto as ContentDeleteRequestDto
    }
}