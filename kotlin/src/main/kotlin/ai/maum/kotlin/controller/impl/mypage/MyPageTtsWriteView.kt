package ai.maum.kotlin.controller.impl.mypage

import ai.maum.kotlin.controller.MyPageController
import ai.maum.kotlin.jpa.common.service.Tts
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.mypage.MyPageTtsWriteViewDto
import ai.maum.kotlin.model.http.dto.mypage.MyPageTtsWriteViewResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException
import java.time.Instant

class MyPageTtsWriteView : HandlerType {
    var myPageController: MyPageController? = null
    var myPageTtsWriteViewDto: MyPageTtsWriteViewDto? = null
    override fun setController(controller: Any) {
        myPageController = controller as MyPageController
    }

    override fun setDto(dto: Any) {
        myPageTtsWriteViewDto = dto as MyPageTtsWriteViewDto
    }

    override fun invoke(): ResponseType {
        val fanmeet = myPageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "fanmeet")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "userId")
        val celebId = myPageTtsWriteViewDto?.celebId
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "celebId")
        val ttsRepository = fanmeet.ttsRepository
        val membershipRepository = fanmeet.membershipRepository
        val membershipHistoryRepository = fanmeet.userMembershipHistoryRepository
        val userMembershipStatus = membershipHistoryRepository.findTopByUserIdAndMembershipCelebAndActiveIsTrueOrderByExpirationDateDesc(userId, celebId)
                ?: throw ResponseStatusException(HttpStatus.FORBIDDEN, "cannot request tts")
        if (userMembershipStatus.expirationDate!! < Instant.now())
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "cannot request tts")
        val membershipInfo = membershipRepository.findByIdAndActiveIsTrue(userMembershipStatus.membership!!.id!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "no membership!")
        if (!membershipInfo.ttsWrite!!)
            throw ResponseStatusException(HttpStatus.FORBIDDEN, "membership insufficient")
        val statusList = mutableListOf<Tts.Status>(Tts.Status.SAMPLE, Tts.Status.REJECTED)
        val monthlyUsed = ttsRepository.findBySubscriberAndCelebAndStatusIsNotInAndActiveIsTrue(
                userId, celebId, statusList
        )
        var monthlyUsedSum: Long = 0
        monthlyUsed.forEach {
            monthlyUsedSum += it.text!!.length
        }
        val responseBody = MyPageTtsWriteViewResponse()
        responseBody.requestLimit = 300L
        responseBody.monthlyRate = membershipInfo.ttsWriteLimit
        responseBody.monthlyUsed = monthlyUsedSum
        return ResponseEntity.ok(responseBody)
    }
}