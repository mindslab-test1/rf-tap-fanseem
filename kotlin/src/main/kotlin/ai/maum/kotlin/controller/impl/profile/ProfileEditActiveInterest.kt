package ai.maum.kotlin.controller.impl.profile

import ai.maum.kotlin.controller.ProfileController
import ai.maum.kotlin.jpa.common.interest.ActiveInterest
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.profile.ProfileEditActiveInterestRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ProfileEditActiveInterest : HandlerType {
    var profileController: ProfileController? = null
    var profileEditActiveInterestRequestDto: ProfileEditActiveInterestRequestDto? = null

    override fun invoke(): ResponseType {
        val fanmeet = profileController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val activeInterestRepository = fanmeet.activeInterestRepository
        val interestRepository = fanmeet.interestRepository
        val dto = profileEditActiveInterestRequestDto
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val allActiveInterest = activeInterestRepository.findAllByWhoAndActiveIsTrue(userId)
        val removeList = allActiveInterest.toMutableList()
        removeList.removeAll { dto.interestList!!.contains(it.what) }

        val interestItemList = mutableListOf<ActiveInterest>()
        for (interestId in dto.interestList!!) {
            val interest = interestRepository.findByIdAndActiveIsTrue(interestId)
                    ?: continue

            val interestItem = activeInterestRepository.findByWhoAndWhat(userId, interestId)
            if (interestItem == null) {
                val activeInterest = ActiveInterest()
                activeInterest.who = userId
                activeInterest.what = interestId

                interestItemList.add(activeInterest)
            } else if (interestItem.active!!) { // Active is true
                continue
            } else { // Active is false
                interestItem.active = true
                interestItemList.add(interestItem)
            }
        }
        for (removeTarget in removeList) {
            removeTarget.active = false
            interestItemList.add(removeTarget)
        }

        activeInterestRepository.saveAll(interestItemList)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        profileController = controller as ProfileController
    }

    override fun setDto(dto: Any) {
        profileEditActiveInterestRequestDto = dto as ProfileEditActiveInterestRequestDto
    }
}