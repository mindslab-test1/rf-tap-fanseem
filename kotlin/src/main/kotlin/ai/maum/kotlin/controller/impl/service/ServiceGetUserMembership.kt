package ai.maum.kotlin.controller.impl.service

import ai.maum.kotlin.controller.ServiceController
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.service.ServiceGetUserMembershipRequestDto
import ai.maum.kotlin.model.http.dto.service.ServiceGetUserMembershipResponseDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ServiceGetUserMembership : HandlerType {
    var serviceController: ServiceController? = null
    var serviceGetUserMembershipRequestDto: ServiceGetUserMembershipRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = serviceController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val repository = fanmeet.userMembershipHistoryRepository
        val dto = serviceGetUserMembershipRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        dto.celeb ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "celeb")

        val history = repository.findTopByUserIdAndMembershipCelebAndActiveIsTrueOrderByExpirationDateDesc(userId, dto.celeb!!)
                ?: return ResponseEntity.ok(Unit)
        if (history.status != UserMembershipHistory.Status.NORMAL && history.status != UserMembershipHistory.Status.TO_BE_EXPIRED)
            return ResponseEntity.ok(Unit)

        val list = emptyList<String>().toMutableList()
        if (history.membership?.badge!!) list.add("멤버십 전용 뱃지")
        if (history.membership?.sticker!!) list.add("전용 스티커")
        if (history.membership?.voiceLetter!!) list.add("TTS 보이스레터 수신")
        if (history.membership?.ttsWrite!!) list.add("TTS 직접 생성 ${history.membership?.ttsWriteLimit}자")

        return ResponseEntity.ok(ServiceGetUserMembershipResponseDto(
                tier = history.membership?.tier,
                name = history.membership?.name,
                badgeImageUrl = history.membership?.badgeImageUrl?.mediaUrl(),
                badge = history.membership?.badge,
                sticker = history.membership?.sticker,
                voiceLetter = history.membership?.voiceLetter,
                ttsWrite = history.membership?.ttsWrite,
                ttsWriteLimit = history.membership?.ttsWriteLimit,
                benefitDescriptionList = list
        ))
    }

    override fun setController(controller: Any) {
        serviceController = controller as ServiceController
    }

    override fun setDto(dto: Any) {
        serviceGetUserMembershipRequestDto = dto as ServiceGetUserMembershipRequestDto
    }
}