package ai.maum.kotlin.controller.impl.content

import ai.maum.kotlin.controller.ContentController
import ai.maum.kotlin.jpa.common.block.Block
import ai.maum.kotlin.jpa.common.comment.Comment
import ai.maum.kotlin.jpa.common.like.Like
import ai.maum.kotlin.jpa.common.scrap.Scrap
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.content.ContentCommentItemDto
import ai.maum.kotlin.model.http.dto.content.ContentItemDto
import ai.maum.kotlin.model.http.dto.content.ContentViewRequestDto
import ai.maum.kotlin.model.http.dto.content.ContentViewResponseDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ContentView : HandlerType {
    var contentController: ContentController? = null
    var contentViewRequestDto: ContentViewRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = contentController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val contentRepository = fanmeet.contentRepository
        val contentCategoryRepository = fanmeet.contentCategoryRepository
        val userRepository = fanmeet.userRepository
        val blockRepository = fanmeet.blockRepository
        val likeRepository = fanmeet.likeRepository
        val scrapRepository = fanmeet.scrapRepository
        val commentRepository = fanmeet.commentRepository

        val dto = contentViewRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "UserId")

        val content = contentRepository.findByIdAndActiveIsTrue(dto.content!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Content")

        var blocks: List<Block> = listOf()
        // Check if the user blocked feed writer
        blocks = blockRepository.findByWhoAndActiveIsTrue(userId)
        for (block in blocks)
            if (block.whom == content.who)
                throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Block") // User blocked feed writer

        // View count increment
        content.viewCount = content.viewCount?.let { it + 1 } ?: 1
        contentRepository.save(content)

        /* Filling response body */
        /* Feed info */
        val responseBody = ContentViewResponseDto()
        val contentItem = ContentItemDto()

        contentItem.title = content.title

        contentItem.blocked = false
        contentItem.content = content.id
        contentItem.feedOwner = content.who

        // Feed writer info
        val feedOwner = userRepository.findByIdAndActiveIsTrue(content.who!!)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "FeedOwner")
        contentItem.feedOwnerName = feedOwner.name
        contentItem.feedOwnerProfileImage = feedOwner.profileImageUrl?.mediaUrl()

        val accessLevel = content.accessLevel ?: 0
        contentItem.accessLevelBadgeImageUrl = fanmeet.membershipRepository.findByCelebAndTierAndActiveIsTrue(content.who!!, accessLevel)?.badgeImageUrl?.mediaUrl()

        // Category info
        val category = contentCategoryRepository.findByIdAndActiveIsTrue(content.category!!)
        contentItem.category = content.category!!
        contentItem.categoryName = category?.name // nullable

        // Like/scrap info
        contentItem.viewCount = content.viewCount
        contentItem.likeCount = content.likeCount
        contentItem.scrapCount = content.scrapCount
        contentItem.commentCount = content.commentCount
        contentItem.liked = likeRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId, content.id!!, Like.TargetType.CONTENT)
        contentItem.scraped = scrapRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId, content.id!!, Scrap.TargetType.CONTENT)

        // Media info
        contentItem.pictures = mutableListOf()
        content.pictureList?.let {
            for (picture in content.pictureList!!) {
                contentItem.pictures!!.add(picture.url!!.mediaUrl())
            }
        }
        contentItem.video = content.video?.url?.mediaUrl()
        contentItem.thumbnail = content.thumbnail?.url?.mediaUrl()
        contentItem.tts = content.tts?.url?.mediaUrl()
        contentItem.youtube = content.youtube

        // Others
        contentItem.text = content.text
        contentItem.updated = content.created

        /* Comment info */
        // Active may not be true since nested comments can be active and should be shown
        val comments = commentRepository.findByFeedAndSectionOrderByCreatedDesc(content.id!!, Comment.Section.CONTENT)
                ?: listOf()
        val commentItemList = mutableListOf<ContentCommentItemDto>()
        val offspringItemList = mutableMapOf<Long, MutableList<ContentCommentItemDto>>()
        val userMembershipHistoryRepository = fanmeet.userMembershipHistoryRepository
        for (comment in comments) {
            val commentItem = ContentCommentItemDto()

            commentItem.id = comment.id

            commentItem.deleted = false
            commentItem.blocked = false

            val commentOwner = userRepository.findByIdAndActiveIsTrue(comment.who!!)

            if (comment.active == false || commentOwner == null) {
                commentItem.deleted = true
                continue
            }
            val block = blocks.find { it.whom == comment.who }
            if (block != null) {
                commentItem.blocked = true
                continue
            }

            commentItem.commentOwner = comment.who!!
            commentItem.commentOwnerName = commentOwner.name
            commentItem.commentOwnerProfileImage = commentOwner.profileImageUrl?.mediaUrl()
            commentItem.badgeUrl = userMembershipHistoryRepository.findTopByUserIdAndMembershipCelebAndStatusIsNotAndActiveIsTrueOrderByExpirationDate(
                    commentOwner.id!!,
                    content.who!!,
                    UserMembershipHistory.Status.EXPIRED
            )?.membership?.badgeImageUrl?.mediaUrl()
            commentItem.text = comment.text
            commentItem.likeCount = comment.likeCount
            commentItem.picture = comment.picture?.mediaUrl()
            commentItem.tts = comment.tts?.url?.mediaUrl()
            commentItem.updated = comment.created
            commentItem.parent = comment.parent
            commentItem.offspring = mutableListOf()
            commentItem.liked = likeRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId, comment.id!!, Like.TargetType.CONTENT_COMMENT)

            if (commentItem.parent == null)
                commentItemList.add(commentItem)
            else {
                if (!offspringItemList.contains(commentItem.parent!!))
                    offspringItemList[commentItem.parent!!] = mutableListOf()
                offspringItemList[commentItem.parent!!]!!.add(commentItem)
            }
        }

        for (commentItem in commentItemList)
            if (offspringItemList.contains(commentItem.id!!))
                commentItem.offspring!!.addAll(offspringItemList[commentItem.id!!]!!).also { commentItem.offspring!!.sortBy{ it.updated }}

        responseBody.feedItem = contentItem
        responseBody.comments = commentItemList

        return ResponseEntity.ok(responseBody)
    }

    override fun setController(controller: Any) {
        contentController = controller as ContentController
    }

    override fun setDto(dto: Any) {
        contentViewRequestDto = dto as ContentViewRequestDto
    }
}