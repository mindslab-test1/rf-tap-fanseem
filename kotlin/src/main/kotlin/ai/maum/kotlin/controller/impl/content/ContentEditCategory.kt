package ai.maum.kotlin.controller.impl.content

import ai.maum.kotlin.controller.ContentController
import ai.maum.kotlin.jpa.common.content.Content
import ai.maum.kotlin.jpa.common.content.ContentCategory
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.content.ContentEditCategoryRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ContentEditCategory : HandlerType {
    var contentController: ContentController? = null
    var contentEditCategoryRequestDto: ContentEditCategoryRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = contentController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val contentCategoryRepository = fanmeet.contentCategoryRepository
        val contentRepository = fanmeet.contentRepository
        val dto = contentEditCategoryRequestDto
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val contentCategoryList = contentCategoryRepository.findByCelebAndActiveIsTrueOrderByCreatedDesc(userId).toMutableList()
        val removedCategoryIds = mutableListOf<Long>()

        dto.removed?.let {
            if (it.isEmpty())
                return@let

            for ((i, removeIndex) in it.sortedDescending().withIndex()) {
                if (contentCategoryList[i].name == "기타")
                    throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Deletion of category `기타` is prohibited")
                contentCategoryList[i].active = false
                removedCategoryIds.add(contentCategoryList[i].id!!)
            }
        }
        val categoryNameList = contentCategoryList.map { it.name }
        dto.added?.let {
            if (it.isEmpty())
                return@let

            for (name in dto.added!!) {
                if (categoryNameList.contains(name))
                    throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Duplicate Category Name")

                val added = ContentCategory()
                added.celeb = userId
                added.name = name

                contentCategoryList.add(added)
            }
        }

        // Set categories of articles with removed categories to "기타"
        val etc = contentCategoryList.find { it.name == "기타" }?.id
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Category `기타` does not exist")
        val contents = contentRepository.findByWhoAndActiveIsTrue(userId)
        val changedContents = mutableListOf<Content>()
        for (content in contents) {
            if (!removedCategoryIds.contains(content.category!!))
                continue

            content.category = etc
            changedContents.add(content)
        }

        contentRepository.saveAll(changedContents)
        contentCategoryRepository.saveAll(contentCategoryList)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        contentController = controller as ContentController
    }

    override fun setDto(dto: Any) {
        contentEditCategoryRequestDto = dto as ContentEditCategoryRequestDto
    }
}