package ai.maum.kotlin.controller.impl.setting

import ai.maum.kotlin.controller.SettingController
import ai.maum.kotlin.jpa.common.setting.Setting
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.setting.SettingSaveFlagsRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class SettingSaveFlags : HandlerType {
    var settingController: SettingController? = null
    var settingSaveFlagsRequestDto: SettingSaveFlagsRequestDto? = null

    override fun invoke(): ResponseType {
        val fanmeet = settingController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val settingRepository = fanmeet.settingRepository
        val dto = settingSaveFlagsRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val setting = settingRepository.findByUserIdAndActiveIsTrue(userId)
                ?: Setting()
        if (setting.id == null) {
            // 기존에 없는 경우 새로 생성
            setting.sound = true
            setting.vibration = true
            setting.myLike = true
            setting.myScrap = true
            setting.myComment = true
            setting.celebFeed = true
            setting.celebContent = true
            setting.celebMessage = true
            setting.celebActivity = true
            setting.ttsRequest = true
            setting.ttsResult = true
        }

        dto.sound?.let { setting.sound = dto.sound }
        dto.vibration?.let { setting.vibration = dto.vibration }
        dto.myLike?.let { setting.myLike = dto.myLike }
        dto.myScrap?.let { setting.myScrap = dto.myScrap }
        dto.myComment?.let { setting.myComment = dto.myComment }
        dto.celebFeed?.let { setting.celebFeed = dto.celebFeed }
        dto.celebContent?.let { setting.celebContent = dto.celebContent }
        dto.celebMessage?.let { setting.celebMessage = dto.celebMessage }
        dto.celebActivity?.let { setting.celebActivity = dto.celebActivity }

        settingRepository.save(setting)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        settingController = controller as SettingController
    }

    override fun setDto(dto: Any) {
        settingSaveFlagsRequestDto = dto as SettingSaveFlagsRequestDto
    }
}