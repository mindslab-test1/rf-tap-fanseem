package ai.maum.kotlin.controller.impl.feed

import ai.maum.kotlin.controller.FeedController
import ai.maum.kotlin.jpa.common.scrap.Scrap
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.feed.FeedScrapRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class FeedScrap : HandlerType {
    var feedController: FeedController? = null
    var feedScrapRequestDto: FeedScrapRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = feedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val feedRepository = fanmeet.feedRepository
        val scrapRepository = fanmeet.scrapRepository
        val scrapBoardRepository = fanmeet.scrapBoardRepository
        val dto = feedScrapRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val feed = dto.feed!!
        val celeb = feedRepository.findByIdAndActiveIsTrue(feed)?.celeb
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "not active")
        val scrapBoard = dto.scrapBoard!!

        if (!scrapBoardRepository.existsByIdAndActiveIsTrue(scrapBoard)!!)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "ScrapBoardRepository !Exists!!")

        if (feedRepository.findByIdAndActiveIsTrue(feed)!!.celeb != celeb)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "FeedRepository Find!!Celeb")

        val feedEntity = feedRepository.findByIdAndActiveIsTrue(feed)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "FeedEntity")
        feedEntity.scrapCount = feedEntity.scrapCount!! + 1

        val scrap = Scrap()
        scrap.userId = userId
        scrap.celeb = celeb
        scrap.board = dto.scrapBoard
        scrap.targetId = feed
        scrap.targetType = Scrap.TargetType.USER_FEED

        feedRepository.save(feedEntity)
        val saved = scrapRepository.save(scrap)
        fanmeet.authorization.scrap(saved.id!!)
        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        feedController = controller as FeedController
    }

    override fun setDto(dto: Any) {
        feedScrapRequestDto = dto as FeedScrapRequestDto
    }
}