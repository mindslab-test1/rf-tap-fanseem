package ai.maum.kotlin.controller.impl.celebmypage

import ai.maum.kotlin.controller.CelebMyPageController
import ai.maum.kotlin.jpa.common.service.Tts
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.celebmypage.CelebMyPageTtsDeclineRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class CelebMyPageTtsDecline : HandlerType {
    var celebMyPageController: CelebMyPageController? = null
    var celebMyPageTtsDeclineRequestDto: CelebMyPageTtsDeclineRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = celebMyPageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val ttsRepository = fanmeet.ttsRepository
        val dto = celebMyPageTtsDeclineRequestDto
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val tts = ttsRepository.findByIdAndActiveIsTrue(dto.tts!!)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Tts")
        if (tts.celeb != userId)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Tts Celeb")
        if (tts.status != Tts.Status.APPEALED)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Tts Status")

        tts.status = Tts.Status.REJECTED
        ttsRepository.save(tts)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        celebMyPageController = controller as CelebMyPageController
    }

    override fun setDto(dto: Any) {
        celebMyPageTtsDeclineRequestDto = dto as CelebMyPageTtsDeclineRequestDto
    }
}