package ai.maum.kotlin.controller.impl.community

import ai.maum.kotlin.controller.CommunityController
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.community.CommunityCelebMembershipListItemDto
import ai.maum.kotlin.model.http.dto.community.CommunityCelebMembershipListRequestDto
import ai.maum.kotlin.model.http.dto.community.CommunityCelebMembershipListResponseDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class CommunityCelebMembershipList : HandlerType {
    var communityController: CommunityController? = null
    var communityCelebMembershipListRequestDto: CommunityCelebMembershipListRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = communityController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "userId")
        val dto = communityCelebMembershipListRequestDto ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto")

        val responseBody = CommunityCelebMembershipListResponseDto()
        responseBody.tier = fanmeet.userMembershipHistoryRepository.findTopByUserIdAndMembershipCelebAndStatusIsNotAndActiveIsTrueOrderByExpirationDate(
                userId, dto.celeb!!, UserMembershipHistory.Status.EXPIRED
        )?.membership?.tier
                ?: 0
        responseBody.membershipList = fanmeet.membershipRepository.findByCelebAndActiveIsTrue(dto.celeb!!).map {
            val item = CommunityCelebMembershipListItemDto()
            item.id = it.id
            item.name = it.name
            item.tier = it.tier
            item.productId = it.productId
            item.price = it.price
            item.badgeImageUrl = it.badgeImageUrl?.mediaUrl()
            return@map item
        }

        return ResponseEntity.ok(responseBody)
    }

    override fun setController(controller: Any) {
        communityController = controller as CommunityController
    }

    override fun setDto(dto: Any) {
        communityCelebMembershipListRequestDto = dto as CommunityCelebMembershipListRequestDto
    }
}