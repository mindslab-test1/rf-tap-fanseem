package ai.maum.kotlin.controller.impl.auth

import ai.maum.kotlin.controller.AuthController
import ai.maum.kotlin.jpa.common.interest.UserInterested
import ai.maum.kotlin.jpa.common.setting.Setting
import ai.maum.kotlin.jpa.common.subscribe.Subscribe
import ai.maum.kotlin.jpa.common.user.User
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.auth.NaverLoginValidationDto
import ai.maum.kotlin.model.http.dto.auth.NaverRegisterDto
import org.springframework.http.*
import org.springframework.web.server.ResponseStatusException

class NaverRegister(private val authController: AuthController, private val naverRegisterDto: NaverRegisterDto) : HandlerType {
    override operator fun invoke(): ResponseType {
        val restTemplate = authController.restTemplate

        val headers = HttpHeaders()
        headers.set("Authorization", "Bearer ${naverRegisterDto.accessToken}")
        headers.set("Content-type", "application/x-www-form-urlencoded;charset=utf-8")

        val entity = HttpEntity<String>(headers)
        // https://openapi.naver.com/v1/nid/verify를 쓰는 방법도 있는데, response 필드명에 슬래시가 있어서 DTO로 하기 껄끄럽다
        val response = restTemplate.exchange("https://openapi.naver.com/v1/nid/me", HttpMethod.GET, entity, NaverLoginValidationDto::class.java)

        if (response.statusCode != HttpStatus.OK)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST)

        val validationResult = response.body ?: throw ResponseStatusException(HttpStatus.EXPECTATION_FAILED)

        val regex = Regex("^[가-힣a-zA-Z0-9@!\\-_*]*$")
        if (!regex.matches(naverRegisterDto.name!!)) throw ResponseStatusException(HttpStatus.FORBIDDEN)

        if (naverRegisterDto.name!!.length >= 15) throw ResponseStatusException(HttpStatus.NOT_ACCEPTABLE)
        naverRegisterDto.hello?.let { if (it.length > 100) throw ResponseStatusException(HttpStatus.I_AM_A_TEAPOT) }

        // Check existence in DB
        var user = authController.userRepository.findByNaverUserIdAndActiveIsTrue(validationResult.response?.id
                ?: throw ResponseStatusException(HttpStatus.EXPECTATION_FAILED))
        user?.let { throw ResponseStatusException(HttpStatus.CONFLICT) }

        // Check name duplication
        user = authController.userRepository.findUserByNameAndActiveIsTrue(naverRegisterDto.name!!)
        user?.let { throw ResponseStatusException(HttpStatus.IM_USED) }

        // Add user
        val register = User()

        naverRegisterDto.profileImage?.let { register.profileImageUrl = authController.mediaUploader.uploadAsyncProfileImage(naverRegisterDto.profileImage!!) }
        naverRegisterDto.bannerImage?.let { register.bannerImageUrl = authController.mediaUploader.uploadAsyncBannerImage(naverRegisterDto.bannerImage!!) }

        register.naverUserId = validationResult.response!!.id!!
        register.name = naverRegisterDto.name!!
        register.interestedList = mutableListOf()

        val allInterestIds = authController.interestRepository.findAllByActiveIsTrue().map { it.id!! }
        if (naverRegisterDto.interests != null && naverRegisterDto.interests != "[]") {
            var interests = naverRegisterDto.interests!!.substring(1, naverRegisterDto.interests!!.length-1).split(", ").map { it.toLong() }.toList()
            for (interestId in naverRegisterDto.interests!!) {
                val interest = UserInterested()
                interest.what = interestId.toLong()

                if (!allInterestIds.contains(interestId.toLong()))
                    continue

                register.interestedList!!.add(interest)
            }
        }

        val savedUser = authController.userRepository.save(register)

        val setting = Setting()
        setting.userId = savedUser.id
        setting.sound = true
        setting.vibration = true
        setting.myLike = true
        setting.myScrap = true
        setting.myComment = true
        setting.celebFeed = true
        setting.celebContent = true
        setting.celebMessage = true
        setting.celebActivity = true
        setting.ttsRequest = true
        setting.ttsResult = true

        val subscribe = Subscribe()
        subscribe.who = savedUser.id
        subscribe.whom = 2

        authController.settingRepository.save(setting)
        authController.subscribeRepository.save(subscribe)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        // Do nothing
    }

    override fun setDto(dto: Any) {
        // Do nothing
    }
}