package ai.maum.kotlin.controller.impl.admin

import ai.maum.kotlin.controller.AdminController
import ai.maum.kotlin.jpa.common.subscribe.Subscribe
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.admin.SubscribeFanmeetRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class SubscribeFanmeet : HandlerType {
    var adminController: AdminController? = null
    var subscribeFanmeetRequestDto: SubscribeFanmeetRequestDto? = null

    override fun invoke(): ResponseType {
        val fanmeet = adminController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val userRepository = fanmeet.userRepository
        val subscribeRepository = fanmeet.subscribeRepository
        val dto = subscribeFanmeetRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        if (dto.secret != "gocldnjTsk")
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Secret does not match")

        val users = userRepository.findByActiveIsTrue()
        for (user in users) {
            val subs = subscribeRepository.findAllByWho(user.id ?: continue)
            subs?.map{it.whom}?.contains(2)?.let{
                if (it)
                    return@let

                val subscribe = Subscribe()
                subscribe.who = user.id
                subscribe.whom = 2
                subscribeRepository.save(subscribe)
            }
        }

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        adminController = controller as AdminController
    }

    override fun setDto(dto: Any) {
        subscribeFanmeetRequestDto = dto as SubscribeFanmeetRequestDto
    }
}