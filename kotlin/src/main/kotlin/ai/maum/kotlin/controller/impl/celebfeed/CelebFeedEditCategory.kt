package ai.maum.kotlin.controller.impl.celebfeed

import ai.maum.kotlin.controller.CelebFeedController
import ai.maum.kotlin.jpa.common.celebfeed.CelebFeed
import ai.maum.kotlin.jpa.common.celebfeed.CelebFeedCategory
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.celebfeed.CelebFeedEditCategoryRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class CelebFeedEditCategory : HandlerType {
    var celebFeedController: CelebFeedController? = null
    var celebFeedEditCategoryRequestDto: CelebFeedEditCategoryRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = celebFeedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val celebFeedCategoryRepository = fanmeet.celebFeedCategoryRepository
        val celebFeedRepository = fanmeet.celebFeedRepository
        val dto = celebFeedEditCategoryRequestDto
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val celebCategoryList = celebFeedCategoryRepository.findByCelebAndActiveIsTrueOrderByCreatedAsc(userId).toMutableList()
        val removedCategoryIds = mutableListOf<Long>()

        dto.removed?.let {
            if (it.isEmpty())
                return@let

            for ((i, removeIndex) in it.sortedDescending().withIndex()) {
                if (celebCategoryList[i].name == "기타")
                    throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Deletion of category `기타` is prohibited")
                celebCategoryList[i].active = false
                removedCategoryIds.add(celebCategoryList[i].id!!)
            }
        }
        val categoryNameList = celebCategoryList.map { it.name }
        dto.added?.let {
            if (it.isEmpty())
                return@let

            for (name in dto.added!!) {
                if (categoryNameList.contains(name))
                    throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Duplicate Category Name")

                val added = CelebFeedCategory()
                added.celeb = userId
                added.name = name

                celebCategoryList.add(added)
            }
        }

        // Set categories of articles with removed categories to "기타"
        val etc = celebCategoryList.find { it.name == "기타" }?.id
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Category `기타` does not exist")
        val celebFeeds = celebFeedRepository.findByWhoAndActiveIsTrue(userId)
        val changedCelebFeeds = mutableListOf<CelebFeed>()
        for (celebFeed in celebFeeds) {
            if (!removedCategoryIds.contains(celebFeed.category!!))
                continue

            celebFeed.category = etc
            changedCelebFeeds.add(celebFeed)
        }

        celebFeedRepository.saveAll(changedCelebFeeds)
        celebFeedCategoryRepository.saveAll(celebCategoryList)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        celebFeedController = controller as CelebFeedController
    }

    override fun setDto(dto: Any) {
        celebFeedEditCategoryRequestDto = dto as CelebFeedEditCategoryRequestDto
    }
}