package ai.maum.kotlin.controller.impl.feed

import ai.maum.kotlin.controller.FeedController
import ai.maum.kotlin.jpa.common.scrap.Scrap
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.feed.FeedDeleteRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class FeedDelete : HandlerType {
    var feedController: FeedController? = null
    var feedDeleteRequestDto: FeedDeleteRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = feedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val feedRepository = fanmeet.feedRepository
        val scrapRepository = fanmeet.scrapRepository
        val dto = feedDeleteRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val feed = feedRepository.findByIdAndActiveIsTrue(dto.feed!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Feed")

        feed.active = false

        val scraps = scrapRepository.findByTargetIdAndTargetTypeAndActiveIsTrue(feed.id!!, Scrap.TargetType.USER_FEED)
        for (scrap in scraps) {
            scrap.active = false
        }

        feedRepository.save(feed)
        scrapRepository.saveAll(scraps)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        feedController = controller as FeedController
    }

    override fun setDto(dto: Any) {
        feedDeleteRequestDto = dto as FeedDeleteRequestDto
    }
}