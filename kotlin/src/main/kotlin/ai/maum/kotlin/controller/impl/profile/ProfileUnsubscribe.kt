package ai.maum.kotlin.controller.impl.profile

import ai.maum.kotlin.controller.ProfileController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.profile.ProfileUnsubscribeRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ProfileUnsubscribe : HandlerType {
    var profileController: ProfileController? = null
    var profileUnsubscribeRequestDto: ProfileUnsubscribeRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = profileController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val subscribeRepository = fanmeet.subscribeRepository
        val userRepository = fanmeet.userRepository
        val dto = profileUnsubscribeRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")
        val target = userRepository.findByIdAndActiveIsTrue(dto.whom!!)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Target")

        val subscribe = subscribeRepository.findByWhoAndWhomAndActiveIsTrue(userId, dto.whom!!) ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "No Subscription")
        subscribe.active = false

        target.subscriberCount = target.subscriberCount?.let { return@let target.subscriberCount!! - 1 }
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "SubscriberCount Null")

        subscribeRepository.save(subscribe)
        userRepository.save(target)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        profileController = controller as ProfileController
    }

    override fun setDto(dto: Any) {
        profileUnsubscribeRequestDto = dto as ProfileUnsubscribeRequestDto
    }
}
