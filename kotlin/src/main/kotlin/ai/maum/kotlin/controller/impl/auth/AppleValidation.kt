package ai.maum.kotlin.controller.impl.auth

import ai.maum.kotlin.controller.AuthController
import ai.maum.kotlin.model.http.dto.auth.AppleIdTokenPayload
import ai.maum.kotlin.model.http.dto.auth.AppleTokenResponseDto
import com.google.gson.Gson
import io.jsonwebtoken.JwsHeader
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.SignatureAlgorithm
import io.jsonwebtoken.io.Decoders
import org.bouncycastle.asn1.pkcs.PrivateKeyInfo
import org.bouncycastle.openssl.PEMParser
import org.bouncycastle.openssl.jcajce.JcaPEMKeyConverter
import org.springframework.core.io.Resource
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.util.LinkedMultiValueMap
import org.springframework.util.MultiValueMap
import org.springframework.web.server.ResponseStatusException
import java.io.File
import java.io.FileReader
import java.security.PrivateKey
import java.util.*

class AppleValidation() {
    private val APPLE_AUTH_URL = "https://appleid.apple.com/auth/token"
    private val APPLE_CLIENT_ID = "ios.ai.maum.fanmeet"
    private val APPLE_KEY_ID = "V77W54M5G2"
    private val APPLE_TEAM_ID = "7A6BG8T7P2"

    fun getTokenPayload(controller: AuthController, authorizationCode: String?, appleKeyFile: Resource): AppleTokenResponseDto {
        val restTemplate = controller.restTemplate
        val token = generateJWT(appleKeyFile);

        val header = HttpHeaders()
        header.set("Content-type", "application/x-www-form-urlencoded")
        val map: MultiValueMap<String, String> = LinkedMultiValueMap()
        map.add("client_id", APPLE_CLIENT_ID)
        map.add("client_secret", token)
        map.add("grant_type", "authorization_code")
        map.add("code", authorizationCode)

        val request = HttpEntity(map, header)
        val response = restTemplate.exchange(APPLE_AUTH_URL, HttpMethod.POST, request, AppleTokenResponseDto::class.java)
        if (response.statusCode != HttpStatus.OK)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST)
        return response.body ?: throw ResponseStatusException(HttpStatus.EXPECTATION_FAILED)
    }

    fun decodeResponseAppleToken(jsonData: AppleTokenResponseDto): AppleIdTokenPayload? {
        val idToken = jsonData.id_token!!
        val payload = idToken.split(".")[1]
        val decoded = String(Decoders.BASE64.decode(payload))
        return Gson().fromJson(decoded, AppleIdTokenPayload::class.java)
    }


    @Throws(Exception::class)
    private fun generateJWT(appleKeyFile: Resource): String {
        val pKey = generatePrivateKey(appleKeyFile)
        return Jwts.builder()
                .setHeaderParam(JwsHeader.KEY_ID, APPLE_KEY_ID)
                .setHeaderParam("alg", "ES256")
                .setIssuer(APPLE_TEAM_ID)
                .setAudience("https://appleid.apple.com")
                .setSubject(APPLE_CLIENT_ID)
                .setExpiration(Date(System.currentTimeMillis() + 1000 * 60 * 5))
                .setIssuedAt(Date(System.currentTimeMillis()))
                .signWith(SignatureAlgorithm.ES256, pKey)
                .compact()
    }

    @Throws(java.lang.Exception::class)
    private fun generatePrivateKey(appleKeyFile: Resource): PrivateKey? {
        val file = File("AuthKey_V77W54M5G2.p8")
        val pemParser = PEMParser(FileReader(file))
        val converter = JcaPEMKeyConverter()
        val `object` = pemParser.readObject() as PrivateKeyInfo
        return converter.getPrivateKey(`object`)
    }
}