package ai.maum.kotlin.controller.impl.service

import ai.maum.kotlin.controller.ServiceController
import ai.maum.kotlin.model.http.dto.service.ServiceNewCountRequestDto
import org.springframework.http.ResponseEntity

class ServiceNewsCount(serviceController: ServiceController, serviceNewsCountRequestDto: ServiceNewCountRequestDto): () -> ResponseEntity<Unit> {
    override operator fun invoke(): ResponseEntity<Unit> {
        return ResponseEntity.ok(Unit)
    }
}