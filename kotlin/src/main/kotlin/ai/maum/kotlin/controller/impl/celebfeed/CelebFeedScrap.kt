package ai.maum.kotlin.controller.impl.celebfeed

import ai.maum.kotlin.controller.CelebFeedController
import ai.maum.kotlin.jpa.common.scrap.Scrap
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.celebfeed.CelebFeedScrapRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class CelebFeedScrap : HandlerType {
    var celebFeedController: CelebFeedController? = null
    var celebFeedScrapRequestDto: CelebFeedScrapRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = celebFeedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val celebFeedRepository = fanmeet.celebFeedRepository
        val scrapRepository = fanmeet.scrapRepository
        val scrapBoardRepository = fanmeet.scrapBoardRepository
        val dto = celebFeedScrapRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val celebFeed = dto.celebFeed!!
        val scrapBoard = dto.scrapBoard!!

        val celebFeedEntity = celebFeedRepository.findByIdAndActiveIsTrue(celebFeed)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "CelebFeedEntity")

        val celeb = celebFeedEntity.who!!

        if (!scrapBoardRepository.existsByIdAndActiveIsTrue(scrapBoard)!!)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "ScrapBoardRepository !Exists!!")

        if (celebFeedRepository.findByIdAndActiveIsTrue(celebFeed)!!.who != celeb)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "CelebFeedRepository Find!!Celeb")

        celebFeedEntity.scrapCount = celebFeedEntity.scrapCount!! + 1

        val scrap = Scrap()
        scrap.userId = userId
        scrap.celeb = celeb
        scrap.board = dto.scrapBoard
        scrap.targetId = celebFeed
        scrap.targetType = Scrap.TargetType.CELEB_FEED

        celebFeedRepository.save(celebFeedEntity)
        val saved = scrapRepository.save(scrap)
        fanmeet.authorization.scrap(saved.id!!)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        celebFeedController = controller as CelebFeedController
    }

    override fun setDto(dto: Any) {
        celebFeedScrapRequestDto = dto as CelebFeedScrapRequestDto
    }
}