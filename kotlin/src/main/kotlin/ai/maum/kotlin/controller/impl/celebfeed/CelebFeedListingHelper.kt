package ai.maum.kotlin.controller.impl.celebfeed

import ai.maum.kotlin.jpa.common.celebfeed.CelebFeed
import ai.maum.kotlin.jpa.common.celebfeed.CelebFeedRepository
import org.springframework.data.domain.Sort
import java.time.Instant

class CelebFeedListingHelper(val celebFeedRepository: CelebFeedRepository) {
    fun listFeeds(get: Long, celeb: Long, created: Instant, sort: Sort) : List<CelebFeed>? {
        return when (get) {
            1L -> celebFeedRepository.findTop1ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            2L -> celebFeedRepository.findTop2ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            3L -> celebFeedRepository.findTop3ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            4L -> celebFeedRepository.findTop4ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            5L -> celebFeedRepository.findTop5ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            6L -> celebFeedRepository.findTop6ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            7L -> celebFeedRepository.findTop7ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            8L -> celebFeedRepository.findTop8ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            9L -> celebFeedRepository.findTop9ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            10L -> celebFeedRepository.findTop10ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            11L -> celebFeedRepository.findTop11ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            12L -> celebFeedRepository.findTop12ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            13L -> celebFeedRepository.findTop13ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            14L -> celebFeedRepository.findTop14ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            15L -> celebFeedRepository.findTop15ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            16L -> celebFeedRepository.findTop16ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            17L -> celebFeedRepository.findTop17ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            18L -> celebFeedRepository.findTop18ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            19L -> celebFeedRepository.findTop19ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            20L -> celebFeedRepository.findTop20ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            21L -> celebFeedRepository.findTop21ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            22L -> celebFeedRepository.findTop22ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            23L -> celebFeedRepository.findTop23ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            24L -> celebFeedRepository.findTop24ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            25L -> celebFeedRepository.findTop25ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            26L -> celebFeedRepository.findTop26ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            27L -> celebFeedRepository.findTop27ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            28L -> celebFeedRepository.findTop28ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            29L -> celebFeedRepository.findTop29ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            30L -> celebFeedRepository.findTop30ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            else -> celebFeedRepository.findTop1ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
        }
    }

    fun listFeedsWith(get: Long, celeb: Long, category: Long, created: Instant, sort: Sort) : List<CelebFeed>? {
        return when (get) {
            1L -> celebFeedRepository.findTop1ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            2L -> celebFeedRepository.findTop2ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            3L -> celebFeedRepository.findTop3ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            4L -> celebFeedRepository.findTop4ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            5L -> celebFeedRepository.findTop5ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            6L -> celebFeedRepository.findTop6ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            7L -> celebFeedRepository.findTop7ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            8L -> celebFeedRepository.findTop8ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            9L -> celebFeedRepository.findTop9ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            10L -> celebFeedRepository.findTop10ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            11L -> celebFeedRepository.findTop11ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            12L -> celebFeedRepository.findTop12ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            13L -> celebFeedRepository.findTop13ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            14L -> celebFeedRepository.findTop14ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            15L -> celebFeedRepository.findTop15ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            16L -> celebFeedRepository.findTop16ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            17L -> celebFeedRepository.findTop17ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            18L -> celebFeedRepository.findTop18ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            19L -> celebFeedRepository.findTop19ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            20L -> celebFeedRepository.findTop20ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            21L -> celebFeedRepository.findTop21ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            22L -> celebFeedRepository.findTop22ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            23L -> celebFeedRepository.findTop23ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            24L -> celebFeedRepository.findTop24ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            25L -> celebFeedRepository.findTop25ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            26L -> celebFeedRepository.findTop26ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            27L -> celebFeedRepository.findTop27ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            28L -> celebFeedRepository.findTop28ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            29L -> celebFeedRepository.findTop29ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            30L -> celebFeedRepository.findTop30ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            else -> celebFeedRepository.findTop1ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
        }
    }
}