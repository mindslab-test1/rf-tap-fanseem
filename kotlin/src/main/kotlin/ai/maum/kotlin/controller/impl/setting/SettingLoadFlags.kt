package ai.maum.kotlin.controller.impl.setting

import ai.maum.kotlin.controller.SettingController
import ai.maum.kotlin.jpa.common.setting.Setting
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.setting.SettingLoadFlagsResponseDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class SettingLoadFlags : HandlerType {

    var settingController: SettingController? = null

    override fun invoke(): ResponseType {
        val fanmeet = settingController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val settingRepository = fanmeet.settingRepository
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val setting = settingRepository.findByUserIdAndActiveIsTrue(userId)
                ?: Setting()
        if (setting.id == null) {
            // 최초 등록으로 간주
            setting.userId = userId
            setting.sound = true
            setting.vibration = true
            setting.myLike = true
            setting.myScrap = true
            setting.myComment = true
            setting.celebFeed = true
            setting.celebContent = true
            setting.celebMessage = true
            setting.celebActivity = true
            setting.ttsRequest = true
            setting.ttsResult = true
            settingRepository.save(setting)
        }

        val responseBody = SettingLoadFlagsResponseDto()
        responseBody.sound = setting.sound
        responseBody.vibration = setting.vibration
        responseBody.myLike = setting.myLike
        responseBody.myScrap = setting.myScrap
        responseBody.myComment = setting.myComment
        responseBody.celebFeed = setting.celebFeed
        responseBody.celebContent = setting.celebContent
        responseBody.celebMessage = setting.celebMessage
        responseBody.celebActivity = setting.celebActivity
        //responseBody.ttsRequest = setting.ttsRequest
        //responseBody.ttsResult = setting.ttsResult

        return ResponseEntity.ok(responseBody)
    }

    override fun setController(controller: Any) {
        settingController = controller as SettingController
    }

    override fun setDto(dto: Any) {
        // Do nothing
    }
}