package ai.maum.kotlin.controller.impl.celebmypage

import ai.maum.kotlin.controller.CelebMyPageController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.celebmypage.CelebMyPageReportListBlockedItemDto
import ai.maum.kotlin.model.http.dto.celebmypage.CelebMyPageReportListBlockedRequestDto
import ai.maum.kotlin.model.http.dto.celebmypage.CelebMyPageReportListBlockedResponseDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class CelebMyPageReportListBlocked : HandlerType {
    var celebMyPageController: CelebMyPageController? = null
    var celebMyPageReportListBlockedRequestDto: CelebMyPageReportListBlockedRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = celebMyPageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val blockRepository = fanmeet.blockRepository
        val userRepository = fanmeet.userRepository
        val dto = celebMyPageReportListBlockedRequestDto
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val blockedList =
                if (dto.reverse!!)
                    blockRepository.findByWhoAndActiveIsTrueOrderByUpdatedAsc(userId)
                else
                    blockRepository.findByWhoAndActiveIsTrueOrderByUpdatedDesc(userId)

        val blockedItemList = mutableListOf<CelebMyPageReportListBlockedItemDto>()
        for (block in blockedList) {
            val blockedItem = CelebMyPageReportListBlockedItemDto()

            val blockedUser = userRepository.findByIdAndActiveIsTrue(block.whom!!)
                    ?: continue

            blockedItem.name = blockedUser.name
            blockedItem.profileImageUrl = blockedUser.profileImageUrl?.mediaUrl()
            blockedItem.user = blockedUser.id

            blockedItemList.add(blockedItem)
        }
        val responseBody = CelebMyPageReportListBlockedResponseDto()
        responseBody.blockedList = blockedItemList

        return ResponseEntity.ok(responseBody)
    }

    override fun setController(controller: Any) {
        celebMyPageController = controller as CelebMyPageController
    }

    override fun setDto(dto: Any) {
        celebMyPageReportListBlockedRequestDto = dto as CelebMyPageReportListBlockedRequestDto
    }
}