package ai.maum.kotlin.controller.impl.mypage

import ai.maum.kotlin.controller.MyPageController
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.mypage.MyPageSubscribeListResponse
import ai.maum.kotlin.model.http.dto.mypage.MyPageSubscribingCelebDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class MyPageSubscribeList : HandlerType {
    var myPageController: MyPageController?= null

    override fun setController(controller: Any) {
        myPageController = controller as MyPageController
    }

    override fun setDto(dto: Any) {

    }

    override fun invoke(): ResponseType {
        val fanmeet = myPageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "fanmeet")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "userId")
        val userRepository = fanmeet.userRepository
        val subscribeRepository = fanmeet.subscribeRepository
        val subscribeList = subscribeRepository.findAllByWhoAndActiveIsTrue(userId)?.mapNotNull {
            userRepository.findByIdAndActiveIsTrueAndCelebActiveIsTrue(it.whom!!)
        }?.toMutableList() ?: mutableListOf() ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "subs list")
        val myPageSubscribeListResponse = MyPageSubscribeListResponse()

        subscribeRepository.findAllByWhoAndActiveIsTrue(userId)?.mapNotNull {
            userRepository.findByIdAndActiveIsTrueAndCelebActiveIsTrue(it.whom!!)
        }?.toMutableList() ?: mutableListOf()
        val userMembershipHistoryRepository = fanmeet.userMembershipHistoryRepository
        val subscribeDataList = subscribeList.map {
            MyPageSubscribingCelebDto(
                it.id!!,
                it.profileImageUrl?.mediaUrl(),
                it.name!!,
                userMembershipHistoryRepository.findTopByUserIdAndMembershipCelebAndStatusIsNotAndActiveIsTrueOrderByExpirationDate(
                        userId, it.id!!, UserMembershipHistory.Status.EXPIRED
                )?.membership?.badgeImageUrl?.mediaUrl()
            )
        }.toList()

        myPageSubscribeListResponse.subscribeList = subscribeDataList

        return ResponseEntity.ok(myPageSubscribeListResponse)
    }

}