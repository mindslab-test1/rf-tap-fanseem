package ai.maum.kotlin.controller.impl.mypage

import ai.maum.kotlin.controller.MyPageController
import ai.maum.kotlin.jpa.common.service.Tts
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.mypage.MyPageTtsListCelebDto
import ai.maum.kotlin.model.http.dto.mypage.MyPageTtsListCelebResponse
import ai.maum.kotlin.model.http.dto.mypage.TtsData
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class MyPageTtsListCeleb : HandlerType {
    var myPageController: MyPageController? = null
    var myPageTtsListCelebDto: MyPageTtsListCelebDto? = null

    override fun setController(controller: Any) {
        myPageController = controller as MyPageController
    }

    override fun setDto(dto: Any) {
        myPageTtsListCelebDto = dto as MyPageTtsListCelebDto
    }

    override fun invoke(): ResponseType {
        val fanmeet = myPageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val dto = myPageTtsListCelebDto
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")
        val ttsListCeleb = fanmeet.ttsRepository.findBySubscriberAndCelebAndStatusAndActiveIsTrue(userId, dto.celebId!!, Tts.Status.APPROVED)
        val ttsList = mutableListOf<Tts>()
        val ttsPayloadList = mutableListOf<TtsData>()
        ttsListCeleb?.forEach {
            val tts = it;
            tts.previewUrl = tts.previewUrl?.mediaUrl()
            tts.url = tts.url?.mediaUrl()
            ttsList.add(it)

            val ttsPayload = TtsData()
            ttsPayload.id = tts.id
            ttsPayload.celeb = tts.celeb
            ttsPayload.timestamp = tts.updated
            ttsPayload.text = tts.text
            ttsPayload.url = tts.url
            ttsPayloadList.add(ttsPayload)
        }
        val responseBody = MyPageTtsListCelebResponse()
        responseBody.ttsListCeleb = ttsPayloadList

        return ResponseEntity.ok(responseBody)
    }

}