package ai.maum.kotlin.controller

import ai.maum.kotlin.controller.impl.message.*
import ai.maum.kotlin.model.http.dto.message.MessageCelebDetailRequestDto
import ai.maum.kotlin.model.http.dto.message.MessageCelebTtsSampleRequestDto
import ai.maum.kotlin.model.http.dto.message.MessageCelebWriteRequestDto
import org.slf4j.LoggerFactory
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
class MessageController(val fanmeet: Fanmeet) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val auth = fanmeet.authorization

    /*
    CelebMessageRepository
    -> 셀럽이 `전송한` 메시지(원본)의 보관

    UserMessageRepository
    -> 유저가 전송한 메시지의 보관

    ReceivedMessageRepository
    -> 받은 메시지(CelebMessageRepository와 UserMessageRepository에 대한 참조)
     */

    @Transactional
    @PostMapping("/message/celeb/list/celeb")
    fun messageCelebList() =
            auth.begin(this, MessageCelebList()).handle().end()

    @Transactional
    @PostMapping("/message/celeb/list/message")
    fun messageCelebDetail(@RequestBody @Valid dto: MessageCelebDetailRequestDto) =
            auth.begin(this, dto, MessageCelebDetail()).handle().end()

    @Transactional
    @PostMapping("/message/celeb/tts/sample")
    fun messageCelebTtsSample(@RequestBody @Valid dto: MessageCelebTtsSampleRequestDto) =
            auth.begin(this, dto, MessageCelebTtsSample())
                    .with(auth.celebAuthority)
                    .handle()
                    .end()

    @Transactional
    @PostMapping("/message/celeb/write")
    fun messageCelebWrite(@RequestBody @Valid dto: MessageCelebWriteRequestDto) =
            auth.tts(dto.tts).begin(this, dto, MessageCelebWrite())
                    .with(auth.celebAuthority)
                    .handle()
                    .cast { with(auth.celebAuthority).also { fanmeet.celebUploadMessage.send(auth.userId()!!, auth.message()!!, auth.tts()) } }
                    .end()

    @PostMapping("/message/celeb/mymessage")
    fun messageCelebMyMessage() = auth.begin(this, MessageCelebMyList())
            .handle().end()
}