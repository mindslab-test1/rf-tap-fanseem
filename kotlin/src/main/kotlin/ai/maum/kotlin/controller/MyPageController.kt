package ai.maum.kotlin.controller

import ai.maum.kotlin.controller.impl.mypage.*
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.http.dto.mypage.*
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
class MyPageController(val fanmeet: Fanmeet) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val auth = fanmeet.authorization

    fun begin(controller: Any, dto: Any?, method: HandlerType) = auth.begin(controller, dto, method)
    fun begin(controller: Any, method: HandlerType) = auth.begin(controller, method)

    // Commented handlers below are assigned to implement after July 1st
/*
    @PostMapping("/mypage/follower/list")
    fun myPageFollowerList(@RequestBody @Valid myPageFollowerListRequestDto: MyPageFollowerListRequestDto) =
            begin().controller(this).dto(myPageFollowerListRequestDto).method(MyPageFollowerList())
                    .handle().end()

    @PostMapping("/mypage/level/info")
    fun myPageLevelInfo(@RequestBody @Valid myPageLevelInfoRequestDto: MyPageLevelInfoRequestDto) =
            begin().controller(this).dto(myPageLevelInfoRequestDto).method(MyPageLevelInfo())
                    .handle().end()

    @PostMapping("/mypage/activity/feed/list")
    fun myPageActivityFeedList(@RequestBody @Valid myPageActivityFeedListRequestDto: MyPageActivityFeedListRequestDto) =
            begin().controller(this).dto(myPageActivityFeedListRequestDto).method(MyPageActivityFeedList())
                    .handle().end()

    @PostMapping("/mypage/activity/feed/listwith")
    fun myPageActivityFeedListWith(@RequestBody @Valid myPageActivityFeedListWithRequestDto: MyPageActivityFeedListWithRequestDto) =
            begin().controller(this).dto(myPageActivityFeedListWithRequestDto).method(MyPageActivityFeedListWith())
                    .handle().end()

    @PostMapping("/mypage/activity/comment/list")
    fun myPageActivityCommentList(@RequestBody @Valid myPageActivityCommentListRequestDto: MyPageActivityCommentListRequestDto) =
            begin().controller(this).dto(myPageActivityCommentListRequestDto).method(MyPageActivityCommentList())
                    .handle().end()

    @PostMapping("/mypage/activity/comment/listwith")
    fun myPageActivityCommentListWith(@RequestBody @Valid myPageActivityCommentListWithRequestDto: MyPageActivityCommentListWithRequestDto) =
            begin().controller(this).dto(myPageActivityCommentListWithRequestDto).method(MyPageActivityCommentListWith())
                    .handle().end()

    @PostMapping("/mypage/activity/viewhistory/list")
    fun myPageActivityViewHistoryList(@RequestBody @Valid myPageActivityViewHistoryListRequestDto: MyPageActivityViewHistoryListRequestDto) =
            begin().controller(this).dto(myPageActivityViewHistoryListRequestDto).method(MyPageActivityViewHistoryList())
                    .handle().end()

    @PostMapping("/mypage/activity/viewhistory/listwith")
    fun myPageActivityViewHistoryListWith(@RequestBody @Valid myPageActivityViewHistoryListWithRequestDto: MyPageActivityViewHistoryListWithRequestDto) =
            begin().controller(this).dto(myPageActivityViewHistoryListWithRequestDto).method(MyPageActivityViewHistoryListWith())
                    .handle().end()
*/

    @PostMapping("/mypage/view")
    fun myPageView() =
            auth.begin(this, MyPageView())
                    .handle().end()

    @PostMapping("/mypage/profile/view")
    fun myPageProfileView() =
            auth.begin(this, MyPageProfileView())
                    .handle().end()

    @PostMapping("/mypage/profile/edit")
    fun myPageProfileEdit(@Valid dto: MyPageProfileEditDto) =
            auth.begin(this, dto, MyPageProfileEdit())
                    .handle().end()

    @PostMapping("/mypage/profile/edit/name")
    fun myPageProfileEditName(@RequestBody @Valid dto: MyPageProfileEditNameDto) =
            auth.begin(this, dto, MyPageProfileEditName())
                    .handle().end()
//
//    @PostMapping("/mypage/profile/edit/category")
//    fun myPageProfileEditCategory(@RequestBody @Valid dto: MyPageProfileEditCategoryDto) =
//            auth.begin(this, dto, MyPAgeProfileEditCategory())
//                    .handle().end()

    @PostMapping("/mypage/subscribeList")
    fun myPageSubscribes() =
            auth.begin(this, MyPageSubscribeList())
                    .handle().end()

    @PostMapping("/mypage/scrap/board/list")
    fun myPageScrapBoardList() =
            begin(this, MyPageScrapBoradList())
                    .handle().end()

    @PostMapping("/mypage/scrap/board/add")
    fun myPageScarpBoardAdd(@RequestBody @Valid dto: MyPageScrapBoardAddDto) =
            begin(this, dto, MyPageScrapBoardAdd())
                    .handle().end()

    @PostMapping("/mypage/scrap/board/delete")
    fun myPageScrapBoardDelete(@RequestBody @Valid dto: MyPageScrapBoardDeleteDto) =
            begin(this, dto, MyPageScrapBoardDelete())
                    .handle().end()

    @PostMapping("/mypage/scrap/board/view")
    fun myPageScrapBoardView(@RequestBody @Valid dto: MyPageScrapBoardViewDto) =
            begin(this, dto, MyPageScrapBoardView())
                    .handle().end()

    @PostMapping("/mypage/tts/request")
    fun myPageTtsRequest(@RequestBody @Valid dto: MyPageTtsRequestDto) =
            auth.tts(dto.id).communityOwner(dto.celebId).begin(this, dto, MyPageTtsRequest())
                    .with(auth.subscriberAuthority).without(auth.blockedByAuthority)
                    .cast { with(auth.subscriberAuthority).without(auth.blockedByAuthority).also { fanmeet.userRequestTts.send(auth.userId()!!, auth.communityOwner()!!, auth.tts()!!) } }
                    .handle().end()

    @PostMapping("/mypage/tts/sample")
    fun myPageTtsSample(@RequestBody @Valid dto: MyPageTtsSampeDto) =
            auth.communityOwner(dto.celebId).ttsLength(dto.text?.length!!.toLong()).begin(this, dto, MyPageTtsSample())
                    .with(auth.subscriberAuthority).without(auth.blockedByAuthority).with(auth.ttsLengthLimitAuthority)
                    .handle().end()

    @PostMapping("/mypage/tts/listceleb")
    fun myPageTtsListCeleb(@RequestBody @Valid dto: MyPageTtsListCelebDto) =
            auth.communityOwner(dto.celebId).begin(this, dto, MyPageTtsListCeleb())
                    .with(auth.subscriberAuthority)
                    .handle().end()

    @PostMapping("/mypage/tts/listall")
    fun myPageTtsListAll() =
            auth.begin(this, MyPageTtsListAll()).handle().end()

    @PostMapping("/mypage/tts/writeview")
    fun myPageTtsWriteView(@RequestBody @Valid dto: MyPageTtsWriteViewDto) =
            auth.begin(this, dto, MyPageTtsWriteView())
                    .handle().end()
}