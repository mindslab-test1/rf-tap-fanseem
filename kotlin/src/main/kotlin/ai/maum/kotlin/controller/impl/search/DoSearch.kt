package ai.maum.kotlin.controller.impl.search

import ai.maum.kotlin.controller.SearchController
import ai.maum.kotlin.jpa.common.user.CelebSearchResultVo
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.search.SearchRequestDto
import ai.maum.kotlin.model.http.dto.search.SearchResponseDto
import ai.maum.kotlin.model.http.dto.search.SearchResultDto
import org.springframework.http.ResponseEntity

class DoSearch : HandlerType {
    var searchController: SearchController? = null
    var searchRequestDto: SearchRequestDto? = null

    override operator fun invoke(): ResponseType {
        val userRepository = searchController?.fanmeet?.userRepository

        val resultEntityList = userRepository?.findCelebBySubstring(searchRequestDto?.text!!)
        val resultVoList = mutableListOf<SearchResultDto>()
        resultEntityList?.forEach {
            val vo = CelebSearchResultVo(it)
            resultVoList.add(SearchResultDto(vo))
        }
        val responseDto = SearchResponseDto(resultVoList)

        return ResponseEntity.ok(responseDto)
    }

    override fun setController(controller: Any) {
        searchController = controller as SearchController
    }

    override fun setDto(dto: Any) {
        searchRequestDto = dto as SearchRequestDto
    }
}
