package ai.maum.kotlin.controller.impl.celebmypage

import ai.maum.kotlin.controller.CelebMyPageController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.celebmypage.CelebMyPageHeaderResponseDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class CelebMyPageHeader : HandlerType {
    var celebMyPageController: CelebMyPageController? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = celebMyPageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val feedRepository = fanmeet.feedRepository
        val userRepository = fanmeet.userRepository
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val user = userRepository.findByIdAndActiveIsTrue(userId)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST)

        val responseBody = CelebMyPageHeaderResponseDto()

        responseBody.feedCount = feedRepository.countByCelebAndActiveIsTrue(userId)
        responseBody.name = user.name!!
        responseBody.profileImageUrl = user.profileImageUrl?.mediaUrl()
        responseBody.subscriberCount = user.subscriberCount!!

        return ResponseEntity.ok(responseBody)
    }

    override fun setController(controller: Any) {
        celebMyPageController = controller as CelebMyPageController
    }

    override fun setDto(dto: Any) {
        // Do nothing
    }
}