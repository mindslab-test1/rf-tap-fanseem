package ai.maum.kotlin.controller.impl.admin

import ai.maum.kotlin.controller.AdminController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.admin.LikeUniquenessRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class LikeUniqueness : HandlerType {
    var adminController: AdminController? = null
    var likeUniquenessRequestDto: LikeUniquenessRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = adminController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val userRepository = fanmeet.userRepository
        val likeRepository = fanmeet.likeRepository
        val dto = likeUniquenessRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        if (dto.secret != "tlsqktemdmlqhgja")
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Secret does not match")

        val users = userRepository.findByActiveIsTrue()
        for (user in users) {
            val active = likeRepository.findByUserIdAndActiveIsTrueOrderByTargetIdAscTargetTypeAsc(user.id ?: continue)
            val inactive = likeRepository.findByUserIdAndActiveIsFalseOrderByTargetIdAscTargetTypeAsc(user.id
                    ?: continue)

            val activeGrouped = active.groupBy { it.targetType }
            val inactiveGrouped = inactive.groupBy { it.targetType }

            // Separate by active/inactive
            for (grouped in arrayOf(activeGrouped, inactiveGrouped)) {
                // Separate by TargetType
                for (typeGroup in grouped) {
                    val idAndTypeGroup = typeGroup.value.groupBy { it.targetId }

                    // Remove duplicate likes and leave most recent one
                    for (targetRecords in idAndTypeGroup) {
                        if (targetRecords.value.size > 1) {
                            // Exclude 0th element from remove target
                            val removeTarget = targetRecords.value.sortedByDescending { it.created }.toMutableList().also { it.removeAt(0) }
                            likeRepository.deleteAll(removeTarget)
                        }
                    }
                }
            }
        }

        // After this call, there could be 1 active like and 1 inactive like targeting same item(article/comment).
        // They would be automatically reduced by real like calls(ex:CelebFeedLike) later.

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        adminController = controller as AdminController
    }

    override fun setDto(dto: Any) {
        likeUniquenessRequestDto = dto as LikeUniquenessRequestDto
    }
}