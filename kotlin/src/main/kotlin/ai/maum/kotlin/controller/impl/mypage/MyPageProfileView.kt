package ai.maum.kotlin.controller.impl.mypage

import ai.maum.kotlin.controller.MyPageController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.mypage.MyPageProfileViewResponse
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class MyPageProfileView : HandlerType {
    var myPageController : MyPageController ?= null

    override fun setController(controller: Any) {
        myPageController = controller as MyPageController
    }

    override fun setDto(dto: Any) {
        // do nothing
    }

    override fun invoke(): ResponseType {
        val fanmeet = myPageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "fanmeet")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "userId")

        val userRepository = fanmeet.userRepository
        val userInfo = userRepository.findByIdAndActiveIsTrue(userId)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "userInfo")

        val responseBody = MyPageProfileViewResponse()
        responseBody.name = userInfo.name
        responseBody.profileImgUrl = userInfo.profileImageUrl?.mediaUrl()
        responseBody.bannerImgUrl = userInfo.bannerImageUrl?.mediaUrl()

        val userInterestedRepository = fanmeet.userInterestedRepository
        val userInterestedData = userInterestedRepository.findWhatByWho(userId)

        responseBody.interestedCategory = userInterestedData

        return ResponseEntity.ok(responseBody)
    }
}