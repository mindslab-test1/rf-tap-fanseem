package ai.maum.kotlin.controller.impl.celebfeed

import ai.maum.kotlin.controller.CelebFeedController
import ai.maum.kotlin.jpa.common.like.Like
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.celebfeed.CelebFeedUnlikeRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class CelebFeedUnlike : HandlerType {
    var celebFeedController: CelebFeedController? = null
    var celebFeedUnlikeRequestDto: CelebFeedUnlikeRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = celebFeedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val likeRepository = fanmeet.likeRepository
        val celebFeedRepository = fanmeet.celebFeedRepository
        val dto = celebFeedUnlikeRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val celebFeed = celebFeedRepository.findByIdAndActiveIsTrue(dto.celebFeed!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "CelebFeed")

        val like = likeRepository.findByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId, dto.celebFeed!!, Like.TargetType.CELEB_FEED)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Like")

        val duplicates = likeRepository.findAllByUserIdAndTargetIdAndTargetTypeAndActiveIsFalse(userId, dto.celebFeed!!, Like.TargetType.CELEB_FEED)
        if (duplicates.isNotEmpty()) {
            val reducedExceptFirst = duplicates.sortedByDescending { it.created!! }.toMutableList().also { it.removeAt(0) }
            if (reducedExceptFirst.isNotEmpty())
                likeRepository.deleteAll(reducedExceptFirst)
        }

        celebFeed.likeCount = celebFeed.likeCount!! - 1
        like.active = false

        celebFeedRepository.save(celebFeed)
        likeRepository.save(like)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        celebFeedController = controller as CelebFeedController
    }

    override fun setDto(dto: Any) {
        celebFeedUnlikeRequestDto = dto as CelebFeedUnlikeRequestDto
    }
}