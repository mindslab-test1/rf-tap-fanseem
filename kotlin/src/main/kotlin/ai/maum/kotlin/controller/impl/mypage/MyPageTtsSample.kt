package ai.maum.kotlin.controller.impl.mypage

import ai.maum.kotlin.controller.MyPageController
import ai.maum.kotlin.jpa.common.service.Tts
import ai.maum.kotlin.model.audio.Wave
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.mypage.MyPageTtsSampeDto
import ai.maum.kotlin.model.http.dto.mypage.MyPageTtsSampleResponse
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class MyPageTtsSample : HandlerType {
    var myPageController: MyPageController? = null
    var myPageTtsSampleDto: MyPageTtsSampeDto? = null

    override fun setController(controller: Any) {
        myPageController = controller as MyPageController
    }

    override fun setDto(dto: Any) {
        myPageTtsSampleDto = dto as MyPageTtsSampeDto
    }

    override fun invoke(): ResponseType {
        val fanmeet = myPageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val ttsAddressRepository = fanmeet.ttsAddressRepository
        val ttsRepository = fanmeet.ttsRepository
        val dto = myPageTtsSampleDto
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        if (dto.text!!.length > 300) throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Text length exceeds common limit")

        // Don't remove
        val ttsAddress = ttsAddressRepository.findByCeleb(dto.celebId!!)
                ?: throw ResponseStatusException(HttpStatus.UNSUPPORTED_MEDIA_TYPE, "No Tts Model")

        val speech: ByteArray
        try {
            speech = fanmeet.ttsManager.generate(dto.celebId!!, dto.text!!)
        } catch (e: Exception) {
            throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Tts Manager")
        }
        val previewSpeech = speech.clone()

        val wavSpeech = Wave(speech)
        val wavPreviewSpeech = Wave(previewSpeech)

        wavSpeech.setWatermark()
        wavPreviewSpeech.setWatermark()

        val speechUrl = fanmeet.mediaUploader.uploadTtsAsync(wavSpeech.getWaveBytes())
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Speech Url")
        val previewSpeechUrl = fanmeet.mediaUploader.uploadTtsAsync(wavPreviewSpeech.getPreviewWaveBytes(0, 5))
                ?: throw  ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "PreviewSpeech Url")

        val tts = Tts()
        tts.celeb = dto.celebId
        tts.text = dto.text
        tts.url = speechUrl
        tts.previewUrl = previewSpeechUrl
        tts.status = Tts.Status.SAMPLE
        tts.subscriber = userId
        val result = ttsRepository.save(tts)

        val responseBody = MyPageTtsSampleResponse()
        responseBody.url = previewSpeechUrl.mediaUrl()
        responseBody.id = result.id

        return ResponseEntity.ok(responseBody)
    }
}