package ai.maum.kotlin.controller.impl.mypage

import ai.maum.kotlin.controller.MyPageController
import ai.maum.kotlin.jpa.common.user.User
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.mypage.MyPageProfileEditNameDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class MyPageProfileEditName: HandlerType {
    var myPageController: MyPageController ?= null
    var myPageProfileEditNameDto: MyPageProfileEditNameDto ?= null

    override fun setController(controller: Any) {
        myPageController = controller as MyPageController
    }

    override fun setDto(dto: Any) {
        myPageProfileEditNameDto = dto as MyPageProfileEditNameDto
    }

    override fun invoke(): ResponseType {
        val fanmeet = myPageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "fanmeet")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "userId")
        val dto = myPageProfileEditNameDto
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "dto")

        val userRepository = fanmeet.userRepository

        val originUser = userRepository.findByIdAndActiveIsTrue(userId)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "user")
        val newUser = User()
        newUser.id = originUser.id
        newUser.name = dto.name
        newUser.subscriberCount = originUser.subscriberCount
        newUser.profileImageUrl = originUser.profileImageUrl
        newUser.bannerImageUrl = originUser.bannerImageUrl
        newUser.gender = originUser.gender
        newUser.age = originUser.age
        newUser.birthday = originUser.birthday
        newUser.googleUserId = originUser.googleUserId
        newUser.kakaoUserId = originUser.kakaoUserId
        newUser.naverUserId = originUser.naverUserId
        newUser.appleUserId = originUser.appleUserId

        newUser.celeb = originUser.celeb
        newUser.interestedList = originUser.interestedList

        val updated = userRepository.save(newUser)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "update")

        return ResponseEntity.ok(Unit)
    }
}