package ai.maum.kotlin.controller.impl.mypage

import ai.maum.kotlin.controller.MyPageController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.mypage.MyPageProfileEditDto
import org.slf4j.LoggerFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class MyPageProfileEdit : HandlerType {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    var myPageController: MyPageController? = null
    var myPageProfileEditDto: MyPageProfileEditDto? = null

    override fun setController(controller: Any) {
        myPageController = controller as MyPageController
    }

    override fun setDto(dto: Any) {
        myPageProfileEditDto = dto as MyPageProfileEditDto
    }

    override fun invoke(): ResponseType {
        val fanmeet = myPageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "fanmeet")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "userId")
        val userRepository = fanmeet.userRepository

        val user = userRepository.findUserByIdAndActiveIsTrue(userId)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "User")
        val dto = myPageProfileEditDto ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "dto")

        dto.profileImage?.let {
            logger.info("ProfileImage Name: ${dto.profileImage!!.originalFilename}")
            val fileName = fanmeet.mediaUploader.uploadAsyncProfileImage(dto.profileImage!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "ProfileImage")

            user.profileImageUrl = fileName
        }
        dto.bannerImage?.let {
            val fileName = fanmeet.mediaUploader.uploadAsyncBannerImage(dto.bannerImage!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "BannerImage")

            user.bannerImageUrl = fileName
        }

        userRepository.save(user)

        return ResponseEntity.ok(Unit)
    }
}