package ai.maum.kotlin.controller.impl.celebfeed

import ai.maum.kotlin.controller.CelebFeedController
import ai.maum.kotlin.jpa.common.celebfeed.CelebFeed
import ai.maum.kotlin.jpa.common.like.Like
import ai.maum.kotlin.jpa.common.scrap.Scrap
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.celebfeed.CelebFeedItemDto
import ai.maum.kotlin.model.http.dto.celebfeed.CelebFeedListRequestDto
import ai.maum.kotlin.model.http.dto.celebfeed.CelebFeedListResponseDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.data.domain.Sort
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException
import java.time.Instant

class CelebFeedList : HandlerType {
    var celebFeedController: CelebFeedController? = null
    var celebFeedListRequestDto: CelebFeedListRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = celebFeedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val celebFeedRepository = fanmeet.celebFeedRepository
        val feedCategoryRepository = fanmeet.celebFeedCategoryRepository
        val userRepository = fanmeet.userRepository
        val blockRepository = fanmeet.blockRepository
        val scrapRepository = fanmeet.scrapRepository
        val likeRepository = fanmeet.likeRepository
        val userMembershipHistoryRepository = fanmeet.userMembershipHistoryRepository
        val dto = celebFeedListRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId

        // Input calibration
        dto.from = dto.from ?: Instant.now()
        dto.category = dto.category ?: 0

        // Response init
        val responseBody = CelebFeedListResponseDto()
        responseBody.feeds = mutableListOf()

        var userLevel = 0L
        userId?.let outerLet@{
            val membership = userMembershipHistoryRepository.findTopByUserIdAndMembershipCelebAndActiveIsTrueOrderByExpirationDateDesc(userId, dto.celeb!!)
            membership?.let{
                it.purchaseToken ?: it.run {
                    if (membership.expirationDate!! < Instant.now()) {
                        membership.status = UserMembershipHistory.Status.EXPIRED
                        userMembershipHistoryRepository.save(membership)
                    }
                }

                if (membership.status != UserMembershipHistory.Status.NORMAL && membership.status != UserMembershipHistory.Status.TO_BE_EXPIRED)
                    return@outerLet
            }
            userLevel = membership?.membership?.tier ?: 0
        }

        // Get feeds as much as requested
        var tailCreated = dto.from!!
        var from = dto.from!!
        while (responseBody.feeds!!.size < dto.get!!) {
            // Retrieve feeds by category
            val celebFeeds = if (dto.category!! > 0) {
                CelebFeedListingHelper(celebFeedRepository).listFeedsWith(
                        dto.get!! - responseBody.feeds!!.size,
                        dto.celeb!!,
                        dto.category!!,
                        from,
                        Sort.by("created").descending()
                )?.toMutableList() ?: mutableListOf()
            } else {
                CelebFeedListingHelper(celebFeedRepository).listFeeds(
                        dto.get!! - responseBody.feeds!!.size,
                        dto.celeb!!,
                        from,
                        Sort.by("created").descending()
                )?.toMutableList() ?: mutableListOf()
            }

            // Fill feed items
            for (celebFeed: CelebFeed in celebFeeds) {
                val celebFeedItem = CelebFeedItemDto()

                // If logged in, skip feed created by blocked user
                celebFeedItem.blocked = false
                userId?.let {
                    val blocks = blockRepository.findByWhoAndActiveIsTrue(userId)
                    for (block in blocks)
                        if (block.whom == celebFeed.who)
                            continue
                }

                // If logged in, check if user liked/scraped the feed
                userId?.let {
                    celebFeedItem.liked = likeRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId, celebFeed.id!!, Like.TargetType.CELEB_FEED)
                    celebFeedItem.scraped = scrapRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId, celebFeed.id!!, Scrap.TargetType.CELEB_FEED)
                }

                // If logged in, check if user has access to the content
                var membershipAccess = false
                if (dto.celeb == userId) membershipAccess = true

                if (!membershipAccess) {
                    userId?.let {
                        if (userLevel >= celebFeed.accessLevel!!)
                            membershipAccess = true
                    }
                }

                // Information about who created the feed
                celebFeedItem.feedOwner = celebFeed.who
                val feedOwner = userRepository.findByIdAndActiveIsTrue(celebFeed.who ?: continue)
                        ?: continue
                celebFeedItem.feedOwnerName = feedOwner.name
                celebFeedItem.feedOwnerProfileImage = feedOwner.profileImageUrl?.mediaUrl()

                val accessLevel = celebFeed.accessLevel ?: 0
                celebFeedItem.accessLevelBadgeImageUrl = fanmeet.membershipRepository.findByCelebAndTierAndActiveIsTrue(celebFeed.who!!, accessLevel)?.badgeImageUrl?.mediaUrl()

                // Information about category specified in the feed
                celebFeedItem.category = celebFeed.category
                try {
                    if (celebFeed.category!! == 0L) throw NullPointerException()
                    val category = feedCategoryRepository.findById(celebFeed.category!!)
                    celebFeedItem.categoryName = category.get().name ?: throw NullPointerException()
                } catch (e: NullPointerException) {
                    celebFeedItem.category = 0
                    celebFeedItem.categoryName = ""
                }

                celebFeedItem.celebFeed = celebFeed.id
                celebFeedItem.commentCount = celebFeed.commentCount
                celebFeedItem.likeCount = celebFeed.likeCount
                celebFeedItem.scrapCount = celebFeed.scrapCount
                celebFeedItem.accessLevelBadgeImageUrl
                celebFeedItem.updated = celebFeed.created
                celebFeedItem.restricted = membershipAccess

                val pictures = mutableListOf<String>()
                celebFeed.pictureList?.let {
                    for (picture in celebFeed.pictureList!!)
                        pictures.add(picture.url!!.mediaUrl())
                }

                if (membershipAccess) {
                    celebFeedItem.text = celebFeed.text
                    celebFeedItem.pictures = pictures
                    celebFeedItem.video = celebFeed.video?.url?.mediaUrl()
                    celebFeedItem.thumbnail = celebFeed.thumbnail?.url?.mediaUrl()
                    celebFeedItem.tts = celebFeed.tts?.url?.mediaUrl()
                    celebFeedItem.youtube = celebFeed.youtube
                }

                if (celebFeed.created!! < from)
                    from = celebFeed.created!!
                responseBody.feeds!!.add(celebFeedItem)
            }
            if (from < tailCreated)
                tailCreated = from

            // Check if there is no more feeds
            // Avoid infinite loop
            if (celebFeeds.size < dto.get!!)
                break
        }
        responseBody.tailCreated = tailCreated

        return ResponseEntity.ok(responseBody)
    }

    override fun setController(controller: Any) {
        celebFeedController = controller as CelebFeedController
    }

    override fun setDto(dto: Any) {
        celebFeedListRequestDto = dto as CelebFeedListRequestDto
    }
}