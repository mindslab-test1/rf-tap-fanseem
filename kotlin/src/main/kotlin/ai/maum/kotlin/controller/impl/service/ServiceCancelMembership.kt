package ai.maum.kotlin.controller.impl.service

import ai.maum.kotlin.controller.ServiceController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.service.ServiceCancelMembershipRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ServiceCancelMembership: HandlerType {
    var serviceController: ServiceController? = null
    var serviceCancelMembershipRequestDto: ServiceCancelMembershipRequestDto?= null

    override fun invoke(): ResponseType {
        val fanmeet = serviceController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val dto = serviceCancelMembershipRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        dto.purchaseToken ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "purchaseToken")
        dto.productId ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "productId")

        try {
            fanmeet.subscriptionCanceler.run(
                    purchaseToken = dto.purchaseToken!!,
                    productId = dto.productId!!
            )
        } catch (e: Exception) {
            throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Membership cancellation failed")
        }

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        serviceController= controller as ServiceController
    }

    override fun setDto(dto: Any) {
        serviceCancelMembershipRequestDto = dto as ServiceCancelMembershipRequestDto
    }


}