package ai.maum.kotlin.controller

import ai.maum.kotlin.controller.impl.celebmypage.*
import ai.maum.kotlin.model.http.dto.celebmypage.*
import org.slf4j.LoggerFactory
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
class CelebMyPageController(val fanmeet: Fanmeet) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val auth = fanmeet.authorization

    @Transactional
    @PostMapping("/community/celebmypage/header")
    fun celebMyPageHeader() =
            auth.begin(this, CelebMyPageHeader())
                    .with(auth.celebAuthority)
                    .handle().end()

    @Transactional
    @PostMapping("/community/celebmypage/tts/listreceived")
    fun celebMyPageTtsListReceived() =
            auth.begin(this, CelebMyPageTtsListReceived())
                    .with(auth.celebAuthority)
                    .handle().end()

    @Transactional
    @PostMapping("/community/celebmypage/tts/listapproved")
    fun celebMyPageTtsListApproved() =
            auth.begin(this, CelebMyPageTtsListApproved())
                    .with(auth.celebAuthority)
                    .handle().end()

    @Transactional
    @PostMapping("/community/celebmypage/tts/approve")
    fun celebMyPageTtsApprove(@RequestBody @Valid dto: CelebMyPageTtsApproveRequestDto) =
            auth.tts(dto.tts).ttsRequesterFromTts(dto.tts).begin(this, dto, CelebMyPageTtsApprove())
                    .with(auth.celebAuthority)
                    .handle()
                    .cast { with(auth.celebAuthority).also { fanmeet.celebAcceptTts.send(auth.userId()!!, auth.ttsRequester()!!, auth.tts()!!) } }
                    .end()

    @Transactional
    @PostMapping("/community/celebmypage/tts/decline")
    fun celebMyPageTtsDecline(@RequestBody @Valid dto: CelebMyPageTtsDeclineRequestDto) =
            auth.tts(dto.tts).ttsRequesterFromTts(dto.tts).begin(this, dto, CelebMyPageTtsDecline())
                    .with(auth.celebAuthority)
                    .handle()
                    .cast { with(auth.celebAuthority).also { fanmeet.celebDeclineTts.send(auth.userId()!!, auth.ttsRequester()!!, auth.tts()!!) } }
                    .end()

    @Transactional
    @PostMapping("/community/celebmypage/report/listcandidate")
    fun celebMyPageReportListCandidate(@RequestBody @Valid dto: CelebMyPageReportListCandidateRequestDto) =
            auth.begin(this, dto, CelebMyPageReportListCandidate())
                    .with(auth.celebAuthority)
                    .handle().end()

    @Transactional
    @PostMapping("/community/celebmypage/report/listblocked")
    fun celebMyPageReportListBlocked(@RequestBody @Valid dto: CelebMyPageReportListBlockedRequestDto) =
            auth.begin(this, dto, CelebMyPageReportListBlocked())
                    .with(auth.celebAuthority)
                    .handle().end()

    @Transactional
    @PostMapping("/community/celebmypage/tts/generate")
    fun celebMyPageTtsGenerate(@RequestBody @Valid dto: CelebMyPageTtsGenerateRequestDto) =
            auth.begin(this, dto, CelebMyPageTtsGenerate())
                    .with(auth.celebAuthority)
                    .handle().end()
}