package ai.maum.kotlin.controller.impl.content

import ai.maum.kotlin.controller.ContentController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.content.ContentDeleteCommentRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ContentDeleteComment : HandlerType {
    var contentController: ContentController? = null
    var contentDeleteCommentRequestDto: ContentDeleteCommentRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = contentController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val commentRepository = fanmeet.commentRepository
        val contentRepository = fanmeet.contentRepository
        val dto = contentDeleteCommentRequestDto
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val comment = commentRepository.findByIdAndActiveIsTrue(dto.comment!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Comment")
        val content = contentRepository.findByIdAndActiveIsTrue(comment.feed!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Content")

        content.commentCount = content.commentCount!! - 1
        comment.active = false

        contentRepository.save(content)
        commentRepository.save(comment)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        contentController = controller as ContentController
    }

    override fun setDto(dto: Any) {
        contentDeleteCommentRequestDto = dto as ContentDeleteCommentRequestDto
    }
}