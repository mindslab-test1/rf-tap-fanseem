package ai.maum.kotlin.controller.impl.feed

import ai.maum.kotlin.controller.FeedController
import ai.maum.kotlin.jpa.common.service.Tts
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.feed.FeedEditCommentRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class FeedEditComment : HandlerType {
    var feedController: FeedController? = null
    var feedEditCommentRequestDto: FeedEditCommentRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = feedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val commentRepository = fanmeet.commentRepository
        val ttsRepository = fanmeet.ttsRepository
        val dto = feedEditCommentRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val comment = commentRepository.findByIdAndActiveIsTrue(dto.comment!!)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST)

        dto.picture?.let {
            comment.picture = fanmeet.mediaUploader.uploadAsync(dto.picture!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Uploading Pictures")
        }

        val tts = dto.tts?.let {
            ttsRepository.findByIdAndActiveIsTrue(dto.tts!!)
                    ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto TTS")
        }
        tts?.let {
            if (userId != tts.subscriber || userId != tts.celeb)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "TTS Ownership")
            if (tts.status != Tts.Status.APPROVED && tts.status != Tts.Status.SELF)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "TTS Status")
            comment.tts = tts
        }

        dto.text?.let{
            comment.text = dto.text
        }

        commentRepository.save(comment)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        feedController = controller as FeedController
    }

    override fun setDto(dto: Any) {
        feedEditCommentRequestDto = dto as FeedEditCommentRequestDto
    }
}