package ai.maum.kotlin.controller.impl.mypage

import ai.maum.kotlin.controller.MyPageController
import ai.maum.kotlin.jpa.common.scrap.ScrapBoard
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.mypage.MyPageScrapBoardDeleteDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class MyPageScrapBoardDelete : HandlerType {
    var myPageController: MyPageController? = null
    var myPageScrapBoardDeleteDto: MyPageScrapBoardDeleteDto? =null

    override fun setController(controller: Any) {
        myPageController = controller as MyPageController
    }

    override fun setDto(dto: Any) {
        myPageScrapBoardDeleteDto = dto as MyPageScrapBoardDeleteDto
    }

    override fun invoke(): ResponseType {
        val fanmeet = myPageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "fanmeet")
        val dto = myPageScrapBoardDeleteDto
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "dto")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "userId")
        val scrapBoardRepository = fanmeet.scrapBoardRepository
        val boardId = dto.boardId
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "boardId")
        val originalBoard = scrapBoardRepository.findByIdAndOwnerIdAndActiveIsTrue(boardId, userId)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "no board found")
        val deleteTarget = ScrapBoard()
        deleteTarget.id = originalBoard.id
        deleteTarget.ownerId = originalBoard.ownerId
        deleteTarget.name = originalBoard.name
        deleteTarget.active = false
        val deleteResult = scrapBoardRepository.save(deleteTarget)
        if(deleteResult.id != boardId)  throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "data consistency")

        return ResponseEntity.ok(Unit)
    }
}