package ai.maum.kotlin.controller.impl.celebfeed

import ai.maum.kotlin.controller.CelebFeedController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.celebfeed.CelebFeedSearchRequestDto
import org.springframework.http.ResponseEntity

class CelebFeedSearch : HandlerType {
    var celebFeedController: CelebFeedController? = null
    var celebFeedSearchRequestDto: CelebFeedSearchRequestDto? = null

    override operator fun invoke(): ResponseType {
        TODO("Future Functionality")
        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        celebFeedController = controller as CelebFeedController
    }

    override fun setDto(dto: Any) {
        celebFeedSearchRequestDto = dto as CelebFeedSearchRequestDto
    }
}