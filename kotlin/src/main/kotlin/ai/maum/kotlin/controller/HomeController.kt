package ai.maum.kotlin.controller

import ai.maum.kotlin.controller.impl.home.GuestHome
import ai.maum.kotlin.controller.impl.home.HomeCelebList
import ai.maum.kotlin.controller.impl.home.LoginHome
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class HomeController(val fanmeet: Fanmeet) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val auth = fanmeet.authorization

    fun begin(controller: Any, dto: Any?, method: HandlerType) = auth.begin(controller, dto, method)
    fun begin(controller: Any, method: HandlerType) = auth.begin(controller, method)

    @PostMapping("/home/login")
    fun loginHome() = begin(this, LoginHome()).handle().end()

    @GetMapping("/home/guest")
    fun guestHome() = begin(this, GuestHome()).handle().end()

    @PostMapping("/home/celebs")
    fun homeCelebList() = begin(this, HomeCelebList()).handle().end()
}