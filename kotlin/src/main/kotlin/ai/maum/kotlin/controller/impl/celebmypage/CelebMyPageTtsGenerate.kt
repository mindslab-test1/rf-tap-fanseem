package ai.maum.kotlin.controller.impl.celebmypage

import ai.maum.kotlin.controller.CelebMyPageController
import ai.maum.kotlin.jpa.common.service.Tts
import ai.maum.kotlin.model.audio.Wave
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.celebmypage.CelebMyPageTtsGenerateRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class CelebMyPageTtsGenerate : HandlerType {
    var celebMyPageController: CelebMyPageController? = null
    var celebMyPageTtsGenerateRequestDto: CelebMyPageTtsGenerateRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = celebMyPageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val ttsAddressRepository = fanmeet.ttsAddressRepository
        val ttsRepository = fanmeet.ttsRepository
        val dto = celebMyPageTtsGenerateRequestDto
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        // Don't remove
        val ttsAddress = ttsAddressRepository.findByCeleb(userId)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "No Tts Model")

        dto.text ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Text Null")
        if (dto.text!!.length > 300)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Text Too Long")
        if (dto.text!!.isEmpty())
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Text Too Short")

        val speech: ByteArray
        try {
            speech = fanmeet.ttsManager.generate(userId, dto.text!!)
        } catch (e: Exception) {
            throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Tts Manager")
        }
        val previewSpeech = speech.clone()

        val wavSpeech = Wave(speech)
        val wavPreviewSpeech = Wave(previewSpeech)

        wavSpeech.setWatermark()
        wavPreviewSpeech.setWatermark()

        val speechUrl = fanmeet.mediaUploader.uploadTtsAsync(wavSpeech.getWaveBytes())
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Speech Url")
        val previewSpeechUrl = fanmeet.mediaUploader.uploadTtsAsync(wavPreviewSpeech.getPreviewWaveBytes(0, 5))
                ?: throw  ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "PreviewSpeech Url")

        val tts = Tts()
        tts.celeb = userId
        tts.text = dto.text
        tts.url = speechUrl
        tts.previewUrl = previewSpeechUrl
        tts.status = Tts.Status.SELF
        tts.subscriber = null

        ttsRepository.save(tts)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        celebMyPageController = controller as CelebMyPageController
    }

    override fun setDto(dto: Any) {
        celebMyPageTtsGenerateRequestDto = dto as CelebMyPageTtsGenerateRequestDto
    }
}