package ai.maum.kotlin.controller

import ai.maum.kotlin.controller.impl.celebfeed.*
import ai.maum.kotlin.model.http.dto.celebfeed.*
import org.slf4j.LoggerFactory
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
class CelebFeedController(val fanmeet: Fanmeet) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val auth = fanmeet.authorization

    @Transactional
    @PostMapping("/community/celebfeed/write")
    fun celebFeedWrite(@Valid dto: CelebFeedWriteRequestDto) =
            auth.begin(this, dto, CelebFeedWrite())
                    .with(auth.celebAuthority)
                    .handle()
                    .cast { without(auth.skipNotificationAuthority).run { fanmeet.celebUploadFeed.send(auth.userId()!!, auth.feed()!!) } }
                    .end()

    @Transactional
    @PostMapping("/community/celebfeed/edit")
    fun celebFeedEdit(@Valid dto: CelebFeedEditRequestDto) =
            auth.celebFeed(dto.celebFeed).begin(this, dto, CelebFeedEdit())
                    .with(auth.celebFeedWriterAuthority)
                    .handle().end()

    @Transactional
    @PostMapping("/community/celebfeed/delete")
    fun celebFeedDelete(@RequestBody @Valid dto: CelebFeedDeleteRequestDto) =
            auth.communityOwnerFromCelebFeed(dto.celebFeed).celebFeed(dto.celebFeed).begin(this, dto, CelebFeedDelete())
                    .or(
                            { with(auth.celebFeedWriterAuthority) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle().end()

    @Transactional
    @PostMapping("/community/celebfeed/search")
    fun celebFeedSearch(@RequestBody @Valid dto: CelebFeedSearchRequestDto) =
            auth.begin(this, dto, CelebFeedSearch())
                    .handle().end()

    @Transactional
    @PostMapping("/community/celebfeed/list")
    fun celebFeedList(@RequestBody @Valid dto: CelebFeedListRequestDto) =
            auth.begin(this, dto, CelebFeedList())
                    .handle().end()

    @Transactional
    @PostMapping("/community/celeb/listcategory")
    fun celebFeedListCategory(@RequestBody @Valid dto: CelebFeedListCategoryRequestDto) =
            auth.begin(this, dto, CelebFeedListCategory())
                    .handle().end()

    @Transactional
    @PostMapping("/community/celebfeed/view")
    fun celebFeedView(@RequestBody @Valid dto: CelebFeedViewRequestDto) =
            auth.communityOwnerFromCelebFeed(dto.celebFeed).celebFeed(dto.celebFeed).begin(this, dto, CelebFeedView())
                    .or(
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.membershipTierPrivilege) },
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.scrappedPrivilege) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle().end()

    @Transactional
    @PostMapping("/community/celebfeed/scrap")
    fun celebFeedScrap(@RequestBody @Valid dto: CelebFeedScrapRequestDto) =
            auth.communityOwnerFromCelebFeed(dto.celebFeed).celebFeedOwnerFromCelebFeed(dto.celebFeed).celebFeed(dto.celebFeed).begin(this, dto, CelebFeedScrap())
                    .or(
                            { without(auth.communityOwnershipAuthority).with(auth.membershipTierPrivilege).with(auth.subscriberAuthority).without(auth.blockedByAuthority) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle()
                    //주체가 셀럽일 경우,셀럽이 내 셀럽피드를 스크랩 하였음
//                    .cast { without(auth.skipNotificationAuthority).with(auth.celebAuthority).also { fanmeet.celebScrapCelebFeed.send(auth.userId()!!, auth.celebFeedOwner()!!, auth.scrap()!!) } }
                    //주체가 셀럽이 아닐 경우, 유저가 내 피드를 스크랩 하였음
                    .cast { without(auth.skipNotificationAuthority).without(auth.celebAuthority).also { fanmeet.userScrapCelebFeed.send(auth.userId()!!, auth.celebFeedOwner()!!, auth.scrap()!!) } }
                    .end()

    @Transactional
    @PostMapping("/community/celebfeed/like")
    fun celebFeedLike(@RequestBody @Valid dto: CelebFeedLikeRequestDto) =
            auth.communityOwnerFromCelebFeed(dto.celebFeed).celebFeedOwnerFromCelebFeed(dto.celebFeed).celebFeed(dto.celebFeed).begin(this, dto, CelebFeedLike())
                    .or(
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.membershipTierPrivilege) },
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.scrappedPrivilege) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle()
                    .cast { without(auth.skipNotificationAuthority).with(auth.celebAuthority).also { fanmeet.celebLikeCelebFeed.send(auth.userId()!!, auth.celebFeedOwner()!!, auth.celebFeed()!!) } }
                    .cast { without(auth.skipNotificationAuthority).without(auth.celebAuthority).also { fanmeet.userLikeCelebFeed.send(auth.userId()!!, auth.celebFeedOwner()!!, auth.celebFeed()!!) } }
                    .end()

    @Transactional
    @PostMapping("/community/celebfeed/unlike")
    fun celebFeedUnlike(@RequestBody @Valid dto: CelebFeedUnlikeRequestDto) =
            auth.celebFeedOwnerFromCelebFeed(dto.celebFeed).celebFeed(dto.celebFeed).begin(this, dto, CelebFeedUnlike())
                    .or(
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.membershipTierPrivilege) },
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.scrappedPrivilege) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle().end()

    @Transactional
    @PostMapping("/community/celebfeed/comment/write")
    fun celebFeedWriteComment(@Valid dto: CelebFeedWriteCommentRequestDto) =
            auth.communityOwner(dto.celeb).celebFeedOwnerFromCelebFeed(dto.celebFeed).celebFeed(dto.celebFeed).parent(dto.parent).parentOwnerFromParent(dto.parent).begin(this, dto, CelebFeedWriteComment())
                    .or(
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.membershipTierPrivilege).with(auth.subscriberAuthority) },
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.scrappedPrivilege) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle()
                    //댓글이며, 주체가 셀럽일 경우 셀럽이 셀럽 피드에 댓글을 남겼음
                    .cast { auth.parent()?:let { without(auth.skipNotificationAuthority).with(auth.celebAuthority).also { fanmeet.celebCommentCelebFeed.send(auth.userId()!!, auth.celebFeedOwner()!!, auth.celebFeed()!!, auth.comment()!!) } } }
                    //댓글이며, 주체가 유저일 경우 유저가 셀럽 피드에 댓글을 남겼음
                    .cast { auth.parent()?:let { without(auth.skipNotificationAuthority).without(auth.celebAuthority).also { fanmeet.userCommentCelebFeed.send(auth.userId()!!, auth.celebFeedOwner()!!, auth.comment()!!) } } }
                    //대댓글이며, 주체가 셀럽일 경우 셀럽이 내 댓글에 답글을 남겼음
                    .cast { auth.parent()?.let { without(auth.skipNotificationAuthority).with(auth.celebAuthority).also { fanmeet.celebSubcomment.send(auth.userId()!!, auth.parentOwner()!!, auth.parent()!!, auth.comment()!!) } } }
                    //대댓글이며, 주체가 셀럽이 아닐 경우 유저가 내 댓글에 답글을 남겼음
                    .cast { auth.parent()?.let { without(auth.skipNotificationAuthority).without(auth.celebAuthority).also { fanmeet.userSubcommentMyComment.send(auth.userId()!!, auth.parentOwner()!!, auth.comment()!!) } } }
                    .end()

    @Transactional
    @PostMapping("/community/celebfeed/comment/edit")
    fun celebFeedEditComment(@Valid dto: CelebFeedEditCommentRequestDto) =
            auth.communityOwnerFromComment(dto.comment).celebFeedFromComment(dto.comment).comment(dto.comment).begin(this, dto, CelebFeedEditComment())
                    .with(auth.commentWriterAuthority).without(auth.blockedByAuthority)
                    .or(
                            { with(auth.membershipTierPrivilege).with(auth.subscriberAuthority) },
                            { with(auth.scrappedPrivilege) }
                    )
                    .handle().end()

    @Transactional
    @PostMapping("/community/celebfeed/comment/delete")
    fun celebFeedDeleteComment(@RequestBody @Valid dto: CelebFeedDeleteCommentRequestDto) =
            auth.communityOwnerFromComment(dto.comment).celebFeedFromComment(dto.comment).comment(dto.comment).begin(this, dto, CelebFeedDeleteComment())
                    .or(
                            { without(auth.communityOwnershipAuthority).with(auth.commentWriterAuthority).without(auth.blockedByAuthority).with(auth.membershipTierPrivilege).with(auth.subscriberAuthority) },
                            { without(auth.communityOwnershipAuthority).with(auth.commentWriterAuthority).without(auth.blockedByAuthority).with(auth.scrappedPrivilege) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle().end()

    @Transactional
    @PostMapping("/community/celebfeed/comment/like")
    fun celebFeedLikeComment(@RequestBody @Valid dto: CelebFeedLikeCommentRequestDto) =
            auth.communityOwnerFromComment(dto.comment).celebFeedFromComment(dto.comment).commentOwnerFromComment(dto.comment).comment(dto.comment).begin(this, dto, CelebFeedLikeComment())
                    .or(
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.membershipTierPrivilege) },
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.scrappedPrivilege) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle()
                    .cast { without(auth.skipNotificationAuthority).with(auth.celebAuthority).also { fanmeet.celebLikeComment.send(auth.userId()!!, auth.commentOwner()!!, auth.comment()!!) } }
//                    .cast { without(auth.skipNotificationAuthority).without(auth.celebAuthority).also { fanmeet.userLikeMyComment.send(auth.userId()!!, auth.commentOwner()!!, auth.comment()!!) } }
                    .end()

    @Transactional
    @PostMapping("/community/celebfeed/comment/unlike")
    fun celebFeedUnlikeComment(@RequestBody @Valid dto: CelebFeedUnlikeCommentRequestDto) =
            auth.communityOwnerFromComment(dto.comment).celebFeedFromComment(dto.comment).begin(this, dto, CelebFeedUnlikeComment())
                    .or(
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.membershipTierPrivilege) },
                            { without(auth.communityOwnershipAuthority).without(auth.blockedByAuthority).with(auth.scrappedPrivilege) },
                            { with(auth.communityOwnershipAuthority) }
                    )
                    .handle().end()

    @Transactional
    @PostMapping("/community/celebfeed/editcategory")
    fun celebFeedEditCategory(@RequestBody @Valid dto: CelebFeedEditCategoryRequestDto) =
            auth.begin(this, dto, CelebFeedEditCategory())
                    .with(auth.celebAuthority)
                    .handle().end()
}