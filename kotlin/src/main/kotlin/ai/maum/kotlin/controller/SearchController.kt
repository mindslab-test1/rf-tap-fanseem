package ai.maum.kotlin.controller

import ai.maum.kotlin.controller.impl.search.DoSearch
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.http.dto.search.SearchRequestDto
import org.slf4j.LoggerFactory
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
class SearchController(val fanmeet: Fanmeet) {
    private val logger = LoggerFactory.getLogger(this.javaClass)
    private val auth = fanmeet.authorization

    fun begin(controller: Any, dto: Any?, method: HandlerType) = auth.begin(controller, dto, method)
    fun begin(controller: Any, method: HandlerType) = auth.begin(controller, method)

    @PostMapping("/search")
    fun doSearch(@RequestBody @Valid dto: SearchRequestDto) = begin(this, dto, DoSearch())
            .handle().end()
}