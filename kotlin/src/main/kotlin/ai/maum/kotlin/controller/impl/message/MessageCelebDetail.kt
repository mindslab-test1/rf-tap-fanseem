package ai.maum.kotlin.controller.impl.message

import ai.maum.kotlin.controller.MessageController
import ai.maum.kotlin.jpa.common.message.MessageFile
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.message.MessageCelebDetailItemDto
import ai.maum.kotlin.model.http.dto.message.MessageCelebDetailRequestDto
import ai.maum.kotlin.model.http.dto.message.MessageCelebDetailResponseDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException
import java.time.Instant

class MessageCelebDetail : HandlerType {
    var messageController: MessageController? = null
    var messageCelebDetailRequestDto: MessageCelebDetailRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = messageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val userMembershipHistoryRepository = fanmeet.userMembershipHistoryRepository
        val receivedMessageRepository = fanmeet.receivedMessageRepository
        val dto = messageCelebDetailRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val userMembership = userMembershipHistoryRepository.findTopByUserIdAndMembershipCelebAndActiveIsTrueOrderByExpirationDateDesc(userId, dto.celeb!!)
        var userAccessLevel = 0L
        userMembership?.let {
            if (userMembership.status != UserMembershipHistory.Status.TO_BE_EXPIRED && userMembership.status != UserMembershipHistory.Status.NORMAL)
                return@let

            userAccessLevel = userMembership.membership?.tier ?: 0
        }

        val from = dto.from ?: Instant.now()
        val receivedMessages = receivedMessageRepository.findTop20ByWhomAndWhatWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(
                userId, dto.celeb!!, from
        )
        val ttsRepository = fanmeet.ttsRepository
        val response = mutableListOf<MessageCelebDetailItemDto>()
        for (message in receivedMessages) {
            val detailItem = MessageCelebDetailItemDto()

//            val isTts = message.what?.file?.what == MessageFile.FileType.TTS
            val isTts = message.what?.tts != null
            if(isTts)
                detailItem.ttsUrl = ttsRepository.findByIdAndActiveIsTrue(message.what?.tts?.id!!)?.url?.mediaUrl()

//            detailItem.ttsUrl = if (isTts) message.what?.file?.url else null
            detailItem.isTts = isTts
            detailItem.text = message.what?.text
            detailItem.createdTime = message.created

            response.add(detailItem)
        }

        val responseBody = MessageCelebDetailResponseDto()
        responseBody.messages = response

        return ResponseEntity.ok(responseBody)
    }

    override fun setController(controller: Any) {
        messageController = controller as MessageController
    }

    override fun setDto(dto: Any) {
        messageCelebDetailRequestDto = dto as MessageCelebDetailRequestDto
    }
}