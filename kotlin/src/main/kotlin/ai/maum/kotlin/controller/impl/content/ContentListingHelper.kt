package ai.maum.kotlin.controller.impl.content

import ai.maum.kotlin.jpa.common.content.Content
import ai.maum.kotlin.jpa.common.content.ContentRepository
import org.springframework.data.domain.Sort
import java.time.Instant

class ContentListingHelper(val contentRepository: ContentRepository) {
    fun listFeeds(get: Long, celeb: Long, created: Instant, sort: Sort) : List<Content>? {
        return when (get) {
            1L -> contentRepository.findTop1ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            2L -> contentRepository.findTop2ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            3L -> contentRepository.findTop3ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            4L -> contentRepository.findTop4ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            5L -> contentRepository.findTop5ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            6L -> contentRepository.findTop6ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            7L -> contentRepository.findTop7ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            8L -> contentRepository.findTop8ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            9L -> contentRepository.findTop9ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            10L -> contentRepository.findTop10ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            11L -> contentRepository.findTop11ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            12L -> contentRepository.findTop12ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            13L -> contentRepository.findTop13ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            14L -> contentRepository.findTop14ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            15L -> contentRepository.findTop15ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            16L -> contentRepository.findTop16ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            17L -> contentRepository.findTop17ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            18L -> contentRepository.findTop18ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            19L -> contentRepository.findTop19ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            20L -> contentRepository.findTop20ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            21L -> contentRepository.findTop21ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            22L -> contentRepository.findTop22ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            23L -> contentRepository.findTop23ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            24L -> contentRepository.findTop24ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            25L -> contentRepository.findTop25ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            26L -> contentRepository.findTop26ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            27L -> contentRepository.findTop27ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            28L -> contentRepository.findTop28ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            29L -> contentRepository.findTop29ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            30L -> contentRepository.findTop30ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
            else -> contentRepository.findTop1ByWhoAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, created, sort)
        }
    }

    fun listFeedsWith(get: Long, celeb: Long, category: Long, created: Instant, sort: Sort) : List<Content>? {
        return when (get) {
            1L -> contentRepository.findTop1ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            2L -> contentRepository.findTop2ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            3L -> contentRepository.findTop3ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            4L -> contentRepository.findTop4ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            5L -> contentRepository.findTop5ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            6L -> contentRepository.findTop6ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            7L -> contentRepository.findTop7ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            8L -> contentRepository.findTop8ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            9L -> contentRepository.findTop9ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            10L -> contentRepository.findTop10ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            11L -> contentRepository.findTop11ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            12L -> contentRepository.findTop12ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            13L -> contentRepository.findTop13ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            14L -> contentRepository.findTop14ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            15L -> contentRepository.findTop15ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            16L -> contentRepository.findTop16ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            17L -> contentRepository.findTop17ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            18L -> contentRepository.findTop18ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            19L -> contentRepository.findTop19ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            20L -> contentRepository.findTop20ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            21L -> contentRepository.findTop21ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            22L -> contentRepository.findTop22ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            23L -> contentRepository.findTop23ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            24L -> contentRepository.findTop24ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            25L -> contentRepository.findTop25ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            26L -> contentRepository.findTop26ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            27L -> contentRepository.findTop27ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            28L -> contentRepository.findTop28ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            29L -> contentRepository.findTop29ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            30L -> contentRepository.findTop30ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
            else -> contentRepository.findTop1ByWhoAndCategoryAndCreatedBeforeAndActiveIsTrueOrderByCreatedDesc(celeb, category, created, sort)
        }
    }
}