package ai.maum.kotlin.controller.impl.celebfeed

import ai.maum.kotlin.controller.CelebFeedController
import ai.maum.kotlin.jpa.common.like.Like
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.celebfeed.CelebFeedLikeRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class CelebFeedLike : HandlerType {
    var celebFeedController: CelebFeedController? = null
    var celebFeedLikeRequestDto: CelebFeedLikeRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = celebFeedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val likeRepository = fanmeet.likeRepository
        val celebFeedRepository = fanmeet.celebFeedRepository
        val dto = celebFeedLikeRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")
        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val celebFeed = celebFeedRepository.findByIdAndActiveIsTrue(dto.celebFeed!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "CelebFeed")

        val duplicates = likeRepository.findAllByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId, dto.celebFeed!!, Like.TargetType.CELEB_FEED)
        if (duplicates.isNotEmpty()) {
            val reducedExceptFirst = duplicates.sortedByDescending { it.created!! }.toMutableList().also { it.removeAt(0) }
            if (reducedExceptFirst.isNotEmpty())
                likeRepository.deleteAll(reducedExceptFirst)
        }

        val unliked = likeRepository.findByUserIdAndTargetIdAndTargetTypeAndActiveIsFalse(userId, dto.celebFeed!!, Like.TargetType.CELEB_FEED)
        val exists = likeRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(userId, dto.celebFeed!!, Like.TargetType.CELEB_FEED)

        if (exists) {
            fanmeet.userInfoFactory.`object`.notification = false
            if (unliked != null)
                likeRepository.delete(unliked)
            return ResponseEntity.ok(Unit)
        }

        unliked?.let {
            fanmeet.userInfoFactory.`object`.notification = false

            celebFeed.likeCount = celebFeed.likeCount!! + 1

            unliked.active = true

            celebFeedRepository.save(celebFeed)
            likeRepository.save(unliked)

            return ResponseEntity.ok(Unit)
        }

        if (!exists) {
            celebFeed.likeCount = celebFeed.likeCount!! + 1

            val like = Like()
            like.targetId = dto.celebFeed!!
            like.targetType = Like.TargetType.CELEB_FEED
            like.userId = userId

            celebFeedRepository.save(celebFeed)
            likeRepository.save(like)

            return ResponseEntity.ok(Unit)
        }

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        celebFeedController = controller as CelebFeedController
    }

    override fun setDto(dto: Any) {
        celebFeedLikeRequestDto = dto as CelebFeedLikeRequestDto
    }
}