package ai.maum.kotlin.controller

import ai.maum.kotlin.controller.impl.admin.*
import ai.maum.kotlin.model.http.dto.admin.*
import org.springframework.core.env.Environment
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import javax.validation.Valid

@RestController
class AdminController(
        val fanmeet: Fanmeet,
        val environment: Environment
) {
    val auth = fanmeet.authorization

    @Transactional
    @PostMapping("/admin/db/initsettings")
    fun initSettings(@Valid @RequestBody dto: InitSettingsRequestDto) =
            auth.begin(this, dto, InitSettings())
                    .handle().end()

    @Transactional
    @PostMapping("/admin/db/checklocation")
    fun checkLocation(@Valid @RequestBody dto: CheckLocationRequestDto) =
            auth.begin(this, dto, CheckLocation())
                    .handle().end()

    @Transactional
    @PostMapping("/admin/db/boundimage")
    fun boundImage(@Valid @RequestBody dto: BoundImageRequestDto) =
            auth.begin(this, dto, BoundImage())
                    .handle().end()
    @Transactional
    @PostMapping("/admin/db/notificationtest")
    fun notificationTest(@Valid @RequestBody dto: NotificationTestRequestDto) =
            auth.begin(this, dto, NotificationTest())
                    .handle().end()
    @Transactional
    @PostMapping("/admin/db/subscribefanmeet")
    fun subscribeFanmeet(@Valid @RequestBody dto: SubscribeFanmeetRequestDto) =
            auth.begin(this, dto, SubscribeFanmeet())
                    .handle().end()
    @Transactional
    @PostMapping("/admin/db/likeuniqueness")
    fun likeUniqueness(@Valid @RequestBody dto: LikeUniquenessRequestDto) =
            auth.begin(this, dto, LikeUniqueness())
                    .handle().end()
}