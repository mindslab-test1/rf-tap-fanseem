package ai.maum.kotlin.controller.impl.feed

import ai.maum.kotlin.controller.FeedController
import ai.maum.kotlin.jpa.common.feed.FeedFile
import ai.maum.kotlin.jpa.common.service.Tts
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.feed.FeedEditRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class FeedEdit : HandlerType {
    var feedController: FeedController? = null
    var feedEditRequestDto: FeedEditRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = feedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val feedRepository = fanmeet.feedRepository
        val feedCategoryRepository = fanmeet.feedCategoryRepository
        val ttsRepository = fanmeet.ttsRepository
        val dto = feedEditRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        // Data range check
        dto.text?.let { if (it.length > 1000) throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto Text") }
        dto.addedPictures?.let { if (it.size > 5) throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto Pictures") }

        // Category check
        dto.category?.let {
            if (dto.category!! > 0)
                feedCategoryRepository.findByIdAndActiveIsTrue(dto.category!!)
                        ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto Category")
            else dto.category = null
        }

        // TTS Ownership check
        val tts = dto.tts?.let { ttsRepository.findByIdAndActiveIsTrue(dto.tts!!) }
        tts?.let {
            if (userId != tts.subscriber || userId != tts.celeb)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "TTS Ownership")
            if (tts.status != Tts.Status.APPROVED && tts.status != Tts.Status.SELF)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "TTS Status")
        }

        // Retrieve Feed
        val feed = feedRepository.findByIdAndActiveIsTrue(feedEditRequestDto!!.feed!!)
                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Feed")

        // Picture Count & Index Check
        var pictureCount = feed.pictureList?.size ?: 0
        dto.removedPictures?.let {
            if (dto.removedPictures!!.size > pictureCount)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto RemovedPictures Size")
            for (index in dto.removedPictures!!)
                if (index >= feed.pictureList!!.size)
                    throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Feed PictureList Size")
            pictureCount -= dto.removedPictures!!.size
        }
        dto.addedPictures?.let {
            pictureCount += dto.addedPictures!!.size
            if (pictureCount > 5)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "PictureCount")
        }

        val updatedPictureList = feed.pictureList?.toMutableList() ?: mutableListOf()
        // Remove Pictures
        dto.removedPictures?.let {
            val descendingOrder = dto.removedPictures!!.sortedDescending()
            for (index in descendingOrder) {
                updatedPictureList.removeAt(index.toInt())
            }
        }

        // Picture Upload
        dto.addedPictures?.let {
            if (dto.addedPictures!!.isEmpty())
                return@let
            val uploadedFiles = feedController!!.fanmeet.mediaUploader.uploadAsyncFeedImage(dto.addedPictures!!)
                    ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "UploadedFiles")
            for (fileName in uploadedFiles) {
                val feedFile = FeedFile()
                feedFile.url = fileName
                feedFile.what = FeedFile.FileType.IMAGE
                updatedPictureList.add(feedFile)
            }
        }

        // Apply Updated Picture List
        feed.pictureList = updatedPictureList

        // Video Upload & Apply Updated
        dto.video?.let {
            dto.thumbnail ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Thumbnail")

            val thumbnailFile = FeedFile()
            thumbnailFile.url = fanmeet.mediaUploader.uploadAsync(dto.thumbnail!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Uploading Thumbnail")
            thumbnailFile.what = FeedFile.FileType.IMAGE
            feed.thumbnail = thumbnailFile

            val videoFile = FeedFile()
            videoFile.url = fanmeet.mediaUploader.uploadAsync(dto.video!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Uploading Video")
            videoFile.what = FeedFile.FileType.VIDEO
            feed.video = videoFile
        }

        dto.category?.let { feed.category = dto.category }
        dto.text?.let { feed.text = dto.text }
        dto.youtube?.let { feed.youtube = dto.youtube }
        dto.tts?.let { feed.tts = tts }

        feedRepository.save(feed)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        feedController = controller as FeedController
    }

    override fun setDto(dto: Any) {
        feedEditRequestDto = dto as FeedEditRequestDto
    }
}