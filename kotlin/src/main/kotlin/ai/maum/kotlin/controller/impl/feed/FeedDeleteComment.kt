package ai.maum.kotlin.controller.impl.feed

import ai.maum.kotlin.controller.FeedController
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.feed.FeedDeleteCommentRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.server.ResponseStatusException

class FeedDeleteComment : HandlerType {
    var feedController: FeedController? = null
    var feedDeleteCommentRequestDto: FeedDeleteCommentRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = feedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val commentRepository = fanmeet.commentRepository
        val feedRepository = fanmeet.feedRepository
        val dto = feedDeleteCommentRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val comment = commentRepository.findByIdAndActiveIsTrue(dto.comment!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Comment")
        val feed = feedRepository.findByIdAndActiveIsTrue(comment.feed!!)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Feed")

        feed.commentCount = feed.commentCount!! - 1
        comment.active = false

        feedRepository.save(feed)
        commentRepository.save(comment)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        feedController = controller as FeedController
    }

    override fun setDto(dto: Any) {
        feedDeleteCommentRequestDto = dto as FeedDeleteCommentRequestDto
    }
}