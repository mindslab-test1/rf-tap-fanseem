package ai.maum.kotlin.controller.impl.auth

import ai.maum.kotlin.controller.AuthController
import ai.maum.kotlin.jpa.authentication.google.GoogleUserToken
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.auth.GoogleLoginDto
import com.google.api.client.extensions.appengine.http.UrlFetchTransport
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier
import com.google.api.client.json.jackson2.JacksonFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException
import java.util.*

class GoogleLogin(private val authController: AuthController, private val googleLoginDto: GoogleLoginDto) : HandlerType {
    override operator fun invoke(): ResponseType {
        // ID token verification
        val verifier = GoogleIdTokenVerifier.Builder(UrlFetchTransport.getDefaultInstance(), JacksonFactory()) //
                .setAudience(Collections.singletonList("612983419070-7dac4sjcpjhicu0ro1e9u3a6sb6bli01.apps.googleusercontent.com")) //
                .build()
        val idToken = verifier.verify(googleLoginDto.idToken) ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST)
        val payload = idToken.payload ?: throw ResponseStatusException(HttpStatus.EXPECTATION_FAILED)

        // Check existence in DB and retrieve user id(row id)
        val user = authController.userRepository.findByGoogleUserIdAndActiveIsTrue(payload.subject)
        val userId: Long = user?.id ?: throw ResponseStatusException(HttpStatus.UNAUTHORIZED)

        // Duplication check (deviceId)
        DeviceIdDuplicationHelper(authController, googleLoginDto.deviceId!!)()

        // Token registration
        val token = GoogleUserToken()
        token.idTokenString = googleLoginDto.idToken!!
        token.googleUserId = payload.subject
        token.userId = userId
        token.deviceId = googleLoginDto.deviceId
        authController.googleUserTokenRepository.save(token)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        // Do nothing
    }

    override fun setDto(dto: Any) {
        // Do nothing
    }
}