package ai.maum.kotlin.controller.impl.celebfeed

import ai.maum.kotlin.controller.CelebFeedController
import ai.maum.kotlin.jpa.common.celebfeed.CelebFeed
import ai.maum.kotlin.jpa.common.celebfeed.CelebFeedFile
import ai.maum.kotlin.jpa.common.service.Tts
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.celebfeed.CelebFeedWriteRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class CelebFeedWrite : HandlerType {
    var celebFeedController: CelebFeedController? = null
    var celebFeedWriteRequestDto: CelebFeedWriteRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = celebFeedController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val celebFeedRepository = fanmeet.celebFeedRepository
        val celebFeedCategoryRepository = fanmeet.celebFeedCategoryRepository
        val ttsRepository = fanmeet.ttsRepository
        val dto = celebFeedWriteRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        var mediaExclusion = 0
        dto.pictures?.let { mediaExclusion++ }
        dto.video?.let { mediaExclusion++ }
        dto.youtube?.let { mediaExclusion++ }
        if (mediaExclusion > 1)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "MediaExclusion")

        // Data range check
        dto.text?.let { if (it.length > 10000) throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto Text") }
        dto.pictures?.let { if (it.size > 5) throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto Pictures") }

        // Category check
        if (dto.category!! > 0)
            celebFeedCategoryRepository.findByIdAndActiveIsTrue(dto.category!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto Category")
        else dto.category = 0

        // Access level validity check
        dto.accessLevel?.let {
            if (it <= 0)
                return@let

            val minimumMembership = fanmeet.membershipRepository.findByCelebAndTierAndActiveIsTrue(userId, it)
                    ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST)
        }

        // TTS Ownership check
        val tts = dto.tts?.let {
            ttsRepository.findByIdAndActiveIsTrue(dto.tts!!)
                    ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto TTS")
        }
        tts?.let {
            if (userId != tts.subscriber || userId != tts.celeb)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "TTS Ownership")
            if (tts.status != Tts.Status.APPROVED && tts.status != Tts.Status.SELF)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "TTS Status")
        }

        // Uploading media files
        var fileNameList = listOf<String>()
        dto.pictures?.let {
            if (dto.pictures!!.isNotEmpty())
                fileNameList = fanmeet.mediaUploader.uploadAsyncFeedImage(dto.pictures!!.toList())
                        ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Uploading Pictures")
        }
        var videoName: String? = null
        var thumbnailName: String? = null
        dto.video?.let {
            dto.thumbnail ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Thumbnail")
            thumbnailName = fanmeet.mediaUploader.uploadAsync(dto.thumbnail!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Uploading Thumbnail")
            videoName = fanmeet.mediaUploader.uploadAsync(dto.video!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Uploading Video")
        }

        // Write feed
        val article = CelebFeed()
        article.who = userId
        article.commentCount = 0
        article.likeCount = 0
        article.scrapCount = 0
        article.text = dto.text ?: ""
        article.category = dto.category
        article.youtube = dto.youtube
        article.tts = tts
        article.accessLevel = dto.accessLevel ?: 0

        article.pictureList = mutableListOf()
        if (fileNameList.isNotEmpty()) {
            for (fileName in fileNameList) {
                val celebFeedFile = CelebFeedFile()
                celebFeedFile.url = fileName
                celebFeedFile.what = CelebFeedFile.FileType.IMAGE
                celebFeedFile.who = userId
                article.pictureList!!.add(celebFeedFile)
            }
        }

        videoName?.let {
            val videoFile = CelebFeedFile()
            videoFile.url = videoName
            videoFile.what = CelebFeedFile.FileType.VIDEO
            videoFile.who = userId
            article.video = videoFile
        }
        thumbnailName?.let {
            val thumbnailFile = CelebFeedFile()
            thumbnailFile.url = thumbnailName
            thumbnailFile.what = CelebFeedFile.FileType.IMAGE
            thumbnailFile.who = userId
            article.thumbnail = thumbnailFile
        }

        val saved = celebFeedRepository.save(article)
        fanmeet.authorization.feed(saved.id!!)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        celebFeedController = controller as CelebFeedController
    }

    override fun setDto(dto: Any) {
        celebFeedWriteRequestDto = dto as CelebFeedWriteRequestDto
    }
}