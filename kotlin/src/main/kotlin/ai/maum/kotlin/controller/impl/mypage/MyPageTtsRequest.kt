package ai.maum.kotlin.controller.impl.mypage

import ai.maum.kotlin.controller.MyPageController
import ai.maum.kotlin.jpa.common.service.Tts
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.mypage.MyPageTtsRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class MyPageTtsRequest : HandlerType {
    var myPageController: MyPageController? = null
    var myPageTtsRequestDto: MyPageTtsRequestDto? = null

    override fun invoke(): ResponseType {
        val fanmeet = myPageController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val ttsRepository = fanmeet.ttsRepository
        val dto = myPageTtsRequestDto
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val originalData = ttsRepository.findByIdAndActiveIsTrue(dto.id!!) ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "original data")

        originalData.status = Tts.Status.APPEALED
        val ttsSaved = ttsRepository.save(originalData)
        if(ttsSaved.id != dto.id) throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "data consistency")

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        myPageController = controller as MyPageController
    }

    override fun setDto(dto: Any) {
        myPageTtsRequestDto = dto as MyPageTtsRequestDto
    }
}