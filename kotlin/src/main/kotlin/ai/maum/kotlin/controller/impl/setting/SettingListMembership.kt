package ai.maum.kotlin.controller.impl.setting

import ai.maum.kotlin.controller.SettingController
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.setting.SettingListMembershipItemDto
import ai.maum.kotlin.model.http.dto.setting.SettingListMembershipResponseDto
import ai.maum.kotlin.model.media.mediaUrl
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class SettingListMembership: HandlerType {
    var settingController: SettingController? = null

    override fun invoke(): ResponseType {
        val fanmeet = settingController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val userRepository = fanmeet.userRepository
        val userMembershipHistoryRepository = fanmeet.userMembershipHistoryRepository

        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        val history = userMembershipHistoryRepository.findByUserIdAndActiveIsTrue(userId).toMutableList()
        history.removeAll {it.status== UserMembershipHistory.Status.EXPIRED}

        val response = mutableListOf<SettingListMembershipItemDto>()
        for (membership in history) {
            val membershipItem = SettingListMembershipItemDto()
            val celebName = userRepository.findByIdAndActiveIsTrue(membership.membership!!.celeb!!)?.name
                    ?: continue

            membershipItem.badgeUrl = membership.membership!!.badgeImageUrl?.mediaUrl()
            membershipItem.celebName = celebName
            membershipItem.expirationDate = membership.expirationDate
            membershipItem.membershipName = membership.membership!!.name
            membershipItem.productId = membership.membership!!.productId
            membershipItem.duration = null // TODO

            response.add(membershipItem)
        }

        val responseBody = SettingListMembershipResponseDto()
        responseBody.activeList = response

        return ResponseEntity.ok(responseBody)
    }

    override fun setController(controller: Any) {
        settingController = controller as SettingController
    }

    override fun setDto(dto: Any) {
        // Do nothing
    }
}