package ai.maum.kotlin.controller.impl.content

import ai.maum.kotlin.controller.ContentController
import ai.maum.kotlin.jpa.common.content.Content
import ai.maum.kotlin.jpa.common.content.ContentFile
import ai.maum.kotlin.jpa.common.service.Tts
import ai.maum.kotlin.model.authorization.fanmeet.HandlerType
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.http.dto.content.ContentWriteRequestDto
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.server.ResponseStatusException

class ContentWrite : HandlerType {
    var contentController: ContentController? = null
    var contentWriteRequestDto: ContentWriteRequestDto? = null

    override operator fun invoke(): ResponseType {
        val fanmeet = contentController?.fanmeet
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Fanmeet")
        val contentRepository = fanmeet.contentRepository
        val contentCategoryRepository = fanmeet.contentCategoryRepository
        val ttsRepository = fanmeet.ttsRepository
        val dto = contentWriteRequestDto ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto")

        val userId = fanmeet.userInfoFactory.`object`.userId
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "UserId")

        var mediaExclusion = 0
        dto.pictures?.let { mediaExclusion++ }
        dto.video?.let { mediaExclusion++ }
        dto.youtube?.let { mediaExclusion++ }
        if (mediaExclusion > 1)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "MediaExclusion")

        // Data range check
        dto.text?.let { if (it.length > 10000) throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto Text") }
        dto.pictures?.let { if (it.size > 5) throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto Pictures") }

        // Category check
        if (dto.category!! > 0)
            contentCategoryRepository.findByIdAndActiveIsTrue(dto.category!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Dto Category")
        else dto.category = 0

        // Access level validity check
        if (dto.accessLevel!! < 0)
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid Access Level")
//        val minimumMembership = fanmeet.membershipRepository.findByCelebAndTierAndActiveIsTrue(userId, dto.accessLevel!!)
//                ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "No Membership With Given Access level")

        // TTS Ownership check
        val tts = dto.tts?.let {
            ttsRepository.findByIdAndActiveIsTrue(dto.tts!!)
                    ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Dto TTS")
        }
        tts?.let {
            if (userId != tts.subscriber || userId != tts.celeb)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "TTS Ownership")
            if (tts.status != Tts.Status.APPROVED && tts.status != Tts.Status.SELF)
                throw ResponseStatusException(HttpStatus.BAD_REQUEST, "TTS Status")
        }

        // Uploading media files
        var fileNameList = listOf<Pair<String, String>>()
        dto.pictures?.let {
            if (dto.pictures!!.isNotEmpty())
                fileNameList = fanmeet.mediaUploader.uploadAsyncFeedImageWithBlurred(dto.pictures!!.toList())
                        ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Uploading Pictures")
        }
        var videoName: String? = null
        var thumbnailName: String? = null
        dto.video?.let {
            dto.thumbnail ?: throw ResponseStatusException(HttpStatus.BAD_REQUEST, "Thumbnail")
            thumbnailName = fanmeet.mediaUploader.uploadAsync(dto.thumbnail!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Uploading Thumbnail")
            videoName = fanmeet.mediaUploader.uploadAsync(dto.video!!)
                    ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Uploading Video")
        }

        // Write feed
        val article = Content()
        article.who = userId
        article.title = dto.title
        article.commentCount = 0
        article.likeCount = 0
        article.scrapCount = 0
        article.text = dto.text ?: ""
        article.category = dto.category
        article.youtube = dto.youtube
        article.tts = tts
        article.accessLevel = dto.accessLevel!!

        article.pictureList = mutableListOf()
        if (fileNameList.isNotEmpty()) {
            for (fileName in fileNameList) {
                val contentFile = ContentFile()
                contentFile.url = fileName.first
                contentFile.blurredUrl = fileName.second
                contentFile.what = ContentFile.FileType.IMAGE
                contentFile.who = userId
                article.pictureList!!.add(contentFile)
            }
        }

        videoName?.let {
            val videoFile = ContentFile()
            videoFile.url = videoName
            videoFile.what = ContentFile.FileType.VIDEO
            videoFile.who = userId
            article.video = videoFile
        }
        thumbnailName?.let {
            val thumbnailFile = ContentFile()
            thumbnailFile.url = thumbnailName
            thumbnailFile.what = ContentFile.FileType.IMAGE
            thumbnailFile.who = userId
            article.thumbnail = thumbnailFile
        }

        val saved = contentRepository.save(article)
        fanmeet.authorization.content(saved.id!!)

        return ResponseEntity.ok(Unit)
    }

    override fun setController(controller: Any) {
        contentController = controller as ContentController
    }

    override fun setDto(dto: Any) {
        contentWriteRequestDto = dto as ContentWriteRequestDto
    }
}