package ai.maum.kotlin.model.http.dto.block

data class BlockUserRequest (
        val who: Long,
        val whom: Long
)