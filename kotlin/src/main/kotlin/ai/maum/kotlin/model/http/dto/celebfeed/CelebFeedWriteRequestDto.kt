package ai.maum.kotlin.model.http.dto.celebfeed

import org.springframework.web.multipart.MultipartFile
import javax.validation.constraints.NotNull

data class CelebFeedWriteRequestDto(
        @field:NotNull
        var category: Long? = null,

        var pictures: List<MultipartFile>? = null,
        var video: MultipartFile? = null,
        var thumbnail: MultipartFile? = null,
        var youtube: String? = null,
        var tts: Long? = null,

        var text: String? = null,

        var accessLevel: Long? = null
)