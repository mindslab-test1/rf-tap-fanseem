package ai.maum.kotlin.model.http.dto.home

data class HomeCelebListDto (
        val allCelebList: List<HomeSubscribingCelebDto>?,
        val subscribingCelebList: List<HomeSubscribingCelebDto>?
)