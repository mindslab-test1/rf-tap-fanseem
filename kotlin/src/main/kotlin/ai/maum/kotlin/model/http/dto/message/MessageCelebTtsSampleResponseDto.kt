package ai.maum.kotlin.model.http.dto.message

import javax.validation.constraints.NotNull

data class MessageCelebTtsSampleResponseDto (
        @field:NotNull
        var url: String? = null,

        @field:NotNull
        var id: Long? = null
)