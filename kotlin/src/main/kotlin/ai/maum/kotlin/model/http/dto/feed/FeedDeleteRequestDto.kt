package ai.maum.kotlin.model.http.dto.feed

import javax.validation.constraints.NotNull

data class FeedDeleteRequestDto(
        @field:NotNull
        var feed: Long? = null
)