package ai.maum.kotlin.model.http.dto.misc

import javax.validation.constraints.NotNull

data class MiscVersionDto(
        @field:NotNull
        var version: String? = "1.0.0"
)