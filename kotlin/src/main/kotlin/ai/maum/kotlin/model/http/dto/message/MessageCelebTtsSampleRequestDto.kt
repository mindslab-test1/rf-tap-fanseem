package ai.maum.kotlin.model.http.dto.message

import javax.validation.constraints.NotNull

data class MessageCelebTtsSampleRequestDto (
        @field:NotNull
        var text: String? = null
)