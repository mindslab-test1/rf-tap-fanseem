package ai.maum.kotlin.model.http.dto.service

import javax.validation.constraints.NotNull

data class IosReceiptVerifyResponse (
        @field:NotNull
        var environment: String,
        @field: NotNull
        var status: Int? = 0,
        @field: NotNull
        var receipt: IosReceipt,
        @field: NotNull
        var latestReceiptInfo: List<IosNotiReceiptInfo>,
        @field: NotNull
        var latestReceipt: String
)