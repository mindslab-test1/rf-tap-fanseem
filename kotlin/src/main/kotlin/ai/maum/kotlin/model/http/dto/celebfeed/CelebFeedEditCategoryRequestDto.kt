package ai.maum.kotlin.model.http.dto.celebfeed

data class CelebFeedEditCategoryRequestDto (
        var added: List<String>? = null,
        var removed: List<Long>? = null
)
