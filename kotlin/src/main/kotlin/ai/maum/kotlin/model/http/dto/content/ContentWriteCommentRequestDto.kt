package ai.maum.kotlin.model.http.dto.content

import org.springframework.web.multipart.MultipartFile
import javax.validation.constraints.NotNull

data class ContentWriteCommentRequestDto (
        @field:NotNull
        var celeb: Long? = null,

        @field:NotNull
        var content: Long? = null,

        var parent: Long? = null,

        @field:NotNull
        var text: String? = null,

        var picture: MultipartFile? = null,
        var tts: Long? = null
)