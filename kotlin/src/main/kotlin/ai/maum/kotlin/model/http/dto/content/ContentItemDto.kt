package ai.maum.kotlin.model.http.dto.content

import java.time.Instant
import javax.validation.constraints.NotNull

data class ContentItemDto(
        @field:NotNull
        var content: Long? = null,

        @field:NotNull
        var title: String? = null,

        @field:NotNull
        var text: String? = null,

        @field:NotNull
        var feedOwner: Long? = null,

        @field:NotNull
        var feedOwnerName: String? = null,

        var feedOwnerProfileImage: String? = null,

        @field:NotNull
        var accessLevelBadgeImageUrl: String? = null,

        @field:NotNull
        var viewCount: Long? = null,

        @field:NotNull
        var commentCount: Long? = null,

        @field:NotNull
        var likeCount: Long? = null,

        @field:NotNull
        var scrapCount: Long? = null,

        @field:NotNull
        var category: Long? = null,
        var categoryName: String? = null,

        @field:NotNull
        var pictures: MutableList<String>? = null,
        var video: String? = null,
        var thumbnail: String? = null,
        var youtube: String? = null,
        var tts: String? = null,

        @field:NotNull
        var updated: Instant? = null,

        var liked: Boolean? = null,
        var scraped: Boolean? = null,
        var blocked: Boolean? = null,
        var restricted: Boolean? = null
)