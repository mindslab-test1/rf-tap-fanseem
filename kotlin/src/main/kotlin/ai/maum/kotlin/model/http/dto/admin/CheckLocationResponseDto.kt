package ai.maum.kotlin.model.http.dto.admin

import javax.validation.constraints.NotNull

data class CheckLocationResponseDto (
        @field:NotNull
        var profiles: List<String>? = null
)