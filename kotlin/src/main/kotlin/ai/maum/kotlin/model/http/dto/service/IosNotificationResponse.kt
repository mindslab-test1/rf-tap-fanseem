package ai.maum.kotlin.model.http.dto.service

import javax.validation.constraints.NotNull

data class IosNotificationResponse(
        @field: NotNull
        var unifiedReceipt: IosUnifiedReceipt? = null,
        @field:NotNull
        var environment: String? = null,
        @field:NotNull
        var autoRenewStatus: String? = null,
        @field:NotNull
        var bvrs: String? = null,
        @field:NotNull
        var autoRenewProductId: String? = null,
        @field:NotNull
        var notificationType: String? = null,
        @field:NotNull
        var expirationIntent: Int?
)
