package ai.maum.kotlin.model.http.dto.misc

import javax.validation.constraints.NotNull

data class MiscNameDuplicationRequestDto(
        @field:NotNull
        var name: String? = null
)