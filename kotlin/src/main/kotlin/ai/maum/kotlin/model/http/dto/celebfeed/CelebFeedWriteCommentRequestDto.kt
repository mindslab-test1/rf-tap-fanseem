package ai.maum.kotlin.model.http.dto.celebfeed

import org.springframework.web.multipart.MultipartFile
import javax.validation.constraints.NotNull

data class CelebFeedWriteCommentRequestDto (
        @field:NotNull
        var celeb: Long? = null,

        @field:NotNull
        var celebFeed: Long? = null,

        var parent: Long? = null,

        @field:NotNull
        var text: String? = null,

        var picture: MultipartFile? = null,
        var tts: Long? = null
)