package ai.maum.kotlin.model.authorization.general

import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

// Implementation of this interface should be @Component and have "request" @Scope.
interface AuthorizationContext<T> {
    var method: HandlerMethod<T>?
    var response: T?
    var subject: Subject<T>
    var lazyLoader: ((Unit) -> Unit)?

    fun with(authority: Authority<T>): AuthorizationContext<T> =
            if (subject.authorized(authority)) this
            else throw ResponseStatusException(HttpStatus.UNAUTHORIZED, "Subject Not Authorized With $authority")

    fun without(authority: Authority<T>): AuthorizationContext<T> =
            if (!subject.authorized(authority)) this
            else throw ResponseStatusException(HttpStatus.UNAUTHORIZED, "Subject Authorized With $authority")

    fun with(privilege: Privilege<T>): AuthorizationContext<T> =
            if (subject.authorized(privilege)) this
            else throw ResponseStatusException(HttpStatus.UNAUTHORIZED, "Subject Not Authorized With $privilege")

    fun without(privilege: Privilege<T>): AuthorizationContext<T> =
            if (!subject.authorized(privilege)) this
            else throw ResponseStatusException(HttpStatus.UNAUTHORIZED, "Subject Authorized With $privilege")

    fun with(permission: Permission<T>): AuthorizationContext<T> =
            if (subject.authorized(permission)) this
            else throw ResponseStatusException(HttpStatus.UNAUTHORIZED, "Subject Not Authorized With $permission")

    fun without(permission: Permission<T>): AuthorizationContext<T> =
            if (!subject.authorized(permission)) this
            else throw ResponseStatusException(HttpStatus.UNAUTHORIZED, "Subject Authorized With $permission")

    fun handle(): AuthorizationContext<T> {
        response = method?.invoke()
        lazyLoader?.invoke(Unit)
        return this
    }

    fun or(vararg operand: AuthorizationContext<T>.() -> AuthorizationContext<T>): AuthorizationContext<T> {
        var ok = false
        var nested = ""
        for (fnc in operand) {
            try {
                fnc(this)
                ok = true
            } catch (e: Exception) {
                if (ok) nested += "; "
                nested += e.message
            }
            if (ok) break
        }

        if (!ok)
            throw ResponseStatusException(HttpStatus.UNAUTHORIZED, "OR Failed; $nested")

        return this
    }


    fun cast(function: AuthorizationContext<T>.() -> Unit): AuthorizationContext<T> {
        try {
            function(this)
        } catch (e: Exception) {
            // Do nothing
        }
        return this
    }

    fun end() = response
}