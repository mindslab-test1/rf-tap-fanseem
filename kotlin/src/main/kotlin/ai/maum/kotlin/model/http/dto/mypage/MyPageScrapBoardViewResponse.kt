package ai.maum.kotlin.model.http.dto.mypage

import java.time.Instant
import javax.validation.constraints.NotNull

data class MyPageScrapBoardViewResponse(
        @field:NotNull
        var scrapList: MutableList<Any> ?= null
)

data class ScrapBoardVo(
        var userId: Long ?=null,
        var feedId: Long ?=null,
        var title: String ?=null,
        var viewCount: Long ?=null,
        var userName: String ?=null,
        var profileImg: String ?=null,
        var postTimeStamp: Instant ?=null,
        var category: String ?=null,
        var imgUrl: List<Any> ?= listOf(),
        var videoUrl: String ?=null,
        var text: String ?=null,
        var scrapCount: Long ?=null,
        var isScrap: Boolean ?=null,
        var likeCount: Long ?=null,
        var isLike: Boolean ?=null,
        var commentCount: Long ?=null,
        var canView: Boolean ?=null,
        @field:NotNull
        var scrapFeedKind: ScrapFeedKind? = null
)

enum class ScrapFeedKind{
        USER_FEED,
        CELEB_FEED,
        CONTENT
}