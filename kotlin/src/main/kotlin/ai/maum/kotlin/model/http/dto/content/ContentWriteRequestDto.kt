package ai.maum.kotlin.model.http.dto.content

import org.springframework.web.multipart.MultipartFile
import javax.validation.constraints.NotNull

data class ContentWriteRequestDto(
        @field:NotNull
        var category: Long? = null,

        @field:NotNull
        var title: String? = null,

        var pictures: List<MultipartFile>? = null,
        var video: MultipartFile? = null,
        var thumbnail: MultipartFile? = null,
        var youtube: String? = null,
        var tts: Long? = null,

        var text: String? = null,

        @field:NotNull
        var accessLevel: Long? = null
)
