package ai.maum.kotlin.model.http.dto.celebfeed

import javax.validation.constraints.NotNull

data class CelebFeedScrapRequestDto (
        @field:NotNull
        var scrapBoard: Long? = null,

        @field:NotNull
        var celebFeed: Long? = null
)
