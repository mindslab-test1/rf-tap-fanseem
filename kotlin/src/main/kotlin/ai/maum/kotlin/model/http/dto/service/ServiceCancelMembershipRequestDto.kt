package ai.maum.kotlin.model.http.dto.service

import javax.validation.constraints.NotNull

data class ServiceCancelMembershipRequestDto (
        @field:NotNull
        var celeb: Long? = null,

        @field:NotNull
        var purchaseToken: String? = null,

        @field:NotNull
        var productId: String? = null
)