package ai.maum.kotlin.model.http.dto.search

data class SearchResponseDto (
        val results: MutableList<SearchResultDto>
)