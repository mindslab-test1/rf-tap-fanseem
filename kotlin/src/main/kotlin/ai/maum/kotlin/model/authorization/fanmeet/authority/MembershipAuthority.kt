package ai.maum.kotlin.model.authorization.fanmeet.authority

import ai.maum.kotlin.jpa.common.celeb.CelebRepository
import ai.maum.kotlin.jpa.common.user.UserRepository
import ai.maum.kotlin.model.authorization.fanmeet.ContextProperty
import ai.maum.kotlin.model.authorization.fanmeet.FanmeetAuthorizationContext
import ai.maum.kotlin.model.authorization.fanmeet.Requires
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.authorization.general.Authority
import ai.maum.kotlin.model.authorization.general.AuthorizationContext
import ai.maum.kotlin.model.authorization.general.Permission
import org.springframework.stereotype.Component

@Requires(ContextProperty.COMMUNITY_OWNER)
@Component
class MembershipAuthority(
        private val userRepository: UserRepository,
        private val celebRepository: CelebRepository
) : Authority<ResponseType> {
    override val permissions: List<Permission<ResponseType>> = listOf()

    override fun load(authorizationContext: AuthorizationContext<ResponseType>) {
        var context = authorizationContext as FanmeetAuthorizationContext

        // 특정 셀럽에 대한 멤버십만을 가져온다

        TODO("아몰라")
    }
}