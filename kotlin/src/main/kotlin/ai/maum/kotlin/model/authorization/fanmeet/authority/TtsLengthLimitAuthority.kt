package ai.maum.kotlin.model.authorization.fanmeet.authority

import ai.maum.kotlin.KotlinApplication
import ai.maum.kotlin.jpa.common.service.Tts
import ai.maum.kotlin.jpa.common.service.TtsRepository
import ai.maum.kotlin.jpa.membership.MembershipRepository
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.jpa.membership.UserMembershipHistoryRepository
import ai.maum.kotlin.model.authorization.fanmeet.ContextProperty
import ai.maum.kotlin.model.authorization.fanmeet.FanmeetAuthorizationContext
import ai.maum.kotlin.model.authorization.fanmeet.Requires
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.authorization.general.Authority
import ai.maum.kotlin.model.authorization.general.AuthorizationContext
import ai.maum.kotlin.model.authorization.general.Permission
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.time.Instant

@Requires(ContextProperty.COMMUNITY_OWNER)
@Requires(ContextProperty.TTS_LENGTH)
@Component
class TtsLengthLimitAuthority(
        private val membershipRepository: MembershipRepository,
        private val userMembershipHistoryRepository: UserMembershipHistoryRepository,
        private val ttsRepository: TtsRepository
) : Authority<ResponseType> {
    override val permissions: List<Permission<ResponseType>> = listOf()
    private val logger = LoggerFactory.getLogger(KotlinApplication::javaClass.toString())

    override fun load(authorizationContext: AuthorizationContext<ResponseType>) {
        val context = authorizationContext as FanmeetAuthorizationContext

        val userId = context.userId ?: return
        val celeb = context.communityOwner ?: return
        val ttsLength = context.ttsLength ?: return

        val subscription = userMembershipHistoryRepository.findTopByUserIdAndMembershipCelebAndActiveIsTrueOrderByExpirationDateDesc(userId, celeb)
                ?: return
        val membership = subscription.membership ?: return
        if (subscription.status == UserMembershipHistory.Status.EXPIRED) return
        if (membership.ttsWrite != true) return

        val limit = membership.ttsWriteLimit ?: return

        val list = ttsRepository.findBySubscriberAndCelebAndStatusIsNotInAndActiveIsTrue(userId, celeb, listOf(Tts.Status.SELF, Tts.Status.REJECTED, Tts.Status.SAMPLE))
        var accumulated = 0L
        val past30 = Instant.now().epochSecond.minus(60 * 60 * 24 * 30)
        for (tts in list) {
            // Appealed tts requests(approvedDate is null) will act like they have been approved just now
            val approved = tts.approvedDate ?: Instant.now()
            if (approved.epochSecond < past30)
                continue

            accumulated += tts.text?.length!!
        }

        if (accumulated <= limit)
            context.subject.grant(this)
    }
}