package ai.maum.kotlin.model.http.dto.content

import javax.validation.constraints.NotNull

data class ContentCategoryItemDto(
        @field:NotNull
        var id: Long? = null,

        @field:NotNull
        var name: String? = null
)