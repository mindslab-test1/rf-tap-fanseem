package ai.maum.kotlin.model.http.dto.profile

import javax.validation.constraints.NotNull

data class ProfileViewRequestDto(
        @field:NotNull
        var user: Long? = null
)