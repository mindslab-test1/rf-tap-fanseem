package ai.maum.kotlin.model.authorization.fanmeet.privilege

import ai.maum.kotlin.jpa.common.celebfeed.CelebFeedRepository
import ai.maum.kotlin.jpa.common.comment.Comment
import ai.maum.kotlin.jpa.common.comment.CommentRepository
import ai.maum.kotlin.jpa.common.content.ContentRepository
import ai.maum.kotlin.jpa.common.scrap.Scrap
import ai.maum.kotlin.jpa.common.scrap.ScrapRepository
import ai.maum.kotlin.jpa.common.user.UserRepository
import ai.maum.kotlin.jpa.membership.UserMembershipHistoryRepository
import ai.maum.kotlin.model.authorization.fanmeet.ContextProperty
import ai.maum.kotlin.model.authorization.fanmeet.FanmeetAuthorizationContext
import ai.maum.kotlin.model.authorization.fanmeet.Requires
import ai.maum.kotlin.model.authorization.fanmeet.ResponseType
import ai.maum.kotlin.model.authorization.general.AuthorizationContext
import ai.maum.kotlin.model.authorization.general.Permission
import ai.maum.kotlin.model.authorization.general.Privilege
import org.springframework.stereotype.Component

@Requires(ContextProperty.COMMUNITY_OWNER, ContextProperty.CONTENT)
@Requires(ContextProperty.COMMUNITY_OWNER, ContextProperty.CELEBFEED)
@Component
class ScrappedPrivilege(
        val userRepository: UserRepository,
        val scrapRepository: ScrapRepository,
        val contentRepository: ContentRepository,
        val celebFeedRepository: CelebFeedRepository
) : Privilege<ResponseType> {
    override var permissions: List<Permission<ResponseType>> = listOf()

    override fun load(authorizationContext: AuthorizationContext<ResponseType>) {
        val context = authorizationContext as FanmeetAuthorizationContext

        context.userId ?: return
        context.communityOwner ?: return
        context.content ?: context.celebFeed ?: return // At least one of them is required nonnull

        context.content?.let {
            val content = contentRepository.findByIdAndActiveIsTrue(context.content!!) ?: return
            if (content.who != context.communityOwner)
                return

            if (!scrapRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(context.userId!!, content.id!!, Scrap.TargetType.CONTENT))
                return

            context.subject.grant(this)
        }

        context.celebFeed?.let {
            val celebFeed = celebFeedRepository.findByIdAndActiveIsTrue(context.celebFeed!!) ?: return
            if (celebFeed.who != context.communityOwner)
                return

            if (!scrapRepository.existsByUserIdAndTargetIdAndTargetTypeAndActiveIsTrue(context.userId!!, celebFeed.id!!, Scrap.TargetType.CELEB_FEED))
                return

            context.subject.grant(this)
        }
    }
}