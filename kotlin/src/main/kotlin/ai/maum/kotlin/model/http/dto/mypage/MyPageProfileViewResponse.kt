package ai.maum.kotlin.model.http.dto.mypage

import javax.validation.constraints.NotNull

data class MyPageProfileViewResponse(
        @field:NotNull
        var name: String ?= null,
        @field:NotNull
        var profileImgUrl: String ?= null,
        @field:NotNull
        var bannerImgUrl: String ?= null,
        @field:NotNull
        var interestedCategory: List<Long> ?= null
)