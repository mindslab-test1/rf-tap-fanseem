package ai.maum.kotlin.model.http.dto.content

data class ContentEditCategoryRequestDto(
        var added: List<String>? = null,
        var removed: List<Long>? = null
)