package ai.maum.kotlin.model.http.dto.feed

import org.springframework.web.multipart.MultipartFile
import javax.validation.constraints.NotNull

data class FeedWriteCommentRequestDto(
        @field:NotNull
        var celeb: Long? = null,

        @field:NotNull
        var feed: Long? = null,

        var parent: Long? = null,

        @field:NotNull
        var text: String? = null,

        var picture: MultipartFile? = null,
        var tts: Long? = null
)