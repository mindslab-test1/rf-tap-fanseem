package ai.maum.kotlin.model.http.dto.message

import java.time.Instant
import javax.validation.constraints.NotNull

data class MessageCelebDetailItemDto(
        var text: String? = null,
        var ttsUrl: String? = null,
        var createdTime: Instant? = null,

        @field:NotNull
        var isTts: Boolean? = null
)