package ai.maum.kotlin.model.http.dto.search

import ai.maum.kotlin.jpa.common.user.CelebSearchResultVo

data class SearchResultDto(
        val id: Long,
        val name: String,
        val profileImageUrl: String?,
        val introduction: String?
) {
    constructor(celebSearchResultVo: CelebSearchResultVo) : this(
            id = celebSearchResultVo.id,
            name = celebSearchResultVo.name,
            profileImageUrl = celebSearchResultVo.profileImageUrl,
            introduction = celebSearchResultVo.introduction
    )
}