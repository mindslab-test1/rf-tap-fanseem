package ai.maum.kotlin.model.http.dto.celebfeed

import javax.validation.constraints.NotNull

data class CelebFeedListCategoryRequestDto(
        @field:NotNull
        var celeb: Long? = null
)