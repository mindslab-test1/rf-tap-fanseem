package ai.maum.kotlin.model.tts

import ai.maum.kotlin.grpc.TtsManager
import ai.maum.kotlin.jpa.common.service.Tts
import ai.maum.kotlin.jpa.common.service.TtsRepository
import ai.maum.kotlin.jpa.ttsgen.TtsAddressRepository
import ai.maum.kotlin.model.audio.Wave
import ai.maum.kotlin.model.media.MediaUploader
import org.springframework.stereotype.Component

@Component
class TtsGenerator(
        val ttsAddressRepository: TtsAddressRepository,
        val ttsRepository: TtsRepository,
        val ttsManager: TtsManager,
        val mediaUploader: MediaUploader
)
{
    fun generate(userId: Long, text: String) : Tts? {
        // Don't remove
        val ttsAddress = ttsAddressRepository.findByCeleb(userId)
                ?: return null

        if (text.length > 300)
            return null
        if (text.isEmpty())
            return null

        val speech: ByteArray
        try {
            speech = ttsManager.generate(userId, text)
        } catch (e: Exception) {
            return null
        }
        val previewSpeech = speech.clone()

        val wavSpeech = Wave(speech)
        val wavPreviewSpeech = Wave(previewSpeech)

        wavSpeech.setWatermark()
        wavPreviewSpeech.setWatermark()

        val speechUrl = mediaUploader.uploadTtsAsync(wavSpeech.getWaveBytes())
                ?: return null
        val previewSpeechUrl = mediaUploader.uploadTtsAsync(wavPreviewSpeech.getPreviewWaveBytes(0, 5))
                ?: return null

        val tts = Tts()
        tts.celeb = userId
        tts.text = text
        tts.url = speechUrl
        tts.previewUrl = previewSpeechUrl
        tts.status = Tts.Status.SELF
        tts.subscriber = null

        return ttsRepository.save(tts)
    }
}