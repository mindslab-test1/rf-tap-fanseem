package ai.maum.kotlin.model.http.dto.community

import javax.validation.constraints.NotNull

data class CommunityCelebMembershipListItemDto(
        @field:NotNull
        var id: Long? = null,

        @field:NotNull
        var name: String? = null,

        @field:NotNull
        var tier: Long? = null,

        @field:NotNull
        var productId: String? = null,

        @field:NotNull
        var price: Long? = null,

        var badgeImageUrl: String? = null
)