package ai.maum.kotlin.model.http.dto.celebfeed

import javax.validation.constraints.NotNull

data class CelebFeedCategoryItemDto(
        @field:NotNull
        var id: Long? = null,

        @field:NotNull
        var name: String? = null
)