package ai.maum.kotlin.model.http.dto.feed.legacy

import org.springframework.web.multipart.MultipartFile

data class FeedUpdateDto (
        val feedNo: Long,
        val categoryNo: Long?,
        val membershipNoList: List<Long>?,

        val text: String?,
        val addedFileList: List<MultipartFile>?,
        val removedFileNoList: List<Long>?,
        val youtubeUrl: String?
)