package ai.maum.kotlin.model.http.rdo

import ai.maum.kotlin.jpa.membership.Membership
import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.stereotype.Component
import java.io.Serializable

// Request Data Object
@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
data class UserInfo(
        var userId: Long? = null,
        var notification: Boolean? = null // Injected default true at fanmeet authorization
) : Serializable