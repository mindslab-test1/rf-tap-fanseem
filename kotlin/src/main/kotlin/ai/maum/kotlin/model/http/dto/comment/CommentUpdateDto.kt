package ai.maum.kotlin.model.http.dto.comment

data class CommentUpdateDto (
        val commentNo: Long,

        val text: String
)