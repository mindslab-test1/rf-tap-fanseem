package ai.maum.kotlin.model.http.dto.search

import javax.validation.constraints.NotNull

data class SearchRequestDto (
        @field:NotNull
        val text: String
)