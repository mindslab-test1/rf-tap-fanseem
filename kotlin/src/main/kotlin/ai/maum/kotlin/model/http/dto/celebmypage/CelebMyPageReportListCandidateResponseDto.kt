package ai.maum.kotlin.model.http.dto.celebmypage

import javax.validation.constraints.NotNull

data class CelebMyPageReportListCandidateResponseDto(
        @field:NotNull
        var candidateList: List<CelebMyPageReportListCandidateItemDto>? = null
)