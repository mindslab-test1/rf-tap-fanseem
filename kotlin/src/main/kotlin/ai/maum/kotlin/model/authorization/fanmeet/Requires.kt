package ai.maum.kotlin.model.authorization.fanmeet

@Repeatable
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.SOURCE)
annotation class Requires(
        vararg val property: ContextProperty
)

enum class ContextProperty {
    COMMUNITY_OWNER,
    FEED,
    CELEBFEED,
    CONTENT,
    COMMENT,
    TTS_LENGTH,
}