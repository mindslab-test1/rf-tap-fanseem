package ai.maum.kotlin.model.http.dto.mypage

import ai.maum.kotlin.jpa.common.service.Tts
import java.time.Instant
import javax.validation.constraints.NotNull

data class MyPageTtsListAllResponse(
        @field:NotNull
        var ttsListAll: List<RequestedTtsData>? = null
)

data class RequestedTtsData(
        @field:NotNull
        var id: Long ?= null,
        @field:NotNull
        var celebId: Long ?= null,
        @field:NotNull
        var celebName: String ?= null,
        @field:NotNull
        var text: String? = null,
        @field:NotNull
        var url: String? = null,
        @field:NotNull
        var previewUrl: String? = null,
        @field:NotNull
        var status: Tts.Status? = null,
        @field:NotNull
        var timestamp: Instant? = null
)