package ai.maum.kotlin.model.http.dto.celebfeed

import javax.validation.constraints.NotNull

data class CelebFeedLikeRequestDto (
        @field:NotNull
        var celebFeed: Long? = null
)