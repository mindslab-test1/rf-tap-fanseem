package ai.maum.kotlin.model.http.dto.mypage

import ai.maum.kotlin.jpa.common.interest.UserInterested
import javax.validation.constraints.NotNull

data class MyPageViewResponse(
        @field:NotNull
        var user: UserVo?= null,
        @field:NotNull
        var subscribeList: List<Any> ?= null,
        @field:NotNull
        var boardList: List<Any> ?= null
)


data class UserVo(
        @field:NotNull
        var name: String ?= null,
        @field:NotNull
        var profileImageUrl: String ?= null,
        @field:NotNull
        var bannerImageUrl: String ?= null,
        @field:NotNull
        var interestedList: MutableList<UserInterested>? = null
)