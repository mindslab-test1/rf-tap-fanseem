package ai.maum.kotlin.model.http.dto.celebfeed

import javax.validation.constraints.NotNull

data class CelebFeedDeleteCommentRequestDto(
        @field:NotNull
        var comment: Long? = null
)
