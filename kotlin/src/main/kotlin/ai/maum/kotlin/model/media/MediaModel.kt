package ai.maum.kotlin.model.media

class MediaModel {
    val mediaServerLocation = "https://file.sangsangkids.co.kr"
}

fun String.mediaUrl() : String {
    return MediaModel().mediaServerLocation + this
}
