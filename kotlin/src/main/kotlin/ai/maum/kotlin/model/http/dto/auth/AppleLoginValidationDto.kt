package ai.maum.kotlin.model.http.dto.auth

data class AppleLoginValidationDto (var resultcode: Long?, var message: String?, var response: AppleTokenResponseDto?)