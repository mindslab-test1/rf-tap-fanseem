package ai.maum.kotlin.model.http.dto.feed

import javax.validation.constraints.NotNull

data class FeedDeleteCommentRequestDto(
        @field:NotNull
        var comment: Long? = null
)
