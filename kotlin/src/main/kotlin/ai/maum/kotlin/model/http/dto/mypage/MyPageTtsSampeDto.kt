package ai.maum.kotlin.model.http.dto.mypage

import javax.validation.constraints.NotNull

data class MyPageTtsSampeDto (
        @field:NotNull
        var text: String? = null,

        @field:NotNull
        var celebId: Long? = null
)