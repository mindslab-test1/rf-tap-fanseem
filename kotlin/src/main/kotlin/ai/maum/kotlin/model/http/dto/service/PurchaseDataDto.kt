package ai.maum.kotlin.model.http.dto.service

import javax.validation.constraints.NotNull

data class PurchaseDataDto (
        @field:NotNull
        var userId: Int? = null,
        @field:NotNull
        var seqNo: Int? = null,
        @field:NotNull
        var voucherSeqNo: Int? = null,
        @field:NotNull
        var startDate: String? = null,
        @field:NotNull
        var endDate: String? = null,
        @field:NotNull
        var sortation: String? = null
)
