package ai.maum.kotlin.model.http.dto.auth

import org.springframework.web.multipart.MultipartFile
import javax.validation.constraints.NotNull

data class NaverRegisterDto(
        @field:NotNull
        var accessToken: String?,
        @field:NotNull
        var name: String?,

        // Nullable
        var hello: String?,
        var profileImage: MultipartFile?,
        var bannerImage: MultipartFile?,
        var interests: String?
)
