package ai.maum.kotlin.model.authorization.general

interface Privilege<T> {
    var permissions: List<Permission<T>>
    fun covers(permission: Permission<T>, context: AuthorizationContext<T>) = permissions.any { it == permission && it.evaluate(context) }
    fun load(context: AuthorizationContext<T>)
}