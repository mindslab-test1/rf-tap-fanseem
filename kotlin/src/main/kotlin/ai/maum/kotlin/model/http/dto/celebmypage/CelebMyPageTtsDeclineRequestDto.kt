package ai.maum.kotlin.model.http.dto.celebmypage

import javax.validation.constraints.NotNull

data class CelebMyPageTtsDeclineRequestDto(
        @field:NotNull
        var tts: Long? = null
)