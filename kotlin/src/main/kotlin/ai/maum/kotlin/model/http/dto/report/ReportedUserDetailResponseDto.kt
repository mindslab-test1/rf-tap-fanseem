package ai.maum.kotlin.model.http.dto.report

data class ReportedUserDetailResponseDto (
        val userId: Long,
        val userName: String,
        val profileImageUrl: String?,
        val reportCount: Long,
        val historyDetailList: MutableList<UserReportedHistoryDetailVo>?
)