package ai.maum.kotlin.model.http.dto.media

import javax.validation.constraints.NotNull

data class MediaFileUrlResponseDto(
    @field:NotNull
    var fullPathUrl: String? = null,

    @field:NotNull
    var pathUrl: String? = null
)