package ai.maum.kotlin.model.http.dto.mypage

import javax.validation.constraints.NotNull

data class MyPageScrapBoardViewDto(
        @field:NotNull
        var boardId: Long ?= null
)