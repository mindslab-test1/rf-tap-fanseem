package ai.maum.kotlin.model.http.dto.celebmypage

import javax.validation.constraints.NotNull

data class CelebMyPageTtsListApprovedResponseDto(
        @field:NotNull
        var approvedList: List<CelebMyPageTtsListApprovedItemDto>? = null
)