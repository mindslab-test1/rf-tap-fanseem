package ai.maum.kotlin.model.http.dto.content

import javax.validation.constraints.NotNull

data class ContentListCategoryRequestDto(
        @field:NotNull
        var celeb: Long? = null
)