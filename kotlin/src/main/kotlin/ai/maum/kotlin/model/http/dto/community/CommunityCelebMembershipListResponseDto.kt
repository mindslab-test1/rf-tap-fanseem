package ai.maum.kotlin.model.http.dto.community

import javax.validation.constraints.NotNull

data class CommunityCelebMembershipListResponseDto(
        @field:NotNull
        var membershipList: List<CommunityCelebMembershipListItemDto>? = null,
        @field:NotNull
        var tier: Long? = null
)