package ai.maum.kotlin.model.http.dto.celebfeed

import java.time.Instant
import javax.validation.constraints.NotNull

data class CelebFeedListResponseDto (
        @field:NotNull
        var feeds: MutableList<CelebFeedItemDto>? = null,

        var tailCreated: Instant? = null
)
