package ai.maum.kotlin.model.http.dto.service

import javax.validation.constraints.NotNull

data class IosNotiReceiptInfo (
        @field:NotNull
        var userId: Int? = null,
        @field:NotNull
        var ishId: Int? = null,
        @field:NotNull
        var expiresDatePst: String? = null,
        @field:NotNull
        var purchaseDate: String? = null,
        @field:NotNull
        var purchaseDateMs: Long? = null,
        @field:NotNull
        var originalPurchaseDateMs: String? = null,
        @field:NotNull
        var transactionId: String? = null,
        @field:NotNull
        var originalTransactionId: String? = null,
        @field:NotNull
        var quantity: String? = null,
        @field:NotNull
        var expiresDateMs: Long? = null,
        @field:NotNull
        var originalPurchaseDatePst: String? = null,
        @field:NotNull
        var productId: String? = null,
        @field:NotNull
        var subscriptionGroupIdentifier: String? = null,
        @field:NotNull
        var webOrderLineItemId: String? = null,
        @field:NotNull
        var expiresDate: String? = null,
        @field:NotNull
        var isInIntroOfferPeriod: String? = null,
        @field:NotNull
        var originalPurchaseDate: String? = null,
        @field:NotNull
        var purchaseDatePst: String? = null,
        @field:NotNull
        var isTrialPeriod: String? = null,
        @field:NotNull
        var cancellationDateMs: String? = null

)