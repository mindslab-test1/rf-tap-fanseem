package ai.maum.kotlin.model.http.dto.auth

data class KakaoLoginValidationDto (var id: Long?, var expiresInMillis: Long?, var appId: Long?)