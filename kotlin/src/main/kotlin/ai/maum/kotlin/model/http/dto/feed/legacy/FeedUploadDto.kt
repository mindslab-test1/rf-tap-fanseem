package ai.maum.kotlin.model.http.dto.feed.legacy

import org.springframework.web.multipart.MultipartFile

data class FeedUploadDto (
        val writerNo: Long,
        val ownerNo: Long,
        val categoryNo: Long,
        val membershipNoList: List<Long>?,

        val text: String?,
        val fileList: List<MultipartFile>?,
        val youtubeUrl: String?
)