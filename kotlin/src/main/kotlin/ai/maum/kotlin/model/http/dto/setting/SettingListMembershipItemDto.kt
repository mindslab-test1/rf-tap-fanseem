package ai.maum.kotlin.model.http.dto.setting

import java.time.Instant
import javax.validation.constraints.NotNull

data class SettingListMembershipItemDto (
        @field:NotNull
        var membershipName: String? = null,
        @field:NotNull
        var celebName: String? = null,
        @field:NotNull
        var expirationDate: Instant? = null,
        @field:NotNull
        var productId: String? = null,

        var badgeUrl: String? = null,
        var duration: Long? = null
)