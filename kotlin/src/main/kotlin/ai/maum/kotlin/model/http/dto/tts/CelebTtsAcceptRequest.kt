package ai.maum.kotlin.model.http.dto.tts

data class CelebTtsAcceptRequest (
        val ttsNo: Long
)