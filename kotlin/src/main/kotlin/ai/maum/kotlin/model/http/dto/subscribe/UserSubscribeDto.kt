package ai.maum.kotlin.model.http.dto.subscribe

data class UserSubscribeDto (
        val who: Long,
        val whom: Long
)