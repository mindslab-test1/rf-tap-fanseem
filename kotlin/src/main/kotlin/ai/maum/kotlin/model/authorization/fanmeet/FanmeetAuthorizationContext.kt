package ai.maum.kotlin.model.authorization.fanmeet

import ai.maum.kotlin.model.authorization.general.AuthorizationContext
import ai.maum.kotlin.model.authorization.general.HandlerMethod
import ai.maum.kotlin.model.authorization.general.Subject
import org.springframework.context.annotation.Scope
import org.springframework.context.annotation.ScopedProxyMode
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
class FanmeetAuthorizationContext : AuthorizationContext<ResponseType> {
    override var method: HandlerMethod<ResponseType>? = null
    override var response: ResponseEntity<Any>? = null
    override var subject: Subject<ResponseType> = Individual()
    override var lazyLoader: ((Unit) -> Unit)? = null

    var userId: Long? = null
    var controller: Any? = null
    var dto: Any? = null

    var communityOwner: Long? = null
    var feed: Long? = null
    var celebFeed: Long? = null
    var comment: Long? = null
    var content: Long? = null
    var scrap: Long? = null

    var message: Long? = null
    var tts: Long? = null
    var ttsRequester: Long? = null

    var feedOwner: Long? = null
    var commentOwner:Long? = null
    var celebFeedOwner: Long? = null
    var contentOwner: Long? = null
    // Comment parent
    var parent: Long? = null
    var parentOwner: Long? = null

    var ttsLength: Long? = null

    fun method(controllerImpl: HandlerType): FanmeetAuthorizationContext {
        controllerImpl.setController(controller!!)
        dto?.let { controllerImpl.setDto(dto!!) }
        method = controllerImpl
        return this
    }
}