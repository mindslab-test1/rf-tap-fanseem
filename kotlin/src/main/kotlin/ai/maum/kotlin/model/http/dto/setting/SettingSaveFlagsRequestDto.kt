package ai.maum.kotlin.model.http.dto.setting

data class SettingSaveFlagsRequestDto(
        var sound: Boolean? = null,
        var vibration: Boolean? = null,
        var myLike: Boolean? = null,
        var myScrap: Boolean? = null,
        var myComment: Boolean? = null,
        var celebFeed: Boolean? = null,
        var celebContent: Boolean? = null,
        var celebMessage: Boolean? = null,
        var celebActivity: Boolean? = null

        /* 이것들 뭐임;;
        var ttsResult: Boolean? = null,
        var ttsRequest: Boolean? = null,
         */
)