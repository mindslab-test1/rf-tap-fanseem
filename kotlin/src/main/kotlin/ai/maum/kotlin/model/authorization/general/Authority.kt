package ai.maum.kotlin.model.authorization.general

interface Authority<T> {
    val permissions: List<Permission<T>>
    fun has(permission: Permission<T>, context: AuthorizationContext<T>) = permissions.any { it == permission && it.evaluate(context) }

    // Callback to be called by AuthorizationContext
    fun load(authorizationContext: AuthorizationContext<T>)
}