package ai.maum.kotlin.model.http.dto.comment

data class CommentResponseDto (
        val commentNo: Long,
        val parentFeedNo: Long,
        val parentCommentNo: Long,

        val timestamp: String,

        val text: String,

        val likeCount: Long,

        val writerNo: Long,
        val writerName: String,
        val writerProfileImageUrl: String?
)