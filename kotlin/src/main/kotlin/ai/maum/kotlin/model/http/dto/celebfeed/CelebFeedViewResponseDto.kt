package ai.maum.kotlin.model.http.dto.celebfeed

import javax.validation.constraints.NotNull

data class CelebFeedViewResponseDto(
        @field:NotNull
        var feedItem: CelebFeedItemDto? = null,

        @field:NotNull
        var comments: MutableList<CelebFeedCommentItemDto>? = null
)