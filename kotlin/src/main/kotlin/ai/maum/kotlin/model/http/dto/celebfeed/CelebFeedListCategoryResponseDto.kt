package ai.maum.kotlin.model.http.dto.celebfeed

import javax.validation.constraints.NotNull

data class CelebFeedListCategoryResponseDto(
        @field:NotNull
        var categories: List<CelebFeedCategoryItemDto>? = null
)