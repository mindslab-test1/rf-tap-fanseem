package ai.maum.kotlin.model.http.dto.mypage

import javax.validation.constraints.NotNull

data class MyPageTtsRequestDto(
        @field:NotNull
        var id: Long? = null,

        @field:NotNull
        var celebId: Long? =null
)