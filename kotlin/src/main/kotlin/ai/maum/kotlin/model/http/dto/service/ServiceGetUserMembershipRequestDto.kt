package ai.maum.kotlin.model.http.dto.service

import javax.validation.constraints.NotNull

data class ServiceGetUserMembershipRequestDto(
        @field:NotNull
        var celeb: Long? = null
)