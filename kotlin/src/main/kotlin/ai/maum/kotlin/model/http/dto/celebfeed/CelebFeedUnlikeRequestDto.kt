package ai.maum.kotlin.model.http.dto.celebfeed

import javax.validation.constraints.NotNull

data class CelebFeedUnlikeRequestDto(
        @field:NotNull
        var celebFeed: Long? = null
)