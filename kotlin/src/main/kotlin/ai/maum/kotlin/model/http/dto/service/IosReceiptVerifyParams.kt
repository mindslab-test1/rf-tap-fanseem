package ai.maum.kotlin.model.http.dto.service

import javax.validation.constraints.NotNull

data class IosReceiptVerifyParams (

        @field:NotNull
        var receiptData: String? = null,
        @field:NotNull
        var userId: Int? = 0
)