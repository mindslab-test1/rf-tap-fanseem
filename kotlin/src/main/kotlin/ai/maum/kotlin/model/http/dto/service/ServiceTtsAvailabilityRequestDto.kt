package ai.maum.kotlin.model.http.dto.service

data class ServiceTtsAvailabilityRequestDto(var tbd: String)