package ai.maum.kotlin.model.http.dto.message

import java.time.Instant
import javax.validation.constraints.NotNull

data class MessageCelebDetailRequestDto(
        @field:NotNull
        var celeb: Long? = null,

        var from: Instant? = null
)