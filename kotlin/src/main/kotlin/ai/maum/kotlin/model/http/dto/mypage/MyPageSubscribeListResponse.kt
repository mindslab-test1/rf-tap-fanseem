package ai.maum.kotlin.model.http.dto.mypage

import javax.validation.constraints.NotNull

data class MyPageSubscribeListResponse(
        @field:NotNull
        var subscribeList: List<MyPageSubscribingCelebDto> ?= null
)
data class MyPageSubscribingCelebDto (
        val id: Long,
        val profileImageUrl: String?,
        val name: String,
        val badgeUrl: String? = null
)