package ai.maum.kotlin.model.http.dto.service

import javax.validation.constraints.NotNull

data class IosUnifiedReceipt (
        @field:NotNull
        var latestReceipt: String? = null,
        @field:NotNull
        var pendingRenewalInfo: MutableList<MutableMap<*, *>?>? = null,
        @field:NotNull
        var environment: String? = null,
        @field:NotNull
        var status: Int = 0,
        @field:NotNull
        var latestReceiptInfo: List<IosNotiReceiptInfo>? = null
)