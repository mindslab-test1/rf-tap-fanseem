package ai.maum.kotlin.model.http.dto.community

import javax.validation.constraints.NotNull

data class CommunityHeaderResponseDto(
        @field:NotNull
        var feedCount: Long? = null,
        @field:NotNull
        var subscriberCount: Long? = null,
        @field:NotNull
        var celeb: Long? = null,
        @field:NotNull
        var celebName: String? = null,

        var celebProfileImage: String? = null,
        var celebHello: String? = null
)