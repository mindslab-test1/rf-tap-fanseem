package ai.maum.kotlin.model.authorization.general

import org.springframework.http.HttpStatus
import org.springframework.web.server.ResponseStatusException

abstract class Subject<T> {
    val authorityList: MutableList<Authority<T>> = mutableListOf()
    val privilegeList: MutableList<Privilege<T>> = mutableListOf()

    val context: AuthorizationContext<T>? = null

    fun authorized(authority: Authority<T>): Boolean = authorityList.contains(authority)
    fun authorized(privilege: Privilege<T>): Boolean = privilegeList.contains(privilege)
    fun authorized(permission: Permission<T>): Boolean = when {
        authorityList.any { it.has(permission, context?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Subject Authorization Context Not Initialized")) } -> true
        privilegeList.any { it.covers(permission, context?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Subject Authorization Context Not Initialized")) } -> true
        else -> false
    }

    fun grant(authority: Authority<T>): Boolean = authorityList.add(authority)
    fun grant(privilege: Privilege<T>): Boolean = privilegeList.add(privilege)
}