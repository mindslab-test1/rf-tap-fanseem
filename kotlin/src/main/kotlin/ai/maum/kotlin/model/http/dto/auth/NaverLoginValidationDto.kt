package ai.maum.kotlin.model.http.dto.auth

data class NaverLoginValidationDto (var resultcode: String?, var message: String?, var response: NaverLoginValidationNestedDto?)