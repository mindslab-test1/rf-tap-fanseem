package ai.maum.kotlin.model.authorization.fanmeet

import ai.maum.kotlin.jpa.common.celebfeed.CelebFeedRepository
import ai.maum.kotlin.jpa.common.comment.Comment
import ai.maum.kotlin.jpa.common.comment.CommentRepository
import ai.maum.kotlin.jpa.common.content.ContentRepository
import ai.maum.kotlin.jpa.common.feed.FeedRepository
import ai.maum.kotlin.jpa.common.service.TtsRepository
import ai.maum.kotlin.model.authorization.fanmeet.authority.*
import ai.maum.kotlin.model.authorization.fanmeet.privilege.MembershipTierPrivilege
import ai.maum.kotlin.model.authorization.fanmeet.privilege.ScrappedPrivilege
import ai.maum.kotlin.model.http.rdo.UserInfo
import org.springframework.beans.factory.ObjectFactory
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

typealias ResponseType = ResponseEntity<Any>
typealias HandlerType = FanmeetHandler<ResponseType>

@Component
class FanmeetAuthorization(
        // Factory
        val contextFactory: ObjectFactory<FanmeetAuthorizationContext>,
        val userInfoFactory: ObjectFactory<UserInfo>,

        // Repository
        val feedRepository: FeedRepository,
        val celebFeedRepository: CelebFeedRepository,
        val contentRepository: ContentRepository,
        val commentRepository: CommentRepository,
        val ttsRepository: TtsRepository,

        // Authority
        val userAuthority: UserAuthority,
        val celebAuthority: CelebAuthority,
        val subscriberAuthority: SubscriberAuthority,
        val blockedByAuthority: BlockedByAuthority,
        //val membershipAuthority: MembershipAuthority,
        val feedWriterAuthority: FeedWriterAuthority,
        val celebFeedWriterAuthority: CelebFeedWriterAuthority,
        val commentWriterAuthority: CommentWriterAuthority,
        val contentWriterAuthority: ContentWriterAuthority,
        val communityOwnershipAuthority: CommunityOwnershipAuthority,
        val ttsLengthLimitAuthority: TtsLengthLimitAuthority,
        val skipNotificationAuthority: SkipNotificationAuthority,

        // Privilege
        val membershipTierPrivilege: MembershipTierPrivilege,
        val scrappedPrivilege: ScrappedPrivilege
) {
    private fun load(context: FanmeetAuthorizationContext) {
        userAuthority.load(context)
        celebAuthority.load(context)
        subscriberAuthority.load(context)
        blockedByAuthority.load(context)
        //membershipAuthority.load(context)
        feedWriterAuthority.load(context)
        celebFeedWriterAuthority.load(context)
        commentWriterAuthority.load(context)
        contentWriterAuthority.load(context)
        communityOwnershipAuthority.load(context)
        ttsLengthLimitAuthority.load(context)

        membershipTierPrivilege.load(context)
        scrappedPrivilege.load(context)
    }

    private fun lateLoad(context: FanmeetAuthorizationContext) {
        // Will be loaded after handler method invocation
        skipNotificationAuthority.load(context)
    }

    fun begin(controller: Any, method: HandlerType): FanmeetAuthorizationContext = begin(controller, null, method)
    fun begin(controller: Any, dto: Any?, method: HandlerType): FanmeetAuthorizationContext {
        userInfoFactory.`object`.notification = true

        val context = contextFactory.`object`
        context.controller = controller
        context.dto = dto
        context.userId = userInfoFactory.`object`.userId

        load(context)

        context.method(method)
        context.lazyLoader = { lateLoad(context) }

        return context
    }

    fun communityOwner() = contextFactory.`object`.communityOwner
    fun communityOwner(communityOwner: Long?) = this.also {
        communityOwner ?: return@also
        contextFactory.`object`.communityOwner = communityOwner
    }

    fun communityOwnerFromFeed(feed: Long?) = this.also {
        feed ?: return@also
        val feedItem = feedRepository.findByIdAndActiveIsTrue(feed)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "CommunityOwnerFromFeed")
        contextFactory.`object`.communityOwner = feedItem.celeb
    }

    fun communityOwnerFromComment(comment: Long?) = this.also {
        comment ?: return@also
        val commentItem = commentRepository.findByIdAndActiveIsTrue(comment)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "CommunityOwnerFromComment")
        contextFactory.`object`.communityOwner = commentItem.celeb
    }

    fun communityOwnerFromCelebFeed(celebFeed: Long?) = this.also {
        celebFeed ?: return@also
        val celebFeedItem = celebFeedRepository.findByIdAndActiveIsTrue(celebFeed)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "CommunityOwnerFromCelebFeed")
        contextFactory.`object`.communityOwner = celebFeedItem.who
    }

    fun communityOwnerFromContent(content: Long?) = this.also {
        content ?: return@also
        val contentItem = contentRepository.findByIdAndActiveIsTrue(content)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "CommunityOwnerFromContent")
        contextFactory.`object`.communityOwner = contentItem.who
    }

    fun feed() = contextFactory.`object`.feed
    fun feed(feed: Long?) = this.also {
        feed ?: return@also
        contextFactory.`object`.feed = feed
    }

    fun feedFromComment(comment: Long?) = this.also {
        comment ?: return@also
        val commentItem = commentRepository.findByIdAndActiveIsTrue(comment)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "FeedFromComment")
        if (commentItem.section != Comment.Section.USER_FEED)
            throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "FeedFromComment Section")
        contextFactory.`object`.feed = commentItem.feed
    }

    fun feedOwner() = contextFactory.`object`.feedOwner
    fun feedOwnerFromFeed(feed: Long?) = this.also {
        feed ?: return@also
        val feedItem = feedRepository.findByIdAndActiveIsTrue(feed)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "FeedOwnerFromFeed")
        contextFactory.`object`.feedOwner = feedItem.who
    }

    fun celebFeed() = contextFactory.`object`.celebFeed
    fun celebFeed(celebFeed: Long?) = this.also {
        celebFeed ?: return@also
        contextFactory.`object`.celebFeed = celebFeed
    }

    fun celebFeedOwner() = contextFactory.`object`.celebFeedOwner
    fun celebFeedOwnerFromCelebFeed(celebFeed: Long?) = this.also {
        celebFeed ?: return@also
        val celebFeedItem = celebFeedRepository.findByIdAndActiveIsTrue(celebFeed)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "CelebFeedOwnerFromCelebFeed")
        contextFactory.`object`.celebFeedOwner = celebFeedItem.who
    }

    fun celebFeedFromComment(comment: Long?) = this.also {
        comment ?: return@also
        val commentItem = commentRepository.findByIdAndActiveIsTrue(comment)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "CelebFeedFromComment")
        if (commentItem.section != Comment.Section.CELEB_FEED)
            throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "CelebFeedFromComment Section")
        contextFactory.`object`.celebFeed = commentItem.feed
    }

    fun content() = contextFactory.`object`.content
    fun content(content: Long?) = this.also {
        content ?: return@also
        contextFactory.`object`.content = content
    }
    fun contentOwner() = contextFactory.`object`.contentOwner
    fun contentOwnerFromContent(content: Long?) = this.also {
        content ?: return@also
        val contentItem = contentRepository.findByIdAndActiveIsTrue(content)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "ContentOwnerFromContent")
        contextFactory.`object`.contentOwner = contentItem.who
    }

    fun contentFromComment(comment: Long?) = this.also {
        comment ?: return@also
        val commentItem = commentRepository.findByIdAndActiveIsTrue(comment)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "FeedFromComment")
        if (commentItem.section != Comment.Section.CONTENT)
            throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "FeedFromComment Section")
        contextFactory.`object`.content = commentItem.feed
    }

    fun comment() = contextFactory.`object`.comment
    fun comment(comment: Long?) = this.also {
        comment ?: return@also
        contextFactory.`object`.comment = comment
    }

    fun commentOwner() = contextFactory.`object`.commentOwner
    fun commentOwner(commentOwner: Long?) = this.also {
        commentOwner ?: return@also
        contextFactory.`object`.commentOwner = commentOwner
    }

    fun commentOwnerFromComment(comment: Long?) = this.also {
        comment ?: return@also
        val commentItem = commentRepository.findByIdAndActiveIsTrue(comment)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "CommentOwnerFromComment")
        contextFactory.`object`.commentOwner = commentItem.who
    }

    fun parent() = contextFactory.`object`.parent
    fun parent(parent: Long?) = this.also {
        parent ?: return@also
        contextFactory.`object`.parent = parent
    }

    fun parentOwner() = contextFactory.`object`.parentOwner
    fun parentOwner(parentOwner: Long?) = this.also {
        parentOwner ?: return@also
        contextFactory.`object`.parentOwner = parentOwner
    }

    fun parentOwnerFromParent(parent: Long?) = this.also {
        parent ?: return@also
        val parentItem = commentRepository.findByIdAndActiveIsTrue(parent)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "ParentOwnerFromParent")
        contextFactory.`object`.parentOwner = parentItem.who
    }

    fun scrap() = contextFactory.`object`.scrap
    fun scrap(scrap: Long?) = this.also {
        scrap ?: return@also
        contextFactory.`object`.scrap = scrap
    }

    fun message() = contextFactory.`object`.message
    fun message(message: Long?) = this.also {
        message ?: return@also
        contextFactory.`object`.message = message
    }

    fun tts() = contextFactory.`object`.tts
    fun tts(tts: Long?) = this.also {
        tts ?: return@also
        contextFactory.`object`.tts = tts
    }

    fun ttsRequester() = contextFactory.`object`.ttsRequester
    fun ttsRequesterFromTts(tts: Long?) = this.also {
        tts ?: return@also
        val ttsItem = ttsRepository.findByIdAndActiveIsTrue(tts)
                ?: throw ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "TtsRequesterFromTts")
        contextFactory.`object`.ttsRequester = ttsItem.subscriber
    }

    fun ttsLength() = contextFactory.`object`.ttsLength
    fun ttsLength(length: Long?) = this.also {
        length ?: return@also
        contextFactory.`object`.ttsLength = length
    }

    fun context(): FanmeetAuthorizationContext = contextFactory.`object`
    fun userId() = contextFactory.`object`.userId
}