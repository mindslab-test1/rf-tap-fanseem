package ai.maum.kotlin.model.http.dto.content

import javax.validation.constraints.NotNull

data class ContentViewRequestDto(
        @field:NotNull
        var content: Long? = null
)