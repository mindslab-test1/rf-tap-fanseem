package ai.maum.kotlin.model.http.dto.feed

import javax.validation.constraints.NotNull

data class FeedScrapRequestDto(
    @field:NotNull
    var scrapBoard: Long? = null,

    @field:NotNull
    var feed: Long? = null
)
