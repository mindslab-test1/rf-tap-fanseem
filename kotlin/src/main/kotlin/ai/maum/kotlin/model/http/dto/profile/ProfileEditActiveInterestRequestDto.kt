package ai.maum.kotlin.model.http.dto.profile

import javax.validation.constraints.NotNull

data class ProfileEditActiveInterestRequestDto(
        @field:NotNull
        var interestList: List<Long>? = null
)