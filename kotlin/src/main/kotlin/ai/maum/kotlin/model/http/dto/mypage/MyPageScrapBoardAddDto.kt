package ai.maum.kotlin.model.http.dto.mypage

import javax.validation.constraints.NotNull

data class MyPageScrapBoardAddDto(
        @field:NotNull
        var boardName: String?= null
)