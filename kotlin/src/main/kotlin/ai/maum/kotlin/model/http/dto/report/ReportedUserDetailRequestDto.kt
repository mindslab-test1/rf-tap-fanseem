package ai.maum.kotlin.model.http.dto.report

import javax.validation.constraints.NotNull

data class ReportedUserDetailRequestDto (
        @field:NotNull
        val userId: Long
)