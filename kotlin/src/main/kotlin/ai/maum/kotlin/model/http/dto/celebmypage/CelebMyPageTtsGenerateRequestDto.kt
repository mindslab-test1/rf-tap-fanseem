package ai.maum.kotlin.model.http.dto.celebmypage

import javax.validation.constraints.NotNull

data class CelebMyPageTtsGenerateRequestDto(
        @field:NotNull
        var text: String? = null
)