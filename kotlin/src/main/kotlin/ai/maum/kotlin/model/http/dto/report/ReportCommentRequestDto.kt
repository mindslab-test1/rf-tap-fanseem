package ai.maum.kotlin.model.http.dto.report

import javax.validation.constraints.NotNull

data class ReportCommentRequestDto (
        @field:NotNull
        val whom: Long?,
        @field:NotNull
        val commentId: Long?,
        val reason: String?
)