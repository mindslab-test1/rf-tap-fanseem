package ai.maum.kotlin.model.http.dto.block

data class BlockedUserListRequest (
        val celebNo: Long
)