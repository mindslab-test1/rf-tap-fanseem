package ai.maum.kotlin.model.http.dto.feed

import javax.validation.constraints.NotNull

data class CategoryItemDto(
        @field:NotNull
        var id: Long? = null,

        @field:NotNull
        var name: String? = null
)