package ai.maum.kotlin.model.http.dto.report

data class ReportCelebFeedResponseDto (
        val message: String
)