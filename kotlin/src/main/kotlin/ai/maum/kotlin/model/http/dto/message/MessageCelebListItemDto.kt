package ai.maum.kotlin.model.http.dto.message

import java.time.Instant
import javax.validation.constraints.NotNull

data class MessageCelebListItemDto(
        @field:NotNull
        var celeb: Long? = null,

        @field:NotNull
        var name: String? = null,

        var profileImageUrl: String? = null,

        var text: String? = null,

        var createdTime: Instant? = null,

        @field:NotNull
        var isTts: Boolean? = null
)