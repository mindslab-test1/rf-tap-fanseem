package ai.maum.kotlin.model.http.dto.like

data class FeedLikeDto (
        val feedNo: Long,
        val userNo: Long
)