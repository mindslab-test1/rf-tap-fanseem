package ai.maum.kotlin.model.http.dto.comment

data class CommentUploadDto (
        val writerNo: Long,
        val parentFeedNo: Long,
        val parentCommentNo: Long,

        val text: String
)