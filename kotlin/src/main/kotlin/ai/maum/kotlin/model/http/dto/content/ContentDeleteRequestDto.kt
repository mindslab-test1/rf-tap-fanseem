package ai.maum.kotlin.model.http.dto.content

import javax.validation.constraints.NotNull

data class ContentDeleteRequestDto(
        @field:NotNull
        var content: Long? = null
)
