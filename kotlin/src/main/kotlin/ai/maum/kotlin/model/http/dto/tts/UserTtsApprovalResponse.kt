package ai.maum.kotlin.model.http.dto.tts

data class UserTtsApprovalResponse (
        val ttsNo: Long,
        val timestamp: String,
        val status: String,
        val ttsUrl: String?
)