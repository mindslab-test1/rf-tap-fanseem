package ai.maum.kotlin.model.http.dto.report

import ai.maum.kotlin.jpa.common.report.Report

data class ReportedUserListElementVo (
        val userId: Long,
        val userName: String,
        val profileImageUrl: String?,
        var reportCount: Long
) {
    constructor(report: Report) : this(
            userId = report.whom!!,
            userName = "",
            profileImageUrl = "",
            reportCount = 0
    )
}