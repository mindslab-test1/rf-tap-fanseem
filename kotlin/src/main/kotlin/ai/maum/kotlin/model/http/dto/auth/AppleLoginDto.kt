package ai.maum.kotlin.model.http.dto.auth

import javax.validation.constraints.NotNull

data class AppleLoginDto (
        @field:NotNull
        var deviceId: String?,
        @field:NotNull
        var accessToken: String?,
        @field:NotNull
        var authorizationCode: String?
)