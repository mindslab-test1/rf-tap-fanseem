package ai.maum.kotlin.model.authorization.general

interface HandlerMethod<T> : () -> T