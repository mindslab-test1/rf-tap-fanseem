package ai.maum.kotlin.model.http.dto.feed

import org.springframework.web.multipart.MultipartFile
import javax.validation.constraints.NotNull

data class FeedEditRequestDto(
        @field:NotNull
        var feed: Long? = null,

        var category: Long? = null,

        var removedPictures: List<Long>? = null,
        var addedPictures: List<MultipartFile>? = null,
        var video: MultipartFile? = null,
        var thumbnail: MultipartFile? = null,
        var youtube: String? = null,
        var tts: Long? = null,

        var text: String? = null
)