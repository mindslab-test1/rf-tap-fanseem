package ai.maum.kotlin.model.http.dto.home

data class HomeListElementDto (
        val timestamp: String,
        val userId: Long,
        val name: String,
        val profileImageUrl: String?,
        val elementType: String,
        val elementId: Long,
        val elementText: String?,
        val elementImageUrl: String?
)