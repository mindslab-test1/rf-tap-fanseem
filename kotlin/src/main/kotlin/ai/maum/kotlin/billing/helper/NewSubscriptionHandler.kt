package ai.maum.kotlin.billing.helper

import ai.maum.kotlin.jpa.membership.MembershipRepository
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.jpa.membership.UserMembershipHistory.*
import ai.maum.kotlin.jpa.membership.UserMembershipHistoryRepository
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.stereotype.Component
import java.io.File
import java.util.*

/*
    1. Validation of request.
    2. Expire current membership.
    3. Insert new membership.
*/
@Component
class NewSubscriptionHandler(
        private val membershipRepository: MembershipRepository,
        private val userMembershipHistoryRepository: UserMembershipHistoryRepository
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @Value("\${android.package.name}")
    lateinit var packageName: String
    @Value("classpath:gcp/serviceAccount.json")
    lateinit var serviceAccount: Resource

    fun run(userId: Long, purchaseToken: String, productId: String) {
        val product = membershipRepository.findByProductIdAndActiveIsTrue(productId)
                ?: throw Exception("NewSubscriptionHandler::invoke(): Membership not found; productId=$productId")

        if (product.productId!! != productId) {
            throw Exception("NewSubscriptionHandler::invoke(): ProductId mismatch; product.productId=${product.productId!!}, productId=$productId")
        }

        val purchase = PurchaseInfoGetter(
                packageName = packageName,
                productId = product.productId!!,
                purchaseToken = purchaseToken,
                serviceAccount = File("serviceAccount.json")
        )() ?: throw Exception("NewSubscriptionHandler::invoke(): Could not retrieve purchase info from google. (\'$packageName\', \'${product.productId!!}\', \'$purchaseToken\')")

        val userMembership = userMembershipHistoryRepository.findTopByUserIdAndMembershipCelebAndActiveIsTrueOrderByExpirationDateDesc(userId, product.celeb!!)

        userMembership?.let {
            if (userMembership.status!! == Status.NORMAL || userMembership.status!! == Status.TO_BE_EXPIRED) {
                throw Exception("NewSubscriptionHandler::invoke(): Conflict; User already has membership from same celeb.")
            }
        }

        val newMembership = UserMembershipHistory()
        newMembership.userId = userId
        newMembership.membership = product
        newMembership.purchaseToken = purchaseToken
        newMembership.expirationDate = Date(purchase.expiryTimeMillis.toLong()).toInstant()
        newMembership.status = Status.NORMAL
        userMembershipHistoryRepository.save(newMembership)
    }
}