package ai.maum.kotlin.billing.helper

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.androidpublisher.AndroidPublisher
import com.google.api.services.androidpublisher.AndroidPublisherScopes
import com.google.auth.http.HttpCredentialsAdapter
import com.google.auth.oauth2.GoogleCredentials
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.stereotype.Component
import java.io.File
import java.io.FileInputStream

/*
    Cancel subscription.
    No any validation implemented.
 */
@Component
class SubscriptionCanceler {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @Value("\${android.package.name}")
    lateinit var packageName: String
    @Value("classpath:gcp/serviceAccount.json")
    lateinit var serviceAccount: Resource

    fun run(purchaseToken: String, productId: String): Boolean {
        val credentials = GoogleCredentials.fromStream(FileInputStream(File(serviceAccount.uri))).createScoped(AndroidPublisherScopes.ANDROIDPUBLISHER)

        val jsonFactory = JacksonFactory.getDefaultInstance()
        val httpTransport = GoogleNetHttpTransport.newTrustedTransport()
        val httpCredentialsAdapter = HttpCredentialsAdapter(credentials)
        val publisher = AndroidPublisher.Builder(httpTransport, jsonFactory, httpCredentialsAdapter).setApplicationName(packageName).build()

        val cancel = publisher.purchases().subscriptions().cancel(packageName, productId, purchaseToken)
        val response = cancel.executeUnparsed()
        if (response.statusCode == 204) {
            // We have to check if pub/sub gives SUBSCRIPTION_CANCELED when we cancel with API.
            // If not, cancel should be handled in both here and listener.
            return true
        }
        return false
    }
}