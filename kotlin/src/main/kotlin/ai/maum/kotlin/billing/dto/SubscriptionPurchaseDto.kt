package ai.maum.kotlin.billing.dto

import ai.maum.kotlin.billing.dto.SubscriptionNotificationDto
import com.fasterxml.jackson.annotation.JsonProperty

data class SubscriptionPurchaseDto (
    @JsonProperty("version")
    val version: String,
    @JsonProperty("packageName")
    val packageName: String,
    @JsonProperty("eventTimeMillis")
    val eventTimeMillis: String,
    @JsonProperty("subscriptionNotification")
    val subscriptionNotification: SubscriptionNotificationDto
)