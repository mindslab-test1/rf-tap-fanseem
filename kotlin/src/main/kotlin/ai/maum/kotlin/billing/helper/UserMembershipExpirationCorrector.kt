package ai.maum.kotlin.billing.helper

import ai.maum.kotlin.jpa.membership.UserMembershipHistory.Status
import ai.maum.kotlin.jpa.membership.UserMembershipHistoryRepository
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component
import java.time.Instant

/*
    Check if userMembership has been expired.
    UserMembership status may not be correct due to un-manageable reasons.

    return true: membership is clear, normal, ok
    return false: expired or not exists
 */
@Component
class UserMembershipExpirationCorrector(
        private val userMembershipHistoryRepository: UserMembershipHistoryRepository
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    fun run(userMembershipId: Long) : Boolean {
        val membership = userMembershipHistoryRepository.findByIdAndActiveIsTrue(userMembershipId) ?: return false
        if (membership.status!! != Status.EXPIRED && membership.expirationDate!! < Instant.now()) {
            membership.status = Status.EXPIRED
            userMembershipHistoryRepository.save(membership)
            return false
        }
        return true
    }
}