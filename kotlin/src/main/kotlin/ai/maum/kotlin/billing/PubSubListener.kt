package ai.maum.kotlin.billing

import ai.maum.kotlin.billing.PubSubListener.NotificationType.*
import ai.maum.kotlin.billing.dto.SubscriptionPurchaseDto
import ai.maum.kotlin.billing.helper.PurchaseInfoGetter
import ai.maum.kotlin.jpa.membership.PubSubListenerHistory
import ai.maum.kotlin.jpa.membership.PubSubListenerHistoryRepository
import ai.maum.kotlin.jpa.membership.UserMembershipHistory
import ai.maum.kotlin.jpa.membership.UserMembershipHistoryRepository
import com.fasterxml.jackson.databind.ObjectMapper
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Profile
import org.springframework.core.io.Resource
import org.springframework.integration.annotation.ServiceActivator
import org.springframework.stereotype.Component
import java.io.File
import java.time.Instant
import java.util.*

// https://developer.android.com/google/play/billing/rtdn-reference
/*
notificationType:
//    (1) SUBSCRIPTION_RECOVERED - A subscription was recovered from account hold.
    (2) SUBSCRIPTION_RENEWED - An active subscription was renewed.
    (3) SUBSCRIPTION_CANCELED - A subscription was either voluntarily or involuntarily cancelled. For voluntary cancellation, sent when the user cancels.
    (4) SUBSCRIPTION_PURCHASED - A new subscription was purchased.
//    (5) SUBSCRIPTION_ON_HOLD - A subscription has entered account hold (if enabled).
//    (6) SUBSCRIPTION_IN_GRACE_PERIOD - A subscription has entered grace period (if enabled).
//    (7) SUBSCRIPTION_RESTARTED - User has reactivated their subscription from Play > Account > Subscriptions (requires opt-in for subscription restoration).
//    (8) SUBSCRIPTION_PRICE_CHANGE_CONFIRMED - A subscription price change has successfully been confirmed by the user.
//    (9) SUBSCRIPTION_DEFERRED - A subscription's recurrence time has been extended.
//    (10) SUBSCRIPTION_PAUSED - A subscription has been paused.
//    (11) SUBSCRIPTION_PAUSE_SCHEDULE_CHANGED - A subscription pause schedule has been changed.
    (12) SUBSCRIPTION_REVOKED - A subscription has been revoked from the user before the expiration time.
    (13) SUBSCRIPTION_EXPIRED - A subscription has expired.
 */
@Profile("prod")
@Component
class PubSubListener(
        private val pubSubListenerHistoryRepository: PubSubListenerHistoryRepository,
        private val userMembershipHistoryRepository: UserMembershipHistoryRepository
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @Value("classpath:gcp/serviceAccount.json")
    lateinit var serviceAccountResource: Resource

    @ServiceActivator(inputChannel = "inputChannel")
    fun messageReceiver(payload: String) {
        logger.info(payload);
        try {
            saveHistory(payload)
            val json = ObjectMapper().readValue(payload, SubscriptionPurchaseDto::class.java)

            val notificationType = notificationTypeMapper(json.subscriptionNotification.notificationType)
            val purchaseToken = json.subscriptionNotification.purchaseToken
            val packageName = json.packageName

            /*
                We can't ensure the concurrency between Google Pub/Sub and App Server.
                So we handle new subscription in controller. Not in listener.
             */
            when (notificationType) {
                SUBSCRIPTION_PURCHASED -> {
                }
                SUBSCRIPTION_RENEWED -> onRenew(purchaseToken = purchaseToken, packageName = packageName)
                SUBSCRIPTION_REVOKED,
                SUBSCRIPTION_CANCELED -> onCanceled(purchaseToken = purchaseToken)
                SUBSCRIPTION_EXPIRED -> onExpired(purchaseToken = purchaseToken)
                else -> {
                    logger.error("PubSubListener::messageReceiver() : Unexpected NotificationType $notificationType")
                }
            }
        } catch (e: Exception) {
            logger.error(e.toString())
        }
    }

    private fun onRenew(purchaseToken: String, packageName: String) {
        val recentMembership = userMembershipHistoryRepository.findTopByPurchaseTokenAndActiveIsTrueOrderByExpirationDateDesc(purchaseToken)
        if (recentMembership == null) {
            logger.error("PubSubListener::onRenew() : No such purchaseToken in UserMembershipHistory; purchaseToken=$purchaseToken")
            return
        }
        val membership = recentMembership.membership
        if (membership == null) {
            logger.error("PubSubListener::onRenew() : Membership not available; recentMembership.membership=${recentMembership.membership?.id?.toString()}")
            return
        }

        if (recentMembership.status != UserMembershipHistory.Status.NORMAL) {
            logger.error("PubSubListener::onRenew() : Renew request for invalid purchase(status not NORMAL); recentMembership.id=${recentMembership.id!!}, recentMembership.status=${recentMembership.status!!.name}")
            return
        }

        val purchase = PurchaseInfoGetter(
                packageName = packageName,
                productId = membership.productId!!,
                purchaseToken = purchaseToken,
                serviceAccount = File("serviceAccount.json")
        )()
        if (purchase == null) {
            logger.error("PubSubListener::renewed() : Could not retrieve purchase info from google; productId=${membership.productId!!}, purchaseToken=$purchaseToken")
            return
        }

        recentMembership.status = UserMembershipHistory.Status.EXPIRED
        recentMembership.expirationDate = Instant.now()
        userMembershipHistoryRepository.save(recentMembership)

        val newMembership = UserMembershipHistory()
        newMembership.userId = recentMembership.userId
        newMembership.membership = membership
        newMembership.purchaseToken = purchaseToken
        newMembership.expirationDate = Date(purchase.expiryTimeMillis.toLong()).toInstant()
        newMembership.status = UserMembershipHistory.Status.NORMAL
        userMembershipHistoryRepository.save(newMembership)
    }

    private fun onCanceled(purchaseToken: String) {
        val history = userMembershipHistoryRepository.findTopByPurchaseTokenAndActiveIsTrueOrderByExpirationDateDesc(purchaseToken)
        if (history == null) {
            logger.error("PubSubListener::onToBeExpired() : No such purchaseToken in UserMembershipHistory; purchaseToken=$purchaseToken")
            return
        }
        history.status = UserMembershipHistory.Status.TO_BE_EXPIRED
        userMembershipHistoryRepository.save(history)
    }

    private fun onExpired(purchaseToken: String) {
        val history = userMembershipHistoryRepository.findTopByPurchaseTokenAndActiveIsTrueOrderByExpirationDateDesc(purchaseToken)
        if (history == null) {
            logger.error("PubSubListener::onExpired() : No such purchaseToken in UserMembershipHistory; purchaseToken=$purchaseToken")
            return
        }
        history.status = UserMembershipHistory.Status.EXPIRED
        userMembershipHistoryRepository.save(history)
    }

    private fun saveHistory(string: String) {
        val pubSubListenerHistory = PubSubListenerHistory()
        pubSubListenerHistory.string = string
        pubSubListenerHistoryRepository.save(pubSubListenerHistory)
    }

    private fun notificationTypeMapper(notificationType: Int): NotificationType {
        return when (notificationType) {
            2 -> SUBSCRIPTION_RENEWED
            3 -> SUBSCRIPTION_CANCELED
            4 -> SUBSCRIPTION_PURCHASED
            12 -> SUBSCRIPTION_REVOKED
            13 -> SUBSCRIPTION_EXPIRED

            // below types should not be happened
            1 -> SUBSCRIPTION_RECOVERED
            5 -> SUBSCRIPTION_ON_HOLD
            6 -> SUBSCRIPTION_IN_GRACE_PERIOD
            7 -> SUBSCRIPTION_RESTARTED
            8 -> SUBSCRIPTION_PRICE_CHANGE_CONFIRMED
            9 -> SUBSCRIPTION_DEFERRED
            10 -> SUBSCRIPTION_PAUSED
            11 -> SUBSCRIPTION_PAUSE_SCHEDULE_CHANGED
            else -> UNEXPECTED
        }
    }

    enum class NotificationType {
        SUBSCRIPTION_RENEWED,
        SUBSCRIPTION_CANCELED,
        SUBSCRIPTION_PURCHASED,
        SUBSCRIPTION_REVOKED,
        SUBSCRIPTION_EXPIRED,

        // below types should not be happened
        SUBSCRIPTION_RECOVERED,
        SUBSCRIPTION_ON_HOLD,
        SUBSCRIPTION_IN_GRACE_PERIOD,
        SUBSCRIPTION_RESTARTED,
        SUBSCRIPTION_PRICE_CHANGE_CONFIRMED,
        SUBSCRIPTION_DEFERRED,
        SUBSCRIPTION_PAUSED,
        SUBSCRIPTION_PAUSE_SCHEDULE_CHANGED,
        UNEXPECTED
    }
}
