package ai.maum.kotlin.firebase.notification.user

import ai.maum.kotlin.firebase.notification.Notification
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.jpa.common.ignore.IgnoreRepository
import ai.maum.kotlin.jpa.common.user.UserRepository

abstract class UserNotification(
        val messageText: String,
        val type: String,
        private val ignoreRepository: IgnoreRepository,
        userRepository: UserRepository,
        googleUserTokenRepository: GoogleUserTokenRepository,
        kakaoUserTokenRepository: KakaoUserTokenRepository,
        naverUserTokenRepository: NaverUserTokenRepository,
        appleUserTokenRepository: AppleUserTokenRepository
) : Notification(
        userRepository,
        googleUserTokenRepository,
        kakaoUserTokenRepository,
        naverUserTokenRepository,
        appleUserTokenRepository
) {
    abstract fun isCurrentNotiMuted(userId: Long): Boolean
}