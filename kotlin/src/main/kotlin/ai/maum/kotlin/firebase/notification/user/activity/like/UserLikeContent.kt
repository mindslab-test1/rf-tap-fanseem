package ai.maum.kotlin.firebase.notification.user.activity.like

import ai.maum.kotlin.firebase.notification.user.UserNotification
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.jpa.common.block.BlockRepository
import ai.maum.kotlin.jpa.common.ignore.IgnoreRepository
import ai.maum.kotlin.jpa.common.setting.SettingRepository
import ai.maum.kotlin.jpa.common.user.UserRepository
import org.springframework.stereotype.Component

@Component
class UserLikeContent (
        private val settingRepository: SettingRepository,
        private val blockRepository: BlockRepository,
        private val ignoreRepository: IgnoreRepository,
        userRepository: UserRepository,
        googleUserTokenRepository: GoogleUserTokenRepository,
        kakaoUserTokenRepository: KakaoUserTokenRepository,
        naverUserTokenRepository: NaverUserTokenRepository,
        appleUserTokenRepository: AppleUserTokenRepository
) : UserNotification(
        "내 콘텐츠에 좋아요가 달렸습니다.",
        "like",
        ignoreRepository,
        userRepository,
        googleUserTokenRepository,
        kakaoUserTokenRepository,
        naverUserTokenRepository,
        appleUserTokenRepository
){
    fun send(likeOwner: Long, contentOwner: Long, content: Long){

        val isMuted = isCurrentNotiMuted(contentOwner)
        val isBlocked = ignoreRepository.existsByWhoAndWhomAndActiveIsTrue(contentOwner, likeOwner)
        if(isMuted || isBlocked ||  likeOwner == contentOwner) return

        val tokenList = getTokens(contentOwner)
        val title = getUserName(userId = likeOwner)
        val data = mapOf(
                Pair("click_action", "FLUTTER_NOTIFICATION_CLICK"),
                Pair("type", type),
                Pair("content", content.toString())
        )
        send(title = title, body = messageText, tokenList = tokenList, data = data)

    }
    override fun isCurrentNotiMuted(userId: Long): Boolean {
        val setting = settingRepository.findByUserIdAndActiveIsTrue(userId) ?: return true
        return !setting.myLike!!
    }
}