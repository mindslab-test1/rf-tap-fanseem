package ai.maum.kotlin.firebase.notification.user.activity.comment

import ai.maum.kotlin.firebase.notification.user.UserNotification
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.jpa.common.block.BlockRepository
import ai.maum.kotlin.jpa.common.comment.Comment
import ai.maum.kotlin.jpa.common.comment.CommentRepository
import ai.maum.kotlin.jpa.common.ignore.IgnoreRepository
import ai.maum.kotlin.jpa.common.setting.SettingRepository
import ai.maum.kotlin.jpa.common.user.UserRepository
import org.springframework.stereotype.Component

@Component
class UserSubcommentMyComment(
        private val settingRepository: SettingRepository,
        private val blockRepository: BlockRepository,
        private val ignoreRepository: IgnoreRepository,
        userRepository: UserRepository,
        private val commentRepository: CommentRepository,
        googleUserTokenRepository: GoogleUserTokenRepository,
        kakaoUserTokenRepository: KakaoUserTokenRepository,
        naverUserTokenRepository: NaverUserTokenRepository,
        appleUserTokenRepository: AppleUserTokenRepository
) : UserNotification(
        "내 댓글에 답글을 달았습니다.",
        "comment",
        ignoreRepository,
        userRepository,
        googleUserTokenRepository,
        kakaoUserTokenRepository,
        naverUserTokenRepository,
        appleUserTokenRepository
) {
    fun send(commentOwner: Long, parentOwner: Long, comment: Long) {
        val isMuted = isCurrentNotiMuted(parentOwner)
        val isBlocked = ignoreRepository.existsByWhoAndWhomAndActiveIsTrue(parentOwner, commentOwner)
        if (isMuted || isBlocked || commentOwner == parentOwner) return

        val tokenList = getTokens(parentOwner)
        val title = getUserName(userId = commentOwner)

        val commentEntity = commentRepository.findByIdAndActiveIsTrue(comment) ?: return
        commentEntity.feed ?: return

        val where = when (commentEntity.section) {
            Comment.Section.USER_FEED -> "feed"
            Comment.Section.CELEB_FEED -> "celebFeed"
            Comment.Section.CONTENT -> "content"
            else -> return
        }

        val data = mapOf(
                Pair("click_action", "FLUTTER_NOTIFICATION_CLICK"),
                Pair("type", type),
                Pair("comment", comment.toString()),
                Pair(where, commentEntity.feed!!.toString())
        )
        send(title = title, body = messageText, tokenList = tokenList, data = data)
    }

    override fun isCurrentNotiMuted(userId: Long): Boolean {
        val setting = settingRepository.findByUserIdAndActiveIsTrue(userId) ?: return true
        return !setting.myComment!!
    }
}