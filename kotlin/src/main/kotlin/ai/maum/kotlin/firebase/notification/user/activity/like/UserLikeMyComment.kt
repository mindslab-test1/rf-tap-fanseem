package ai.maum.kotlin.firebase.notification.user.activity.like

import ai.maum.kotlin.firebase.notification.user.UserNotification
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.jpa.common.block.BlockRepository
import ai.maum.kotlin.jpa.common.comment.Comment
import ai.maum.kotlin.jpa.common.comment.CommentRepository
import ai.maum.kotlin.jpa.common.ignore.IgnoreRepository
import ai.maum.kotlin.jpa.common.setting.SettingRepository
import ai.maum.kotlin.jpa.common.user.UserRepository
import org.springframework.stereotype.Component

@Component
class UserLikeMyComment(
        private val settingRepository: SettingRepository,
        private val blockRepository: BlockRepository,
        private val ignoreRepository: IgnoreRepository,
        userRepository: UserRepository,
        private val commentRepository: CommentRepository,
        googleUserTokenRepository: GoogleUserTokenRepository,
        kakaoUserTokenRepository: KakaoUserTokenRepository,
        naverUserTokenRepository: NaverUserTokenRepository,
        appleUserTokenRepository: AppleUserTokenRepository
) : UserNotification(
        "내 댓글에 좋아요가 달렸습니다.",
        "like",
        ignoreRepository,
        userRepository,
        googleUserTokenRepository,
        kakaoUserTokenRepository,
        naverUserTokenRepository,
        appleUserTokenRepository
) {
    fun send(likeOwner: Long, commentOwner: Long, comment: Long) {
        val isMuted = isCurrentNotiMuted(commentOwner)
        val isBlocked = ignoreRepository.existsByWhoAndWhomAndActiveIsTrue(commentOwner, likeOwner)
        if (isMuted || isBlocked || likeOwner == commentOwner) return

        val tokenList = getTokens(commentOwner)
        val title = getUserName(userId = likeOwner)

        val commentEntity = commentRepository.findByIdAndActiveIsTrue(comment) ?: return
        val where = when (commentEntity.section) {
            Comment.Section.USER_FEED -> "feed"
            Comment.Section.CELEB_FEED -> "celebFeed"
            Comment.Section.CONTENT -> "content"
            else -> return
        }

        val data = mapOf(
                Pair("click_action", "FLUTTER_NOTIFICATION_CLICK"),
                Pair("type", type),
                Pair("comment", comment.toString()),
                Pair(where, commentEntity.feed.toString())
        )
        send(title = title, body = messageText, tokenList = tokenList, data = data)
    }

    override fun isCurrentNotiMuted(userId: Long): Boolean {
        val setting = settingRepository.findByUserIdAndActiveIsTrue(userId) ?: return true
        return !setting.myLike!!
    }
}