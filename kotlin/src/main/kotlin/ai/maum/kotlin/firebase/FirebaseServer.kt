package ai.maum.kotlin.firebase

import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.stereotype.Component

@Component
class FirebaseServer() {
    private val logger = LoggerFactory.getLogger(this::class.java)

    @Value("classpath:firebase/rf-tap-fanseem-2d3a4-firebase-adminsdk-c3rn2-6018aef937.json")
    lateinit var token: Resource

    fun init() {
        try {
            val options = FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(token.inputStream))
                    .setDatabaseUrl("https://rf-tap-fanseem.firebaseio.com")
                    .build()

            if (FirebaseApp.getApps().isEmpty()) {
                FirebaseApp.initializeApp(options);
                logger.info("initialize firebase app")
            }
        } catch (e: Exception) {
            logger.error(e.toString())
        }
    }
}