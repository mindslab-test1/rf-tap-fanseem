package ai.maum.kotlin.firebase.notification.user.tts

import ai.maum.kotlin.firebase.notification.user.UserNotification
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.jpa.common.block.BlockRepository
import ai.maum.kotlin.jpa.common.ignore.IgnoreRepository
import ai.maum.kotlin.jpa.common.scrap.Scrap
import ai.maum.kotlin.jpa.common.setting.SettingRepository
import ai.maum.kotlin.jpa.common.user.UserRepository
import org.springframework.stereotype.Component

@Component
class UserRequestTts(
        private val settingRepository: SettingRepository,
        private val blockRepository: BlockRepository,
        private val ignoreRepository: IgnoreRepository,
        userRepository: UserRepository,
        googleUserTokenRepository: GoogleUserTokenRepository,
        kakaoUserTokenRepository: KakaoUserTokenRepository,
        naverUserTokenRepository: NaverUserTokenRepository,
        appleUserTokenRepository: AppleUserTokenRepository
) : UserNotification(
        "TTS 서비스 요청이 있습니다.",
        "tts",
        ignoreRepository,
        userRepository,
        googleUserTokenRepository,
        kakaoUserTokenRepository,
        naverUserTokenRepository,
        appleUserTokenRepository
) {
    fun send(requestOwner: Long, celebId: Long, request: Long){

        val isMuted = isCurrentNotiMuted(celebId)
        val isBlocked = ignoreRepository.existsByWhoAndWhomAndActiveIsTrue(celebId, requestOwner)
        if (isMuted || isBlocked || requestOwner == celebId) return

        val tokenList = getTokens(celebId)
        val title = getUserName(userId = requestOwner)

        val data = mapOf(
                Pair("click_action", "FLUTTER_NOTIFICATION_CLICK"),
                Pair("type", type),
                Pair("request", request.toString())
        )
        send(title = title, body = messageText, tokenList = tokenList, data = data)
    }
    fun sendHighlight (requestOwner: Long, celebId: Long, request: Long){

        val isMuted = isCurrentNotiMuted(celebId)
        val isBlocked = ignoreRepository.existsByWhoAndWhomAndActiveIsTrue(celebId, requestOwner)
        if (isMuted || isBlocked || requestOwner == celebId) return

        val tokenList = getTokens(celebId)
        val title = getUserName(userId = requestOwner)

        val data = mapOf(
                Pair("click_action", "FLUTTER_NOTIFICATION_CLICK"),
                Pair("type", type),
                Pair("request", request.toString())
        )
        send(title = title, body = messageText, tokenList = tokenList, data = data)
    }
    override fun isCurrentNotiMuted(userId: Long): Boolean {
        val setting = settingRepository.findByUserIdAndActiveIsTrue(userId) ?: return true
        return !setting.ttsResult!!
    }
}