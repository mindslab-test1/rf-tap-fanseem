package ai.maum.kotlin.firebase.notification.user.activity.like

import ai.maum.kotlin.firebase.notification.user.UserNotification
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.jpa.common.block.BlockRepository
import ai.maum.kotlin.jpa.common.ignore.IgnoreRepository
import ai.maum.kotlin.jpa.common.setting.SettingRepository
import ai.maum.kotlin.jpa.common.user.UserRepository
import org.springframework.stereotype.Component

@Component
class UserLikeCelebFeed (
        private val settingRepository: SettingRepository,
        private val blockRepository: BlockRepository,
        private val ignoreRepository: IgnoreRepository,
        userRepository: UserRepository,
        googleUserTokenRepository: GoogleUserTokenRepository,
        kakaoUserTokenRepository: KakaoUserTokenRepository,
        naverUserTokenRepository: NaverUserTokenRepository,
        appleUserTokenRepository: AppleUserTokenRepository
) : UserNotification(
        "내 피드를 좋아합니다.",
        "like",
        ignoreRepository,
        userRepository,
        googleUserTokenRepository,
        kakaoUserTokenRepository,
        naverUserTokenRepository,
        appleUserTokenRepository
){
    fun send(likeOwner: Long, celebFeedOwner: Long, celebFeed: Long){
        val isMuted = isCurrentNotiMuted(celebFeedOwner)
        val isBlocked = ignoreRepository.existsByWhoAndWhomAndActiveIsTrue(celebFeedOwner, likeOwner)
        if(isMuted || isBlocked || likeOwner == celebFeedOwner) return

        val tokenList = getTokens(celebFeedOwner)
        val title = getUserName(userId = likeOwner)
        val data = mapOf(
                Pair("click_action", "FLUTTER_NOTIFICATION_CLICK"),
                Pair("type", type),
                Pair("celebFeed", celebFeed.toString())
        )
        send(title = title, body = messageText, tokenList = tokenList, data = data)
    }

    override fun isCurrentNotiMuted(userId: Long): Boolean {
        val setting = settingRepository.findByUserIdAndActiveIsTrue(userId) ?: return true
        return !setting.myLike!!
    }
}