package ai.maum.kotlin.firebase.notification.celeb.activity.comment

import ai.maum.kotlin.firebase.notification.celeb.CelebNotification
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.jpa.common.block.BlockRepository
import ai.maum.kotlin.jpa.common.setting.CelebNotiSettingRepository
import ai.maum.kotlin.jpa.common.setting.SettingRepository
import ai.maum.kotlin.jpa.common.subscribe.SubscribeRepository
import ai.maum.kotlin.jpa.common.user.UserRepository
import ai.maum.kotlin.jpa.membership.MembershipRepository
import ai.maum.kotlin.jpa.membership.UserMembershipHistoryRepository
import org.springframework.stereotype.Component

@Component
class CelebCommentContent(
        private val settingRepository: SettingRepository,
        private val blockRepository: BlockRepository,
        celebNotiSettingRepository: CelebNotiSettingRepository,
        subscribeRepository: SubscribeRepository,
        userRepository: UserRepository,
        membershipRepository: MembershipRepository,
        membershipHistoryRepository: UserMembershipHistoryRepository,
        googleUserTokenRepository: GoogleUserTokenRepository,
        kakaoUserTokenRepository: KakaoUserTokenRepository,
        naverUserTokenRepository: NaverUserTokenRepository,
        appleUserTokenRepository: AppleUserTokenRepository
) : CelebNotification(
        "셀럽이 내 콘텐츠에 댓글을 남겼습니다.",
        "comment",
        blockRepository,
        celebNotiSettingRepository,
        subscribeRepository,
        userRepository,
        membershipRepository,
        membershipHistoryRepository,
        googleUserTokenRepository,
        kakaoUserTokenRepository,
        naverUserTokenRepository,
        appleUserTokenRepository
) {
    val highlightMessageText = "셀럽이 내 콘텐츠에 댓글을 남겼습니다."

    fun send(celeb: Long, contentOwner: Long, content: Long, comment: Long) {
        val tokenList = getFilteredSubscribersTokenList(celebId = celeb, excludeId = contentOwner)
        val title = getUserName(userId = celeb)
        val data = mapOf(
                Pair("click_action", "FLUTTER_NOTIFICATION_CLICK"),
                Pair("type", type),
                Pair("celeb", celeb.toString()),
                Pair("content", content.toString()),
                Pair("comment", comment.toString())
        )
//        send(title = title, body = messageText, tokenList = tokenList, data = data)

        sendHighlight(celeb = celeb, contentOwner = contentOwner, content = content, comment = comment)
    }
    fun sendHighlight(celeb: Long, contentOwner: Long, content: Long, comment: Long){
        val setting = settingRepository.findByUserIdAndActiveIsTrue(contentOwner) ?: return
        val isBlocked = blockRepository.existsByWhoAndWhomAndActiveIsTrue(celeb,contentOwner)
        if(!setting.myComment!! || isBlocked) return

        val tokenList = getTokens(contentOwner)
        val title = getUserName(userId = celeb)
        val data = mapOf(
                Pair("click_action", "FLUTTER_NOTIFICATION_CLICK"),
                Pair("type", type),
                Pair("celeb", celeb.toString()),
                Pair("content", content.toString()),
                Pair("comment", comment.toString())
        )
        send(title = title, body = highlightMessageText, tokenList = tokenList, data = data)
    }
    override fun isCurrentNotiMuted(userId: Long): Boolean {
        val setting = settingRepository.findByUserIdAndActiveIsTrue(userId) ?: return true
        return !setting.celebActivity!!
    }
}