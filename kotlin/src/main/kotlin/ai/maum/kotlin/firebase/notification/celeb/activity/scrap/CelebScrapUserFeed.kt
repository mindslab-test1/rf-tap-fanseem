package ai.maum.kotlin.firebase.notification.celeb.activity.scrap

import ai.maum.kotlin.firebase.notification.celeb.CelebNotification
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.jpa.common.block.BlockRepository
import ai.maum.kotlin.jpa.common.scrap.Scrap
import ai.maum.kotlin.jpa.common.scrap.ScrapRepository
import ai.maum.kotlin.jpa.common.setting.CelebNotiSettingRepository
import ai.maum.kotlin.jpa.common.setting.SettingRepository
import ai.maum.kotlin.jpa.common.subscribe.SubscribeRepository
import ai.maum.kotlin.jpa.common.user.UserRepository
import ai.maum.kotlin.jpa.membership.MembershipRepository
import ai.maum.kotlin.jpa.membership.UserMembershipHistoryRepository
import org.springframework.stereotype.Component

@Component
class CelebScrapUserFeed(
        private val settingRepository: SettingRepository,
        private val blockRepository: BlockRepository,
        celebNotiSettingRepository: CelebNotiSettingRepository,
        subscribeRepository: SubscribeRepository,
        userRepository: UserRepository,
        private val scrapRepository: ScrapRepository,
        membershipRepository: MembershipRepository,
        membershipHistoryRepository: UserMembershipHistoryRepository,
        googleUserTokenRepository: GoogleUserTokenRepository,
        kakaoUserTokenRepository: KakaoUserTokenRepository,
        naverUserTokenRepository: NaverUserTokenRepository,
        appleUserTokenRepository: AppleUserTokenRepository
) : CelebNotification(
        "셀럽이 피드를 스크랩했습니다.",
        "scrap",
        blockRepository,
        celebNotiSettingRepository,
        subscribeRepository,
        userRepository,
        membershipRepository,
        membershipHistoryRepository,
        googleUserTokenRepository,
        kakaoUserTokenRepository,
        naverUserTokenRepository,
        appleUserTokenRepository
) {
    val highlightMessageText = "셀럽이 내 피드를 스크랩했습니다."

    fun send(celeb: Long, feedOwner: Long, scrap: Long) {
        val tokenList = getFilteredSubscribersTokenList(celebId = celeb, excludeId = feedOwner)
        val title = getUserName(userId = celeb)

        val scrapEntity = scrapRepository.findByIdAndActiveIsTrue(scrap) ?: return
        if (scrapEntity.targetType != Scrap.TargetType.USER_FEED) return
        scrapEntity.targetId ?: return

        val data = mapOf(
                Pair("click_action", "FLUTTER_NOTIFICATION_CLICK"),
                Pair("type", type),
                Pair("celeb", celeb.toString()),
                Pair("scrap", scrap.toString()),
                Pair("feed", scrapEntity.targetId!!.toString())
        )
        send(title = title, body = messageText, tokenList = tokenList, data = data)

        sendHighlight(celeb = celeb, feedOwner = feedOwner, scrap = scrap)
    }

    private fun sendHighlight(celeb: Long, feedOwner: Long, scrap: Long) {
        val setting = settingRepository.findByUserIdAndActiveIsTrue(feedOwner) ?: return
        val isBlocked = blockRepository.existsByWhoAndWhomAndActiveIsTrue(celeb, feedOwner)
        if (!setting.myScrap!! || isBlocked) return

        val tokenList = getTokens(feedOwner)
        val title = getUserName(userId = celeb)

        val scrapEntity = scrapRepository.findByIdAndActiveIsTrue(scrap) ?: return
        if (scrapEntity.targetType != Scrap.TargetType.USER_FEED) return
        scrapEntity.targetId ?: return

        val data = mapOf(
                Pair("click_action", "FLUTTER_NOTIFICATION_CLICK"),
                Pair("type", type),
                Pair("celeb", celeb.toString()),
                Pair("scrap", scrap.toString()),
                Pair("feed", scrapEntity.targetId!!.toString())
        )
        send(title = title, body = highlightMessageText, tokenList = tokenList, data = data)
    }

    override fun isCurrentNotiMuted(userId: Long): Boolean {
        val setting = settingRepository.findByUserIdAndActiveIsTrue(userId) ?: return true
        return !setting.celebActivity!!
    }
}