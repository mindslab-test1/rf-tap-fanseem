package ai.maum.kotlin.firebase.notification.celeb.activity.comment

import ai.maum.kotlin.firebase.notification.celeb.CelebNotification
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.jpa.common.block.BlockRepository
import ai.maum.kotlin.jpa.common.comment.Comment
import ai.maum.kotlin.jpa.common.comment.CommentRepository
import ai.maum.kotlin.jpa.common.setting.CelebNotiSettingRepository
import ai.maum.kotlin.jpa.common.setting.SettingRepository
import ai.maum.kotlin.jpa.common.subscribe.SubscribeRepository
import ai.maum.kotlin.jpa.common.user.UserRepository
import ai.maum.kotlin.jpa.membership.MembershipRepository
import ai.maum.kotlin.jpa.membership.UserMembershipHistoryRepository
import org.springframework.stereotype.Component

@Component
class CelebSubcomment(
        private val settingRepository: SettingRepository,
        private val blockRepository: BlockRepository,
        celebNotiSettingRepository: CelebNotiSettingRepository,
        subscribeRepository: SubscribeRepository,
        userRepository: UserRepository,
        private val commentRepository: CommentRepository,
        membershipRepository: MembershipRepository,
        membershipHistoryRepository: UserMembershipHistoryRepository,
        googleUserTokenRepository: GoogleUserTokenRepository,
        kakaoUserTokenRepository: KakaoUserTokenRepository,
        naverUserTokenRepository: NaverUserTokenRepository,
        appleUserTokenRepository: AppleUserTokenRepository
) : CelebNotification(
        "셀럽이 댓글을 남겼습니다.",
        "comment",
        blockRepository,
        celebNotiSettingRepository,
        subscribeRepository,
        userRepository,
        membershipRepository,
        membershipHistoryRepository,
        googleUserTokenRepository,
        kakaoUserTokenRepository,
        naverUserTokenRepository,
        appleUserTokenRepository
) {
    val highlightMessageText = "셀럽이 내 댓글에 답글을 달았습니다."

    fun send(celeb: Long, parentOwner: Long, parent: Long, comment: Long) {
        val tokenList = getFilteredSubscribersTokenList(celebId = celeb, excludeId = parentOwner)
        val title = getUserName(userId = celeb)

        val commentEntity = commentRepository.findByIdAndActiveIsTrue(comment) ?: return
        commentEntity.feed ?: return

        val where = when (commentEntity.section) {
            Comment.Section.USER_FEED -> "feed"
            Comment.Section.CELEB_FEED -> "celebFeed"
            Comment.Section.CONTENT -> "content"
            else -> return
        }

        val data = mapOf(
                Pair("click_action", "FLUTTER_NOTIFICATION_CLICK"),
                Pair("type", type),
                Pair("celeb", celeb.toString()),
                Pair("parent", parent.toString()),
                Pair("comment", comment.toString()),
                Pair(where, commentEntity.feed!!.toString())
        )
//        send(title = title, body = messageText, tokenList = tokenList, data = data)

        sendHighlight(celeb = celeb, parentOwner = parentOwner, parent = parent, comment = comment)
    }

    private fun sendHighlight(celeb: Long, parentOwner: Long, parent: Long, comment: Long) {
        val setting = settingRepository.findByUserIdAndActiveIsTrue(parentOwner) ?: return
        val isBlocked = blockRepository.existsByWhoAndWhomAndActiveIsTrue(celeb, parentOwner)
        if (!setting.myComment!! || isBlocked) return

        val tokenList = getTokens(parentOwner)
        val title = getUserName(userId = celeb)

        val commentEntity = commentRepository.findByIdAndActiveIsTrue(comment) ?: return
        commentEntity.feed ?: return

        val where = when (commentEntity.section) {
            Comment.Section.USER_FEED -> "feed"
            Comment.Section.CELEB_FEED -> "celebFeed"
            Comment.Section.CONTENT -> "content"
            else -> return
        }

        val data = mapOf(
                Pair("click_action", "FLUTTER_NOTIFICATION_CLICK"),
                Pair("type", type),
                Pair("celeb", celeb.toString()),
                Pair("parent", parent.toString()),
                Pair("comment", comment.toString()),
                Pair(where, commentEntity.feed!!.toString())
        )
        send(title = title, body = highlightMessageText, tokenList = tokenList, data = data)
    }

    override fun isCurrentNotiMuted(userId: Long): Boolean {
        val setting = settingRepository.findByUserIdAndActiveIsTrue(userId) ?: return true
        return !setting.celebActivity!!
    }
}