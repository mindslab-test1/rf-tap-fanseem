package ai.maum.kotlin.firebase.notification.user.activity.scrap

import ai.maum.kotlin.firebase.notification.user.UserNotification
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.jpa.common.block.BlockRepository
import ai.maum.kotlin.jpa.common.ignore.IgnoreRepository
import ai.maum.kotlin.jpa.common.scrap.Scrap
import ai.maum.kotlin.jpa.common.scrap.ScrapRepository
import ai.maum.kotlin.jpa.common.setting.SettingRepository
import ai.maum.kotlin.jpa.common.user.UserRepository
import org.springframework.stereotype.Component

@Component
class UserScrapCelebFeed(
        private val settingRepository: SettingRepository,
        private val blockRepository: BlockRepository,
        private val ignoreRepository: IgnoreRepository,
        userRepository: UserRepository,
        private val scrapRepository: ScrapRepository,
        googleUserTokenRepository: GoogleUserTokenRepository,
        kakaoUserTokenRepository: KakaoUserTokenRepository,
        naverUserTokenRepository: NaverUserTokenRepository,
        appleUserTokenRepository: AppleUserTokenRepository
): UserNotification(
        "내 피드를 스크랩했습니다.",
        "scrap",
        ignoreRepository,
        userRepository,
        googleUserTokenRepository,
        kakaoUserTokenRepository,
        naverUserTokenRepository,
        appleUserTokenRepository
) {
    fun send(scrapOwner:Long, celebFeedOwner: Long, scrap: Long){

        val isMuted = isCurrentNotiMuted(celebFeedOwner)
        val isBlocked = ignoreRepository.existsByWhoAndWhomAndActiveIsTrue(celebFeedOwner, scrapOwner)
        if (isMuted || isBlocked || scrapOwner == celebFeedOwner) return

        val tokenList = getTokens(celebFeedOwner)
        val title = getUserName(userId = scrapOwner)

        val scrapEntity = scrapRepository.findByIdAndActiveIsTrue(scrap) ?: return
        if (scrapEntity.targetType != Scrap.TargetType.CELEB_FEED) return
        scrapEntity.targetId ?: return

        val data = mapOf(
                Pair("click_action", "FLUTTER_NOTIFICATION_CLICK"),
                Pair("type", type),
                Pair("scrap", scrap.toString()),
                Pair("celebFeed", scrapEntity.targetId!!.toString())
        )
        send(title = title, body = messageText, tokenList = tokenList, data = data)
    }

    override fun isCurrentNotiMuted(userId: Long): Boolean {
        val setting = settingRepository.findByUserIdAndActiveIsTrue(userId) ?: return true
        return !setting.myScrap!!
    }
}