package ai.maum.kotlin.grpc

import ai.maum.kotlin.jpa.ttsgen.TtsAddressRepository
import ai.maum.rftapfanseemserver.grpc.GrpcTtsEndpoint
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Component

@Component
class TtsManager(
        private val ttsAddressRepository: TtsAddressRepository
) {
    private val logger = LoggerFactory.getLogger(this.javaClass)

    @Throws
    fun generate(celebId: Long, ttsText: String) : ByteArray {
        val address = ttsAddressRepository.findByCeleb(celebId) ?: throw Exception("No such tts model exists in database. (celebId: $celebId)")
        var endpoint : GrpcTtsEndpoint? = null
        try {
            endpoint = GrpcTtsEndpoint(address.ip!!, address.port!!)
            val byteArray = endpoint.getTtsWaveBytes(ttsText).toByteArray()
            endpoint.shutdown()
            return byteArray
        } catch (e: Exception) {
            endpoint?.shutdown()
            logger.error(e.toString())
            throw Exception("Failed to get tts.")
        }
    }
}