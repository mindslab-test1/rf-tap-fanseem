package ai.maum.kotlin.filter.extension

class UrlStartsWith(private val startsWith: String) : UrlCheck {
    override fun assert(url: String): Boolean = url.startsWith(startsWith)
}