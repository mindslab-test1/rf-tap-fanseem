package ai.maum.kotlin.filter.extension

class UrlBlacklist(private val blacklist: Array<String>) : UrlCheck {
    override fun assert(url: String): Boolean = !blacklist.contains(url)
}