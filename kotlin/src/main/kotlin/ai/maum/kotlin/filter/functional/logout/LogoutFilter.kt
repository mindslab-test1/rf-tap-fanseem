package ai.maum.kotlin.filter.functional.logout

import ai.maum.kotlin.filter.extension.FilterUrlWrapper
import ai.maum.kotlin.filter.extension.UrlWhitelist
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import org.springframework.core.annotation.Order
import org.springframework.stereotype.Component

// Filter-level API
@Order(201)
@Component
class LogoutFilter(googleUserTokenRepository: GoogleUserTokenRepository,
                   kakaoUserTokenRepository: KakaoUserTokenRepository,
                   naverUserTokenRepository: NaverUserTokenRepository,
                   appleUserTokenRepository: AppleUserTokenRepository)
    : FilterUrlWrapper(LogoutFilterImpl(googleUserTokenRepository, kakaoUserTokenRepository, naverUserTokenRepository, appleUserTokenRepository),
        UrlWhitelist(arrayOf("/auth/logout", "/auth/refresh")))