package ai.maum.kotlin.filter.functional.refresh

import ai.maum.kotlin.filter.extension.FilterImpl
import ai.maum.kotlin.jpa.authentication.apple.AppleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.google.GoogleUserTokenRepository
import ai.maum.kotlin.jpa.authentication.kakao.KakaoUserTokenRepository
import ai.maum.kotlin.jpa.authentication.naver.NaverUserTokenRepository
import ai.maum.kotlin.model.http.dto.auth.KakaoLoginValidationDto
import ai.maum.kotlin.model.http.dto.auth.NaverLoginValidationDto
import ai.maum.kotlin.model.http.rdo.UserInfo
import com.google.api.client.extensions.appengine.http.UrlFetchTransport
import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier
import com.google.api.client.json.jackson2.JacksonFactory
import org.springframework.beans.factory.ObjectFactory
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.web.client.RestTemplate
import java.io.IOException
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.ServletException
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse

class TokenRefreshFilterImpl(var userInfoFactory: ObjectFactory<UserInfo>,
                             var googleUserTokenRepository: GoogleUserTokenRepository,
                             var kakaoUserTokenRepository: KakaoUserTokenRepository,
                             var naverUserTokenRepository: NaverUserTokenRepository,
                             var appleUserTokenRepository: AppleUserTokenRepository,
                             var restTemplate: RestTemplate) : FilterImpl {
    @Throws(IOException::class, ServletException::class)
    override fun run(request: HttpServletRequest, response: HttpServletResponse, chain: FilterChain) {
        var token: String = "l"
        var next: String = ""

        try {
            token = request.getHeader("token")
            next = request.getHeader("next")

            when (request.getHeader("social")) {
                "google" -> refreshFromGoogle(token!!, next!!)
                "kakao" -> refreshFromKakao(token!!, next!!)
                "naver" -> refreshFromNaver(token!!, next!!)
                "apple" -> refreshFromApple(token!!, next!!)
                else -> throw Exception()
            }
        } catch (e: Exception) {
            // If error, force logout
            // Next filter is logout filter
            chain.doFilter(request, response)
        }

        response.status = HttpServletResponse.SC_OK
    }

    private fun refreshFromGoogle(token: String, next: String) {
        val verifier = GoogleIdTokenVerifier.Builder(UrlFetchTransport.getDefaultInstance(), JacksonFactory()) //
                .setAudience(Collections.singletonList("612983419070-7dac4sjcpjhicu0ro1e9u3a6sb6bli01.apps.googleusercontent.com")) //
                .build()
        val idToken: GoogleIdToken? = verifier.verify(next)
        val payload: GoogleIdToken.Payload = idToken?.payload ?: throw Exception("goodbye")

        // Changing tokens
        val userTokens = googleUserTokenRepository.findByIdTokenString(token) ?: emptyList()
        for (each in userTokens) {
            each.idTokenString = next
            googleUserTokenRepository.save(each)
        }
    }

    private fun refreshFromKakao(token: String, next: String) {
        val headers = HttpHeaders()
        headers.set("Authorization", "Bearer $next")
        headers.set("Content-type", "application/x-www-form-urlencoded;charset=utf-8")

        val entity = HttpEntity<String>(headers)
        val response = restTemplate.exchange("http://kapi.kakao.com/v1/user/access_token_info", HttpMethod.GET, entity, KakaoLoginValidationDto::class.java)

        if (response.statusCode != HttpStatus.OK)
            throw Exception("hi")

        // Changing tokens
        val userTokens = kakaoUserTokenRepository.findByAccessToken(token) ?: emptyList()
        for (each in userTokens) {
            each.accessToken = next
            kakaoUserTokenRepository.save(each)
        }
    }

    private fun refreshFromNaver(token: String, next: String) {
        val headers = HttpHeaders()
        headers.set("Authorization", "Bearer $next")
        headers.set("Content-type", "application/x-www-form-urlencoded;charset=utf-8")

        val entity = HttpEntity<String>(headers)
        val response = restTemplate.exchange("https://openapi.naver.com/v1/nid/me", HttpMethod.GET, entity, NaverLoginValidationDto::class.java)

        if (response.statusCode != HttpStatus.OK)
            throw Exception("hi")

        // Changing tokens
        val userTokens = naverUserTokenRepository.findByAccessToken(token) ?: emptyList()
        for (each in userTokens) {
            each.accessToken = next
            naverUserTokenRepository.save(each)
        }
    }

    private fun refreshFromApple(token: String, next: String) {

        val headers = HttpHeaders()

        headers.set("Authorization", "Bearer $next")
        headers.set("Content-type", "application/x-www-form-urlencoded;charset=utf-8")

        val entity = HttpEntity<String>(headers)
        TODO( "애플 로그인 관련한 HttpHeader 설정 및 url , response 값에 따른 처리가 필요합니다." )

//        if (response.statusCode != HttpStatus.OK)
//            throw Exception("hi")

//        val userTokens = appleUserTokenRepository.findByAccessToken(token) ?: emptyList()
//        for (each in userTokens) {
//            each.accessToken = next
//            appleUserTokenRepository.save(each)
//        }
    }
}