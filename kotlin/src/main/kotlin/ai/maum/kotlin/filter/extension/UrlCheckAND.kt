package ai.maum.kotlin.filter.extension

class UrlCheckAND(private val check1: UrlCheck, private val check2: UrlCheck) : UrlCheck {
    override fun assert(url: String): Boolean = check1.assert(url) && check2.assert(url)
}