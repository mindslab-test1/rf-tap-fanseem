package ai.maum.mediaserver;

import ai.maum.mediaserver.grpc.GrpcInternalMediaServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collections;

@SpringBootApplication
public class MediaserverApplication implements CommandLineRunner {
    private static final Logger logger = LoggerFactory.getLogger(MediaserverApplication.class);

    @Autowired
    private GrpcInternalMediaServer grpcInternalMediaServer;

    public static void main(String[] args) {
        SpringApplication.run(MediaserverApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        grpcInternalMediaServer.start();

        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                // Use stderr here since the logger may have been grpcNotifyServerreset by its JVM shutdown hook.
                System.err.println("*** shutting down gRPC server since JVM is shutting down");
                try {
                    grpcInternalMediaServer.stop();
                } catch (InterruptedException e) {
                    logger.error("shutdown interrupted");
                }
            }
        });
    }
}
