package ai.maum.mediaserver.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Controller
public class WebExternalMediaController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${audio.repo.path}")
    private String audioRepoPath;

    @Value("${image.repo.path}")
    private String imageRepoPath;

    @Value("${video.repo.path}")
    private String videoRepoPath;

    @RequestMapping(value = "/")
    public ResponseEntity<String> rootroot() {
        logger.info("root");
        return ResponseEntity.ok("ok");
    }

    @GetMapping(value = "/rf-tap/audio/{*file}")
    public ResponseEntity<Resource> getAudioFile(@PathVariable("*file") String fileName) {
        logger.info("get audio request: " + fileName);
        try {
            Path path = Paths.get(audioRepoPath+fileName);
            ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "audio/wav");
            return ResponseEntity.ok().headers(responseHeaders).body(resource);
        } catch (Exception e) {
            logger.error(e.toString());
            return null;
        }
    }

    @GetMapping(value = "/rf-tap/image/{*file}")
    public ResponseEntity<Resource> getImageFile(@PathVariable("*file") String fileName) {
        logger.info("get image request: " + fileName);
        try {
            Path path = Paths.get(imageRepoPath+fileName);
            ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "image/png");
            return ResponseEntity.ok().headers(responseHeaders).body(resource);
        } catch (Exception e) {
            logger.error(e.toString());
            return null;
        }
    }

    @GetMapping(value = "/rf-tap/video/{*file}")
    public ResponseEntity<Resource> getVideoFile(@PathVariable("*file") String fileName) {
        logger.info("get video request: " + fileName);
        try {
            Path path = Paths.get(videoRepoPath + fileName);
            ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));
            HttpHeaders responseHeaders = new HttpHeaders();
            responseHeaders.set("Content-Type", "video/mpeg");
            return ResponseEntity.ok().headers(responseHeaders).body(resource);
        } catch (Exception e) {
            logger.error(e.toString());
            return null;
        }
    }
}
