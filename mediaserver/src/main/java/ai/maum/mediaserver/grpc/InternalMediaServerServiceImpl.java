package ai.maum.mediaserver.grpc;

import ai.maum.*;
import com.google.protobuf.ByteString;
import io.grpc.stub.StreamObserver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
class InternalMediaServerServiceImpl extends InternalMediaServerServiceGrpc.InternalMediaServerServiceImplBase {
    private static final Logger logger = LoggerFactory.getLogger(InternalMediaServerServiceImpl.class);

    @Value("${audio.repo.path}")
    private String audioRepoPath;

    @Value("${image.repo.path}")
    private String imageRepoPath;

    @Value("${video.repo.path}")
    private String videoRepoPath;

    @Value("${server.ip}")
    private String serverIpAddress;

    @Value("${server.port}")
    private int serverPort;

    InternalMediaServerServiceImpl() {
    }

    public byte[] ByteStringListToBytes(List<ByteString> payloadLis) {
        int totalSize = 0;
        for (ByteString iter : payloadLis) {
            totalSize += iter.size();
        }
        int idx = 0;
        byte[] result = new byte[totalSize];
        for (ByteString iter : payloadLis) {
            byte[] tmp = iter.toByteArray();
            for (byte b : tmp) {
                result[idx] = b;
                idx++;
            }
        }
        return result;
    }

    String GetFileName(byte[] bytePayload) {
        int hash = Arrays.hashCode(bytePayload);
        String hashStr = Integer.toHexString(hash);
        logger.info("hashStr = " + hashStr);
        return hashStr;
    }

    String SaveBytesToFile(byte[] bytePayload, String basePath, String fileExtension) {
        String fileName = GetFileName(bytePayload);
        String filePath = basePath + fileName + "." + fileExtension;
        try {
            FileOutputStream fos = new FileOutputStream(filePath);
            fos.write(bytePayload);
            fos.close();
        } catch (IOException e) {
            logger.error(e.toString());
            return null;
        }
        return fileName + "." + fileExtension;
    }

    @Override
    public StreamObserver<MSAudioPayload> uploadAudio(StreamObserver<MSUploadResult> responseObserver) {
        List<ByteString> audioPayloadList = new ArrayList<ByteString>();
        return new StreamObserver<MSAudioPayload>() {
            String fileExtension;

            @Override
            public void onNext(MSAudioPayload audioPayload) {
                fileExtension = audioPayload.getFileExtension();
                audioPayloadList.add(audioPayload.getAudioPayload());
            }

            @Override
            public void onError(Throwable throwable) {
                logger.error(throwable.toString());
                responseObserver.onNext(MSUploadResult.newBuilder().setUploadStatus(MSUploadStatus.MS_ERROR).build());
                responseObserver.onCompleted();
            }

            @Override
            public void onCompleted() {
                byte[] resultBytes = ByteStringListToBytes(audioPayloadList);
                String resultFileName = SaveBytesToFile(resultBytes, audioRepoPath, fileExtension);
                responseObserver.onNext(MSUploadResult.newBuilder()
                        .setUploadStatus(MSUploadStatus.MS_OK)
                        .setUrl("http://" + serverIpAddress + ":" + Integer.toString(serverPort) + "/rf-tap/audio/" + resultFileName).build());
                responseObserver.onCompleted();
            }
        };
    }

    @Override
    public StreamObserver<MSImagePayload> uploadImage(StreamObserver<MSUploadResult> responseObserver) {
        List<ByteString> imagePayloadList = new ArrayList<ByteString>();
        return new StreamObserver<MSImagePayload>() {
            String fileExtension = null;

            @Override
            public void onNext(MSImagePayload imagePayload) {
                fileExtension = imagePayload.getFileExtension();
                imagePayloadList.add(imagePayload.getImagePayload());
            }

            @Override
            public void onError(Throwable throwable) {
                logger.error(throwable.toString());
                responseObserver.onNext(MSUploadResult.newBuilder().setUploadStatus(MSUploadStatus.MS_ERROR).build());
                responseObserver.onCompleted();
            }

            @Override
            public void onCompleted() {
                byte[] resultBytes = ByteStringListToBytes(imagePayloadList);
                String resultFileName = SaveBytesToFile(resultBytes, imageRepoPath, fileExtension);
                responseObserver.onNext(MSUploadResult.newBuilder()
                        .setUploadStatus(MSUploadStatus.MS_OK)
                        .setUrl("http://" + serverIpAddress + ":" + Integer.toString(serverPort) + "/rf-tap/image/" + resultFileName).build());
                logger.info("http://" + serverIpAddress + ":" + Integer.toString(serverPort) + "/rf-tap/image/" + resultFileName);
                responseObserver.onCompleted();
            }
        };
    }

    @Override
    public StreamObserver<MSVideoPayload> uploadVideo(StreamObserver<MSUploadResult> responseObserver) {
        List<ByteString> videoPayloadList = new ArrayList<ByteString>();
        return new StreamObserver<MSVideoPayload>() {
            String fileExtension;

            @Override
            public void onNext(MSVideoPayload videoPayload) {
                fileExtension = videoPayload.getFileExtension();
                videoPayloadList.add(videoPayload.getVideoPayload());
            }

            @Override
            public void onError(Throwable throwable) {
                logger.error(throwable.toString());
                responseObserver.onNext(MSUploadResult.newBuilder().setUploadStatus(MSUploadStatus.MS_ERROR).build());
                responseObserver.onCompleted();
            }

            @Override
            public void onCompleted() {
                byte[] resultBytes = ByteStringListToBytes(videoPayloadList);
                String resultFileName = SaveBytesToFile(resultBytes, videoRepoPath, fileExtension);
                responseObserver.onNext(MSUploadResult.newBuilder()
                        .setUploadStatus(MSUploadStatus.MS_OK)
                        .setUrl("http://" + serverIpAddress + ":" + Integer.toString(serverPort) + "/rf-tap/video/" + resultFileName).build());
                responseObserver.onCompleted();
            }
        };
    }
}