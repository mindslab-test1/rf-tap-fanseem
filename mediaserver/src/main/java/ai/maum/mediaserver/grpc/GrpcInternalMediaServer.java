package ai.maum.mediaserver.grpc;

import io.grpc.Server;
import io.grpc.ServerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@Component
public class GrpcInternalMediaServer {
    private static final Logger logger = LoggerFactory.getLogger(GrpcInternalMediaServer.class);

    @Value("${grpc.server.port}")
    private int port;
    private Server server;

    @Autowired
    InternalMediaServerServiceImpl internalMediaServerService;

    public void start() throws IOException {
        server = ServerBuilder.forPort(this.port).addService(internalMediaServerService).build();
        server.start();
        logger.info(String.format("grpc server start on port: %s", this.port));
    }

    public void stop() throws InterruptedException {
        if (server != null) {
            server.shutdown().awaitTermination(30, TimeUnit.SECONDS);
        }
    }
}

