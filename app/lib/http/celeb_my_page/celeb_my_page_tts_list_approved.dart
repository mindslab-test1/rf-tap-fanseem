
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

import 'dto/http_celeb_my_page_tts_list_approved_item_dto.dart';

Future<List<HttpCelebMyPageTtsListApprovedItemDto>> httpCelebMyPageTtsListApproved({BuildContext context}) async {
  assert(context != null);
  Map<String, dynamic> response = await postAsUser(context, "/community/celebmypage/tts/listapproved", null);
  if (response == null || response['status'] != 200) return null;
  Map<String, dynamic> data = response['data'];

  List<HttpCelebMyPageTtsListApprovedItemDto> list = [];
  for (Map<String, dynamic> iter in List.from(data['approvedList'])) {
    list.add(HttpCelebMyPageTtsListApprovedItemDto(
      tts: iter['tts'],
      subscriberName: iter['subscriberName'],
      text: iter['text'],
      timestamp: iter['created'],
      ttsUrl: iter['ttsUrl']
    ));
  }

  return list;
}