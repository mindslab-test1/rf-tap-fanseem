
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/http/message/dto/message_celeb_list_dto.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';
import 'package:rf_tap_fanseem/http/service/dto/http_servie_get_user_membership_dto.dart';
import 'package:rf_tap_fanseem/view/messaging/component/messaging_data.dart';
import 'package:rf_tap_fanseem/view/messaging/states/messaging_provider.dart';

Future<List<HttpMessageCelebListDto>> httpMessageCelebList({BuildContext context}) async {
  assert(context != null);
  Map<String, dynamic> response = await postAsUser(context, "/message/celeb/list/celeb", null);
  if (response == null || response['status'] != 200) return null;

  if (response['data'] == null) return null;

  List<HttpMessageCelebListDto> list = [];
  Map<String, dynamic> data = response['data'];
  for (Map<String, dynamic> iter in List.from(data['list'])) {
    list.add(HttpMessageCelebListDto(
      celebId: iter['celeb'],
      name: iter['name'],
      profileImageUrl: iter['profileImageUrl'],
      text: iter['text'] == null ? "" : iter['text'],
      createdTime: iter['createdTime'] == null ? "" : iter['createdTime'],
      isTts: iter['isTts']
    ));
  }
  return list;
}

//class BaseMessageData {
//  final String timestamp;
//  final String text;
//  final MessageType type;
//  final String membership;
//
//  const BaseMessageData({
//    this.timestamp,
//    this.text,
//    this.type = MessageType.text,
//    this.membership,
//  });
//}

//{
//"text": null,
//"ttsUrl": null,
//"createdTime": "2020-07-13T12:32:11Z",
//"isTts": false
//}

Future<bool> httpMessageCelebMessageList({BuildContext context}) async {
  assert(context != null);
  if(Provider.of<MessagingDisplayProvider>(context).init) return true;
  Map<String, dynamic> response = await postAsUser(context, "/message/celeb/list/message", {
    "celeb": Provider.of<MessagingDisplayProvider>(context).celebId
  });

  if (response["status"] != 200) return false;

  List responseData = response["data"]["messages"];
  List<BaseMessageData> celebMessageData = [];
  for(dynamic data in responseData){
    celebMessageData.add(BaseMessageData(
      text: data["text"],
      timestamp: data["createdTime"],
      type: data["isTts"] ? MessageType.tts : MessageType.text,
      contentUrl: data["isTts"] ? data["ttsUrl"] : null
    ));
  }

  Provider.of<MessagingDisplayProvider>(context, listen: false).displayMessageData.addAll(celebMessageData);
  Provider.of<MessagingDisplayProvider>(context, listen: false).init = true;

  return true;
}

Future<bool> httpMessageCelebMyMessageList({BuildContext context}) async {
  assert(context != null);
  if(Provider.of<MessagingDisplayProvider>(context).init) return true;
  Map<String, dynamic> response = await postAsUser(context, "/message/celeb/mymessage", {});

  if (response["status"] != 200) return false;

  List responseData = response["data"]["messages"];
  List<BaseMessageData> celebMessageData = [];
  for(dynamic data in responseData){
    celebMessageData.add(BaseMessageData(
      text: data["text"],
      timestamp: data["createdTime"],
      type: data["isTts"] ? MessageType.tts : MessageType.text,
      contentUrl: data["isTts"] ? data["ttsUrl"] : null,
    ));
  }

  Provider.of<MessagingDisplayProvider>(context, listen: false).displayMessageData.addAll(celebMessageData.reversed);
  Provider.of<MessagingDisplayProvider>(context, listen: false).init = true;

  return true;
}

Future<bool> httpMessageCelebAddPrevMessage({BuildContext context}) async {
  assert(context != null);
  MessagingDisplayProvider provider = Provider.of<MessagingDisplayProvider>(context, listen: false);
  Map<String, dynamic> response = await postAsUser(context, "/message/celeb/list/message", {
    "from": provider.displayMessageData.last.timestamp,
    "celeb": provider.celebId
  });
  if(response["status"] != 200) return false;
  if(response["data"]["messages"].length < 20) provider.endOfData = true;

  List responseData = response["data"]["messages"];
  List<BaseMessageData> celebMessageData = [];
  for(dynamic data in responseData){
    celebMessageData.add(BaseMessageData(
      text: data["text"],
      timestamp: data["createdTime"],
      type: data["isTts"] ? MessageType.tts : MessageType.text,
    ));
  }

  provider.displayMessageData.addAll(celebMessageData);
  provider.loading = false;
  return true;
}
