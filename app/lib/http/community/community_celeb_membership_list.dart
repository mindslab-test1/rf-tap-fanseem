
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/community/dto/http_community_celeb_membership_list_dto.dart';
import 'package:rf_tap_fanseem/http/community/dto/http_community_celeb_membership_list_item_vo.dart';
import 'package:rf_tap_fanseem/http/community/dto/http_community_header_dto.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<HttpCommunityCelebMembershipListDto> httpCommunityCelebMembershipList({BuildContext context, int celebId}) async {
  assert(context != null && celebId != null);
  Map<String, dynamic> body = {
    "celeb": celebId
  };
  Map<String, dynamic> response = await postAsUser(context, "/community/celeb/membership/list", body);
  if (response == null || response['status'] != 200) return null;

  Map<String, dynamic> data = response['data'];

  List<HttpCommunityCelebMembershipListItemVo> membershipList = [];
  for (Map<String, dynamic> iter in data['membershipList']) {
    membershipList.add(HttpCommunityCelebMembershipListItemVo(
      membershipId: iter['id'],
      membershipName: iter['name'],
      membershipTier: iter['tier'],
      productId: iter['productId'],
      price: iter['price'],
      imageUrl: iter['badgeImageUrl'] == null ? "" : iter['badgeImageUrl']
    ));
  }

  return HttpCommunityCelebMembershipListDto(
    membershipList: membershipList,
    myTier: data["tier"]
  );
}