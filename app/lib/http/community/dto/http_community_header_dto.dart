
class HttpCommunityHeaderDto {
  final int feedCount;
  final int subscriberCount;
  final int celebId;
  final String name;
  final String profileImageUrl;
  final String introduction;

  HttpCommunityHeaderDto({
    this.feedCount,
    this.subscriberCount,
    this.celebId,
    this.name,
    this.profileImageUrl,
    this.introduction
  }) : assert(feedCount != null && subscriberCount != null && celebId != null && name != null);
}
