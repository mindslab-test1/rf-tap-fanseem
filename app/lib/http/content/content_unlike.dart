
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_comment_item_vo.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_feed_item_vo.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_feed_view_response_dto.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<bool> httpContentUnlike({BuildContext context, int contentId}) async {
  assert(context != null && contentId != null);
  Map<String, dynamic> body = {
    "content": contentId,
  };
  Map<String, dynamic> response = await postAsUser(context, "/community/content/unlike", body);
  if (response == null || response['status'] != 200) return false;

  return true;
}