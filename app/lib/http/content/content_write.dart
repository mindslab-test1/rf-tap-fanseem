
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<bool> httpContentWrite({
  BuildContext context,
  int categoryId,
  List<String> imageUrls,
  String videoUrl,
  String videoThumbnailUrl,
  String youtubeUrl,
  int ttsId,
  String text,
  String title,
  int accessLevel
}) async {
  assert(context != null && categoryId != null && accessLevel != null && title != null);

  List<MultipartFile> fileList = [];
  for (String path in imageUrls) {
    fileList.add(await MultipartFile.fromPath("pictures", path, filename: path.split("/").last));
  }
  if (videoUrl != null) {
    fileList.add(await MultipartFile.fromPath("video", videoUrl, filename: videoUrl.split("/").last));
  }
  if (videoUrl != null) {
    fileList.add(MultipartFile.fromBytes("thumbnail", [1], filename: "dummy.png"));
//    fileList.add(await MultipartFile.fromPath("thumbnail", "", filename: "".split("/").last));
  }

  Map<String, dynamic> body = {
    "category": categoryId,
    "text": text,
    "accessLevel": accessLevel,
    "title": title
  };
  if (youtubeUrl != null) body['youtube'] = youtubeUrl;
  if (ttsId != null) body['tts'] = ttsId;

  Map<String, dynamic> response = await multipartPostAsUser(context, "/community/content/write", body, fileList);
  if (response == null || response['status'] != 200) return false;

  return true;
}