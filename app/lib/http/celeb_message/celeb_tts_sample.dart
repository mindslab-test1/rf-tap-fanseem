import 'package:flutter/cupertino.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<CelebTtsSampleDto> httpCelebTtsSample({BuildContext context, String text}) async {
  Map<String, dynamic> body = {
    "text" : text
  };

  Map<String, dynamic> response = await postAsUser(context, "/message/celeb/tts/sample", body);
  if(response["status"] != 200){
    return CelebTtsSampleDto(statusCode: response["status"]);
  }
  return CelebTtsSampleDto(
    statusCode: 200,
    url: response["data"]["url"],
    ttsId: response["data"]["id"]
  );
}

class CelebTtsSampleDto{
  final int statusCode;
  final String url;
  final int ttsId;

  const CelebTtsSampleDto({
    this.statusCode,
    this.url,
    this.ttsId
  });
}