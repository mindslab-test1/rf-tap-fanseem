import 'package:rf_tap_fanseem/http/login/dto/http_login_response_dto.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<HttpLoginResponseDto> httpNaverLogin({String deviceTokenId, String accessToken}) async {
  assert(deviceTokenId != null && accessToken != null);
  Map<String, dynamic> body = {
    "deviceId": deviceTokenId,
    "accessToken": accessToken
  };
  Map<String, dynamic> response = await postAsGuest("/auth/login/naver", body);
  if (response == null) return null;
  return HttpLoginResponseDto(
    status: response['status'],
    userId: response['data']['userId'],
    isCeleb: response['data']['isCeleb']
  );
}