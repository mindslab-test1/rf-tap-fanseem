
class HttpLoginResponseDto {
  final int status;
  final int userId;
  final bool isCeleb;
  final String iosRefreshToken;
  @override
  String toString() {
    return 'HttpLoginResponseDto{status: $status, userId: $userId, isCeleb: $isCeleb, iosRefreshToken: $iosRefreshToken}';
  }

  HttpLoginResponseDto({this.status, this.userId, this.isCeleb, this.iosRefreshToken});

}