import 'package:flutter/material.dart';
import 'dart:developer' as developer;
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_comment_item_vo.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_feed_item_vo.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_feed_view_response_dto.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';
import 'package:rf_tap_fanseem/providers/selected_celeb_provider.dart';
import 'package:rf_tap_fanseem/util/timestamp.dart';

Future<bool> httpCelebFeedEditCategory({BuildContext context, List<String> added, List<int> removed}) async {
  assert(context != null);
  Map<String, dynamic> body = {
    "added": added,
    "removed": removed
  };
  developer.log('$body');
  Map<String, dynamic> response = await postAsUser(context, "/community/celebfeed/editcategory", body);
  developer.log('$response');

  if (response == null || response['status'] != 200) return false;

  return true;
}