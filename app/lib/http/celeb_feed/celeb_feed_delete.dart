import 'package:flutter/material.dart';
import '../rest_call_functions.dart';

Future<bool> httpCelebFeedDelete({BuildContext context, int feed}) async {
  assert(context != null && feed != null);
  Map<String, dynamic> body = {
    "celebFeed": feed,
  };
  Map<String, dynamic> response = await postAsUser(context, "/community/celebfeed/delete", body);
  if (response == null || response['status'] != 200) return false;

  return true;
}