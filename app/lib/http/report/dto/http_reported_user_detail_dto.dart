

import 'package:rf_tap_fanseem/http/report/dto/http_report_history_detail_vo.dart';

class HttpReportedUserDetailDto {
  final int userId;
  final String userName;
  final String profileImageUrl;
  final int reportCount;
  final List<HttpReportHistoryDetailVo> historyDetailList;

  const HttpReportedUserDetailDto({
    this.userId,
    this.userName,
    this.profileImageUrl,
    this.reportCount,
    this.historyDetailList
  }) : assert(userId != null && userName != null && reportCount != null);
}
