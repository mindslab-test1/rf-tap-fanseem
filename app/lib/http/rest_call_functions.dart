import 'dart:convert';
import 'dart:developer' as developer;
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/providers/login_provider.dart';

//String _baseUrl = "http://10.0.2.2:12400";
//String _baseUrl = "http://114.108.173.102:12400";
//String _baseUrl = "http://114.108.173.102:22400";
String _baseUrl = "https://media.maum.ai/rftap";

Future<Map<String, dynamic>> getAsGuest(String url) async {
  try {
    Response response = await get(_baseUrl + url);
    Map<String, dynamic> data;
    if (response.body != null && response.body != "") data = jsonDecode(response.body);
    return {
      "status": response.statusCode,
      "data": data
    };
  } catch (e) {
      developer.log("Error at getAsGuest(): " + e.toString());
    return null;
  }
}

Future<Map<String, dynamic>> postAsGuest(String url, Map<String, dynamic> body) async {
  try {
    Response response = await post(
        _baseUrl + url,
        headers: {"Content-Type": "application/json"},
        body: jsonEncode(body)
    );
    Map<String, dynamic> data;
    if (response.body != null && response.body != "") data = jsonDecode(response.body);
    return {
      "status": response.statusCode,
      "data": data
    };
  } catch (e) {
      developer.log("Error at postAsGuest(): " + e.toString());
    return null;
  }
}


Future<Map<String, dynamic>> postAsUser(BuildContext context, String url, Map<String, dynamic> body) async {
  try {
    LoginVerifyProvider provider = Provider.of<LoginVerifyProvider>(context, listen: false);
    Map<String, String> headers = {
      "Content-Type": "application/json",
      "social": provider.social,
      "token": provider.token
    };

    Response response = await post(
        _baseUrl + url,
        headers: headers,
        body: jsonEncode(body),
    );

    Map<String, dynamic> data;
    if (response.body != null && response.body != "") data = jsonDecode(response.body);
    return {
      "status": response.statusCode,
      "data": data
    };
  } catch (e) {
    developer.log("Error at postAsUser(): " + e.toString());
    return null;
  }
}

Future<Map<String, dynamic>> multipartPostAsGuest(String url, Map<String, dynamic> body, List<MultipartFile> files) async {
  try {
    var request = MultipartRequest('POST', Uri.parse(_baseUrl+url));
    body.forEach((key, value) {
      request..fields[key] = value.toString();
    });
    if (files != null) request.files?.addAll(files);

    StreamedResponse response = await request.send();
    return {
      "status": response.statusCode,
    };
  } catch (e) {
      developer.log("Error at multipartPostAsUser(): " + e.toString());
    return null;
  }
}

Future<Map<String, dynamic>> multipartPostAsUser(BuildContext context, String url, Map<String, dynamic> body, List<MultipartFile> files) async {
  try {
    LoginVerifyProvider provider = Provider.of<LoginVerifyProvider>(context, listen: false);
    var request = MultipartRequest('POST', Uri.parse(_baseUrl+url))
      ..headers["social"]=provider.social
      ..headers["token"]=provider.token;
    body.forEach((key, value) {
      request..fields[key] = value.toString();
    });
    if (files != null) request.files?.addAll(files);

    StreamedResponse response = await request.send();
    return {
      "status": response.statusCode,
    };
  } catch (e) {
      developer.log("Error at multipartPostAsUser(): " + e.toString());
    return null;
  }
}