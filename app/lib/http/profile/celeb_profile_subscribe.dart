
import 'package:flutter/cupertino.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';

Future<bool> httpCelebProfileSubscribe({BuildContext context, int whomId}) async {
  assert(context != null && whomId != null);
  Map<String, dynamic> body = {
    "whom": whomId
  };
  Map<String, dynamic> response = await postAsUser(context, "/profile/subscribe", body);
  if (response == null || response['status'] != 200) return false;
  return true;
}