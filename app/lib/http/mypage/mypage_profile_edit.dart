import 'package:flutter/cupertino.dart';
import 'package:http/http.dart';
import 'package:http_parser/http_parser.dart';

import '../rest_call_functions.dart';

Future<bool> httpEditProfileImages({BuildContext context, String profileImagePath, String coverImagePath}) async {
  List<MultipartFile> files = [];
  if (profileImagePath != null) {
    files.add(await MultipartFile.fromPath("profileImage", profileImagePath, filename: profileImagePath.split("/").last));
  }
  if (coverImagePath != null) {
    files.add(await MultipartFile.fromPath("bannerImage", coverImagePath, filename: coverImagePath.split("/").last));
  }
  
  Map<String, dynamic> response = await multipartPostAsUser(context, "/mypage/profile/edit", {}, files);
  return response["status"] == 200;
}

Future<bool> httpEditName({BuildContext context, String name}) async {
  Map<String, dynamic> body = {
    "name": name
  };

  Map<String, dynamic> response = await postAsUser(context, "/mypage/profile/edit/name", body);

  return response["status"] == 200;
}
