
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/components/celeb_info_components.dart';
import 'package:rf_tap_fanseem/http/rest_call_functions.dart';
import 'package:rf_tap_fanseem/http/service/dto/http_servie_get_user_membership_dto.dart';
import 'package:rf_tap_fanseem/providers/mypage_view_provider.dart';
import 'package:rf_tap_fanseem/util/timestamp.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/component/scrap_board_component.dart';
import 'package:rf_tap_fanseem/view/setting_membership/membership_mange_route.dart';
import 'package:rf_tap_fanseem/view/tts_service/tts_service_sub_view.dart';

import '../../providers/scrap_board_provider.dart';
import '../../providers/tts_storage_provider.dart';
import '../../view/main/dialog/scrap_dialog.dart';
import '../rest_call_functions.dart';

Future<Map> httpMyPageRootViewRepo({BuildContext context}) async {
  assert(context != null);
  Map<String, dynamic> response = await postAsUser(context, "/mypage/view", {});
  dynamic user = response["data"]["user"];

  List responseCelebData = response["data"]["subscribeList"];
  List<CelebData> celebDataList = [];

  List responseBoardList = response["data"]["boardList"];
  List<ScrapBoardCardData> boardDataList = [];

  for(dynamic data in responseCelebData){
    celebDataList.add(CelebData(
      celebId: data["id"],
      profileImgUrl: data["profileImageUrl"],
      name: data["name"],
      badgeUrl: data["badgeUrl"]
    ));
  }

  for(dynamic data in responseBoardList){
    boardDataList.add(ScrapBoardCardData(
        boardId: data["id"],
        name: data["name"],
        datetimeString: formattedTime(data["updated"]),
        feedCnt: data["feedCount"]
    ));
  }

  return {
    "user": user,
    "celebList": celebDataList,
    "boardList": boardDataList
  };
}

Future<bool> httpMyPageCelebList({BuildContext context}) async {
  assert(context != null);
  Map<String, dynamic> response = await postAsUser(context, "/mypage/subscribeList", {});
  if(response["status"] != 200) return false;

  List responseData = response["data"]["subscribeList"];
  List<CelebData> celebDataList = [];
  for(dynamic data in responseData){
    celebDataList.add(CelebData(
      celebId: data["id"],
      profileImgUrl: data["profileImageUrl"],
      name: data["name"],
      badgeUrl: data["badgeUrl"]
    ));
  }

  Provider.of<MyPageViewProvider>(context, listen: false).celebViewData = celebDataList;

  return true;
}

Future<bool> httpMyPageScrapBoardList({BuildContext context}) async {
  assert(context != null);
  Map<String, dynamic> response = await postAsUser(context, "/mypage/scrap/board/list", {});
  if(response["status"] != 200) return false;

  List responseData = response["data"]["scrapBoardList"];
  List<ScrapBoardData> boardList = [];
  List<ScrapBoardCardData> boardDataList = [];
  for(dynamic data in responseData){
    boardList.add(ScrapBoardData(
      boardId: data["id"],
      boardName: data["name"],
    ));
    boardDataList.add(ScrapBoardCardData(
        boardId: data["id"],
        name: data["name"],
        datetimeString: formattedTime(data["updated"]),
        feedCnt: data["feedCount"]
    ));
  }

  Provider.of<MyPageViewProvider>(context, listen: false).boardViewData = boardDataList;
  Provider.of<ScrapBoardProvider>(context, listen: false).boardList = boardList;

  return true;
}

Future<bool> httpMyPageRequestedTtsList({BuildContext context}) async {
  assert(context != null);
  Map<String, dynamic> response = await postAsUser(context, "/mypage/tts/listall", {});
  if(response["status"] != 200) return false;

  List responseData = response["data"]["ttsListAll"];
  List<RequestedTtsData> requestedTtsList = [];
  for(dynamic data in responseData){
    RequestedTtsStatus status;
    switch(data["status"]){
      case "APPEALED":
        status = RequestedTtsStatus.waiting;
        break;
      case "APPROVED":
        status = RequestedTtsStatus.ok;
        break;
      case "REJECTED":
        status = RequestedTtsStatus.canceled;
        break;
      default:
        continue;
    }

    requestedTtsList.add(RequestedTtsData(
        ttsId: data["id"],
        celebId: data["celeb"],
        celebName: data["celebName"],
        timestamp: formattedTime(data["timestamp"]),
        text: data["text"],
        url: status == RequestedTtsStatus.ok ? data["url"] : (status == RequestedTtsStatus.waiting ? data["previewUrl"] : null),
        status: status
    ));
  }

  Provider.of<MyPageViewProvider>(context, listen: false).requestedTtsList = requestedTtsList;

  return true;
}

Future<bool> httpMyPageCelebRequestedTtsList({BuildContext context}) async {
  assert(context != null);
  Map<String, dynamic> body = {
    "celebId": Provider.of<TtsStorageProvider>(context, listen: false).celebId
  };

  Map<String, dynamic> response = await postAsUser(context, "/mypage/tts/listceleb", body);
  if(response["status"] != 200) return false;

  List responseData = response["data"]["ttsListCeleb"];
  List<RequestedTtsData> requestedTtsList = [];

  for(dynamic data in responseData){
    requestedTtsList.add(RequestedTtsData(
        ttsId: data["id"],
        celebId: data["celeb"],
        celebName: data["celebName"],
        timestamp: data["timestamp"],
        text: data["text"],
        url: data["url"]
    ));
  }

  Provider.of<TtsStorageProvider>(context, listen: false).ttsCelebRequestList = requestedTtsList;
  return true;
}

Future<bool> httpMyPageMembershipList({BuildContext context}) async {
  assert(context != null);
  Map<String, dynamic> response = await postAsUser(context, "/setting/membership/list", {});
  if(response["status"] != 200) return false;

  List responseData = response["data"]["activeList"];
  List<MembershipData> membershipList = [];

  for(dynamic data in responseData){
    membershipList.add(MembershipData(
      membershipName: data["membershipName"],
      celebName: data["celebName"],
      expirationDate: formattedTimeAfter(data["expirationDate"]),
      badgeUrl: data["badgeUrl"],
      duration: data["duration"]
    ));
  }

  Provider.of<MyPageViewProvider>(context, listen: false).membershipList = membershipList;

  return true;
}