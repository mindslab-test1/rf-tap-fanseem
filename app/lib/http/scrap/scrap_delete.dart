
import 'package:flutter/cupertino.dart';

import '../rest_call_functions.dart';

Future<bool> httpScrapDeleteBoard({BuildContext context, int boardId}) async {
  assert(context != null);
  Map<String, dynamic> body = {
    "boardId": boardId
  };
  Map<String, dynamic> response = await postAsUser(context, "/mypage/scrap/board/delete", body);
  return response["status"] == 200;
}