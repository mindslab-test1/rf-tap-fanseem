
import 'package:rf_tap_fanseem/http/feed/dto/http_feed_item_vo.dart';

class HttpFeedListDto {
  final String tailCreated;
  final List<HttpFeedItemVo> feeds;

  const HttpFeedListDto({
    this.tailCreated,
    this.feeds
  }) : assert(feeds != null);
}