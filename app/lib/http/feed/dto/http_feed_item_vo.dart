
class HttpFeedItemVo {
  final int feedId;
  final int celebId;

  final String title;
  final String text;

  final String badgeImageUrl;

  final int feedOwnerId;
  final String feedOwnerName;
  final String feedOwnerProfileImageUrl;
  final int feedOwnerMembershipId;

  final int commentCount;
  final int likeCount;
  final int scrapCount;
  final int viewCount;

  final int categoryId;
  final String categoryName;

  final List<String> pictureUrls;
  final String videoUrl;
  final String videoThumbnailUrl;
  final String youtubeUrl;

  final String updatedTime;
  final bool liked;
  final bool scraped;
  final bool blocked;
  final bool restricted;

  @override
  String toString() {
    return 'HttpFeedItemVo{feedId: $feedId, celebId: $celebId, title: $title, text: $text, badgeImageUrl: $badgeImageUrl, feedOwnerId: $feedOwnerId, feedOwnerName: $feedOwnerName, feedOwnerProfileImageUrl: $feedOwnerProfileImageUrl, feedOwnerMembershipId: $feedOwnerMembershipId, commentCount: $commentCount, likeCount: $likeCount, scrapCount: $scrapCount, viewCount: $viewCount, categoryId: $categoryId, categoryName: $categoryName, pictureUrls: $pictureUrls, videoUrl: $videoUrl, videoThumbnailUrl: $videoThumbnailUrl, updatedTime: $updatedTime, liked: $liked, scraped: $scraped, blocked: $blocked, restricted: $restricted}';
  }

  HttpFeedItemVo({
    this.feedId,
    this.celebId,
    this.title,
    this.text,
    this.badgeImageUrl,
    this.feedOwnerId,
    this.feedOwnerName,
    this.feedOwnerProfileImageUrl,
    this.feedOwnerMembershipId,
    this.commentCount,
    this.likeCount,
    this.scrapCount,
    this.viewCount,
    this.categoryId,
    this.categoryName,
    this.pictureUrls,
    this.videoUrl,
    this.youtubeUrl,
    this.updatedTime,
    this.liked,
    this.scraped,
    this.blocked,
    this.restricted = false,
    this.videoThumbnailUrl
  }) : assert(
  feedId != null &&
      celebId != null &&
      feedOwnerId != null &&
      feedOwnerName != null &&
      commentCount != null &&
      likeCount != null &&
      scrapCount != null &&
      categoryId != null &&
      pictureUrls != null &&
      updatedTime != null &&
      restricted != null
  );
}