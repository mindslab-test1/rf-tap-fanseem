
class HttpCommentItemVo {
  int commentId;
  String text;

  int commentOwnerId;
  String commentOwnerName;
  String commentOwnerProfileImageUrl;
  String badgeUrl;

  int likeCount;

  bool blocked;
  bool deleted;
  bool liked;

  String pictureUrl;
  String ttsUrl;

  String updatedTime;
  int parentId;
  List<HttpCommentItemVo> replies;
}



