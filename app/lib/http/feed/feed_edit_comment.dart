import 'package:flutter/material.dart';

import '../rest_call_functions.dart';

Future<bool> httpFeedEditComment({BuildContext context, int comment, String text, int tts}) async {
  assert(context != null && comment != null && text != null);
  Map<String, dynamic> body = {
    "comment": comment,
    "text": text,
    "tts": tts
  };
  Map<String, dynamic> response = await postAsUser(context, "/community/feed/comment/edit", body);
  if (response == null || response['status'] != 200) return false;

  return true;
}