

import 'package:rf_tap_fanseem/http/report/dto/http_report_history_detail_vo.dart';

class HttpServiceGetUserMembershipDto {
  final int tier;
  final String name;
  final String badgeImageUrl;
  final bool badge;
  final bool sticker;
  final bool voiceLetter;
  final bool ttsWrite;
  final int ttsWriteLimit;
  final List<String> benefitDescriptionList;

  const HttpServiceGetUserMembershipDto({
    this.tier,
    this.name,
    this.badgeImageUrl,
    this.badge,
    this.sticker,
    this.voiceLetter,
    this.ttsWrite,
    this.ttsWriteLimit,
    this.benefitDescriptionList = const []
  });
}
