
class HttpRecentActivityVo {
  final String timestamp;
  final int userId;
  final String name;
  final String profileImageUrl;
  final String elementType;
  final int elementId;
  final String elementText;
  final String elementImageUrl;

  const HttpRecentActivityVo({
    this.timestamp,
    this.userId,
    this.name,
    this.profileImageUrl,
    this.elementType,
    this.elementId,
    this.elementText,
    this.elementImageUrl
  }) : assert(timestamp != null && userId != null && name != null && elementType != null && elementId != null);

  @override
  String toString() {
    return 'HttpRecentActivityVo{timestamp: $timestamp, userId: $userId, name: $name, profileImageUrl: $profileImageUrl, elementType: $elementType, elementId: $elementId, elementText: $elementText, elementImageUrl: $elementImageUrl}';
  }
}