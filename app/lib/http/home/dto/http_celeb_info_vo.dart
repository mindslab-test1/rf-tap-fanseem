
class HttpCelebInfoVo {
  final int id;
  final String profileImageUrl;
  final String name;
  final String badgeUrl;

  const HttpCelebInfoVo({
    this.id,
    this.profileImageUrl,
    this.name,
    this.badgeUrl
  }) : assert(id != null && name != null);

  @override
  String toString() {
    return 'HttpSubscribingCelebVo{id: $id, profileImageUrl: $profileImageUrl, name: $name}';
  }
}