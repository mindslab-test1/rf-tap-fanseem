
import 'package:rf_tap_fanseem/http/home/dto/http_celeb_info_vo.dart';

class HttpHomeCelebListDto {
  final List<HttpCelebInfoVo> allCelebList;
  final List<HttpCelebInfoVo> subscribingCelebList;

  HttpHomeCelebListDto({this.allCelebList, this.subscribingCelebList});

  @override
  String toString() {
    return 'HttpHomeCelebListDto{allCelebList: $allCelebList, subscribingCelebList: $subscribingCelebList}';
  }
}