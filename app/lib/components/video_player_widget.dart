import 'dart:io';

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:chewie/chewie.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rf_tap_fanseem/view/content_write/content_write_route.dart';
import 'package:video_player/video_player.dart';
import '../routes.dart';
import 'package:async/async.dart';

class VideoPlayerWidget extends StatefulWidget {
  final bool isNetwork;
  final bool autoPlay;
  final String url;
  const VideoPlayerWidget(
      {Key key, this.isNetwork = false, this.autoPlay = false, this.url})
      : super(key: key);

  @override
  _VideoPlayerWidgetState createState() => _VideoPlayerWidgetState();
}

class _VideoPlayerWidgetState extends State<VideoPlayerWidget> {
  AsyncMemoizer _memoizer = new AsyncMemoizer();
  VideoPlayerController _playerController;
  ChewieController _chewieController;

  List<String> getBackButtonInterceptorName() {
    List<String> list =
    BackButtonInterceptor.describe().split(new RegExp(r"([ |\n])"));
    List<String> names = [];
    for (int i = 0; i < list.length; i++) {
      if (list[i] == "BackButtonInterceptor:") {
        names.add(list[i + 1].substring(0, list[i + 1].length - 1));
      }
    }
    return names;
  }

  setPlayer() {
    return this._memoizer.runOnce(() async {
      if (widget.isNetwork) {
        _playerController = VideoPlayerController.network(
          widget.url,
        );
      } else {
        _playerController = VideoPlayerController.file(File(widget.url));
      }
      await _playerController.initialize();
      _playerController.addListener(() async {
        if (_playerController.value.duration ==
            _playerController.value.position) {
          await _playerController
              .seekTo(Duration(hours: 0, minutes: 0, seconds: 0));
          await _playerController.pause();
        }
      });
      _chewieController = ChewieController(
          aspectRatio: _playerController.value.size.width /
              _playerController.value.size.height,
          videoPlayerController: _playerController,
          deviceOrientationsAfterFullScreen: [DeviceOrientation.portraitUp],
          fullScreenByDefault: false,
          autoPlay: false,
          looping: false,
          allowedScreenSleep: false,
          autoInitialize: true
      );
      _chewieController.addListener(() {
        if (_chewieController.isFullScreen) {
          BackButtonInterceptor.removeByName("pop");
          BackButtonInterceptor.add((pop) {
            _chewieController.exitFullScreen();
            return true;
          }, name: "fullscreen");
        }
        else {
          if (getBackButtonInterceptorName()[0] == "fullscreen") {
            BackButtonInterceptor.removeByName("fullscreen");
            BackButtonInterceptor.add((pop) {
              if(ContentWriteRoute.isUploading){
                Fluttertoast.showToast(msg: "컨텐츠 업로드 중입니다.", gravity: ToastGravity.TOP);
              }
              else{
                Navigator.popUntil(context,ModalRoute.withName(kRouteHome));
              }
              return true;
            }, name: "pop", zIndex: 2, ifNotYetIntercepted: true);
          }
        }
      });
      return true;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: (MediaQuery.of(context).size.width - 60) * 9 / 16,
      child: Center(
        child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: FutureBuilder(
                future: setPlayer(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    return Chewie(
                      controller: _chewieController,
                    );
                  }
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                })),
      ),
    );
  }

  @override
  void dispose() {
    _playerController.dispose();
    _chewieController.dispose();
    super.dispose();
  }
}
