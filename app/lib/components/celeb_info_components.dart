
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/view/main/component/partial_components.dart';

import '../colors.dart';

class CelebListInfoItem extends StatelessWidget {
  final String celebName;
  final String greeting;
  final String profileImgUrl;

  const CelebListInfoItem({Key key, this.celebName = "", this.greeting = "", this.profileImgUrl}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          UserAvatar(
            imgUrl: profileImgUrl,
          ),
          SizedBox(
            width: 15,
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                celebName,
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 17,
                    fontWeight: FontWeight.bold
                ),
              ),
              SizedBox(height: 3,),
              Text(
                greeting,
                textAlign: TextAlign.left,
                style: TextStyle(
                    color: kNavigationPressableColor,
                    fontSize: 13
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}


class CelebData {
  final int celebId;
  final String name;
  final String greeting;
  final String profileImgUrl;
  final String badgeUrl;

  CelebData({this.celebId, this.name, this.greeting  = "", this.profileImgUrl, this.badgeUrl});
}

