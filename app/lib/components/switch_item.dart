import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/colors.dart';

class SwitchItem extends StatelessWidget {
  final Function(bool) onChange;
  final String name;
  final bool value;

  const SwitchItem({
    Key key,
    @required
    this.onChange,
    this.name = "",
    this.value = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(left: 45, right: 30),
      height: 60,
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        border: Border.all(
          color: kBorderColor,
          width: 0.5
        ),
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.09),
            blurRadius: 8,
            offset: Offset(0, 2)
          )
        ]
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            child: Text(
              name,
              textAlign: TextAlign.left,
              style: TextStyle(
                color: Colors.black,
                fontSize: 13,
              ),
            ),
          ),
          Container(
            width: 55,
            child: Switch(
              value: value,
              onChanged: onChange,
            ),
          )
        ],
      ),
    );
  }
}