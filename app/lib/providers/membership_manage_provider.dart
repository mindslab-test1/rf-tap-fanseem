
import 'package:flutter/material.dart';

class MembershipManageProvider extends ChangeNotifier{
  String _membershipName;
  String _celebName;
  String _badgeUrl;
  String _createDate;
  String _updateDate;
  String _productId;
  int _cost;
  int _continuous;

  String get membershipName => _membershipName;
  String get celebName => _celebName;
  String get badgeUrl => _badgeUrl;
  String get createDate => _createDate;
  String get updateDate => _updateDate;
  String get productId => _productId;
  int get cost => _cost;
  int get continuous => _continuous;

  void setEndMembershipInfo({
    String membershipName,
    String celebName,
    String badgeUrl,
    String createDate,
    String updateDate,
    String productId,
    int remainingDays,
    int continuous,
    int cost,
  }){
    this._membershipName = membershipName;
    this._celebName = celebName;
    this._badgeUrl = badgeUrl;
    this._createDate = createDate;
    this._updateDate = updateDate;
    this._productId = productId;
    this._cost = cost;
    this._continuous = continuous;
    notifyListeners();
  }
}