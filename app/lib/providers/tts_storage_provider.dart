import 'package:flutter/material.dart';

import '../view/tts_service/tts_service_sub_view.dart';
import '../view/tts_service/tts_service_sub_view.dart';

class TtsStorageProvider extends ChangeNotifier {
  int _celebId;
  String _celebName;
  String _profileImgUrl;
  Function _onBackButton = () => null;

  List<RequestedTtsData> _ttsCelebRequestList = [];

  int get celebId => _celebId;
  String get celebName => _celebName;
  String get profileImgUrl => _profileImgUrl;
  Function get onBackButton => _onBackButton;

  List<RequestedTtsData> get ttsCelebRequestList => _ttsCelebRequestList;

  set onBackButton(Function value){
    _onBackButton = value;
    notifyListeners();
  }

  set ttsCelebRequestList(List<RequestedTtsData> value){
    _ttsCelebRequestList = value;
    notifyListeners();
  }

  void setTtsStorageCelebInfo({int celebId, String celebName, String profileImgUrl}){
    _celebId = celebId;
    _celebName = celebName;
    _profileImgUrl = profileImgUrl;
    notifyListeners();
  }
}