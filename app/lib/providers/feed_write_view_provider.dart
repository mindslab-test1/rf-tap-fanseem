
import 'package:flutter/material.dart';

class FeedWriteViewProvider extends ChangeNotifier{
  FeedWriteType _writeType = FeedWriteType.general;
  String _title = "피드 글쓰기";

  FeedWriteType get writeType => _writeType;
  String get title => _title;

  set writeType(FeedWriteType value){
    _writeType = value;
    switch(value){

      case FeedWriteType.general:
      case FeedWriteType.celeb:
        _title = "피드 글쓰기";
        break;
      case FeedWriteType.contents:
        _title = "콘텐츠 올리기";
        break;
    }
    notifyListeners();
  }
}

enum FeedWriteType {
  general,
  celeb,
  contents
}