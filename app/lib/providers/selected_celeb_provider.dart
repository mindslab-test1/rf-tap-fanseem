
import 'package:flutter/cupertino.dart';

class SelectedCelebProvider extends ChangeNotifier {
  int _celebId = 0;
  String _celebName = "";

  String get celebName => _celebName;

  set celebName(String value) {
    _celebName = value;
    notifyListeners();
  }

  int get celebId => _celebId;

  set celebId(int value) {
    _celebId = value;
    notifyListeners();
  }
}