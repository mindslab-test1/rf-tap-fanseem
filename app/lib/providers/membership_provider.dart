
import 'package:flutter/material.dart';

class MembershipProvider extends ChangeNotifier{
  String _membershipId = "";

  String _membershipName = "__default__";
  int _membershipCost = 0;

  String get membershipId => _membershipId;

  String get membershipName => _membershipName;
  int get membershipCost => _membershipCost;

  void setMembershipInfo(String membershipId, String membershipName, int membershipCost, ){
    _membershipId = membershipId;
    _membershipName = membershipName;
    _membershipCost = membershipCost;
    notifyListeners();
  }
}