
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/profile/dto/http_profile_view_dto.dart';

class CelebProfileProvider extends ChangeNotifier{
  int _celebId = 0;
  String _celebName = "";
  HttpProfileViewDto _celeb;

  HttpProfileViewDto get celeb => _celeb;
  set celeb(HttpProfileViewDto value) {
    _celeb = value;
  }

  int get celebId => _celebId;
  String get celebName => _celebName;

  void setCelebProfile({int celebId, String celebName}){
    _celebId = celebId;
    _celebName = celebName;
    notifyListeners();
  }
}