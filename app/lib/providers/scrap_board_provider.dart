
import 'package:flutter/material.dart';

import '../view/main/component/feed_component.dart';
import '../view/main/dialog/scrap_dialog.dart';

class ScrapBoardProvider extends ChangeNotifier{
  int _boardId = 0;
  String _boardName = "";
  List<ScrapBoardData> _boardList = [];
  List<ScrapFeedData> _boardFeedList = [];

  int get boardId => _boardId;
  String get boardName => _boardName;
  List<ScrapBoardData> get boardList => _boardList;
  List<ScrapFeedData> get boardFeedList => _boardFeedList;


  set boardList(List<ScrapBoardData> value) {
    _boardList = value;
    notifyListeners();
  }

  set boardFeedList(List<ScrapFeedData> value) {
    _boardFeedList = value;
    notifyListeners();
  }

  void setBoard({boardId, boardName}){
    _boardId = boardId;
    _boardName = boardName;
    notifyListeners();
  }
}