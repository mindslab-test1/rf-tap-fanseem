

import 'dart:io';

import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/routes.dart';

VoidCallback navigateToLogin(BuildContext context){
  String routeName = Platform.isIOS ? kRouteIosLogin : kRouteLogin;
  return () => Navigator.pushNamed(context, routeName);
}

VoidCallback dummyFunction() => () => null;