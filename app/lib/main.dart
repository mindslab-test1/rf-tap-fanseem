import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/providers/celeb_profile_provider.dart';
import 'package:rf_tap_fanseem/providers/celeb_tts_provider.dart';
import 'package:rf_tap_fanseem/providers/comment_reply_provider.dart';
import 'package:rf_tap_fanseem/providers/content_write_data_provider.dart';
import 'package:rf_tap_fanseem/providers/feed_write_data_provider.dart';
import 'package:rf_tap_fanseem/providers/login_provider.dart';
import 'package:rf_tap_fanseem/providers/membership_manage_provider.dart';
import 'package:rf_tap_fanseem/providers/mypage_edit_provider.dart';
import 'package:rf_tap_fanseem/providers/mypage_view_provider.dart';
import 'package:rf_tap_fanseem/providers/report_manage_provider.dart';
import 'package:rf_tap_fanseem/providers/scrap_board_provider.dart';
import 'package:rf_tap_fanseem/providers/selected_celeb_provider.dart';
import 'package:rf_tap_fanseem/providers/tts_storage_provider.dart';
import 'package:rf_tap_fanseem/providers/tts_write_provider.dart';
import 'package:rf_tap_fanseem/util/firebase_listener.dart';
import 'package:rf_tap_fanseem/view/celeb_profile/celeb_profile_route.dart';
import 'package:rf_tap_fanseem/view/content_write/content_write_route.dart';
import 'package:rf_tap_fanseem/view/edit_profile/edit_profile_route.dart';
import 'package:rf_tap_fanseem/view/feed_detail/feed_detail_photo.dart';
import 'package:rf_tap_fanseem/view/feed_detail/feed_detail_route.dart';
import 'package:rf_tap_fanseem/view/feed_write/feed_write_route.dart';
import 'package:rf_tap_fanseem/providers/feed_write_view_provider.dart';
import 'package:rf_tap_fanseem/providers/feed_data_provider.dart';
import 'package:rf_tap_fanseem/providers/membership_provider.dart';
import 'package:rf_tap_fanseem/providers/navigator_provider.dart';
import 'package:rf_tap_fanseem/providers/setting_page_provider.dart';
import 'package:rf_tap_fanseem/providers/sub_view_controller_provider.dart';
import 'package:rf_tap_fanseem/view/login/login_ios_route.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage_celeb/tts_voice/tts_voice_route.dart';
import 'package:rf_tap_fanseem/view/membership_detail/membership_detail_route.dart';
import 'package:rf_tap_fanseem/view/membership_end/membership_end_route.dart';
import 'package:rf_tap_fanseem/view/membership_join/member_join_route.dart';
import 'package:rf_tap_fanseem/view/messaging/messaging_route.dart';
import 'package:rf_tap_fanseem/view/messaging/states/messaging_provider.dart';
import 'package:rf_tap_fanseem/view/report_detail/report_detail_route.dart';
import 'package:rf_tap_fanseem/view/report_manage/report_manage_route.dart';
import 'package:rf_tap_fanseem/view/scrap_board/scrap_board_route.dart';
import 'package:rf_tap_fanseem/view/setting_celeb_notice/push_celebs_route.dart';
import 'package:rf_tap_fanseem/view/setting_membership/membership_mange_route.dart';
import 'package:rf_tap_fanseem/view/setting_push/setting_push_route.dart';
import 'package:rf_tap_fanseem/view/signup/signup_route.dart';
import 'package:rf_tap_fanseem/view/signup/states/signup_category_provider.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/view/login/login_route.dart';
import 'package:rf_tap_fanseem/view/edit_profile/edit_category_route.dart';
import 'package:rf_tap_fanseem/view/main/main_route.dart';
import 'package:rf_tap_fanseem/view/signup/states/signup_data_provider.dart';
import 'package:rf_tap_fanseem/view/splash/splash_route.dart';
import 'package:rf_tap_fanseem/view/tts_service/tts_service_route.dart';
import 'package:rf_tap_fanseem/view/tts_storage/tts_storage_route.dart';
import 'package:rf_tap_fanseem/view/tts_write/tts_write_route.dart';

import 'routes.dart';

void main() {
  InAppPurchaseConnection.enablePendingPurchases();
  WidgetsFlutterBinding.ensureInitialized();
  FlutterDownloader.initialize(
      debug: false // optional: set false to disable printing logs to console
  );
  runApp(FanseemApp());
  FirebaseListener();
}

class FanseemApp extends StatelessWidget {
  static final navigatorKey = new GlobalKey<NavigatorState>();
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<LoginVerifyProvider>(create: (_) => LoginVerifyProvider(),),
        ChangeNotifierProvider<MainNavigationProvider>(create: (_) => MainNavigationProvider(),),
        ChangeNotifierProvider<SettingPageProvider>(create: (_) => SettingPageProvider(),),
        ChangeNotifierProvider<CategoryProvider>(create: (_) => CategoryProvider(),),
        ChangeNotifierProvider<SubViewControllerProvider>(create: (_) => SubViewControllerProvider(),),
        ChangeNotifierProvider<FeedDataProvider>(create: (_) => FeedDataProvider(),),
        ChangeNotifierProvider<FeedWriteViewProvider>(create: (_) => FeedWriteViewProvider(),),
        ChangeNotifierProvider<FeedWriteDataProvider>(create: (_) => FeedWriteDataProvider(),),
        ChangeNotifierProvider<ContentWriteDataProvider>(create: (_) => ContentWriteDataProvider(),),
        ChangeNotifierProvider<MessagingDisplayProvider>(create: (_) => MessagingDisplayProvider(),),
        ChangeNotifierProvider<MembershipProvider>(create: (_) => MembershipProvider(),),
        ChangeNotifierProvider<MembershipManageProvider>(create: (_) => MembershipManageProvider(),),
        ChangeNotifierProvider<ScrapBoardProvider>(create: (_) => ScrapBoardProvider(),),
        ChangeNotifierProvider<CelebProfileProvider>(create: (_) => CelebProfileProvider(),),
        ChangeNotifierProvider<SignUpDataProvider>(create: (_) => SignUpDataProvider(),),
        ChangeNotifierProvider<SelectedCelebProvider>(create: (_) => SelectedCelebProvider(),),
        ChangeNotifierProvider<ReportManageProvider>(create: (_) => ReportManageProvider(),),
        ChangeNotifierProvider<TtsStorageProvider>(create: (_) => TtsStorageProvider(),),
        ChangeNotifierProvider<TtsWriteProvider>(create: (_) => TtsWriteProvider(),),
        ChangeNotifierProvider<MyPageViewProvider>(create: (_) => MyPageViewProvider(),),
        ChangeNotifierProvider<MyPageEditProvider>(create: (_) => MyPageEditProvider(),),
        ChangeNotifierProvider<CelebTtsProvider>(create: (_) => CelebTtsProvider(),),
        ChangeNotifierProvider<CommentReplyProvider>(create: (_) => CommentReplyProvider(),)
      ],
      child: GestureDetector(
      onTap: (){FocusScope.of(context).requestFocus(new FocusNode());},
      child : MaterialApp(
            theme: ThemeData(
              primaryColor: kMainColor,
              backgroundColor: kMainBackgroundColor,
            ),
            initialRoute: kRouteSplash,
            navigatorKey: navigatorKey,
            routes: routes,
//        onGenerateRoute: (settings) => PageRouteBuilder(
//          pageBuilder: (_, __, ___) => routes[settings.name],
//          transitionsBuilder: (_, animation, __, child) => FadeTransition(opacity: animation, child: child,)
//        ),
          )),
      );
  }
}

dynamic routes = {
  kRouteInitial: (context) => Scaffold(),
  kRouteSplash: (context) => SplashScreenRoute(),
  kRouteHome: (context) => MainRoute(),
  kRouteLogin: (context) => LoginRoute(),
  kRouteIosLogin: (context) => LoginIosRoute(),
  kRouteCategoryEdit: (context) => EditCategoryRoute(),
  kRouteWriteFeed: (context) => FeedWriteRoute(),
  kRouteWriteContent: (context) => ContentWriteRoute(),
  kRouteEditProfile: (context) => EditProfileRoute(),
  kRouteFeedDetail: (context) => FeedDetailRoute(),
  kRouteFeedDetailPhoto: (context) => FeedDetailPhotoView(),
  kRouteMessaging: (context) => MessagingRoute(),
  kRouteMembershipJoin: (context) => MembershipRoute(),
  kRouteMembershipManage: (context) => MembershipManageRoute(),
  kRouteMembershipDetail: (context) => MembershipDetailRoute(),
  kRouteMembershipEnd: (context) => MembershipEndRoute(),
  kRouteScrapBoard: (context) => ScrapBoardRoute(),
  kRouteCelebProfile: (context) => CelebProfileRoute(),
  kRouteSignUp: (context) => SignUpRoute(),
  kRouteSettingsPush: (context) => PushSettingRoute(),
  kRouteSettingsCelebPush: (context) => CelebPushSettingsRoute(),
  kRouteTtsService: (context) => TtsServiceRoute(),
  kRouteTtsAuth: (context) => TtsCelebAuthRoute(),
  kRouteTtsStorage: (context) => TtsStorageRoute(),
  kRouteTtsWrite: (context) => TtsWriteRoute(),
  kRouteReportManage: (context) => ReportManageRoute(),
  kRouteReportDetail: (context) => ReportDetailRoute(),
};

void toDo() => Fluttertoast.showToast(msg: "TODO", toastLength: Toast.LENGTH_SHORT);