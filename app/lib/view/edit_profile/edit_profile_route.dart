
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/providers/mypage_edit_provider.dart';
import 'package:rf_tap_fanseem/view/edit_profile/edit_profile_body.dart';

import '../../colors.dart';

class EditProfileRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0.0,
        automaticallyImplyLeading: false,
        title: _EditProfileAppBar(),
      ),
      body: EditProfileBody(),
    );
  }
}

class _EditProfileAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 30,
                  ),
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                        child: Container(
                          child: Icon(
                            Icons.arrow_back_ios,
                            size: 30,
                            color: kAppBarButtonColor,
                          ),
                        )
                    ),
                  ),
                ]
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Text(
                "내 정보 수정",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: kAppBarButtonColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(),
          ),
        ],
      ),
    );
  }
}
