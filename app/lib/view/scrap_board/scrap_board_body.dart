
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/http/scrap/scrap_view.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/view/main/component/feed_component.dart';

import '../../providers/scrap_board_provider.dart';
import '../../providers/scrap_board_provider.dart';

class ScrapBoardBody extends StatefulWidget {
  @override
  _ScrapBoardBodyState createState() => _ScrapBoardBodyState();
}

class _ScrapBoardBodyState extends State<ScrapBoardBody> {
  Future<bool> _getData(BuildContext context) async {
    return await httpScrapBoardView(
        context: context,
        boardId: Provider.of<ScrapBoardProvider>(context, listen: false).boardId
    );
  }

  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add((pop) {
      Navigator.popUntil(context, ModalRoute.withName(kRouteHome));
      return true;
    }, name: "pop_scrap_board", zIndex: 1, ifNotYetIntercepted: true);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder(
          future: _getData(context),
          builder: (context, snapshot) {
            if(snapshot.hasData) {
              return SingleChildScrollView(
                child: ScrapFeedListView(
                  feedData: Provider.of<ScrapBoardProvider>(context).boardFeedList,
                ),
              );
            } else {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            }
          }
      ),
    );
  }

  @override
  void dispose() {
    BackButtonInterceptor.removeByName("pop_scrap_board");
    super.dispose();
  }
}

List<ScrapFeedData> _testData = [
  ScrapFeedData(
      title: "강아지",
      viewCount: 10,
      userName: "윤도현",
      postTimestamp: "1900.01.01",
      text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ornare enim ut dignissim ultrices. Aenean hendrerit aliquet quam, nec mattis lacus rutrum vitae. Curabitur et mattis ipsum, ac mattis eros. Proin sollicitudin tellus nec dui vulputate, et iaculis ipsum semper. Fusce facilisis lacus tortor, eu malesuada risus blandit quis. Pellentesque et mauris semper eros pulvinar dapibus vitae sit amet dolor. Aliquam cursus urna odio, id ornare libero fringilla sit amet. Etiam sollicitudin hendrerit urna. Curabitur viverra arcu in mauris blandit aliquet. Aliquam vitae libero tempor, vulputate odio quis, pretium orci. Sed lobortis ultrices sapien eu molestie. Cras volutpat ante facilisis ante luctus lobortis sit amet in tortor.",
      imgUrl: ["http://114.108.173.102:12303/rf-tap/image/5ee1a86b.jpg"],
      likeCount: 10
  ),
  ScrapFeedData(
      userName: "Lorem Ipsum",
      text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ornare enim ut dignissim ultrices. Aenean hendrerit aliquet quam, nec mattis lacus rutrum vitae. Curabitur et mattis ipsum, ac mattis eros. Proin sollicitudin tellus nec dui vulputate, et iaculis ipsum semper. Fusce facilisis lacus tortor, eu malesuada risus blandit quis. Pellentesque et mauris semper eros pulvinar dapibus vitae sit amet dolor. Aliquam cursus urna odio, id ornare libero fringilla sit amet. Etiam sollicitudin hendrerit urna. Curabitur viverra arcu in mauris blandit aliquet. Aliquam vitae libero tempor, vulputate odio quis, pretium orci. Sed lobortis ultrices sapien eu molestie. Cras volutpat ante facilisis ante luctus lobortis sit amet in tortor.\n\nProin sit amet orci sit amet sem sodales vestibulum ut at est. Cras lacinia pulvinar ante vel tempus. Donec a tempus purus, at fermentum leo. Praesent metus massa, venenatis quis ante vitae, rutrum pellentesque elit. Sed et rutrum sapien, fermentum rutrum ligula. Pellentesque auctor risus non sem elementum dapibus. Mauris varius nisl velit, sodales viverra enim aliquam non. Sed orci velit, efficitur eget cursus nec, egestas vel ex. Aenean a metus in felis tincidunt finibus. Quisque malesuada, massa id placerat elementum, quam arcu fermentum libero, posuere volutpat ante nunc eget lorem. Nunc maximus ut libero non mollis. Phasellus vel egestas tellus. Mauris leo ex, blandit sed ornare vitae, maximus ut quam. Nulla facilisi. Nunc at suscipit justo. Etiam scelerisque nisi non urna commodo pharetra.",
      imgUrl: ["http://114.108.173.102:12303/rf-tap/image/a0a99e0c.jpg","http://114.108.173.102:12303/rf-tap/image/6feef7bd.png","http://114.108.173.102:12303/rf-tap/image/1bc1d401.jpg","http://114.108.173.102:12303/rf-tap/image/d50b8395.jpg",]
  )
];
