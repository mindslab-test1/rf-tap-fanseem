
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/http/mypage/mypage_view.dart';
import 'package:rf_tap_fanseem/providers/membership_manage_provider.dart';
import 'package:rf_tap_fanseem/providers/mypage_view_provider.dart';
import 'package:rf_tap_fanseem/routes.dart';

import '../../colors.dart';

class MembershipManageRoute extends StatelessWidget {
  Future<bool> _getData(BuildContext context) async {
    return await httpMyPageMembershipList(context: context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0.0,
        automaticallyImplyLeading: false,
        title: _MemberManageAppbar(),
      ),
      body: Container(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              height: 51,
              child: Row(
                children: <Widget>[
                  Text(
                    "가입중인 멤버십",
                    textAlign: TextAlign.start,
                    style: TextStyle(
                      color: kMainColor,
                      fontSize: 17,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ],
              ),
            ),
            FutureBuilder(
              future: _getData(context),
              builder: (context, snapshot) {
                if(snapshot.hasData)
                  return SingleChildScrollView(
                    child: Column(
                      children: Provider.of<MyPageViewProvider>(context).membershipList.isEmpty ? [
                        Container(
                          height: 300,
                          child: Center(
                            child: Text("가입한 멤버십이 없습니다."),
                          ),
                        )
                      ] :
                        Provider.of<MyPageViewProvider>(context).membershipList.map((e) => _MembershipInfo(
                          membershipName: e.membershipName,
                          celebName: e.celebName,
                          badgeUrl: e.badgeUrl,
                          updateDate: e.expirationDate,
                        )).toList()
                    ),
                  );
                return Container(
                  height: 300,
                  child: Center(
                    child: CircularProgressIndicator(),
                  ),
                );
              }
            ),
          ],
        ),
      ),
    );
  }
}

class _MembershipInfo extends StatelessWidget {
  final String membershipName;
  final String celebName;
  final String badgeUrl;
  final String createDate;
  final String updateDate;

  const _MembershipInfo({
    Key key,
    this.membershipName = "Ambitous",
    this.celebName = "권욱제",
    this.badgeUrl,
    this.createDate,
    this.updateDate,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO calculate
    int remainingDays = 30;
    int continuous = 1;

    return GestureDetector(
      onTap: () {
        // TODO get cost
        int cost = 0;
        Provider.of<MembershipManageProvider>(context, listen: false).setEndMembershipInfo(
          membershipName: membershipName,
          celebName: celebName,
          badgeUrl: badgeUrl,
          createDate: createDate,
          updateDate: updateDate,
          remainingDays: remainingDays,
          continuous: continuous,
          cost: cost
        );

        return Navigator.pushNamed(context, kRouteMembershipDetail);
      },
      child: Container(
        height: 89,
        decoration: const BoxDecoration(
          color: kMainBackgroundColor,
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.16),
              blurRadius: 6,
              offset: const Offset(0, 2),
            ),
          ]
        ),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    width: 88,
                    child: Center(
                      child: Container(
                        width: 44,
                        child: badgeUrl == null ? Container() : CachedNetworkImage(
                          imageUrl: badgeUrl,
                          fit: BoxFit.fitWidth,
                          placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                          errorWidget: (context, url, error) => Icon(Icons.error),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          membershipName,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 15,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                        SizedBox(height: 2,),
                        Text(
                          celebName,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 13,
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
//                      Text(
//                        "남은 기간: $remainingDays일",
//                        textAlign: TextAlign.center,
//                        style: TextStyle(
//                          color: kNegativeTextColor,
//                          fontSize: 12
//                        ),
//                      ),
//                      SizedBox(height: 2,),
//                      Text(
//                        "$continuous개월 연속구독중",
//                        style: TextStyle(
//                          color: kNegativeTextColor,
//                          fontSize: 10
//                        ),
//                      )
                    ],
                  ),
                  SizedBox(
                    width: 30,
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}


class _MemberManageAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 30,
                  ),
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                        child: Container(
                          child: Icon(
                            Icons.arrow_back_ios,
                            size: 30,
                            color: kAppBarButtonColor,
                          ),
                        )
                    ),
                  ),
                ]
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Text(
                "내 멤버십 관리",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: kAppBarButtonColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(),
          ),
        ],
      ),
    );
  }
}

class MembershipData{
  final String membershipName;
  final String celebName;
  final String expirationDate;
  final String badgeUrl;
  final int duration;
  final String productId;

  MembershipData({
    this.membershipName,
    this.celebName,
    this.expirationDate,
    this.badgeUrl,
    this.duration,
    this.productId
  });
}