import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/components/progress_dialog.dart';
import 'package:rf_tap_fanseem/http/celeb_feed/celeb_feed_write.dart';
import 'package:rf_tap_fanseem/http/feed/feed_write.dart';
import 'package:rf_tap_fanseem/providers/feed_write_data_provider.dart';
import 'package:rf_tap_fanseem/providers/login_provider.dart';
import 'package:rf_tap_fanseem/providers/selected_celeb_provider.dart';
import 'package:rf_tap_fanseem/view/feed_write/feed_write_body.dart';
import 'package:rf_tap_fanseem/providers/feed_write_view_provider.dart';

import '../../colors.dart';

class FeedWriteRoute extends StatelessWidget {
  static bool isUploading = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: _FeedWriteAppBar(),
      ),
      body: FeedWriteBody(),
    );
  }
}

class _FeedWriteAppBar extends StatefulWidget {
  @override
  __FeedWriteAppBarState createState() => __FeedWriteAppBarState();
}

class __FeedWriteAppBarState extends State<_FeedWriteAppBar> {
  bool _isUploading = false;
  ProgressDialog _pr;

  @override
  Widget build(BuildContext context) {
    _pr = ProgressDialog(
        context,
        showLogs: true,
        isDismissible: false,
        customBody: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 45,
                width: 45,
//              child: Image.asset(
//                "assets/icon/feather_loader.png", fit: BoxFit.contain,
//              ),
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(kNegativeTextColor),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                "업로드 중입니다.",
                style: TextStyle(
                    fontSize: 12,
                    color: kNegativeTextColor
                ),
              )
            ],
          ),
        )
    );
    _pr.style(
        backgroundColor: Colors.transparent,
        elevation: 0,
        padding: const EdgeInsets.all(0)
    );

    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 30,
                  ),
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                        child: Icon(
                          FontAwesomeIcons.times,
                          size: 27,
                          color: kAppBarButtonColor,
                        )
                    ),
                  ),
                ]
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Text(
                Provider.of<FeedWriteViewProvider>(context).title,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: kAppBarButtonColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    child: GestureDetector(
                      onTap: () async {
                        FeedWriteRoute.isUploading=true;
                        await _pr.show();
                        await Future.delayed(Duration(milliseconds: 200));
                        if (_isUploading) {
                          Fluttertoast.showToast(
                              msg: "업로드 중입니다...", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                          return;
                        }
                        _isUploading = true;
                        FeedWriteDataProvider provider = Provider.of<FeedWriteDataProvider>(
                            context, listen: false);
                        if (provider.text == null) {
                          await _pr.hide();
                          Fluttertoast.showToast(
                              msg: "빈 텍스트는 업로드할 수 없습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                          return;
                        }
                        bool isUserFeedWrite = (
                            Provider.of<LoginVerifyProvider>(context, listen: false).userId !=
                                Provider.of<SelectedCelebProvider>(context, listen: false).celebId
                        );

                        bool isSuccess = await (isUserFeedWrite ? httpFeedWrite(
                            context: context,
                            celebId: Provider
                                .of<SelectedCelebProvider>(context, listen: false)
                                .celebId,
                            categoryId: provider.categoryId,
                            text: provider.text,
                            imageUrls: provider.imageUrls,
                            youtubeUrl: provider.youtubeUrl,
                            videoUrl: provider.videoUrl
                        ) : httpCelebFeedWrite(
                            context: context,
                            categoryId: provider.categoryId,
                            text: provider.text,
                            imageUrls: provider.imageUrls,
                            videoUrl: provider.videoUrl,
                            youtubeUrl: provider.youtubeUrl,
                            accessLevel: provider.accessLevel
                        ));
                        if (isSuccess) {
                          await _pr.hide();
                          Navigator.pop(context, true);
                        }
                        else {
                          _isUploading = false;
                          await _pr.hide();
                          Fluttertoast.showToast(msg: "피드 업로드에 실패했습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                        }
                        FeedWriteRoute.isUploading=false;
                      },
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "완료",
                          style: TextStyle(
                            color: Theme.of(context).backgroundColor,
                            fontSize: 15,
                          ),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 24,)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
