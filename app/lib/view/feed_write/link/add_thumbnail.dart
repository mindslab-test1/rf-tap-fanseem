//library add_thumbnail;

export 'thumbnail_list_vew.dart';
export 'package:rf_tap_fanseem/view/feed_write/link/src/model/media_info.dart';
import 'package:rf_tap_fanseem/view/feed_write/link/src/bloc/bloc.dart';
import 'package:rf_tap_fanseem/view/feed_write/link/src/model/media_info.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'src/widget/add_thumbnail_widget.dart';

class Thumbnail {
  static Future<void> addLink({
    BuildContext context,
    ValueChanged<MediaInfo> onLinkAdded,
  }) async {
    var media = await showGeneralDialog(
        context: context,
        barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 150),
        pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) => MultiBlocProvider(
          providers: [
            BlocProvider<ThumbnailBloc>(
              create: (BuildContext context) => ThumbnailBloc(),
            ),
          ],
          child: AddMediaDialogContent(
            titleText: "링크 삽입",
            textFieldHintText: "링크 주소를 입력하세요"
          ),
        ));
    if (media != null) {
      onLinkAdded(media);
    }
  }
}
