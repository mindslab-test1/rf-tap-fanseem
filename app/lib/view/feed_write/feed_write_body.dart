
import 'dart:io';
import 'package:async/async.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/components/progress_dialog.dart';
import 'package:rf_tap_fanseem/components/video_player_widget.dart';
import 'package:rf_tap_fanseem/components/youtube_player_widget.dart';
import 'package:rf_tap_fanseem/http/celeb_feed/celeb_feed_listcategory.dart';
import 'package:rf_tap_fanseem/http/feed/feed_listcategory.dart';
import 'package:rf_tap_fanseem/providers/feed_write_data_provider.dart';
import 'package:rf_tap_fanseem/providers/feed_write_view_provider.dart';
import 'package:rf_tap_fanseem/providers/login_provider.dart';
import 'package:rf_tap_fanseem/providers/selected_celeb_provider.dart';
import 'package:rf_tap_fanseem/util/photo_util.dart';
import 'package:rf_tap_fanseem/view/feed_write/feed_write_route.dart';
import 'package:rf_tap_fanseem/view/main/component/feed_component.dart';
import 'package:rf_tap_fanseem/view/main/dialog/category_edit_dialog.dart';
import 'package:rf_tap_fanseem/view/main/dialog/scope_dialog.dart';

import '../../routes.dart';
import 'link/add_thumbnail.dart';

class FeedWriteBody extends StatefulWidget {
  final List<String> categories;
  const FeedWriteBody({Key key, this.categories = const []}) : super(key: key);

  @override
  _FeedWriteBodyState createState() => _FeedWriteBodyState();
}

class _FeedWriteBodyState extends State<FeedWriteBody> {

  // TODO sort this thing out!
  List<Widget> _basicIcons = [];
  int _selected;
  bool _textOver;

  AsyncMemoizer _asyncMemoizer = new AsyncMemoizer();
  TextEditingController _textController = TextEditingController(

  );

  List<CategoryData> _categories;

  Future<void> _getData() {
    return this._asyncMemoizer.runOnce(() async {
      int userId = Provider.of<LoginVerifyProvider>(context, listen: false).userId;
      int celebId = Provider.of<SelectedCelebProvider>(context, listen: false).celebId;
      try {
        bool isUserFeedWrite = (userId != celebId);
        _categories = List();
        _categories = await (isUserFeedWrite ? httpFeedListcategory() : httpCelebFeedListcategory(celebId)).then(
                (value) => value.map(
                    (e) => CategoryData(
                    categoryId: e.id,
                    categoryName: e.name
                )
            ).toList()
        );
        Provider.of<FeedWriteDataProvider>(context, listen: false).categoryId=_categories[_selected].categoryId;
      } catch (e) {
        print(e.toString());
      }
    });
  }

  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add((pop) {
      if(FeedWriteRoute.isUploading){
        Fluttertoast.showToast(msg: "피드 업로드 중입니다.", gravity: ToastGravity.TOP);
        return true;
      }
      else {
        Navigator.popUntil(context, ModalRoute.withName(kRouteHome));
        return true;
      }
    }, name: "pop", ifNotYetIntercepted: true);
    _textOver = false;
    _selected = 0;
    Provider.of<FeedWriteDataProvider>(context, listen: false).removeSetContent();

    ProgressDialog _pr = ProgressDialog(
        context,
        showLogs: true,
        isDismissible: false,
        customBody: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 45,
                width: 45,
//              child: Image.asset(
//                "assets/icon/feather_loader.png", fit: BoxFit.contain,
//              ),
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(kNegativeTextColor),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                "업로드 중입니다.",
                style: TextStyle(
                    fontSize: 12,
                    color: kNegativeTextColor
                ),
              )
            ],
          ),
        )
    );
    _pr.style(
        backgroundColor: Colors.transparent,
        elevation: 0,
        padding: const EdgeInsets.all(0)
    );
    _basicIcons = [
      SizedBox(width: 15,),
      GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () async {
          FocusScope.of(context).unfocus();
          FeedWriteDataProvider provider = Provider.of<FeedWriteDataProvider>(context, listen: false);
          if (provider.contentType != FeedContentType.none) {
            Fluttertoast.showToast(msg: "한 번에 한 종류의 미디어만 업로드할 수 있습니다.", gravity: ToastGravity.TOP);
            return;
          }
          await _pr.show();
          File image = await openCamera();
          if (image != null) {
            provider.imageUrls.add(image.path);
            provider.contentType = FeedContentType.images;
          }
          setState(() {

          });
          await _pr.hide();
        },
        child: Container(
          width: 70,
          height: 70,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                  width: 30,
                  height: 27,
                  child: Icon(Icons.photo_camera, size: 27, color: kSelectableTextColor,)
              ),
              SizedBox(height: 5,),
              Text(
                "카메라",
                style: TextStyle(
                  color: kSelectableTextColor,
                  fontSize: 10,
                ),
              )
            ],
          ),
        ),
      ),
      GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () async {
          FocusScope.of(context).unfocus();
          FeedWriteDataProvider provider = Provider.of<FeedWriteDataProvider>(context, listen: false);
          if (provider.contentType != FeedContentType.none) {
            Fluttertoast.showToast(msg: "한 번에 한 종류의 미디어만 업로드할 수 있습니다.", gravity: ToastGravity.TOP);
            return;
          }
          await _pr.show();
          List<File> images = await openImagesPicker();
          if (images != null && images.length > 0) {
            provider.imageUrls.addAll(images.map((e) => e.path).toList());
            provider.contentType = FeedContentType.images;
          }
          setState(() {

          });
          await _pr.hide();
        },
        child: Container(
          width: 70,
          height: 70,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                  width: 27,
                  height: 27,
                  child: Icon(Icons.photo, size: 27, color: kSelectableTextColor,)
              ),
              SizedBox(height: 5,),
              Text(
                "갤러리",
                style: TextStyle(
                  color: kSelectableTextColor,
                  fontSize: 10,
                ),
              )
            ],
          ),
        ),
      ),
      GestureDetector(
        behavior: HitTestBehavior.translucent,
        onTap: () async {
          FocusScope.of(context).unfocus();
          FeedWriteDataProvider provider = Provider.of<FeedWriteDataProvider>(context, listen: false);
          if (provider.contentType != FeedContentType.none) {
            Fluttertoast.showToast(msg: "한 번에 한 종류의 미디어만 업로드할 수 있습니다.", gravity: ToastGravity.TOP);
            return;
          }
          await _pr.show();
          File video = await openVideoPicker();
          if (video != null) {
            provider.videoUrl = video.path;
            provider.contentType = FeedContentType.video;
          }
          setState(() {

          });
          await _pr.hide();
        },
        child: Container(
          width: 70,
          height: 70,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                  width: 27,
                  height: 27,
                  child: Icon(Icons.photo, size: 27, color: kSelectableTextColor,)
              ),
              SizedBox(height: 5,),
              Text(
                "동영상",
                style: TextStyle(
                  color: kSelectableTextColor,
                  fontSize: 10,
                ),
              )
            ],
          ),
        ),
      ),
      GestureDetector(
        onTap: () async {
          await _pr.show();
            await Thumbnail.addLink(
            context: context,
            onLinkAdded: (value) {
              Provider.of<FeedWriteDataProvider>(context, listen: false).youtubeUrl = value.url;
              Provider.of<FeedWriteDataProvider>(context, listen: false).contentType = FeedContentType.youtube;
            }
        );
        setState(() {});
        await _pr.hide();
        },
        behavior: HitTestBehavior.translucent,
        child: Container(
          width: 70,
          height: 70,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                  width: 30.8,
                  height: 22,
                  child: Icon(FontAwesomeIcons.youtube, color: kSelectableTextColor,)
              ),
              SizedBox(height: 10,),
              Text(
                "유튜브링크",
                style: TextStyle(
                  color: kSelectableTextColor,
                  fontSize: 10,
                ),
              )
            ],
          ),
        ),
      ),
      Container(
        width: 70,
        height: 70,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
                width: 28.4,
                height: 28.4,
                child: Icon(FontAwesomeIcons.box, color: kSelectableTextColor,)
            ),
            SizedBox(
              height: 3.8,
            ),
            Text(
              "TTS보관함",
              style: TextStyle(
                color: kSelectableTextColor,
                fontSize: 10,
              ),
            )
          ],
        ),
      ),
      Provider.of<LoginVerifyProvider>(context, listen: false).userId == Provider.of<SelectedCelebProvider>(context, listen: false).celebId ? GestureDetector(
        onTap: () => showGeneralDialog(
          context: context,
          pageBuilder: (context, animation, secondAnimation) => ScopeDialog(),
          barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
          barrierDismissible: false,
          transitionDuration: const Duration(milliseconds: 150),
        ),
        child: Container(
          width: 70,
          height: 70,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                  width: 28.4,
                  height: 28.4,
                  child: Icon(FontAwesomeIcons.lock, color: kSelectableTextColor,)
              ),
              SizedBox(
                height: 3.8,
              ),
              Text(
                "공개 범위",
                style: TextStyle(
                  color: kSelectableTextColor,
                  fontSize: 10,
                ),
              )
            ],
          ),
        ),
      ) : Container(),
      SizedBox(width: 15,),
    ];
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<void>(
      future: _getData(),
      builder: (context, snapshot) {
        return Container(
          decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 57,
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  physics: BouncingScrollPhysics(),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(width: 25,),
                      Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: _categories.asMap().entries.map((e) => Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 5),
                          child: GestureDetector(
                            onTap: () => () {
                              setState(() => _selected = e.key);
                              Provider.of<FeedWriteDataProvider>(context, listen: false).categoryId = e.value.categoryId;
                            }(),
                            behavior: HitTestBehavior.translucent,
                            child: FeedTabCategoryTab(
                              name: e.value.categoryName,
                              selected: e.key == _selected,
                            ),
                          ),
                        )).toList(),
                      ),
                      SizedBox(width: 25,),
                    ],
                  ),
                ),
              ),
              Divider(
                height: 0.5,
                indent: 30,
                endIndent: 30,
              ),
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      _getContentWidget(context),
                      Container(
                        padding: const EdgeInsets.symmetric(horizontal: 25, vertical: 15),
                        height: 480,
                        child: Column(
                          children: [
                            Expanded(
                              child: TextField(
                                autofocus: true,
                                keyboardType: TextInputType.multiline,
                                maxLines: 40,
                                onChanged: (value) {
                                  Provider.of<FeedWriteDataProvider>(context, listen: false).text = value;
                                },
                                controller: _textController,
                                decoration: InputDecoration.collapsed(
                                  hintText: "본문을 입력하세요.\n\n"
                                      " * 최대 작성 가능한 글자수는 ${Provider.of<FeedWriteViewProvider>(context).writeType == FeedWriteType.general ? 1000 : 10000}자 입니다.\n"
                                      " * 사진 (최대 5장 / 1장당 최대 50MB) 영상(500MB)까지 업로드 가능합니다.\n\n"
                                      "※ 타인의 명예를 훼손하거나 욕설, 비방하는 게시글은 관리자에 의해 삭제될 수 있으며, 커뮤니티 이용이 제한될 수 있습니다.\n\n"
                                      "※ 영상 업로드는, 가로로 촬영한 16:9 비율의 영상으로 부탁드립니다. 현재 16:9 비율 이외의 영상이 찌그러지는 현상이 있어, 불편하시더라도 당분간만 이해해주시면 감사하겠습니다.\n\n"
                                      " * 16:9 비율의 해상도(3840x2160, 1920x1080, 1280x720) \n"
                                      " * 16:10 / 4:3 / 3:2 / 1:1 등으로 촬영된 영상 업로드시 왜곡되는 현상 존재\n"
                                      " * 권장 코덱: 아래의 코덱 정보가 아닌 영상을 업로드하여 문제가 생기는 경우, 별도로 문의해주시길 바랍니다.\n"
                                      " - 비디오 코덱: H.264 - 오디오 코덱: MPEG-4 AAC",
                                  hintStyle: TextStyle(
                                    color: kBorderColor,
                                    fontSize: 14,
                                  ),
                                ),
                                maxLength: Provider.of<FeedWriteViewProvider>(context).writeType == FeedWriteType.general ? 1000 : 10000,
                                maxLengthEnforced: true,
                                style: TextStyle(
                                  color: _textOver ? Colors.red : Colors.black
                                ),
                              ),
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                width: double.infinity,
                height: 70,
                decoration: BoxDecoration(
                    color: Theme.of(context).backgroundColor,
                    boxShadow: [
                      BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.16),
                        blurRadius: 6,
                      )
                    ]
                ),
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  physics: BouncingScrollPhysics(),
                  child: Padding(
                    padding: const EdgeInsets.only(bottom: 6),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: _basicIcons,
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      }
    );
  }

  @override
  void dispose() {
    BackButtonInterceptor.removeByName("pop");
    super.dispose();
  }

  Widget _getContentWidget(BuildContext context){
    FeedWriteDataProvider dataProvider = Provider.of<FeedWriteDataProvider>(context);
    Widget child;
    switch(dataProvider.contentType){
      case FeedContentType.none:
        return Container();
      case FeedContentType.images:
        child = Container(
          child: CarouselSlider(
            items: dataProvider.imageUrls.map((e) =>
                Container(
                  child: Center(
                    child: Image.file(
                      File(e),
                      fit: BoxFit.cover,
                      width: MediaQuery.of(context).size.width - 60,
                    ),
                  ),
                )
            ).toList(),
            options: CarouselOptions(
                viewportFraction: 1,
                enableInfiniteScroll: false,
                enlargeCenterPage: false,
                scrollPhysics: dataProvider.imageUrls.length == 1 ? NeverScrollableScrollPhysics() : BouncingScrollPhysics()
            ),
          ),
        );
        break;
      case FeedContentType.video:
        child = VideoPlayerWidget(url: dataProvider.videoUrl,);
        break;
      case FeedContentType.youtube:
        child = YoutubePlayerWidget(youtubeUrl: dataProvider.youtubeUrl,);
        break;
      default:
        return Container();
    }

    return Stack(
      children: <Widget>[
        child,
        Positioned(
          top: 10,
          right: 40,
          child: GestureDetector(
            onTap: () {
              setState(() {

              });
              return dataProvider.removeSetContent();
            },
            child: Container(
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  color: Theme.of(context).backgroundColor,
                  boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.36),
                        blurRadius: 8,
                        offset: Offset(0, 2)
                    )
                  ]
              ),
              child: Icon(
                Icons.cancel,
                color: Colors.red,
              ),
            ),
          ),
        ),
      ],
    );
  }
}


