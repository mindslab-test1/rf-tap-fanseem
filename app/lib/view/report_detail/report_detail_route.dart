
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/providers/report_manage_provider.dart';
import 'package:rf_tap_fanseem/view/main/component/partial_components.dart';

import '../../colors.dart';

class ReportDetailRoute extends StatefulWidget {
  @override
  _ReportDetailRouteState createState() => _ReportDetailRouteState();
}

class _ReportDetailRouteState extends State<ReportDetailRoute> {
  bool _isBlocked;

  @override
  void initState() {
    super.initState();
    initData();
  }

  void initData(){
    _isBlocked = true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: _ReportDetailAppBar(),
      ),
      body: Container(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
              height: 104,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      UserAvatar(),
                      SizedBox(width: 15,),
                      Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            Provider.of<ReportManageProvider>(context).username,
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 17,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(height: 1,),
                          Text(
                            "신고누적 : ${Provider.of<ReportManageProvider>(context).reportCount}회",
                            style: TextStyle(
                              color: kNegativeTextColor,
                              fontSize: 12,
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                  GestureDetector(
                    onTap: () {
                      // TODO block reactions
                      setState(() {
                        _isBlocked = !_isBlocked;
                      });
                    },
                    child: Container(
                      width: 56,
                      height: 27,
                      decoration: BoxDecoration(
                        color: kNegativeTextColor,
                        borderRadius: BorderRadius.circular(14),
                      ),
                      child: Center(
                        child: Text(
                          _isBlocked ? "차단됨" : "차단",
                          style: TextStyle(
                            color: kMainBackgroundColor,
                            fontSize: 12
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: SingleChildScrollView(

              ),
            )
          ],
        ),
      ),
    );
  }
}

class _ReportDetailAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  width: 30,
                ),
                GestureDetector(
                  onTap: () => Navigator.pop(context),
                  behavior: HitTestBehavior.translucent,
                  child: Container(
                    child: Icon(
                      Icons.arrow_back_ios,
                      size: 30,
                      color: kAppBarButtonColor,
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
            flex: 6,
            child: Text(
              "${Provider.of<ReportManageProvider>(context).username}의 신고 내역",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Theme.of(context).backgroundColor,
                  fontSize: 20,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(),
          )
        ],
      ),
    );
  }
}

