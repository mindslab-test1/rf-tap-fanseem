import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/util/photo_util.dart';
import 'package:rf_tap_fanseem/view/signup/states/signup_data_provider.dart';

import '../../../colors.dart';

class SignUpPicture extends StatefulWidget {
  @override
  _SignUpPictureState createState() => _SignUpPictureState();
}

class _SignUpPictureState extends State<SignUpPicture> {
  String _profileImg;
  String _coverImg;

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
        ),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 176,
              child: Stack(
                fit: StackFit.expand,
                children: <Widget>[
                  Positioned(
                    top: 0.0,
                    width: MediaQuery.of(context).size.width,
                    child: Container(height: 120,
                      width: double.infinity,
                      child: ColorFiltered(
                        colorFilter: ColorFilter.mode(
                            Color.fromRGBO(0, 0, 0, 0.55),
                            BlendMode.darken
                        ),
                        child: _coverImg == null ? Container(
                          color: Colors.grey,
                        ) : Image.file(File(_coverImg), fit: BoxFit.fitWidth),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 12,
                    right: 13,
                    child: GestureDetector(
                      onTap: () async {
                        File file = await openImagePicker();
                        if (file != null) {
                          setState(() {
                            _coverImg = file.path;
                          });
                        }
                      },
                      behavior: HitTestBehavior.translucent,
                      child: Container(
                        child: Image.asset(
                          "assets/icon/add_a_photo.png",
                          width: 45.1,
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 0.0,
                    left: MediaQuery.of(context).size.width / 2 - 50,
                    child: Align(
                      child: Container(
                        width: 100,
                        height: 100,
                        child: CircleAvatar(
                            radius: 50,
                            backgroundColor: Theme.of(context).backgroundColor,
                            child: CircleAvatar(
                              radius: 49,
                              backgroundColor: kMainColor,
                              backgroundImage: _profileImg == null || _profileImg == "" ?
                                AssetImage("assets/icon/seem_off_3x.png",) :
                                FileImage(File(_profileImg),),
//                              backgroundImage: _profileImg == null ? AssetImage(
//                                "assets/icon/seem_off_3x.png",
//                              ) : FileImage(File(_profileImg)),
                            )
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 13,
                    right: 124,
                    child: GestureDetector(
                      onTap: () async {
                        File file = await openImagePicker();
                        if (file != null) {
                          setState(() {
                            _profileImg = file.path;
                          });
                        }
                      },
                      behavior: HitTestBehavior.translucent,
                      child: Container(
                        child: Image.asset(
                          "assets/icon/add_a_photo.png",
                          width: 45.1,
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 30,),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 45),
              child: Text(
                "자신을 뽐낼 프로필 사진과 배경 사진을 선택해주세요.\n"
                    "해당 사진은 언제든 마이페이지를 통해 수정가능해요!",
                style: TextStyle(
                  color: kSelectableTextColor,
                  fontSize: 13,
                ),
              ),
            ),
            Expanded(
              child: Container(),
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 49),
              height: 58,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  GestureDetector(
                    onTap: () => Provider.of<SignUpDataProvider>(context, listen: false).setStep(SignUpSteps.nickname),
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                      width: MediaQuery.of(context).size.width / 2 - 49,
                      height: 58,
                      decoration: BoxDecoration(
                          color: Theme.of(context).backgroundColor,
                          border: Border.all(color: kBorderColor),
                          borderRadius: BorderRadius.circular(12),
                          boxShadow: [
                            BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.16),
                                blurRadius: 6,
                                offset: Offset(0, 3)
                            )
                          ]
                      ),
                      child: Center(
                        child: Text(
                          "건너 뛰기",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: kNavigationPressableColor,
                              fontSize: 17
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 10,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 49),
              child: GestureDetector(
                onTap: () {
                  if (_profileImg != null && _coverImg != null) {
                    Provider.of<SignUpDataProvider>(context, listen: false)
                      ..setImages(
                        profileImagePath: _profileImg,
                        coverImagePath: _coverImg
                      )
                      ..setStep(
                          SignUpSteps.nickname
                      );
                  }
                },
                behavior: HitTestBehavior.translucent,
                child: Container(
                  height: 58,
                  decoration: BoxDecoration(
                      color: (_profileImg == null || _coverImg == null) ? Theme.of(context).backgroundColor : Theme.of(context).primaryColor,
                      border: Border.all(color: kBorderColor),
                      borderRadius: BorderRadius.circular(12),
                      boxShadow: [
                        BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.16),
                            blurRadius: 6,
                            offset: Offset(0, 3)
                        )
                      ]
                  ),
                  child: Center(
                    child: Text(
                      "다음 단계",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        color: (_profileImg == null || _coverImg == null) ? kBorderColor : Theme.of(context).backgroundColor,
                        fontSize: 17,
                      ),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 40,
            )
          ],
        )
    );
  }
}