import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/components/progress_dialog.dart';
import 'package:rf_tap_fanseem/http/celeb_feed/celeb_feed_view.dart';
import 'package:rf_tap_fanseem/http/celeb_feed/celeb_feed_write_comment.dart';
import 'package:rf_tap_fanseem/http/content/content_write_comment.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_comment_item_vo.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_feed_item_vo.dart';
import 'package:rf_tap_fanseem/http/feed/dto/http_feed_view_response_dto.dart';
import 'package:rf_tap_fanseem/http/feed/feed_view.dart';
import 'package:rf_tap_fanseem/http/feed/feed_write_comment.dart';
import 'package:rf_tap_fanseem/providers/comment_reply_provider.dart';
import 'package:rf_tap_fanseem/providers/feed_data_provider.dart';
import 'package:rf_tap_fanseem/providers/login_provider.dart';
import 'package:rf_tap_fanseem/providers/selected_celeb_provider.dart';
import 'package:rf_tap_fanseem/view/main/component/feed_component.dart';
import 'package:rf_tap_fanseem/view/main/dialog/login_require_dialog.dart';

import '../../colors.dart';
import '../../http/content/content_view.dart';
import 'feed_view_components.dart';

class FeedDetailBody extends StatefulWidget {
  final bool isContent;
  final String title;
  final int viewCount;
  final int feedId;
  final bool isUserFeed;


  const FeedDetailBody({
    Key key,
    this.feedId,
    this.isUserFeed,
    this.isContent = false,
    this.title,
    this.viewCount
  }) : super(key: key);


  @override
  _FeedDetailBodyState createState() => _FeedDetailBodyState();

}

class _FeedDetailBodyState extends State<FeedDetailBody> {

  FeedData _feedData;
  List<CommentData> _commentDataList;

  Future<bool> _getData(BuildContext context, int feedId, bool isUserFeed, bool isContent) async {
    HttpFeedViewResponseDto _data;
    if (isUserFeed) {
      _data = await httpFeedView(context: context, feedId: feedId);
    }
    else if (isContent) {
      _data = await httpContentView(context: context, contentId: feedId);
    }
    else {
      _data = await httpCelebFeedView(context: context, feedId: feedId);
    }
    HttpFeedItemVo feedItemVo = _data.feedItem;
    _feedData = FeedData(
        userId: feedItemVo.feedOwnerId,
        feedId: feedItemVo.feedId,
        userName: feedItemVo.feedOwnerName,
        profileImg: feedItemVo.feedOwnerProfileImageUrl,
        membershipBadgeUrl: feedItemVo.badgeImageUrl,
        postTimestamp: feedItemVo.updatedTime,
        category: feedItemVo.categoryName,
        imgUrl: feedItemVo.pictureUrls,
        videoUrl: feedItemVo.videoUrl,
        youtubeUrl: feedItemVo.youtubeUrl,
        text: feedItemVo.text,
        scrapCount: feedItemVo.scrapCount,
        isScrap: feedItemVo.scraped,
        likeCount: feedItemVo.likeCount,
        isLike: feedItemVo.liked,
        commentCount: feedItemVo.commentCount,
        canView: true,
        isUserFeed: isUserFeed,
        isContent: widget.isContent
    );

    CommentData commentMapper(HttpCommentItemVo vo) {
      return CommentData(
          userId: vo.commentOwnerId,
          userName: vo.commentOwnerName,
          profileImg: vo.commentOwnerProfileImageUrl,
          badgeUrl: vo.badgeUrl,
          likeCount: vo.likeCount,
          isLike: vo.liked,
          text: vo.text,
          timeStamp: vo.updatedTime,
          commentId: vo.commentId,
          replies: vo.replies.map((e) => commentMapper(e)).toList(),
          type: _feedData.isContent ? CommentType.content : (_feedData.isUserFeed ? CommentType.userFeed : CommentType.celebFeed)
      );
    }
    _commentDataList = _data.comments.map((e) => commentMapper(e)).toList();

    return true;
  }


  @override
  void initState() {
    BackButtonInterceptor.add((pop) {
      Navigator.popUntil(context, ModalRoute.withName('/home'));
      return true;
    }, name: "pop", zIndex: 2, ifNotYetIntercepted:true);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    FeedDataProvider provider = Provider.of<FeedDataProvider>(context, listen: false);
    return FutureBuilder<bool>(
        future: _getData(context, widget.feedId==null?provider.feedData.feedId:widget.feedId, widget.isUserFeed==null?provider.isUserFeed:widget.isUserFeed,widget.isContent),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Container(
              decoration: BoxDecoration(
                  color: Theme
                      .of(context)
                      .backgroundColor
              ),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    child: SingleChildScrollView(
                      child: /*widget.isContent ? Container() :*/ Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          FeedDetailView(
                            feedData: _feedData,
                          ),
                          FeedCommentView(
                            comments: _commentDataList,
                          ),
                        ],
                      ),
                    ),
                  ),
                  _CommentEditBar(
                      isUserFeed: _feedData.isUserFeed,
                      isContent: _feedData.isContent,
                      onWriteComment: () { setState(() {}); }
                  )
                ],
              ),
            );
          } else {
            return Center(child: CircularProgressIndicator(),);
          }
        }
    );
  }

  @override
  void dispose() {
    BackButtonInterceptor.removeByName("pop");
    super.dispose();
  }
}

class _CommentEditBar extends StatefulWidget {

  final Function onWriteComment;
  final bool isUserFeed;
  final bool isContent;

  _CommentEditBar({
    Key key,
    this.onWriteComment,
    this.isUserFeed,
    this.isContent
  }) : super(key: key);

  @override
  __CommentEditBarState createState() => __CommentEditBarState();
}

class __CommentEditBarState extends State<_CommentEditBar> {
  TextEditingController _commentController = TextEditingController();
  bool _textOver = false;
  int inputLineCount;
  double _inputHeight;

  bool _processing;
  ProgressDialog _pr;

  @override
  void initState(){
    super.initState();
    inputLineCount = 1;
    _inputHeight = 45;
    _processing = false;
  }

  @override
  Widget build(BuildContext context) {
    final Map arguments = ModalRoute.of(context).settings.arguments;
    num feedId;
    num celebId;
    bool isContent;
    bool isUserFeed;
    if(arguments!=null){
      feedId = arguments['feedId'];
      if(arguments['celebId']==null||Provider.of<LoginVerifyProvider>(context,listen: false).isCeleb){
        celebId = Provider.of<LoginVerifyProvider>(context, listen: false).userId;
      }
      else{
        celebId = arguments['celebId'];
      }
      isContent = arguments['isContent'];
      isUserFeed = arguments['isUserFeed'];
    } else{
      feedId = null;
      celebId = null;
      isContent = null;
      isUserFeed = null;
    }
    _pr = ProgressDialog(
        context,
        showLogs: true,
        isDismissible: false,
        customBody: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 45,
                width: 45,
//              child: Image.asset(
//                "assets/icon/feather_loader.png", fit: BoxFit.contain,
//              ),
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(kNegativeTextColor),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                "업로드 중입니다.",
                style: TextStyle(
                    fontSize: 12,
                    color: kNegativeTextColor
                ),
              )
            ],
          ),
        )
    );
    _pr.style(
        backgroundColor: Colors.transparent,
        elevation: 0,
        padding: const EdgeInsets.all(0)
    );

    return Container(
      height: _inputHeight + 20,
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          boxShadow: [
            BoxShadow(
              blurRadius: 6,
              color: Color.fromRGBO(0, 0, 0, 0.16),
            )
          ]
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(width: 20,),
//          Icon(
//            FontAwesomeIcons.plus,
//            color: Color.fromRGBO(159, 159, 159, 1),
//            size: 15,
//          ),
//          SizedBox(width: 22,),
          Expanded(
              child: Container(
                height: _inputHeight,
                decoration: BoxDecoration(
                    color: Theme.of(context).backgroundColor,
                    borderRadius: BorderRadius.circular(18),
                    boxShadow: [
                      BoxShadow(
                        blurRadius: 6,
                        color: Color.fromRGBO(0, 0, 0, 0.16),
                      )
                    ]
                ),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        padding: const EdgeInsets.only(left: 20, right: 13, bottom: 3),
                        child: ConstrainedBox(
                          constraints: BoxConstraints(
                              minHeight : 45,
                              maxHeight : 150
                          ),
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: TextField(
                              style: TextStyle(
                                color: Colors.black,
                              ),
                              controller: _commentController,
                              keyboardType: TextInputType.multiline,
                              minLines: 1,
                              maxLines: null,
                              onChanged: (String value) {
                                if (value.length > 200) {
                                  if(!_textOver){
                                    setState(() => _textOver = true);
                                    Fluttertoast.showToast(msg: "작성 가능 글자수를 넘었습니다", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                                  }
                                  value = value.substring(0, 200);
                                  _commentController.text = value;
                                  _commentController.selection = TextSelection.fromPosition(TextPosition(offset: _commentController.text.length));
                                } else if (value.length <= 200 && _textOver) {
                                  setState(() => _textOver = false);
                                }
                                int count = ("\n").allMatches(value).length;
                                if(inputLineCount != count && count <= 4){
                                  setState(() {
                                    inputLineCount = count;
                                    _inputHeight = (45 + (count * 18)).toDouble();
                                  });
                                }
//                                int count = ('\n').allMatches(value).length;
//                                if(value.replaceAll("\n", "").length > 200 && !_textOver){
//                                  setState(() => _textOver = true);
//                                  Fluttertoast.showToast(msg: "작성 가능 글자수를 넘었습니다", toastLength: Toast.LENGTH_SHORT);
//                                } else if (inputLineCount != count && count <= 4){
//                                  var inputHeight = 45 + (count * 18);
//                                  setState(() {
//                                    inputLineCount = count;
//                                    _inputHeight = inputHeight.toDouble();
//                                  });
//                                } else {
//                                  if (value.replaceAll("\n", "").length <= 200 && _textOver) setState(() => _textOver = false);
//                                }
                              },
                              decoration: InputDecoration(
                                enabledBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.transparent, width: 0),
                                ),
                                focusedBorder: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.transparent, width: 0),
                                ),
                                border: UnderlineInputBorder(
                                  borderSide: BorderSide(color: Colors.transparent, width: 0),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Container(
                      width: 64,
                      decoration: BoxDecoration(
                          color: Theme.of(context).backgroundColor,
                          borderRadius: BorderRadius.circular(18),
                          border: Border.all(
                              color: Colors.grey
                          ),
                          boxShadow: [
                            BoxShadow(
                              blurRadius: 6,
                              color: Color.fromRGBO(0, 0, 0, 0.16),
                            )
                          ]
                      ),
                      child: GestureDetector(
                        onTap: () async {
                          if(!Provider.of<LoginVerifyProvider>(context, listen: false).isLogin)
                            showGeneralDialog(
                              context: context,
                              pageBuilder: (context, animation, secondAnimation) => LoginPrompt(),
                              barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                              barrierDismissible: false,
                              transitionDuration: const Duration(milliseconds: 150),
                            );
                          else{
                            await _pr.show();
                            await Future.delayed(Duration(milliseconds: 200));
                            if (_processing) {
                              Fluttertoast.showToast(msg: "댓글 작성 중입니다...", gravity: ToastGravity.TOP);
                              return;
                            }
                            _processing = true;
                            if (_commentController.text.trim() != null &&
                                _commentController.text.length > 200) {
                              await _pr.hide();
                              Fluttertoast.showToast(msg: "작성 가능 글자수를 넘었습니다", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                            }
                            else if (_commentController.text != null &&
                                _commentController.text.length > 0) {
                              bool ok;
                              if (widget.isContent==null?isContent:widget.isContent)
                                ok = await httpContentWriteComment(context: context, celebId: celebId==null?Provider.of<SelectedCelebProvider>(context, listen: false).celebId:celebId, contentId: feedId==null?Provider.of<FeedDataProvider>(context, listen: false).feedData.feedId:feedId,
                                    text: _commentController.text,
                                    parentId: Provider.of<CommentReplyProvider>(context, listen: false).selectedCommentParentId,
                                    imageUrl: null,
                                    ttsId: null
                                );
                              if (ok == null) ok = await ((widget.isUserFeed==null?isUserFeed:widget.isUserFeed)
                                  ? httpFeedWriteComment(
                                  context: context,
                                  celebId: celebId==null?Provider
                                      .of<SelectedCelebProvider>(
                                      context, listen: false)
                                      .celebId:celebId,
                                  feedId: feedId==null?Provider
                                      .of<FeedDataProvider>(
                                      context, listen: false)
                                      .feedData
                                      .feedId:feedId,
                                  text: _commentController.text,
                                  parentId: Provider
                                      .of<CommentReplyProvider>(
                                      context, listen: false)
                                      .selectedCommentParentId,
                                  imageUrl: null,
                                  ttsId: null
                              )
                                  : httpCelebFeedWriteComment(
                                  context: context,
                                  celebId: celebId==null?Provider
                                      .of<SelectedCelebProvider>(
                                      context, listen: false)
                                      .celebId:celebId,
                                  feedId: feedId==null?Provider
                                      .of<FeedDataProvider>(
                                      context, listen: false)
                                      .feedData
                                      .feedId:feedId,
                                  text: _commentController.text,
                                  parentId: Provider
                                      .of<CommentReplyProvider>(
                                      context, listen: false)
                                      .selectedCommentParentId,
                                  imageUrl: null,
                                  ttsId: null
                              ));
                              if (!ok) {
                                await _pr.hide();
                                Fluttertoast.showToast(msg: "댓글 업로드에 실패했습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                              }
                              else {
                                await _pr.hide();
                                _commentController.clear();
                                widget.onWriteComment.call();
                                Provider.of<CommentReplyProvider>(context, listen: false).reset();
                              }
                              _processing = false;
                            }
                            else {
                              await _pr.hide();
                              _processing = false;
                              Fluttertoast.showToast(msg: "빈 댓글은 작성할 수 없습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                            }
                          }
                        },
                        child: Center(
                          child: Text(
                            "작성",
                            style: TextStyle(
                                color: Colors.grey,
                                fontSize: 14,
                                fontWeight: FontWeight.bold
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
          ),
          SizedBox(width: 14,),
        ],
      ),
    );
  }
}
