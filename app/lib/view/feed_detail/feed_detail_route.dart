import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/providers/feed_data_provider.dart';
import 'package:rf_tap_fanseem/providers/login_provider.dart';
import 'package:rf_tap_fanseem/view/feed_detail/feed_detail_body.dart';
import 'package:rf_tap_fanseem/view/main/component/feed_component.dart';
import 'package:rf_tap_fanseem/view/main/dialog/feed_action_dialog.dart';

import '../../colors.dart';
import '../../routes.dart';

class FeedDetailRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Map arguments = ModalRoute.of(context).settings.arguments;
    num feedId;
    bool isContent;
    bool isUserFeed;
    if(arguments!=null){
      feedId = arguments['feedId'];
      isContent = arguments['isContent'];
      isUserFeed = arguments['isUserFeed'];
    } else{
      feedId = null;
      isContent = null;
      isUserFeed = null;
    }
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0.0,
        automaticallyImplyLeading: false,
        title: _FeedDetailAppBar(arguments: arguments),
      ),
      body: FeedDetailBody(
        feedId : arguments==null?null:feedId,
        isContent: arguments==null?Provider.of<FeedDataProvider>(context).isContent:isContent,
        isUserFeed : arguments==null?null:isUserFeed
      ),
    );
  }
}

class _FeedDetailAppBar extends StatelessWidget {
  final Map arguments;
  
  _FeedDetailAppBar({this.arguments});


  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 30,
                  ),
                  GestureDetector(
                    onTap: () => arguments==null?Navigator.pop(context):Navigator.popAndPushNamed(context,'/home'),
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                        child: Icon(
                          FontAwesomeIcons.times,
                          size: 27,
                          color: kAppBarButtonColor,
                        )
                    ),
                  ),
                ]
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Text(
                "피드 상세보기",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: kAppBarButtonColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    bool isEditAction = (Provider.of<LoginVerifyProvider>(context, listen: false).isLogin && (Provider.of<LoginVerifyProvider>(context, listen: false).userId == Provider.of<FeedDataProvider>(context, listen: false).feedData.userId));
                    showGeneralDialog(
                      context: context,
                      pageBuilder: (context, animation, secondAnimation) =>
                      (Provider.of<LoginVerifyProvider>(context, listen: false).isLogin && (Provider.of<LoginVerifyProvider>(context, listen: false).userId == Provider.of<FeedDataProvider>(context, listen: false).feedData.userId))
                          ? FeedEditActionsDialog() : FeedReportActionsDialog(),
                      barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                      barrierDismissible: false,
                      transitionDuration: const Duration(milliseconds: 150),
                    ).then((value) {
                      if(isEditAction && value) Navigator.pop(context);
                    });
                  },
                  behavior: HitTestBehavior.translucent,
                  child: Container(
                    height: 50,
                    width: 50,
                    decoration: BoxDecoration(
                        color: Color.fromRGBO(0, 0, 0, 0),
                        shape: BoxShape.circle
                    ),
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          width: 5,
                          height: 5,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              shape: BoxShape.circle
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 4.0),
                          child: Container(
                            width: 5,
                            height: 5,
                            decoration: BoxDecoration(
                                color: Colors.white,
                                shape: BoxShape.circle
                            ),
                          ),
                        ),
                        Container(
                          width: 5,
                          height: 5,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              shape: BoxShape.circle
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: 30,)
              ],
            ),
          ),
        ],
      ),
    );
  }
}

