
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_options.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';

import 'package:rf_tap_fanseem/components/video_player_widget.dart';
import 'package:rf_tap_fanseem/components/youtube_player_widget.dart';
import 'package:rf_tap_fanseem/http/celeb_feed/celeb_feed_delete_comment.dart';
import 'package:rf_tap_fanseem/http/celeb_feed/celeb_feed_like_comment.dart';
import 'package:rf_tap_fanseem/http/celeb_feed/celeb_feed_unlike_comment.dart';
import 'package:rf_tap_fanseem/http/content/content_delete_comment.dart';
import 'package:rf_tap_fanseem/http/content/content_like_comment.dart';
import 'package:rf_tap_fanseem/http/content/content_unlike_comment.dart';
import 'package:rf_tap_fanseem/http/feed/feed_delete_comment.dart';
import 'package:rf_tap_fanseem/http/feed/feed_like_comment.dart';
import 'package:rf_tap_fanseem/http/feed/feed_unlike_comment.dart';
import 'package:rf_tap_fanseem/providers/comment_reply_provider.dart';
import 'package:rf_tap_fanseem/providers/feed_data_provider.dart';
import 'package:rf_tap_fanseem/providers/login_provider.dart';
import 'package:rf_tap_fanseem/view/feed_detail/feed_detail_photo.dart';
import 'package:rf_tap_fanseem/view/main/component/feed_component.dart';
import 'package:rf_tap_fanseem/view/main/dialog/feed_comment_dialog.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../colors.dart';

class FeedDetailView extends StatelessWidget {
  final FeedData feedData;
  final String title;
  final int viewCount;

  const FeedDetailView({
    Key key,
    this.feedData,
    this.title,
    this.viewCount
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          boxShadow: [
            BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.09),
                blurRadius: 8,
                offset: Offset(0,1)
            )
          ]
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 13),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                FeedProfileLayer(
                  userName: feedData.userName,
                  profileImg: feedData.profileImg,
                  badgeUrl: feedData.membershipBadgeUrl,
                  isRestrict: !feedData.isUserFeed,
                ),
                FeedGeneralMetaData(
                  timestamp: feedData.postTimestamp,
                  category: feedData.category,
                )
              ],
            ),
          ),
          Divider(
            color: Color.fromRGBO(228, 228, 228, 1),
            height: 0.5,
            thickness: 0.5,
            indent: 14.5,
            endIndent: 14.5,
          ),
          SizedBox(height: 9.5,),
          (feedData.imgUrl.isNotEmpty || feedData.videoUrl != null || feedData.youtubeUrl != null) ?
          _FeedMediaContent(imgUrl: feedData.imgUrl, videoUrl: feedData.videoUrl, youtubeUrl: feedData.youtubeUrl,) :
          Container(),
          (feedData.text.length != 0) ?
          _FeedTextContent(text: feedData.text,) :
          Container(),
          Divider(
            color: Color.fromRGBO(228, 228, 228, 1),
            height: 0.5,
            thickness: 0.5,
            indent: 14.5,
            endIndent: 14.5,
          ),
          feedData.isUserFeed ? FeedButtons(
            scrapCount: feedData.scrapCount,
            isScrap: feedData.isScrap,
            likeCount: feedData.likeCount,
            isLike: feedData.isLike,
            commentCount: feedData.commentCount,
            feedId: feedData.feedId,
            canView: feedData.canView,
          ) : CelebFeedButtons(
            scrapCount: feedData.scrapCount,
            isScrap: feedData.isScrap,
            likeCount: feedData.likeCount,
            isLike: feedData.isLike,
            commentCount: feedData.commentCount,
            feedId: feedData.feedId,
            canView: feedData.canView,
            isContent: feedData.isContent,
          ),
        ],
      ),
    );
  }
}

class _FeedMediaContent extends StatefulWidget {
  final List<String> imgUrl;
  final String youtubeUrl;
  final String videoUrl;


  const _FeedMediaContent({
    Key key,
    this.imgUrl = const [],
    this.youtubeUrl,
    this.videoUrl
  }) : assert(imgUrl.length != 0 || videoUrl != null || youtubeUrl != null),
        super(key: key);

  @override
  __FeedMediaContentState createState() => __FeedMediaContentState();
}

class __FeedMediaContentState extends State<_FeedMediaContent> {
  int _current = 0;
  @override
  Widget build(BuildContext context ) {
    Widget display;
    if(widget.imgUrl.isNotEmpty) {
      display = Stack(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 0),
            child: CarouselSlider(
              options: CarouselOptions(
                onPageChanged: (index, reason){
                  setState(() {
                    _current = index;
                  });
                },
                viewportFraction: 1,
                enableInfiniteScroll: false,
                enlargeCenterPage: false,
                scrollPhysics: widget.imgUrl.length == 1 ? NeverScrollableScrollPhysics() : BouncingScrollPhysics(),
              ),
              items: widget.imgUrl.map((e) =>
                  GestureDetector(
                      onTap: () => Navigator.push(context, MaterialPageRoute(
                          builder: (context) => FeedDetailPhotoView(imgUrl: e)
                      )),
                      child: Container(
                        child: Center(
                            child: CachedNetworkImage(
                              imageUrl: e,
                              placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                              errorWidget: (context, url, error) => Icon(Icons.error),
                            )
                        ),
                      )
                  ),
              ).toList(),
            ),
          ),
          Positioned.fill(
            child: Align(
              alignment: Alignment.bottomCenter,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: widget.imgUrl.map((e) {
                  int index = widget.imgUrl.indexOf(e);
                  return Container(
                    width: 8.0,
                    height: 8.0,
                    margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 2.0),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: _current == index
                          ? kMainColor
                          : Colors.white,
                    ),
                  );
                }).toList(),
              ),
            ),
          )
        ],
      );
    } else if(widget.youtubeUrl != null){
      display = Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Container(
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
            child: YoutubePlayerWidget(
              youtubeUrl: widget.youtubeUrl,
            ),
          ),
          SizedBox(height: 10),
          Container(
            child: GestureDetector(
              onTap: () async {
                if(await canLaunch(widget.youtubeUrl)){
                  await launch(widget.youtubeUrl);
                }
              },
              child: Text(
                widget.youtubeUrl,
                style: TextStyle(
                  color: Color.fromRGBO(0, 0, 238, 1)
                ),
              ),
            ),
          ),
          SizedBox(height: 10,)
        ],
      );
    } else if(widget.videoUrl != null){
      display = Container(
        padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
        child: VideoPlayerWidget(
          isNetwork: true,
          url: widget.videoUrl,
        ),
      );
    }
    return display;
  }
}

class _FeedTextContent extends StatelessWidget {
  final String text;

  const _FeedTextContent({
    Key key,
    this.text = ""
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 45),
      alignment: Alignment.centerLeft,
      child: Text(
        text == null ? "" : text,
        style: TextStyle(
          color: Colors.black,
          fontSize: 13,
        ),
      ),
    );
  }
}

class FeedCommentView extends StatelessWidget {
  final List<CommentData> comments;

  const FeedCommentView({
    Key key,
    this.comments = const []
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: comments.isNotEmpty ? comments.map((e) {
          // when comment does not have replies
          if(e.replies.isEmpty) return Container(
            padding: EdgeInsets.symmetric(vertical: 15),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  child: _FeedCommentBlock(
                    userId: e.userId,
                    userName: e.userName,
                    profileImg: e.profileImg,
                    likeCount: e.likeCount,
                    isLike: e.isLike,
                    text: e.text,
                    timeStamp: e.timeStamp,
                    commentId: e.commentId,
                    parentId: e.commentId,
                    type: e.type,
                    badgeUrl: e.badgeUrl,
                  ),
                ),
                SizedBox(
                  width: 50,
                )
              ],
            ),
          );
          else{
            // when comment has replies
            List<Widget> allCommentList = [
              Padding(
                padding: const EdgeInsets.only(bottom: 15),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: _FeedCommentBlock(
                        userId: e.userId,
                        userName: e.userName,
                        profileImg: e.profileImg,
                        likeCount: e.likeCount,
                        isLike: e.isLike,
                        text: e.text,
                        timeStamp: e.timeStamp,
                        commentId: e.commentId,
                        parentId: e.commentId,
                        type: e.type,
                        badgeUrl: e.badgeUrl,
                      ),
                    ),
                    SizedBox(
                      width: 50,
                    ),
                  ],
                ),
              )
            ];

            e.replies.forEach((element) => allCommentList.add(
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 15),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        width: 50,
                      ),
                      Expanded(
                        child: _FeedCommentBlock(
                          userId: element.userId,
                          userName: element.userName,
                          profileImg: element.profileImg,
                          likeCount: element.likeCount,
                          isLike: element.isLike,
                          text: element.text,
                          timeStamp: element.timeStamp,
                          commentId: element.commentId,
                          parentId: e.commentId,
                          type: element.type,
                          badgeUrl: element.badgeUrl,
                        ),
                      ),
                    ],
                  ),
                )
            ));

            return Container(
              padding: EdgeInsets.symmetric(vertical: 15),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: allCommentList,
              ),
            );
          }
        }).toList() : <Widget>[Container(
          padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 30),
          child: Center(
            child: Text(
                "아직 댓글이 없습니다!"
            ),
          ),
        )],
      ),
    );
  }
}

class _FeedCommentBlock extends StatelessWidget {
  final int userId;
  final String userName;
  final String profileImg;
  final String badgeUrl;
  final int likeCount;
  final bool isLike;
  final String text;
  final String timeStamp;
  final int commentId;
  final int parentId;
  final CommentType type;

  const _FeedCommentBlock({
    Key key,
    this.userId,
    this.userName,
    this.profileImg,
    this.badgeUrl,
    this.likeCount,
    this.isLike,
    this.text,
    this.timeStamp,
    this.commentId,
    this.parentId,
    this.type
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      behavior: HitTestBehavior.translucent,
      onLongPress: () {
        if(Provider.of<LoginVerifyProvider>(context, listen: false).userId == userId)
          showGeneralDialog(
              context: context,
              pageBuilder: (context, animation, secondAnimation) => FeedCommentDialog(),
              barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
              barrierDismissible: false,
              transitionDuration: const Duration(milliseconds: 150)
          ).then((value) {
            if(value == null) return true;
            switch(value){
              case "edit":
                showGeneralDialog(
                    context: context,
                    pageBuilder: (context, animation, secondAnimation) => FeedCommentEditDialog(),
                    barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                    barrierDismissible: false,
                    transitionDuration: const Duration(milliseconds: 150)
                );
                break;
              case "delete":
                bool isUserFeed = Provider.of<FeedDataProvider>(context, listen: false).isUserFeed;
                bool isContent = Provider.of<FeedDataProvider>(context, listen: false).isContent;
                if(isUserFeed) httpFeedDeleteComment(context : context, comment: commentId).then((value) {
                  if(value) Fluttertoast.showToast(msg: "댓글이 삭제되었습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                  else Fluttertoast.showToast(msg: "댓글 삭제에 실패했습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                });
                else if(isContent){
                  httpContentDeleteComment(context : context, comment: commentId).then((value) {
                    if(value) Fluttertoast.showToast(msg: "댓글이 삭제되었습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                    else Fluttertoast.showToast(msg: "댓글 삭제에 실패했습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                  });
                }
                else httpCelebFeedDeleteComment(context: context, comment: commentId).then((value){
                    if(value) Fluttertoast.showToast(msg: "댓글이 삭제되었습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                    else Fluttertoast.showToast(msg: "댓글 삭제에 실패했습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                  });
                break;
              default:
                return true;
            }
            return true;
          });
      },
      child: Container(
        child: Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              child: Center(
                child: CircleAvatar(
                  radius: 18,
                  backgroundColor: kMainColor,
                  backgroundImage: profileImg == null || profileImg == "" ?
                  AssetImage("assets/icon/seem_off_3x.png",) :
                  NetworkImage(profileImg,),
//                backgroundImage: this.profileImg == null ? AssetImage(
//                    "assets/icon/seem_off_3x.png"
//                ) : NetworkImage(
//                    this.profileImg
//                ),
                ),
              ),
            ),
            SizedBox(width: 15,),
            Expanded(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(width: 15,),
                  _CommentDisplayBlock(
                    userName: this.userName,
                    likeCount: this.likeCount,
                    badgeUrl: this.badgeUrl,
                    isLike: this.isLike,
                    text: this.text,
                    commentId: this.commentId,
                    type: this.type,
                  ),
                  SizedBox(height: 5,),
                  _CommentTimeAndReply(
                    timeStamp: this.timeStamp,
                    showReply: parentId == commentId,
                    replyState: Provider.of<CommentReplyProvider>(context).selectedCommentId == commentId,
                    onTap: () {
                      Provider.of<CommentReplyProvider>(context, listen: false).selectedCommentId == commentId ?
                        Provider.of<CommentReplyProvider>(context, listen: false).reset()  :
                        Provider.of<CommentReplyProvider>(context, listen: false).setReplyTarget(commentId, parentId);
                    },
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _CommentDisplayBlock extends StatelessWidget {
  final String userName;
  final String badgeUrl;
  final int likeCount;
  final bool isLike;
  final String text;
  final int commentId;
  final CommentType type;

  const _CommentDisplayBlock({
    Key key,
    this.userName,
    this.badgeUrl,
    this.likeCount,
    this.isLike,
    this.text,
    this.commentId,
    this.type
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          borderRadius: BorderRadius.circular(12),
          boxShadow: [
            BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.1),
                blurRadius: 8,
                offset: Offset(0, 3)
            )
          ]
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _CommentUserAndLike(
            commentId: commentId,
            userName: userName,
            badgeUrl: badgeUrl,
            likeCount: likeCount,
            isLike: isLike,
            type: type,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 15, right: 33),
            child: Text(
              text,
              style: TextStyle(
                color: Colors.black,
                fontSize: 13,
              ),
            ),
          ),
          SizedBox(height: 15,)
        ],
      ),
    );
  }
}

class _CommentUserAndLike extends StatefulWidget {
  final String userName;
  final String badgeUrl;
  final int likeCount;
  final bool isLike;
  final int commentId;
  final CommentType type;

  const _CommentUserAndLike({
    Key key,
    this.userName,
    this.badgeUrl,
    this.likeCount,
    this.isLike,
    this.commentId,
    this.type
  }) : super(key: key);

  @override
  _CommentUserAndLikeState createState() => _CommentUserAndLikeState();
}

class _CommentUserAndLikeState extends State<_CommentUserAndLike> {
  String _userName;
  int _likeCount;
  bool _isLike;
  int _commentId;
  CommentType _type;

  @override
  void initState() {
    super.initState();
    this._userName = widget.userName;
    this._likeCount = widget.likeCount;
    this._isLike = widget.isLike;
    this._commentId = widget.commentId;
    this._type = widget.type;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(left: 15, right: 10, top: 15, bottom: 15),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Text(
                this._userName,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 13
                ),
              ),
              SizedBox(width: 10,),
              widget.badgeUrl == null ? Container() : Image.network(
                widget.badgeUrl,
                width: 11,
                fit: BoxFit.contain,
              )
            ],
          ),
          GestureDetector(
            onTap: () async {
              bool ok;
              switch (_type) {
                case CommentType.celebFeed:
                  ok = await (this._isLike ? httpCelebFeedUnlikeComment(context: context, commentId: _commentId) : httpCelebFeedLikeComment(context: context, commentId: _commentId));
                  break;
                case CommentType.userFeed:
                  ok = await (this._isLike ? httpFeedUnlikeComment(context: context, commentId: _commentId) : httpFeedLikeComment(context: context, commentId: _commentId));
                  break;
                case CommentType.content:
                  ok = await (this._isLike ? httpContentUnlikeComment(context: context, commentId: _commentId) : httpContentLikeComment(context: context, commentId: _commentId));
                  break;
              }

              if (ok) {
                setState(() {
                  this._isLike = !this._isLike;
                  _likeCount += (this._isLike ? 1 : -1);
                });
              }
              else {
                Fluttertoast.showToast(msg: "좋아요를 남기는 데 실패했습니다.", gravity: ToastGravity.TOP);
              }
            },
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Image.asset(
                  "assets/icon/like_${this._isLike ? "on" : "off"}_3x.png",
                  width: 11,
                  fit: BoxFit.fitWidth,
                ),
                SizedBox(width: 5.1,),
                Text(
                  "${this._likeCount}",
                  style: TextStyle(
                    color: kSelectableTextColor,
                    fontSize: 13,
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

class _CommentTimeAndReply extends StatelessWidget {
  final String timeStamp;
  final bool replyState;
  final bool showReply;
  final Function onTap;

  const _CommentTimeAndReply({
    Key key,
    this.timeStamp,
    this.replyState = false,
    this.showReply = true,
    this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            child: Text(
              timeStamp,
              style: TextStyle(
                  color: kNavigationPressableColor,
                  fontSize: 11
              ),
            ),
          ),
          showReply ? Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              GestureDetector(
                onTap: onTap,
                behavior: HitTestBehavior.translucent,
                child: Row(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    SizedBox(width: 10,),
                    Text(
                      replyState ? "취소" : "답글달기",
                      style: TextStyle(
                          color: replyState ? kNegativeTextColor : kNavigationPressableColor,
                          fontSize: 11
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(width: 16,)
            ],
          ) : Container()
        ],
      ),
    );
  }
}

class CommentData {
  final int userId;
  final String userName;
  final String profileImg;
  final String badgeUrl;
  final int likeCount;
  final bool isLike;
  final String text;
  final String timeStamp;
  final int commentId;
  final List<CommentData> replies;
  final CommentType type;

  CommentData({
    this.userId,
    this.userName,
    this.profileImg,
    this.badgeUrl,
    this.likeCount,
    this.isLike,
    this.text,
    this.timeStamp,
    this.commentId,
    this.replies = const [],
    this.type
  });
}

enum CommentType {
  userFeed,
  celebFeed,
  content
}