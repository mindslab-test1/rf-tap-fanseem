
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/data/signup_data.dart';
import 'package:rf_tap_fanseem/http/profile/celeb_profile_subscribe.dart';
import 'package:rf_tap_fanseem/http/profile/celeb_profile_unsubscribe.dart';
import 'package:rf_tap_fanseem/http/profile/celeb_profile_view.dart';
import 'package:rf_tap_fanseem/http/profile/dto/http_profile_view_dto.dart';
import 'package:rf_tap_fanseem/providers/celeb_profile_provider.dart';

import '../../colors.dart';
import '../signup/component/signup_category_view.dart';

class CelebProfileBody extends StatefulWidget {

  @override
  _CelebProfileBodyState createState() => _CelebProfileBodyState();
}

class _CelebProfileBodyState extends State<CelebProfileBody> {
  HttpProfileViewDto _data;

  Future<bool> _getData(BuildContext context, int userId) async {
    _data = await httpCelebProfileView(context: context, userId: userId);
    if (_data == null) return false;
    Provider.of<CelebProfileProvider>(context, listen: false).celeb = _data;
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      future: _getData(context, Provider.of<CelebProfileProvider>(context, listen: false).celebId),
      builder: (context, snapshot) {
        if (snapshot.hasData && snapshot.data) {
          return Container(
              color: Color.fromRGBO(247, 247, 247, 1),
              child: SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    _CelebProfileStack(
                      userName: Provider
                          .of<CelebProfileProvider>(context)
                          .celebName,
                      profileImgUrl: _data.profileImageUrl,
                      coverImgUrl: _data.bannerImageUrl,
                      followerCnt: Provider
                          .of<CelebProfileProvider>(context)
                          .celeb
                          .subscriberCount,
                      feedCnt: _data.feedCount,
                      isSubscribing: Provider
                          .of<CelebProfileProvider>(context)
                          .celeb
                          .isSubscribing,
                      level: null,
                      description: "hi",
                      onSubscribeButtonTap: () {
                        setState(() {});
                      },
                    ),
                    _CelebCategory(
                      categories: [
                        CategoryEnum.asmr
                      ],
                    ),
                    Divider(
                      color: kBorderColor,
                      indent: 14.5,
                      endIndent: 14.5,
                      thickness: 0.5,
                    ),
                    _CelebGreetings(
                      greetings: "안녕하세요",
                    ),
                    Divider(
                      color: kBorderColor,
                      indent: 14.5,
                      endIndent: 14.5,
                      thickness: 0.5,
                    ),
                    _CelebIntroduction(
                      data: _data.introductions?.map((e) =>
                          IntroItemData(
                              name: e.keys.elementAt(0),
                              description: e.values.elementAt(0)
                          ))?.toList(),
                    ),
                  ],
                ),
              )
          );
        } else {
          return Center(child: CircularProgressIndicator(),);
        }
      }
    );
  }
}

class _CelebProfileStack extends StatelessWidget {
  final String userName;
  final String profileImgUrl;
  final String coverImgUrl;
  final int followerCnt;
  final int feedCnt;
  final int level;
  final bool isSubscribing;
  final String description;
  final Function onSubscribeButtonTap;

  const _CelebProfileStack({
    Key key,
    this.followerCnt = 0,
    this.feedCnt = 0,
    this.level = 1,
    this.userName = "",
    this.profileImgUrl,
    this.coverImgUrl,
    this.isSubscribing,
    this.description = "",
    this.onSubscribeButtonTap
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        height: 287.0 - 42,
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.16),
                  blurRadius: 8,
                  offset: Offset(0, 1)
              )
            ]
        ),
        child: Stack(
          fit: StackFit.expand,
          children: <Widget>[
            // info & cover image
            Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  height: 120,
                  width: double.infinity,
                  color: kMainColor,
                  child: ColorFiltered(
                    colorFilter: ColorFilter.mode(
                        Color.fromRGBO(0, 0, 0, 0.55),
                        BlendMode.darken
                    ),
                    child: coverImgUrl == null ? Image.asset(
                      "assets/logo/splash_X3.png",
                      fit: BoxFit.fitWidth,
                    ) : Image.network(coverImgUrl, fit: BoxFit.fitWidth),
                  ),
                ),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      flex: 3,
                      child: Column(
                        children: <Widget>[
                          Text(
                            "구독자",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 15
                            ),
                          ),
                          Text(
                            "$followerCnt",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 15,
                                fontWeight: FontWeight.bold
                            ),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          SizedBox(height: 36,),
                          Container(
                            height: 25,
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Container(
                                  height: 25,
                                  child: Text(
                                    userName,
                                    style: TextStyle(
                                        color: Colors.black,
                                        fontSize: 17,
                                        fontWeight: FontWeight.bold
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 3,
                      child: Column(
                        children: <Widget>[
                          Text(
                            "피드",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 15
                            ),
                          ),
                          Text(
                            "$followerCnt",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 15,
                                fontWeight: FontWeight.bold
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
                Divider(
                  color: kBorderColor,
                  indent: 14.5,
                  endIndent: 14.5,
                  thickness: 0,
                ),
//                SizedBox(
//                  height: 15,
//                ),
//                Container(
//                  height: 42,
//                  child: Row(
//                    mainAxisSize: MainAxisSize.max,
//                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                    children: <Widget>[
//                      GestureDetector(
//                        onTap: () => Provider.of<SubViewControllerProvider>(context, listen: false)
//                            .get(MyPageSubRoutes.myPageRoot)
//                            .jumpToPage(MyPageSubPages.myFollowers.index),
//                        behavior: HitTestBehavior.translucent,
//                        child: Text(
//                          "팔로워\n$followerCnt",
//                          textAlign: TextAlign.center,
//                          style: TextStyle(
//                              color: Colors.black,
//                              fontSize: 15,
//                              fontWeight: FontWeight.bold
//                          ),
//                        ),
//                      ),
//                      Text(
//                        "팔로우\n$followingCnt",
//                        textAlign: TextAlign.center,
//                        style: TextStyle(
//                            color: Colors.black,
//                            fontSize: 15,
//                            fontWeight: FontWeight.bold
//                        ),
//                      ),
//                      GestureDetector(
//                        onTap: () => Provider.of<SubViewControllerProvider>(context, listen: false)
//                            .get(MyPageSubRoutes.myPageRoot)
//                            .jumpToPage(MyPageSubPages.myLevel.index),
//                        behavior: HitTestBehavior.translucent,
//                        child: RichText(
//                          textAlign: TextAlign.center,
//                          text: TextSpan(
//                              style: TextStyle(
//                                  color: Colors.black,
//                                  fontSize: 15,
//                                  fontWeight: FontWeight.bold
//                              ),
//                              children: [
//                                TextSpan(text: "LEVEL\n"),
//                                TextSpan(
//                                    text: "$level",
//                                    style: TextStyle(
//                                      color: Theme.of(context).primaryColor,
//                                    )
//                                )
//                              ]
//                          ),
//                        ),
//                      ),
//                    ],
//                  ),
//                ),
                _CelebSnsInfo()
              ],
            ),
            // CircleAvatar
            Positioned(
              top: 46,
              left: MediaQuery.of(context).size.width / 2 - 50,
              child: Align(
                child: Container(
                  width: 100,
                  height: 100,
                  child: CircleAvatar(
                      radius: 50,
                      backgroundColor: Theme.of(context).backgroundColor,
                      child: CircleAvatar(
                        radius: 49,
                        backgroundColor: kMainColor,
                        backgroundImage: profileImgUrl == null || profileImgUrl == "" ?
                          AssetImage("assets/icon/seem_off_3x.png") :
                          NetworkImage(profileImgUrl),
                      )
                  ),
                ),
              ),
            ),
            // Edit Profile Button
//            Positioned(
//              top: 15,
//              right: 17,
//              child: GestureDetector(
//                onTap: () {
//                  Navigator.pushNamed(context, kRouteEditProfile).then((value) => Fluttertoast.showToast(msg: "TODO", toastLength: Toast.LENGTH_SHORT));
//                },
//                behavior: HitTestBehavior.translucent,
//                child: Container(
//                  width: 88,
//                  height: 23,
//                  child: Image.asset(
//                    "assets/button/edit_profile.png",
//                    fit: BoxFit.fitWidth,
//                  ),
//                ),
//              ),
//            )
            Positioned(
              top: 85,
              left: 30,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
//                  Container(
//                    width: 74,
//                    height: 23,
//                    decoration: BoxDecoration(
//                        border: Border.all(
//                          color: Colors.white,
//                          width: 1,
//                        ),
//                        borderRadius: BorderRadius.circular(12)
//                    ),
//                    child: Center(
//                      child: Text(
//                        "알림 받기",
//                        style: TextStyle(
//                          color: Colors.white,
//                          fontSize: 10,
//                        ),
//                      ),
//                    ),
//                  ),
//                  SizedBox(height: 7,),
                  GestureDetector(
                    onTap: () async {
                      var provider = Provider.of<CelebProfileProvider>(context, listen: false);
                      int celebId = provider.celebId;
                      bool ok = await (isSubscribing ? httpCelebProfileUnsubscribe(context: context, whomId: celebId) : httpCelebProfileSubscribe(context: context, whomId: celebId));
                      if (!ok) {
                        Fluttertoast.showToast(msg: "구독${isSubscribing ? " 취소" : ""}에 실패했습니다.", gravity: ToastGravity.TOP);
                      }
                      else {
                        onSubscribeButtonTap();
                      }
                    },
                    child: Container(
                      width: 75,
                      height: 23,
                      child: Image.asset(
                        "assets/button/subscribe_${Provider.of<CelebProfileProvider>(context).celeb.isSubscribing ? "on" : "off"}.png",
                        fit: BoxFit.fitHeight,
                      ),
                    ),
                  )
                ],
              ),
            ),
            // TODO send to community
//            Positioned(
//                top: 85,
//                right: 30,
//                child: Container(
//                  width: 75,
//                  height: 23,
//                  decoration: BoxDecoration(
//                    border: Border.all(
//                        color: kBorderColor,
//                        width: 1
//                    ),
//                    borderRadius: BorderRadius.circular(12),
//                  ),
//                  child: Center(
//                    child: Row(
//                      mainAxisSize: MainAxisSize.max,
//                      mainAxisAlignment: MainAxisAlignment.center,
//                      children: <Widget>[
//                        Text(
//                          "커뮤니티",
//                          textAlign: TextAlign.right,
//                          style: TextStyle(
//                            color: Colors.white,
//                            fontSize: 10,
//                          ),
//                        ),
//                        SizedBox(
//                          width: 5.2,
//                        ),
//                        Icon(
//                          Icons.arrow_forward_ios,
//                          color: Colors.white,
//                          size: 10,
//                        )
//                      ],
//                    ),
//                  ),
//                )
//            )
          ],
        )
    );
  }
}

// TODO
class _CelebSnsInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
        ],
      ),
    );
  }
}

class _CelebCategory extends StatelessWidget {
  final List<CategoryEnum> categories;

  const _CelebCategory({
    Key key,
    this.categories = const []
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "카테고리",
            style: TextStyle(
                color: Colors.black,
                fontSize: 15,
                fontWeight: FontWeight.bold
            ),
          ),
          SizedBox(height: 20,),
          Center(
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: categories.map((e) => Container(
                width: 40.5,
                height: 40.5,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: kFunctionButtonColor
                ),
                child: Image.asset(getCategoryIcon(e), fit: BoxFit.fitWidth,),
              )).toList(),
            ),
          )
        ],
      ),
    );
  }
}

class _CelebGreetings extends StatelessWidget {
  final String greetings;

  const _CelebGreetings({
    Key key,
    this.greetings
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text(
            "인사말",
            textAlign: TextAlign.start,
            style: TextStyle(
                color: Colors.black,
                fontSize: 15,
                fontWeight: FontWeight.bold
            ),
          ),
          SizedBox(height: 20,),
          Container(
            padding: const EdgeInsets.only(left: 15),
            child: Text(
              "$greetings",
              style: TextStyle(
                color: kSelectableTextColor,
                fontSize: 13,
              ),
            ),
          )
        ],
      ),
    );
  }
}


class _CelebIntroduction extends StatelessWidget {
  final List<IntroItemData> data;

  const _CelebIntroduction({
    Key key,
    this.data = const []
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 15),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text(
            "셀럽 소개",
            textAlign: TextAlign.start,
            style: TextStyle(
                color: Colors.black,
                fontSize: 15,
                fontWeight: FontWeight.bold
            ),
          ),
          SizedBox(height: 10,),
          Container(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: data.map((e) => Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                  child: _CelebIntroItem.buildWith(e),
                )).toList(),
              )
          )
        ],
      ),
    );
  }
}

class _CelebIntroItem extends StatelessWidget {
  final String name;
  final String description;

  const _CelebIntroItem({
    Key key,
    this.name,
    this.description
  }) : super(key: key);

  static _CelebIntroItem buildWith(IntroItemData data){
    return _CelebIntroItem(
      name: data.name,
      description: data.description,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
            width: 48,
            child: Text(
              name,
              textAlign: TextAlign.start,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(
                color: Colors.black,
                fontSize: 13,
              ),
            ),
          ),
          SizedBox(width: 20,),
          Text(
            description,
            textAlign: TextAlign.start,
            overflow: TextOverflow.ellipsis,
            style: TextStyle(
              color: kSelectableTextColor,
              fontSize: 13,
            ),
          )
        ],
      ),
    );
  }
}

class IntroItemData {
  final String name;
  final String description;

  IntroItemData({
    this.name,
    this.description
  });
}