
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/providers/celeb_profile_provider.dart';
import 'package:rf_tap_fanseem/view/celeb_profile/celeb_profile_body.dart';

import '../../colors.dart';

class CelebProfileRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        centerTitle: true,
        titleSpacing: 0,
        title: _CelebProfileAppbar(),
      ),
      body: CelebProfileBody(),
    );
  }
}

class _CelebProfileAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  width: 30,
                ),
                GestureDetector(
                  onTap: () => Navigator.pop(context),
                  behavior: HitTestBehavior.translucent,
                  child: Container(
                    child: Icon(
                      Icons.arrow_back_ios,
                      size: 30,
                      color: kAppBarButtonColor,
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
            flex: 6,
            child: Text(
              "${Provider.of<CelebProfileProvider>(context, listen: false).celebName}의 프로필",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Theme.of(context).backgroundColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 20
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(),
          )
        ],
      ),
    );
  }
}
