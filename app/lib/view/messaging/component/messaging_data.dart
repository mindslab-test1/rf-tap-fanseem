
import 'dart:async';

import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/util/timestamp.dart';

class MessageBlock extends StatefulWidget {
  final String timestamp;
  final String text;
  final MessageType type;
  final String membership;
  final String contentUrl;

  const MessageBlock({
    Key key,
    this.timestamp,
    this.text,
    this.type,
    this.membership,
    this.contentUrl,
  }) : super(key: key);

  static MessageBlock buildWith(BaseMessageData data){
    return MessageBlock(
      timestamp: data.timestamp,
      text: data.text == null ? "" : data.text,
      type: data.type,
      membership: data.membership,
      contentUrl: data.contentUrl,
    );
  }

  @override
  _MessageBlockState createState() => _MessageBlockState();
}

class _MessageBlockState extends State<MessageBlock> {
  bool _isAudioPlaying = false;
  AudioPlayer _audioPlayer;
  StreamSubscription _playerCompletionSubscription;
  StreamSubscription _playerErrorSubscription;

  @override
  void initState() {
    super.initState();
    _audioPlayer = AudioPlayer(mode: PlayerMode.MEDIA_PLAYER);
    this._playerCompletionSubscription = this._audioPlayer.onPlayerCompletion.listen((event) {
      setState(() => _isAudioPlaying = false);
    });
    this._playerErrorSubscription = this._audioPlayer.onPlayerError.listen((event) {
      Fluttertoast.showToast(msg: "TTS 듣기 중 문제가 발생했습니다...", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
      setState(() => _isAudioPlaying = false);
    });
    _initData();
  }

  void _initData() async {
    await Future.delayed(Duration(milliseconds: 500));
  }

  Widget _getChild(){
    switch(widget.type){
      case MessageType.text:
        return Text(
          widget.text,
          textAlign: TextAlign.left,
          maxLines: 10,
          overflow: TextOverflow.ellipsis,
          style: TextStyle(
              color: Colors.black,
              fontSize: 13
          ),
        );
      case MessageType.tts:
        return Container(
          child: Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                height: 28,
                child: Icon(
                  Icons.keyboard_voice,
                  color: kFunctionButtonColor,
                ),
              ),
              SizedBox(
                width: 66,
              ),
              GestureDetector(
                onTap: () {
                  if(_isAudioPlaying) return;
                  setState(() {
                    _isAudioPlaying = true;
                  });
                  this._play(widget.contentUrl);
                },
                child: Container(
                  height: 28,
                  child: Icon(
                    FontAwesomeIcons.play,
                    color: Colors.black,
                  ),
                ),
              )
            ],
          ),
        );
      default:
        throw UnimplementedError("unimplemented!");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        borderRadius: BorderRadius.circular(12),
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.1),
            blurRadius: 14,
            offset: Offset(0, 3)
          )
        ]
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(15),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Container(
                  child: Text(
                    // TODO membership badge
                    ""
                  ),
                ),
                Container(
                  child: Text(
                    formattedTime(widget.timestamp),
                    textAlign: TextAlign.right,
                    style: TextStyle(
                      color: kNavigationPressableColor,
                      fontSize: 12,
                    ),
                  ),
                )
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 30),
            child: Center(
              child: _getChild(),
            ),
          ),
          SizedBox(
            height: 28,
          )
        ],
      ),
    );
  }

  @override
  void dispose() {
    _audioPlayer.dispose();
    _playerCompletionSubscription.cancel();
    _playerErrorSubscription.cancel();
    super.dispose();
  }

  void _play(String url) async {
    await this._audioPlayer.play(url);
  }
}

class BaseMessageData {
  final String timestamp;
  final String text;
  final MessageType type;
  final String membership;
  final String contentUrl;

  const BaseMessageData({
    this.timestamp,
    this.text,
    this.type = MessageType.text,
    this.membership,
    this.contentUrl
  });
}

class UserMessageData {
  final bool isSender;
  final BaseMessageData data;

  const UserMessageData({
    this.isSender,
    this.data
  });
}

enum MessageType {
  text,
  tts
}
