
import 'package:flutter/cupertino.dart';
import 'package:rf_tap_fanseem/view/messaging/component/messaging_data.dart';

class MessagingDisplayProvider extends ChangeNotifier {
  MessagingType _type = MessagingType.celebSend;
  String _title = "메세지 보내기";
  int _celebId = 0;
  String _name = "Lorem Ipsum";

  List<BaseMessageData> _displayMessageData = [];
  bool _init = false;
  bool _endOfData = false;
  bool _loading = false;

  MessagingType get type => _type;
  int get celebId => _celebId;
  String get title => _title;
  String get name => _name;
  List<BaseMessageData> get displayMessageData => _displayMessageData;
  bool get init => _init;
  bool get endOfData => _endOfData;
  bool get loading => _loading;

  set displayMessageData(List<BaseMessageData> value){
    _displayMessageData = value;
    notifyListeners();
  }

  set init(bool value){
    _init = value;
    notifyListeners();
  }

  set endOfData(bool value){
    _endOfData = value;
    notifyListeners();
  }

  set loading(bool value){
    _loading = value;
    notifyListeners();
  }

  void setMessagingView(MessagingType type, String name, int celebId){
    this._type = type;
    this._name = name;
    this._celebId = celebId;
    this._init = false;
    switch(type){
      case MessagingType.getCeleb:
      case MessagingType.getUser:
        _title = "$name의 메세지";
        break;
      case MessagingType.celebSend:
        _title = "메세지 보내기";
        break;
    }
  }

  void reset(){
    _type = MessagingType.celebSend;
    _title = "메세지 보내기";
    _celebId = 0;
    _name = "Lorem Ipsum";

    _displayMessageData = [];
    _init = false;
    _endOfData = false;
    _loading = false;
    notifyListeners();
  }

  @override
  String toString() {
    return 'MessagingDisplayProvider{_type: $_type, _title: $_title, _celebId: $_celebId, _name: $_name, _displayMessageData: $_displayMessageData, _init: $_init, _endOfData: $_endOfData, _loading: $_loading}';
  }
}

enum MessagingType {
  getCeleb,
  getUser,
  celebSend
}