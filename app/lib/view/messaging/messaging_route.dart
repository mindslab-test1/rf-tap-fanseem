
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/view/messaging/messaging_body.dart';
import 'package:rf_tap_fanseem/view/messaging/states/messaging_provider.dart';

import '../../colors.dart';
import '../../routes.dart';
import 'component/messaging_data.dart';

class MessagingRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      BackButtonInterceptor.add((stopDefaultButtonEvent) {
        Navigator.popUntil(context,ModalRoute.withName(kRouteHome));
        return true;
      }, zIndex: 10, ifNotYetIntercepted: true);
    });

    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0.0,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: _MessagingAppBar(),
      ),
      body: Provider.of<MessagingDisplayProvider>(context).type != MessagingType.getUser ?
        CelebMessagingBody(
//          initialData: _testInitialBaseMessages,
        ) : MessagingBody(
//          initialData: _testInitialUserMessages,
        ),
    );
  }
}

class _MessagingAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  width: 30,
                ),
                GestureDetector(
                  onTap: () => Navigator.pop(context),
                  behavior: HitTestBehavior.translucent,
                  child: Container(
                    child: Icon(
                      Icons.arrow_back_ios,
                      size: 30,
                      color: kAppBarButtonColor,
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
            flex: 6,
            child: Center(
              child: Text(
                Provider.of<MessagingDisplayProvider>(context).title,
                style: TextStyle(
                    color: Theme.of(context).backgroundColor,
                    fontSize: 20
                ),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(),
          )
        ],
      ),
    );
  }
}

List<BaseMessageData> _testInitialBaseMessages = [
  BaseMessageData(
      timestamp: "2월 20일 오후 10:16",
      text: "첫 메시지",
      type: MessageType.text
  ),
  BaseMessageData(
      timestamp: "2월 19일 오후 10:16",
      text: "두번째 메시지",
      type: MessageType.text
  ),
  BaseMessageData(
      timestamp: "2월 20일 오후 10:16",
      text: "첫 메시지",
      type: MessageType.text
  ),
  BaseMessageData(
      timestamp: "2월 19일 오후 10:16",
      text: "두번째 메시지",
      type: MessageType.text
  ),
  BaseMessageData(
      timestamp: "2월 20일 오후 10:16",
      text: "첫 메시지",
      type: MessageType.text
  ),
  BaseMessageData(
      timestamp: "2월 19일 오후 10:16",
      text: "두번째 메시지",
      type: MessageType.text
  ),
  BaseMessageData(
      timestamp: "2월 20일 오후 10:16",
      text: "첫 메시지",
      type: MessageType.text
  ),
  BaseMessageData(
      timestamp: "2월 19일 오후 10:16",
      text: "두번째 메시지",
      type: MessageType.text
  ),
  BaseMessageData(
      timestamp: "2월 20일 오후 10:16",
      text: "첫 메시지",
      type: MessageType.text
  ),
  BaseMessageData(
      timestamp: "2월 19일 오후 10:16",
      text: "두번째 메시지",
      type: MessageType.text
  ),
  BaseMessageData(
      timestamp: "2월 20일 오후 10:16",
      text: "첫 메시지",
      type: MessageType.text
  ),
  BaseMessageData(
      timestamp: "2월 19일 오후 10:16",
      text: "두번째 메시지",
      type: MessageType.text
  ),
];

List<UserMessageData> _testInitialUserMessages = _testInitialBaseMessages.asMap().entries.map((e) => UserMessageData(
    isSender: e.key % 2 == 0,
    data: e.value
)).toList();
