

import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/view/report_manage/report_manage_body.dart';

import '../../colors.dart';

class ReportManageRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        automaticallyImplyLeading: false,
        centerTitle: true,
        title: _ReportManageAppBar(),
      ),
      body: Container(
        child: DefaultTabController(
          initialIndex: 0,
          length: 2,
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 50,
                decoration: BoxDecoration(
                    color: Theme.of(context).backgroundColor,
                    boxShadow: [
                      BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.05),
                          blurRadius: 11,
                          offset: Offset(0, 3)
                      )
                    ]
                ),
                child: TabBar(
                  indicatorColor: Theme.of(context).primaryColor,
                  labelColor: Theme.of(context).primaryColor,
                  labelStyle: TextStyle(
                      color: Theme.of(context).primaryColor,
                      fontSize: 17,
                      fontWeight: FontWeight.bold
                  ),
                  unselectedLabelColor: kNavigationPressableColor,
                  unselectedLabelStyle: TextStyle(
                      fontSize: 15
                  ),
                  tabs: <Widget>[
                    Tab(child: Text("신고 대상 목록")),
                    Tab(child: Text("차단한 유저")),
                  ],
                ),
              ),
              Expanded(
                child: TabBarView(
                  children: <Widget>[
                    ReportManageBody(),
                    ReportManageBody(isReport: false,)
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}

class _ReportManageAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  width: 30,
                ),
                GestureDetector(
                  onTap: () => Navigator.pop(context),
                  behavior: HitTestBehavior.translucent,
                  child: Container(
                    child: Icon(
                      Icons.arrow_back_ios,
                      size: 30,
                      color: kAppBarButtonColor,
                    ),
                  ),
                )
              ],
            ),
          ),
          Expanded(
            flex: 6,
            child: Text(
              "신고/차단 관리",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Theme.of(context).backgroundColor,
                  fontSize: 20,
                  fontWeight: FontWeight.bold
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(),
          )
        ],
      ),
    );
  }
}

