import 'dart:async';
import 'package:audioplayers/audioplayers.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/components/celeb_info_components.dart';
import 'package:rf_tap_fanseem/components/progress_dialog.dart';
import 'package:rf_tap_fanseem/http/mypage/mypage_tts_write.dart';
import 'package:rf_tap_fanseem/providers/selected_celeb_provider.dart';
import 'package:rf_tap_fanseem/providers/tts_write_provider.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/view/main/component/partial_components.dart';
import 'package:rf_tap_fanseem/view/tts_write/tts_write_component.dart';

import '../../colors.dart';
import '../../providers/mypage_view_provider.dart';
import '../../providers/tts_write_provider.dart';


class TtsWriteRoute extends StatefulWidget {
  @override
  _TtsWriteRouteState createState() => _TtsWriteRouteState();
}

class _TtsWriteRouteState extends State<TtsWriteRoute> {
  ProgressDialog _pr;
  TextEditingController _editingController = TextEditingController();

  AudioPlayer _audioPlayer;
  StreamSubscription _playerCompletionSubscription;
  StreamSubscription _playerErrorSubscription;

  bool _isAudioPlaying = false;
  int _textLength = 0;
  int _monthlyLimit = 0;
  int _monthlyUsed = 0;

  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add((stopDefaultButtonEvent) {
      Navigator.popUntil(context,ModalRoute.withName(kRouteTtsService));
      return true;
    }, zIndex: 11, ifNotYetIntercepted: true);
    _audioPlayer = AudioPlayer(mode: PlayerMode.MEDIA_PLAYER);
    this._playerCompletionSubscription = this._audioPlayer.onPlayerCompletion.listen((event) {
      setState(() => _isAudioPlaying = false);
    });
    this._playerErrorSubscription = this._audioPlayer.onPlayerError.listen((event) {
      print(event);
      Fluttertoast.showToast(msg: "TTS 미리듣기 중 문제가 발생했습니다...", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
      setState(() => _isAudioPlaying = false);
    });
    _initData();
  }

  void _initData() async {
    await Future.delayed(Duration(milliseconds: 500));

    int index = 0;
    Provider.of<TtsWriteProvider>(context, listen: false).ttsCelebList = Provider.of<MyPageViewProvider>(context, listen: false).celebList;
    if(Provider.of<TtsWriteProvider>(context, listen: false).fromCommunity){
      int selectedCelebId = Provider.of<SelectedCelebProvider>(context, listen: false).celebId;
      List<CelebData> celebList = Provider.of<TtsWriteProvider>(context, listen: false).ttsCelebList;
      index = celebList.indexWhere((element) => element.celebId == selectedCelebId);
      if(index == -1) {
        index = 0;
        print("warning: could not find celeb with id: $selectedCelebId");
      }
    }

    Provider.of<TtsWriteProvider>(context, listen: false).selectedCeleb = Provider.of<MyPageViewProvider>(context, listen: false).celebList[index];

    await httpTtsWriteView(Provider.of<MyPageViewProvider>(context, listen: false).celebList[index].celebId, context: context);
    setState(() {
      _monthlyLimit = Provider.of<TtsWriteProvider>(context, listen: false).monthlyLimit;
      _monthlyUsed = Provider.of<TtsWriteProvider>(context, listen: false).monthlyUsed;
    });
  }

  @override
  Widget build(BuildContext context) {
    _pr = ProgressDialog(
        context,
        showLogs: true,
        isDismissible: false,
        customBody: Center(
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: 45,
                width: 45,
//              child: Image.asset(
//                "assets/icon/feather_loader.png", fit: BoxFit.contain,
//              ),
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(kNegativeTextColor),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              Text(
                "미리듣기 준비중입니다.",
                style: TextStyle(
                    fontSize: 12,
                    color: kNegativeTextColor
                ),
              )
            ],
          ),
        )
    );
    _pr.style(
        backgroundColor: Colors.transparent,
        elevation: 0,
        padding: const EdgeInsets.all(0)
    );

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        titleSpacing: 0.0,
        automaticallyImplyLeading: false,
        title: _TtsWriteAppBar(),
      ),
      body: Container(
//        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 15,),
            Padding(
              padding: const EdgeInsets.only(left: 30, right: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "TTS 셀럽 선택",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                  GestureDetector(
                    onTap: () => showGeneralDialog(
                      context: context,
                      pageBuilder: (context, animation, secondAnimation) => TtsWriteCelebDialog(),
                      barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                      barrierDismissible: false,
                      transitionDuration: const Duration(milliseconds: 150),
                    ).then((value) => setState(() {
                      _monthlyLimit = Provider.of<TtsWriteProvider>(context, listen: false).monthlyLimit;
                      _monthlyUsed = Provider.of<TtsWriteProvider>(context, listen: false).monthlyUsed;
                    })),
                    child: Container(
                      width: 63,
                      height: 23,
                      decoration: BoxDecoration(
                          color: kMainBackgroundColor,
                          borderRadius: BorderRadius.circular(12),
                          border: Border.all(color: kBorderColor, width: 1),
                          boxShadow: [
                            BoxShadow(
                                color: Color.fromRGBO(0, 0, 0, 0.16),
                                blurRadius: 3,
                                offset: Offset(0, 2)
                            )
                          ]
                      ),
                      child: Center(
                        child: Text(
                          "더보기 >",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            color: kSelectableTextColor,
                            fontSize: 10,
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 15,),
            Center(
              child: Provider.of<TtsWriteProvider>(context).ttsCelebList.isEmpty ? CircularProgressIndicator() : SubscribingCelebIcons(
                celebName: Provider.of<TtsWriteProvider>(context).selectedCeleb.name,
                imgUrl: Provider.of<TtsWriteProvider>(context).selectedCeleb.profileImgUrl,
              ),
            ),
            SizedBox(height: 15,),
            Padding(
              padding: const EdgeInsets.only(left: 30, right: 15),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    "본문 입력",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 15,),
            Expanded(
              child: Container(
                  padding: const EdgeInsets.symmetric(horizontal: 30),
                  child: Container(
                    decoration: BoxDecoration(
                        color: kMainBackgroundColor,
                        borderRadius: BorderRadius.circular(12),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.1),
                              blurRadius: 14,
                              offset: Offset(0, 3)
                          )
                        ]
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 10),
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Expanded(
                            child: TextField(
                              expands: true,
                              controller: _editingController,
                              onChanged: (value) => setState(() => _textLength = value.length),
                              maxLength: 300,
                              maxLengthEnforced: true,
                              maxLines: null,
                              decoration: InputDecoration.collapsed(
                                  hintText: "TTS 내용을 입력하세요.",
                                  hintStyle: TextStyle(
                                      color: kBorderColor,
                                      fontSize: 14
                                  )
                              ),
                            ),
                          ),
                          SizedBox(height: 2,),
                          Container(
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text(
                                  "1회당 요청 가능한 글자 수",
                                  style: TextStyle(
                                      fontSize: 11,
                                      color: kSelectableTextColor
                                  ),
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                  )
              ),
            ),
            Container(
              height: 95,
              child: Center(
                child: Container(
                  height: 70,
                  child: GestureDetector(
                    onTap: () async {
                      await _pr.show();
                      await Future.delayed(Duration(milliseconds: 200));
                      if(_monthlyLimit < 1 || _monthlyLimit == null){
                        await _pr.hide();
                        Fluttertoast.showToast(msg: "멤버십 권한이 필요한 서비스입니다", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                        return;
                      }
                      if(!this._isAudioPlaying){
                        String ttsText = _editingController.text;
                        if(_monthlyUsed + ttsText.length > _monthlyLimit) {
                          await _pr.hide();
                          Fluttertoast.showToast(msg: "요청 가능한 글자수를 초과하였습니다", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                        } else {
                          if(ttsText.length < 1) {
                          await _pr.hide();
                          Fluttertoast.showToast(msg: "빈 문장은 재생할 수 없습니다", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                        } else{
                          this._isAudioPlaying = true;
                          String previewUrl = await httpTtsWriteSample(
                              Provider.of<TtsWriteProvider>(context, listen: false).selectedCeleb.celebId,
                              ttsText,
                              context: context
                          );
                          if(previewUrl == null) {
                            await _pr.hide();
                            Fluttertoast.showToast(msg: "문제가 발생했습니다...", toastLength: Toast.LENGTH_SHORT);
                            this._isAudioPlaying = false;
                            return;
                          }
                          Fluttertoast.showToast(msg: "다음이 재생됩니다: $ttsText", toastLength: Toast.LENGTH_SHORT);
                          print("${Provider.of<TtsWriteProvider>(context, listen: false).ttsId}");
                          await _pr.hide();
                          this._play(previewUrl);
                        }
                        }
                      } else{
                        await _pr.hide();
                        Fluttertoast.showToast(msg: "TTS 미리듣기 중입니다", toastLength: Toast.LENGTH_SHORT);
                      }
                    },
                    behavior: HitTestBehavior.translucent,
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          FontAwesomeIcons.play,
                          size: 27,
                        ),
                        SizedBox(height: 5,),
                        Text(
                          "미리 듣기",
                          style: TextStyle(
                            color: kSelectableTextColor,
                            fontSize: 10,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              height: 36,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  RichText(
                    text: TextSpan(
                        style: TextStyle(
                            color: kNavigationPressableColor,
                            fontSize: 10,
                            fontWeight: FontWeight.bold
                        ),
                        children: [
                          TextSpan(
                              text: "이번 달 사용 글자 "
                          ),
                          TextSpan(
                              text: "$_monthlyUsed ",
                              style: TextStyle(
                                  color: kMainColor
                              )
                          ),
                          TextSpan(
                              text: "/ $_monthlyLimit",
                              style: TextStyle(
                                  color: Colors.black
                              )
                          )
                        ]
                    ),
                    maxLines: 1,
                  ),
//                  RichText(
//                    text: TextSpan(
//                        style: TextStyle(
//                            fontSize: 13,
//                            fontWeight: FontWeight.bold
//                        ),
//                        children: [
//                          TextSpan(
//                              text: "$_textLength ",
//                              style: TextStyle(
//                                  color: kMainColor
//                              )
//                          ),
//                          TextSpan(
//                              text: "/ 300",
//                              style: TextStyle(
//                                  color: Colors.black
//                              )
//                          )
//                        ]
//                    ),
//                  )
                ],
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 70,
              decoration: BoxDecoration(
                  color: Theme.of(context).backgroundColor,
                  boxShadow: [
                    BoxShadow(
                        color: Color.fromRGBO(0, 0, 0, 0.16),
                        blurRadius: 6
                    )
                  ]
              ),
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 10, horizontal: MediaQuery.of(context).size.width * 0.55 / 2),
                child: GestureDetector(
                  onTap: () async {
                    await _pr.show();
                    await Future.delayed(Duration(milliseconds: 200));
                    if(_monthlyLimit < 1 || _monthlyLimit == null){
                      await _pr.hide();
                      Fluttertoast.showToast(msg: "멤버십 권한이 필요한 서비스입니다", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                    }
                    else{
                      String ttsText = _editingController.text;
                      if(_monthlyUsed + ttsText.length > _monthlyLimit){
                        await _pr.hide();
                        Fluttertoast.showToast(msg: "요청 가능한 글자수를 초과하였습니다", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                      }
                      else if(ttsText.length < 1) {
                        await _pr.hide();
                        Fluttertoast.showToast(msg: "빈 문장은 재생할 수 없습니다", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                      } else {
                        await _pr.hide();
                        bool ok = await showGeneralDialog(
                          context: context,
                          pageBuilder: (context, animation, secondAnimation) => _TtsOkDialog(),
                          barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                          barrierDismissible: false,
                          transitionDuration: const Duration(milliseconds: 150),
                        );
                        if(!ok) return;
                        await _pr.show();
                        await Future.delayed(Duration(milliseconds: 200));
                        httpTtsWriteRequest(
                            ttsId: Provider.of<TtsWriteProvider>(context, listen: false).ttsId,
                            celebId: Provider.of<TtsWriteProvider>(context, listen: false).selectedCeleb.celebId,
                            context: context
                        ).then((value) async {
                          await _pr.hide();
                          value ? Navigator.pop(context, true) : Fluttertoast.showToast(msg: "요청에 실패했습니다...");
                        });
                      }
                    }
                  },
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.55,
                    decoration: BoxDecoration(
                        color: Theme.of(context).backgroundColor,
                        border: Border.all(
                          color: kMainColor,
                        ),
                        borderRadius: BorderRadius.all(Radius.circular(12)),
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.16),
                              offset: Offset(0, 3),
                              blurRadius: 6
                          )
                        ]
                    ),
                    child: Center(
                      child: Text(
                        "승인 요청",
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: kMainColor
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  void dispose() {
    this._audioPlayer?.dispose();
    this._playerCompletionSubscription?.cancel();
    this._playerErrorSubscription?.cancel();
    super.dispose();
  }

  void _play(String url) async {
    await this._audioPlayer.play(url);
  }
}

class _TtsWriteAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 30,
                  ),
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                        child: Container(
                          child: Icon(
                            FontAwesomeIcons.times,
                            size: 27,
                            color: kAppBarButtonColor,
                          ),
                        )
                    ),
                  ),
                ]
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Text(
                "TTS 직접 작성",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: kAppBarButtonColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(),
          ),
        ],
      ),
    );
  }
}

class _TtsOkDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(26),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width - 28,
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(26),
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.64),
                  blurRadius: 14
              )
            ]
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 28),
              child: Center(
                child: SingleChildScrollView(
                  child: Text(
                      "TTS 직접생성을 통해 추출한 TTS 합성음성은, 셀럽의 1차 창작물이므로 이에 대한 저작권은 셀럽에게 있습니다.\n"
                          "셀럽이 요청 TTS를 승인한 것을 2차 저작물의 제작을 허락한 것으로 간주하므로 2차 저작물을 창작하여 공유하는 것은 자유롭지만, 셀럽이 해당 게시물의 삭제를 요청할 경우 이에 응해야 합니다.\n"
                          "또한, 악용 사례로 인한 명예훼손 등의 피해 발생시, 셀럽의 정상적인 활동을 보호하기 위해 강력한 법적 조치를 시행할 예정입니다.\n"
                          "따라서, 여러분의 상상력을 발휘하여 긍정적인 콘텐츠를 생산하는 것은 좋지만, 셀럽의 활동에 지장이 가지 않도록 심사숙고하여 활용하여 주시길 바랍니다. (자세한 TTS 사용법은 셀럽의 공지사항 탭에서 확인할 수 있습니다.\n"
                  ),
                ),
              ),
            ),
            Container(
              height: 55,
              decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(color: Color.fromRGBO(228, 228, 228, 0.64), width: 0.5)
                ),
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () => Navigator.pop(context, false),
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "취소",
                          style: TextStyle(
                            fontSize: 17,
                            color: kUnSelectedColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                  VerticalDivider(
                    width: 0.5,
                  ),
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () => Navigator.pop(context, true),
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "확인",
                          style: TextStyle(
                              fontSize: 17,
                              color: Colors.black
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}


