import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/components/switch_item.dart';
import 'package:rf_tap_fanseem/http/mypage/mypage_setting.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/view/main/component/category_component.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PushSettingBody extends StatefulWidget {
  @override
  _PushSettingBodyState createState() => _PushSettingBodyState();
}

class _PushSettingBodyState extends State<PushSettingBody> {
  // TODO get settings from shared preferences
  Map _currentSettings = pushSettings;
  bool _init = false;

  List<PushSwitchData> _alertSettings;
  List<PushSwitchData> _myActivitySettings;
  List<PushSwitchData> _celebActivitySettings;
  bool _myActivitySettingsGeneral = true;

  @override
  void initState() {
    super.initState();
    _alertSettings = [];
    _myActivitySettings = [];
    _celebActivitySettings = [];
//    _currentSettings["alertSettings"].forEach((e) => _alertSettings.add(PushSwitchData(
//      optionName: e["optionName"],
//      optionState: e["optionState"],
//    )));
//    _currentSettings["myActivitySettings"].forEach((e) => _myActivitySettings.add(PushSwitchData(
//      optionName: e["optionName"],
//      optionState: e["optionState"],
//    )));
//    _currentSettings["celebActivitySettings"].forEach((e) => _celebActivitySettings.add(PushSwitchData(
//      optionName: e["optionName"],
//      optionState: e["optionState"],
//    )));
    BackButtonInterceptor.add((pop) {
      Navigator.popUntil(context, ModalRoute.withName(kRouteHome));
      return true;
    }, zIndex: 1, name: "pop_setting_push");
  }

  save(String key, dynamic value) async {
    SharedPreferences sharedPrefs = await SharedPreferences.getInstance();
    setState(() {
      _myActivitySettingsGeneral = sharedPrefs.getBool("myBool");
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: FutureBuilder(
          future: _getData(),
          builder: (context, snapshot) {
            if(snapshot.hasData)
              return SingleChildScrollView(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    HeaderAndBodyComponent(
                      headerTitle: "알림 설정",
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: _alertSettings.map((e) => SwitchItem(
                          name: e.optionName,
                          value: e.optionState,
                          onChange: (value) => setState(() => e.optionState = !e.optionState),
                        )).toList(),
                      ),
                    ),
                    HeaderAndBodyComponent(
                      headerTitle: "상황별 푸시",
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Column(
                            children: <Widget>[
//                              Container(
//                                padding: const EdgeInsets.symmetric(horizontal: 30),
//                                child: Row(
//                                  mainAxisSize: MainAxisSize.max,
//                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                  children: <Widget>[
//                                    Text(
//                                      "나의 활동",
//                                      style: TextStyle(
//                                          color: kSelectableTextColor,
//                                          fontSize: 12
//                                      ),
//                                    ),
//                                    Container(
//                                      child: Switch(
//                                        value: _myActivitySettingsGeneral,
//                                        onChanged: (bool value){
//                                          setState(() {
//                                            _myActivitySettingsGeneral = value;
//                                          });
//                                          save('myBool', value);
//                                        },
////                                onChanged: (bool value) => setState(() => _myActivitySettingsGeneral = value),
//                                      ),
//                                    )
//                                  ],
//                                ),
//                              ),
                              Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: _myActivitySettings.map((e) => SwitchItem(
                                  name: e.optionName,
                                  value: e.optionState,
                                  onChange: (value) => setState(() => e.optionState = !e.optionState),
                                )).toList(),
                              )
                            ],
                          ),
                          Column(
                            children: <Widget>[
//                              Container(
//                                padding: const EdgeInsets.symmetric(horizontal: 30),
//                                child: Row(
//                                  mainAxisSize: MainAxisSize.max,
//                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                  children: <Widget>[
//                                    Text(
//                                      "셀럽 활동",
//                                      style: TextStyle(
//                                          color: kSelectableTextColor,
//                                          fontSize: 12
//                                      ),
//                                    ),
//                                    Container(
//                                      child: Switch(
//                                        value: _myActivitySettingsGeneral,
//                                        onChanged: (value) => setState(() => _myActivitySettingsGeneral = value),
//                                      ),
//                                    )
//                                  ],
//                                ),
//                              ),
                              Column(
                                mainAxisSize: MainAxisSize.min,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: _celebActivitySettings.map((e) => SwitchItem(
                                  name: e.optionName,
                                  value: e.optionState,
                                  onChange: (value) => setState(() => e.optionState = !e.optionState),
                                )).toList(),
                              ),
//                      SizedBox(height: 3,),
//                      NextPageBarHolder(
//                        childrenValues: [
//                          NextPageBarData(
//                            onTap: () => Navigator.pushNamed(context, kRouteSettingsCelebPush),
//                            text: "셀럽별 알림 설정",
//                            textStyle: TextStyle(
//                              color: Colors.black,
//                              fontSize: 13
//                            )
//                          )
//                        ],
//                      )
                            ],
                          )
                        ],
                      ),
                    )
                  ],
                ),
              );
            return Container(
              child: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }
      ),
    );
  }

  Future<bool> _getData() async {
    if(_init) return true;
    Map<String, List<PushSwitchData>> data = await httpMyPageSettings(context: context);
    print(data);
    setState(() {
      _alertSettings = data["alertSettings"];
      _myActivitySettings = data["myActivitySettings"];
      _celebActivitySettings = data["celebActivitySettings"];
    });
    _init = true;
    return true;
  }

  @override
  void dispose() {
    BackButtonInterceptor.removeByName("pop_setting_push");
    super.dispose();
  }
}

class PushSwitchData{
  String optionName;
  bool optionState;

  PushSwitchData({this.optionName, this.optionState});
}

Map pushSettings = {
  "alertSettings": [
    {"optionName": "소리", "optionState": true,},
    {"optionName": "진동", "optionState": true,},
  ],
  "myActivitySettings": [
    {"optionName": "좋아요", "optionState": true,},
    {"optionName": "스크랩", "optionState": true,},
    {"optionName": "댓글", "optionState": true,},
  ],
  "celebActivitySettings": [
    {"optionName": "새로운 피드", "optionState": true,},
    {"optionName": "새로운 콘텐츠", "optionState": true,},
    {"optionName": "셀럽 메세지", "optionState": true,},
    {"optionName": "셀럽의 좋아요", "optionState": true,},
    {"optionName": "셀럽의 스크랩", "optionState": true,},
    {"optionName": "셀럽의 댓글", "optionState": true,},
  ]
};

