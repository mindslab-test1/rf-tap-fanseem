

import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/providers/function_injection.dart';

class HeaderAndBodyComponent extends StatelessWidget {
  final String headerTitle;
  final Widget child;
  final bool addMoreButton;
  final void Function() moreButtonTap;

  const HeaderAndBodyComponent({Key key, @required this.headerTitle, @required this.child, this.addMoreButton = false, this.moreButtonTap, })
      : assert(!addMoreButton || moreButtonTap != null), super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            height: 57,
            child: Padding(
              padding: EdgeInsets.only(left: 30, top: 15, right: 15, bottom: 20),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    headerTitle,
                    textAlign: TextAlign.left,
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                  addMoreButton ? GestureDetector(
                    onTap: moreButtonTap,
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                      width: 63,
                      child: Image.asset(
                        "assets/button/more_button.png",
                        fit: BoxFit.fitWidth,
                      ),
                    ),
                  ) : Container(width: 1,)
                ],
              ),
            ),
          ),
          child
        ],
      ),
    );
  }
}

class NextPageBarHolder extends StatelessWidget {
  final List<NextPageBarData> childrenValues;

  const NextPageBarHolder({Key key, this.childrenValues}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          boxShadow: [
            BoxShadow(
              offset: Offset(0, 2),
              color: Color.fromRGBO(0, 0, 0, 0.09),
              blurRadius: 8,
            )
          ]
      ),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: childrenValues.map((e) => GestureDetector(
          onTap: e.onTap,
          behavior: HitTestBehavior.translucent,
          child: Container(
            height: 60,
            width: double.infinity,
            padding: EdgeInsets.only(left: 45, top: 21, right: 30),
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(
                    width: 0.5,
                    color: kBorderColor
                )
              )
            ),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  e.text,
                  style: e.textStyle == null ? const TextStyle(
                      fontSize: 13,
                      color: kSelectableTextColor
                  ) : e.textStyle
                ),
                Container(
                    width: 12,
                    height: 20,
                    child: Image.asset("assets/icon/right_3x.png", fit: BoxFit.contain,)
                )
              ],
            ),
          ),
        )).toList(),
      ),
    );
  }
}

class NextPageBarData {
  final void Function() onTap;
  final String text;
  final TextStyle textStyle;

  NextPageBarData({
    this.onTap = dummyFunction,
    @required this.text,
    this.textStyle
  });
}