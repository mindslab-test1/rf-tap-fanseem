
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/providers/login_provider.dart';
import 'package:rf_tap_fanseem/providers/navigator_provider.dart';

import '../../../main.dart';
import '../../../routes.dart';

class MainRouteAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MainNavigationProvider appBarProvider = Provider.of<MainNavigationProvider>(context);
    return appBarProvider.searchState ? _SearchAppBar() : _PrimaryAppBar();
  }
}

class _SearchAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MainNavigationProvider appBarProvider = Provider.of<MainNavigationProvider>(context);

    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: appBarProvider.leftButtonFunction,
            child: Container(
              child: Icon(
                Icons.arrow_back_ios,
                size: 30,
                color: kAppBarButtonColor,
              ),
            ),
          ),
          SizedBox(
            width: 19,
          ),
          Expanded(
            child: Container(
              height: 34,
              padding: const EdgeInsets.only(left: 18, right: 14.7),
              decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                borderRadius: BorderRadius.circular(19),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Expanded(
                    child: Text(
                      "TODO",
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                      ),
                    ),
                  ),
                  GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: appBarProvider.rightButtonFunction,
                    child: Container(
                      width: 22.1, height: 22.1,
                      child: Icon(
                        Icons.cancel,
                        size: 22.1,
                        color: kBorderColor,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class _PrimaryAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    MainNavigationProvider appBarProvider = Provider.of<MainNavigationProvider>(context);

    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Expanded(
          flex: 3,
          child: _LeadingButton(leftButton: appBarProvider.leftButton,),
        ),
        Expanded(
          flex: 3,
          child: _CenterWidget(title: appBarProvider.title,),
        ),
        Expanded(
          flex: 3,
          child: _TrailingButton(rightButton: appBarProvider.rightButton,),
        )
      ],
    );;
  }
}


class _CenterWidget extends StatelessWidget {
  String title;

  _CenterWidget({this.title})
      : assert(title != null);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        title,
        style: TextStyle(
          fontSize: 20,
          fontWeight: FontWeight.bold,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }
}

class _LeadingButton extends StatelessWidget {
  final BarLeftButton leftButton;

  _LeadingButton({this.leftButton});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            width: 30,
          ),
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: Provider.of<MainNavigationProvider>(context).leftButtonFunction,
            child: _getOptionIcon(context)
          )
        ],
      ),
    );
  }

  Widget _getOptionIcon(context){
    switch(this.leftButton){
      case BarLeftButton.back:
        return Container(
          child: Icon(
            Icons.arrow_back_ios,
            size: 30,
            color: kAppBarButtonColor,
          ),
        );
      case BarLeftButton.crossOut:
        return Container(
          child: Icon(
            FontAwesomeIcons.times,
            size: 27,
            color: kAppBarButtonColor,
          ),
        );
      case BarLeftButton.login:
        bool isLogin = Provider.of<LoginVerifyProvider>(context, listen: false).isLogin;
        return isLogin? Container() : Container(
          child: Text(
            "로그인",
            style: TextStyle(
              fontSize: 15,
              color: kAppBarButtonColor,
              fontWeight: FontWeight.bold,
            ),
          ),
        );
      default:
        return Container();
    }
  }
}

class _TrailingButton extends StatelessWidget {
  final BarRightButton rightButton;

  _TrailingButton({this.rightButton});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onTap: Provider.of<MainNavigationProvider>(context).rightButtonFunction,
            child: _getOptionIcon()
          ),
          SizedBox(
            width: 30,
          )
        ],
      ),
    );
  }

  Widget _getOptionIcon(){
    switch(this.rightButton){
      case BarRightButton.details:
        return Container(
          child: Icon(
            FontAwesomeIcons.ellipsisH,
            size: 27,
            color: kAppBarButtonColor,
          ),
        );
      case BarRightButton.search:
        return Container(
          child: Icon(
            Icons.search,
            size: 30,
            color: kAppBarButtonColor,
          ),
        );
      case BarRightButton.settings:
        return Container(
          child: Icon(
            Icons.settings,
            size: 30,
            color: kAppBarButtonColor,
          ),
        );
      case BarRightButton.edit:
        return Container(
          child: Text(
            "편집",
            style: TextStyle(
              fontSize: 15,
              color: kAppBarButtonColor,
              fontWeight: FontWeight.bold,
            ),
          ),
        );
      case BarRightButton.reset:
        // TODO: Handle this case.
        return Container();
      case BarRightButton.done:
        return Container(
          child: Text(
            "완료",
            style: TextStyle(
              fontSize: 15,
              color: kAppBarButtonColor,
              fontWeight: FontWeight.bold,
            ),
          ),
        );
      default:
        return Container();
    }
  }
}
