
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/view/main/body/home/home_sub_routes.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/my_page_sub_routes.dart';
import 'package:rf_tap_fanseem/providers/navigator_provider.dart';
import 'package:rf_tap_fanseem/providers/sub_view_controller_provider.dart';
import 'dart:io';

class MainBotNavigationBar extends StatelessWidget {
  final PageController pageController;

  const MainBotNavigationBar({Key key, this.pageController}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    MainNavigationProvider _appBarProvider = Provider.of<MainNavigationProvider>(context);
    SubViewControllerProvider _subviewProvider = Provider.of<SubViewControllerProvider>(context);

    void onItemTapped(int index){
      _appBarProvider.navigateRoot(index, context);
      PageController subPageController = _subviewProvider.get(_indexToRoots(index));
      // ignore: invalid_use_of_protected_member, null_aware_in_logical_operator
      if(subPageController != null && subPageController?.positions?.isNotEmpty)
        subPageController.jumpToPage(0);
      this.pageController.jumpToPage(index);
//      this.pageController.animateToPage(index, duration: Duration(milliseconds: 500), curve: Curves.easeOut).then((value) => _appBarProvider.navButtonHit = false);
    }

    return BottomNavigationBar(
      items: [
        BottomNavigationBarItem(
          icon: Icon(FontAwesomeIcons.home),
          title: Container(height: 15,)
        ),
        BottomNavigationBarItem(
          icon: Icon(FontAwesomeIcons.bell),
          title: Container(height: 15,)
        ),
        if(Platform.isAndroid) BottomNavigationBarItem(
          icon: Icon(FontAwesomeIcons.gift),
          title: Container(height: 15,)
        ),
        BottomNavigationBarItem(
          icon: Icon(FontAwesomeIcons.user),
          title: Container(height: 15,)
        )
      ],
      type: BottomNavigationBarType.fixed,
      currentIndex: _appBarProvider.currentIndex,
      iconSize: MediaQuery.of(context).size.width * 0.07,
      onTap: onItemTapped,
      backgroundColor: Theme.of(context).backgroundColor,
      selectedItemColor: Theme.of(context).primaryColor,
      unselectedItemColor: kNavigationPressableColor,
      showSelectedLabels: false,
      showUnselectedLabels: false,
    );
  }
}

String _indexToRoots(int index){
  switch (index){
    case 0:
      return HomeSubRoutes.homeRoot;
    case 3:
      return MyPageSubRoutes.myPageRoot;
    default:
      return "EXPECT_NULL";
  }
}


