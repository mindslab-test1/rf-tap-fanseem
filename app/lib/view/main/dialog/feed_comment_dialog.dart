import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rf_tap_fanseem/http/feed/feed_edit_comment.dart';

import '../../../colors.dart';
import 'package:rf_tap_fanseem/view/feed_detail/feed_view_components.dart';

class FeedCommentDialog extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(26),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width - 28,
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(26),
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.64),
                  blurRadius: 14
              )
            ]
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
//            GestureDetector(
//              onTap: () => Navigator.pop(context, "edit"),
//              behavior: HitTestBehavior.translucent,
//              child: Container(
//                height: 57,
//                child: Center(
//                  child: Text(
//                    "수정",
//                    style: TextStyle(
//                        color: Colors.black,
//                        fontSize: 17,
//                        fontWeight: FontWeight.bold
//                    ),
//                  ),
//                ),
//              ),
//            ),
//            Divider(
//              thickness: 0.5,
//              height: 5,
//            ),
            GestureDetector(
              onTap: () => Navigator.pop(context, "delete"),
              behavior: HitTestBehavior.translucent,
              child: Container(
                height: 57,
                child: Center(
                  child: Text(
                    "삭제",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 17,
                        fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ),
            ),
            Divider(
              thickness: 0.5,
              height: 5,
            ),
            Container(
              height: 57,
              child: GestureDetector(
                onTap: () => Navigator.pop(context),
                behavior: HitTestBehavior.translucent,
                child: Center(
                  child: Text(
                    "닫기",
                    style: TextStyle(
                      fontSize: 17,
                      color: Colors.black,
                    ),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

// TODO
class FeedCommentEditDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(26),
        ),
        child: Container(
          width: MediaQuery.of(context).size.width - 28,
          decoration: BoxDecoration(
              color: Theme.of(context).backgroundColor,
              borderRadius: BorderRadius.circular(26),
              boxShadow: [
                BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.64),
                    blurRadius: 14
                )
              ]
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                height: 57,
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "댓글 수정",
                      style: TextStyle(
                          color: Colors.black,
                          fontSize: 15,
                          fontWeight: FontWeight.bold
                      ),
                    )
                  ],
                ),
              ),
              Divider(
                color: kBorderColor,
                thickness: 0.5,
                height: 0,
              ),

              SizedBox(height: 16),
              // text field
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 31),
                child: Container(
                    width: MediaQuery.of(context).size.width - 88,
                    decoration: BoxDecoration(
                        border: Border.all(color: kBorderColor),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                              color: Color.fromRGBO(0, 0, 0, 0.16),
                              blurRadius: 6,
                              offset: Offset(0, 3)
                          )
                        ]
                    ),
                    child: TextField(
                      decoration: InputDecoration(
                        contentPadding: EdgeInsets.symmetric(horizontal: 20),
                        hintText: "",
                        hintStyle: TextStyle(fontSize: 14.0, color: Color.fromRGBO(112, 112, 112, 1)),
                        border: InputBorder.none,
                      ),
                      cursorColor: Colors.black,
                    ),
                ),
              ),
              Container(
                height: 55,
                decoration: BoxDecoration(
                  border: Border(
                      top: BorderSide(color: Color.fromRGBO(228, 228, 228, 0.64), width: 0.5)
                  ),
                ),
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: GestureDetector(
                        onTap: () => Navigator.pop(context),
                        behavior: HitTestBehavior.translucent,
                        child: Center(
                          child: Text(
                            "취소",
                            style: TextStyle(
                              fontSize: 17,
                              color: Colors.grey,
                            ),
                          ),
                        ),
                      ),
                    ),
                    VerticalDivider(
                      width: 0.5,
                    ),
                    Expanded(
                      flex: 1,
                      child: GestureDetector(
                        onTap: () {
                          Fluttertoast.showToast(msg: "댓글이 수정되었습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                          httpFeedEditComment();
                          Navigator.pop(context);
                        },
                        behavior: HitTestBehavior.translucent,
                        child: Center(
                          child: Text(
                            "완료",
                            style: TextStyle(
                                fontSize: 17,
                                color: Colors.black
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        )
    );
  }
}