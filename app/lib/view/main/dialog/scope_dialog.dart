
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/providers/content_write_data_provider.dart';
import 'package:rf_tap_fanseem/providers/feed_write_data_provider.dart';

class ScopeDialog extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(26),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width - 28,
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(26),
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.64),
                  blurRadius: 14
              )
            ]
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              height: 57,
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "공개범위 설정",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 15,
                        fontWeight: FontWeight.bold
                    ),
                  )
                ],
              ),
            ),
            _MembershipRadios(),
            Container(
              height: 67,
              child: Row(
                children: <Widget>[
//                  Expanded(
//                    flex: 1,
//                    child: GestureDetector(
//                      onTap: () => Navigator.pop(context),
//                      behavior: HitTestBehavior.translucent,
//                      child: Center(
//                        child: Text(
//                          "닫기",
//                          style: TextStyle(
//                            fontSize: 17,
//                            color: Colors.grey,
//                          ),
//                        ),
//                      ),
//                    ),
//                  ),
//                  VerticalDivider(
//                    width: 0.5,
//                  ),
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      // TODO set scope
                      onTap: () => Navigator.pop(context, ),
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "완료",
                          style: TextStyle(
                              fontSize: 17,
                              color: Colors.black
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _MembershipRadios extends StatefulWidget {
  final List<String> memberships;

  const _MembershipRadios({
    Key key,
    this.memberships = const [
      "일반 멤버십",
      "우수 멤버십",
      "프리미엄 멤버십"
    ]
  }) : super(key: key);

  @override
  __MembershipRadiosState createState() => __MembershipRadiosState();
}

class __MembershipRadiosState extends State<_MembershipRadios> {
  String _selected;
  List<String> _membershipList;

  @override
  void initState() {
    super.initState();
    _selected = "전체 공개";
    _membershipList = ["전체 공개"];
    _membershipList.addAll(widget.memberships);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: _membershipList.asMap().entries.map((e) => Container(
          padding: const EdgeInsets.only(left: 38, right: 46),
          height: 58,
          decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            boxShadow: [
              BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.16),
                blurRadius: 6,
                offset: Offset(0, 1),
              )
            ]
          ),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                e.value,
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 13,
                  fontWeight: FontWeight.bold
                ),
              ),
              Radio(
                value: e.value,
                groupValue: _selected,
                onChanged: (value) {
                  Provider.of<ContentWriteDataProvider>(context, listen: false).tier = e.key;
                  Provider.of<FeedWriteDataProvider>(context, listen: false).accessLevel = e.key;
                  setState(() => _selected = value);
                },
              )
            ],
          ),
        )).toList(),
      ),
    );
  }
}

