
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/colors.dart';

class MembershipPrompt extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(26),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width - 28,
//        height: 200,
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(26),
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.64),
                  blurRadius: 14
              )
            ]
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 28),
              child: Center(
                  child: RichText(
                    textAlign: TextAlign.left,
                    text: TextSpan(
                        style: TextStyle(
                            fontSize: 15,
                            color: kSelectableTextColor
                        ),
                        children: [
                          TextSpan(
                            text: "해당 피드는",
                          ),
                          TextSpan(
                            text: " 멤버십 ",
                            style: TextStyle(
                                color: Theme.of(context).primaryColor
                            ),
                          ),
                          TextSpan(
                            text: "전용 콘텐츠 입니다. 멤버십 가입 이 후 즐기실 수 있습니다!",
                          )
                        ]
                    ),
                  )
              ),
            ),
            Container(
              height: 55,
              decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(color: Color.fromRGBO(228, 228, 228, 0.64), width: 0.5)
                ),
              ),
              child: Row(
                children: <Widget>[
//                    Expanded(
//                      flex: 1,
//                      child: GestureDetector(
//                        onTap: () => Navigator.pop(context),
//                        behavior: HitTestBehavior.translucent,
//                        child: Center(
//                          child: Text(
//                            "취소",
//                            style: TextStyle(
//                              fontSize: 17,
//                              color: Colors.black,
//                            ),
//                          ),
//                        ),
//                      ),
//                    ),
//                    VerticalDivider(
//                      width: 0.5,
//                    ),
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () => Navigator.pop(context),
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "확인",
                          style: TextStyle(
                              fontSize: 17,
                              color: kNegativeTextColor
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
