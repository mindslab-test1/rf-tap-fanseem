
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/http/report/report_comment.dart';

import '../../../colors.dart';

class ReportDialog extends StatelessWidget {
  List<bool> checkList = [false,false,false,false,false];
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(26),
        ),
        content: StatefulBuilder(
          builder: (context, setState){
            return SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                scrollDirection: Axis.vertical,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: 57,
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "신고 항목",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 15,
                                fontWeight: FontWeight.bold
                            ),
                          )
                        ],
                      ),
                    ),
                    Divider(
                      color: kBorderColor,
                      thickness: 0.5,
                      height: 0,
                    ),
                    Container(
                      height: 50,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.only(left: 30),
                              child: Text("선정적, 음란성의 내용",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                      fontWeight: FontWeight.normal
                                  )
                              )
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 30),
                            child: ClipRRect(
                              clipBehavior: Clip.hardEdge,
                              borderRadius: BorderRadius.all(Radius.circular(90)),
                              child: SizedBox(
                                width: 27,
                                height: 27,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Theme(
                                      data: ThemeData(
                                        unselectedWidgetColor: Colors.transparent,

                                      ),
                                      child: Transform.scale(
                                        scale: 1.5,
                                        child: Checkbox(
                                          value: checkList[0],
                                          activeColor: Color.fromRGBO(255, 177, 118, 1),
                                          onChanged: (state) {
                                            setState((){
                                              print(setState.toString());
                                              checkList[0] = state;
                                            });
                                          },
                                          materialTapTargetSize: MaterialTapTargetSize.padded,
                                        ),
                                      )
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      color: kBorderColor,
                      thickness: 0.5,
                      height: 0,
                    ),
                    Container(
                      height: 50,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.only(left: 30),
                              child: Text("폭력적, 혐오적 내용",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                      fontWeight: FontWeight.normal
                                  )
                              )
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 30),
                            child: ClipRRect(
                              clipBehavior: Clip.hardEdge,
                              borderRadius: BorderRadius.all(Radius.circular(90)),
                              child: SizedBox(
                                width: 27,
                                height: 27,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Theme(
                                      data: ThemeData(
                                        unselectedWidgetColor: Colors.transparent,
                                      ),
                                      child: Transform.scale(
                                        scale: 1.5,
                                        child: Checkbox(
                                          value: checkList[1],
                                          activeColor: Color.fromRGBO(255, 177, 118, 1),
                                          onChanged: (state) {
                                            setState((){
                                              print(state);
                                              return checkList[1] = state;
                                            });
                                          },
                                          materialTapTargetSize: MaterialTapTargetSize.padded,
                                        ),
                                      )
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      color: kBorderColor,
                      thickness: 0.5,
                      height: 0,
                    ),
                    Container(
                      height: 50,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.only(left: 30),
                              child: Text("인신공격, 비속어, 욕설",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                      fontWeight: FontWeight.normal
                                  )
                              )
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 30),
                            child: ClipRRect(
                              clipBehavior: Clip.hardEdge,
                              borderRadius: BorderRadius.all(Radius.circular(90)),
                              child: SizedBox(
                                width: 27,
                                height: 27,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Theme(
                                      data: ThemeData(
                                        unselectedWidgetColor: Colors.transparent,
                                      ),
                                      child: Transform.scale(
                                        scale: 1.5,
                                        child: Checkbox(
                                          value: checkList[2],
                                          activeColor: Color.fromRGBO(255, 177, 118, 1),
                                          onChanged: (state) {
                                            setState((){
                                              checkList[2] = state;
                                            });
                                          },
                                          materialTapTargetSize: MaterialTapTargetSize.padded,
                                        ),
                                      )
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      color: kBorderColor,
                      thickness: 0.5,
                      height: 0,
                    ),

                    Container(
                      height: 50,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.only(left: 30),
                              child: Text("낚시, 도배 등의 악의적인 글",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                      fontWeight: FontWeight.normal
                                  )
                              )
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 30),
                            child: ClipRRect(
                              clipBehavior: Clip.hardEdge,
                              borderRadius: BorderRadius.all(Radius.circular(90)),
                              child: SizedBox(
                                width: 27,
                                height: 27,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Theme(
                                      data: ThemeData(
                                        unselectedWidgetColor: Colors.transparent,
                                      ),
                                      child: Transform.scale(
                                        scale: 1.5,
                                        child: Checkbox(
                                          value: checkList[3],
                                          activeColor: Color.fromRGBO(255, 177, 118, 1),
                                          onChanged: (state) {
                                            setState((){
                                              checkList[3] = state;
                                            });
                                          },
                                          materialTapTargetSize: MaterialTapTargetSize.padded,
                                        ),
                                      )
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Divider(
                      color: kBorderColor,
                      thickness: 0.5,
                      height: 0,
                    ),
                    Container(
                      height: 50,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                              padding: EdgeInsets.only(left: 30),
                              child: Text("기타",
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 12,
                                      fontWeight: FontWeight.normal
                                  )
                              )
                          ),
                          Padding(
                            padding: EdgeInsets.only(right: 30),
                            child: ClipRRect(
                              clipBehavior: Clip.hardEdge,
                              borderRadius: BorderRadius.all(Radius.circular(90)),
                              child: SizedBox(
                                width: 27,
                                height: 27,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 1,
                                    ),
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  child: Theme(
                                      data: ThemeData(
                                        unselectedWidgetColor: Colors.transparent,
                                      ),
                                      child: Transform.scale(
                                        scale: 1.5,
                                        child: Checkbox(
                                          value: checkList[4],
                                          activeColor: Color.fromRGBO(255, 177, 118, 1),
                                          onChanged: (state) {
                                            setState((){
                                              print("123123123123123");
                                              checkList[4] = state;
                                            });
                                          },
                                          materialTapTargetSize: MaterialTapTargetSize.padded,
                                        ),
                                      )
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Container(
                      height: 57,
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            "신고 사유",
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 15,
                                fontWeight: FontWeight.bold
                            ),
                          )
                        ],
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 31),
                      child: Container(
                        height: 70,
                        width: MediaQuery.of(context).size.width - 88,
                        decoration: BoxDecoration(
                            border: Border.all(color: kBorderColor),
                            color: Colors.white,
                            boxShadow: [
                              BoxShadow(
                                  color: Color.fromRGBO(0, 0, 0, 0.16),
                                  blurRadius: 6,
                                  offset: Offset(0, 3)
                              )
                            ]
                        ),
                        child: TextField(
                          maxLines: 2,
                          decoration: InputDecoration(
                            contentPadding: EdgeInsets.symmetric(horizontal: 20),
                            hintText: "신고 사유를 입력해주세요!",
                            hintStyle: TextStyle(fontSize: 14.0, color: Color.fromRGBO(112, 112, 112, 1)),
                            border: InputBorder.none,
                          ),
                          cursorColor: Colors.black,
                        ),
                      ),
                    ),

                    SizedBox(height: 16),

                    Container(
                      height: 55,
                      decoration: BoxDecoration(
                        border: Border(
                            top: BorderSide(color: Color.fromRGBO(228, 228, 228, 0.64), width: 0.5)
                        ),
                      ),
                      child: Row(
                        children: <Widget>[
                          Expanded(
                            flex: 1,
                            child: GestureDetector(
                              onTap: () => Navigator.pop(context),
                              behavior: HitTestBehavior.translucent,
                              child: Center(
                                child: Text(
                                  "닫기",
                                  style: TextStyle(
                                    fontSize: 17,
                                    color: Colors.black,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          VerticalDivider(
                            width: 0.5,
                          ),
                          Expanded(
                            flex: 1,
                            child: GestureDetector(
                              onTap: () => httpReportComment(context : context, whomId: null, commentId: null, reason: null),
                              behavior: HitTestBehavior.translucent,
                              child: Center(
                                child: Text(
                                  "제출",
                                  style: TextStyle(
                                      fontSize: 17,
                                      color: kNegativeTextColor
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                )
            );
          })
        );
    }
}
