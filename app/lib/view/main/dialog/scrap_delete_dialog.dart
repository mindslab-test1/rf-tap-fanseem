
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/http/scrap/scrap_delete.dart';
import 'package:rf_tap_fanseem/providers/function_injection.dart';

import '../../../http/mypage/mypage_view.dart';

class DeleteScrapDialog extends StatelessWidget {
  final int boardId;
  final String boardName;

  const DeleteScrapDialog({Key key, this.boardId, this.boardName}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(26),
      ),
      child: Container(
        width: MediaQuery.of(context).size.width - 28,
        decoration: BoxDecoration(
            color: Theme.of(context).backgroundColor,
            borderRadius: BorderRadius.circular(26),
            boxShadow: [
              BoxShadow(
                  color: Color.fromRGBO(0, 0, 0, 0.64),
                  blurRadius: 14
              )
            ]
        ),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 28),
              child: Center(
                  child: RichText(
                    textAlign: TextAlign.left,
                    maxLines: 5,
                    overflow: TextOverflow.ellipsis,
                    text: TextSpan(
                        style: TextStyle(
                            fontSize: 15,
                            color: kSelectableTextColor
                        ),
                        children: [
                          TextSpan(
                            text: "#$boardName",
                            style: TextStyle(
                                color: Theme.of(context).primaryColor
                            ),
                          ),
                          TextSpan(
                            text: "을(를) 삭제하시겠습니까? 삭제 시 해당 스크랩 보드의 내용은 복구가 불가능 합니다.",
                          )
                        ]
                    ),
                  )
              ),
            ),
            Container(
              height: 55,
              decoration: BoxDecoration(
                border: Border(
                    top: BorderSide(color: Color.fromRGBO(228, 228, 228, 0.64), width: 0.5)
                ),
              ),
              child: Row(
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () => Navigator.pop(context),
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "취소",
                          style: TextStyle(
                            fontSize: 17,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                    ),
                  ),
                  VerticalDivider(
                    width: 0.5,
                  ),
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () async {
                        await httpScrapDeleteBoard(context: context, boardId: boardId);
                        Navigator.pop(context);
                        httpMyPageScrapBoardList(context: context);
                      },
                      behavior: HitTestBehavior.translucent,
                      child: Center(
                        child: Text(
                          "삭제",
                          style: TextStyle(
                              fontSize: 17,
                              color: kNegativeTextColor
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
