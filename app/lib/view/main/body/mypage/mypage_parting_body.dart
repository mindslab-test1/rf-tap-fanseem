

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/providers/login_provider.dart';
import 'package:rf_tap_fanseem/view/main/body/require_login_body.dart';

import '../mypage_celeb/my_page_celeb_body.dart';
import 'my_page_body.dart';

class MyPagePartingBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    bool isLogin = Provider.of<LoginVerifyProvider>(context).isLogin;
    bool isCeleb = Provider.of<LoginVerifyProvider>(context).isCeleb;
    if(isLogin == null ? true : !isLogin) return RequireLoginBody();
    else if(isCeleb == null ? true : !isCeleb) return MyPageBody();
    else return MyPageCelebBody();
  }
}
