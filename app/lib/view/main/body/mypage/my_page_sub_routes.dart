class MyPageSubRoutes {
  static const String myPageRoot = "/myPage/root";
  static const String myPageSettings = "/myPage/settings";
  static const String myPageScrapBoard = "/myPage/scrapBoard";
}