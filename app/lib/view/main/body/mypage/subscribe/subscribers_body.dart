

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/http/mypage/mypage_view.dart';
import 'package:rf_tap_fanseem/providers/mypage_view_provider.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/my_page_body.dart';
import 'package:rf_tap_fanseem/view/main/component/category_component.dart';
import 'package:rf_tap_fanseem/view/main/component/partial_components.dart';
import 'package:rf_tap_fanseem/providers/function_injection.dart';
import 'package:rf_tap_fanseem/providers/navigator_provider.dart';
import 'package:rf_tap_fanseem/providers/sub_view_controller_provider.dart';

import '../my_page_sub_routes.dart';

class MySubScribes extends StatefulWidget {
  @override
  _MySubScribesState createState() => _MySubScribesState();
}

class _MySubScribesState extends State<MySubScribes> {
  Future<bool> _getData() async {
    return await httpMyPageCelebList(context: context);
  }

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((e) =>
        Provider.of<MainNavigationProvider>(context, listen: false).setAll(
            title: "셀럽 리스트",
            leftButton: BarLeftButton.back,
            rightButton: BarRightButton.none,
            leftFunction: myPageLeftBackButton(context),
            rightFunction: dummyFunction
        )
    );

    BackButtonInterceptor.add(myPageRootBackInterceptor(context));
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _getData(),
      builder: (context, snapshot) {
        if(!snapshot.hasData) return Container(
          child: Center(
            child: CircularProgressIndicator(),
          ),
        );

        return Container(
          child: Provider.of<MyPageViewProvider>(context).celebViewData.isEmpty ? Container(child: Center(child: Text("나만의 셀럽을 찾아 구독해보세요!"),),) : SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                HeaderAndBodyComponent(
                  headerTitle: "구독중",
                  child: Container(
                    decoration: BoxDecoration(
                        color: Theme.of(context).backgroundColor,
                        boxShadow: [
                          BoxShadow(
                            offset: Offset(0, 2),
                            color: Color.fromRGBO(0, 0, 0, 0.09),
                            blurRadius: 8,
                          )
                        ]
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: Provider.of<MyPageViewProvider>(context).celebViewData.map((e) => UserListItem(
                        userName: e.name, imgUrl: e.profileImgUrl, badgeUrl: e.badgeUrl,
                      )).toList(),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }
    );
  }

  @override
  void dispose() {
    BackButtonInterceptor.removeAll();
    super.dispose();
  }
}