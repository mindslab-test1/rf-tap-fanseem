
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/my_page_body.dart';
import 'package:rf_tap_fanseem/view/main/component/partial_components.dart';
import 'package:rf_tap_fanseem/providers/function_injection.dart';
import 'package:rf_tap_fanseem/providers/navigator_provider.dart';

class MyFollowersBody extends StatefulWidget {

  const MyFollowersBody({Key key,})
      : super(key: key);

  @override
  _MyFollowersBodyState createState() => _MyFollowersBodyState();
}

class _MyFollowersBodyState extends State<MyFollowersBody> {
  List<UserAvatarData> _followers;

  @override
  void initState() {
    super.initState();
    // TODO get followers
    _followers = [];
    _followers = [
      UserAvatarData(userName: "윤도현"),
      UserAvatarData(userName: "윤도현"),
      UserAvatarData(userName: "윤도현"),
      UserAvatarData(userName: "윤도현"),
      UserAvatarData(userName: "윤도현"),
    ];

    BackButtonInterceptor.add(myPageRootBackInterceptor(context));
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((e) =>
        Provider.of<MainNavigationProvider>(context, listen: false).setAll(
            title: "팔로워 목록",
            leftButton: BarLeftButton.back,
            rightButton: BarRightButton.none,
            leftFunction: myPageLeftBackButton(context),
            rightFunction: dummyFunction
        )
    );

    return this._followers.isEmpty ?
    Container(
      child: Center(
        child: Text("현재 팔로워가 없습니다..."),
      ),
    ):
    Container(
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        boxShadow: [
          BoxShadow(
            offset: Offset(0, 2),
            color: Color.fromRGBO(0, 0, 0, 0.09),
            blurRadius: 8,
          )
        ]
      ),
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: this._followers.map((e) => UserListItem.buildWith(e)).toList(),
        ),
      ),
    );
  }

  @override
  void dispose() {
    BackButtonInterceptor.removeAll();
    super.dispose();
  }
}
