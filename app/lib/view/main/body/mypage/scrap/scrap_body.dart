

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/http/mypage/mypage_view.dart';
import 'package:rf_tap_fanseem/providers/mypage_view_provider.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/component/scrap_board_component.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/my_page_body.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/scrap/scrap_root.dart';
import 'package:rf_tap_fanseem/providers/sub_view_controller_provider.dart';

import '../my_page_sub_routes.dart';

class MyScrapBoardBody extends StatefulWidget {
  @override
  _MyScrapBoardBodyState createState() => _MyScrapBoardBodyState();
}

class _MyScrapBoardBodyState extends State<MyScrapBoardBody> {
  PageController _pageController;

  @override
  void initState(){
    super.initState();
    _pageController = PageController(
        initialPage: 0,
        keepPage: false
    );

    BackButtonInterceptor.add(myPageRootBackInterceptor(context));

    BackButtonInterceptor.add((pop) {
      if(pop) return false;
      // ignore: invalid_use_of_protected_member
      if(_pageController.positions.isEmpty) return false;
      else if(_pageController.page > 0.0){
        _pageController.animateToPage(_pageController.page.ceil() - 1, duration: Duration(milliseconds: 300), curve: Curves.easeOut);
        return true;
      } else return false;
    });
  }

  Future<bool> _setData() async{
    return await httpMyPageScrapBoardList(context: context);
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<SubViewControllerProvider>(context, listen: false)
          .add(MyPageSubRoutes.myPageScrapBoard, _pageController);
    });
//    TODO send to root
//    WidgetsBinding.instance.addPostFrameCallback((e) =>
//        Provider.of<MainNavigationProvider>(context, listen: false).setAll(
//            title: "나의 스크랩",
//            leftButton: BarLeftButton.back,
//            rightButton: BarRightButton.edit,
//            leftFunction: myPageLeftBackButton(context),
//            rightFunction: dummyFunction
//        )
//    );

    return
//      PageView(
//      controller: _pageController,
//      physics: NeverScrollableScrollPhysics(),
//      children: <Widget>[
        FutureBuilder(
          future: _setData(),
          builder: (context, snapshot) {
            if(!snapshot.hasData)
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            else if(snapshot.hasData && snapshot.data)
              return ScrapRootView(
                dataList: Provider.of<MyPageViewProvider>(context).boardViewData,
              );
            return Container(
              child: Center(
                child: Text(
                  "오류가 발생하여 보드를 불러오지 못했습니다..."
                ),
              ),
            );
          }
        );
//      ],
//    );
  }

  @override
  void dispose() {
    BackButtonInterceptor.removeAll();
    super.dispose();
  }
}
