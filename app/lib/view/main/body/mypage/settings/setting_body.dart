
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/my_page_body.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/my_page_sub_routes.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/settings/setting_detail.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/settings/setting_root.dart';
import 'package:rf_tap_fanseem/providers/sub_view_controller_provider.dart';

class SettingBody extends StatefulWidget {

  SettingBody({Key key}) : super(key: key);

  @override
  _SettingBodyState createState() => _SettingBodyState();
}

class _SettingBodyState extends State<SettingBody> {
  PageController _pageController;

  @override
  void initState(){
    super.initState();
    _pageController = PageController(
        initialPage: 0,
        keepPage: false
    );

    BackButtonInterceptor.add(myPageRootBackInterceptor(context), ifNotYetIntercepted: true);

    BackButtonInterceptor.add((pop) {
      // ignore: invalid_use_of_protected_member
      if(_pageController.positions.isEmpty) return false;
      else if(_pageController.page > 0.0){
        _pageController.animateToPage(_pageController.page.ceil() - 1, duration: Duration(milliseconds: 300), curve: Curves.easeOut);
        return true;
      } else return false;
    }, ifNotYetIntercepted: true);
  }

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      Provider.of<SubViewControllerProvider>(context, listen: false)
          .add(MyPageSubRoutes.myPageSettings, _pageController);
    });

    return PageView(
      controller: _pageController,
      physics: NeverScrollableScrollPhysics(),
      children: <Widget>[
        SettingRootView(),
        SettingDetailView(),
      ],
    );
  }

  @override
  void dispose() {
    BackButtonInterceptor.removeAll();
    super.dispose();
  }
}
