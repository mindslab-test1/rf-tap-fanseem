
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/my_page_sub_routes.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage/settings/setting_utlis.dart';
import 'package:rf_tap_fanseem/providers/function_injection.dart';
import 'package:rf_tap_fanseem/providers/navigator_provider.dart';
import 'package:rf_tap_fanseem/providers/setting_page_provider.dart';
import 'package:rf_tap_fanseem/providers/sub_view_controller_provider.dart';

class SettingDetailView extends StatelessWidget {

  const SettingDetailView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SettingPageProvider pageProvider = Provider.of<SettingPageProvider>(context);

    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<MainNavigationProvider>(context, listen: false).setAll(
        title: getDetailSettingName(pageProvider.settings, pageProvider.index),
        leftButton: BarLeftButton.back,
        rightButton: BarRightButton.none,
        leftFunction:
            () => Provider.of<SubViewControllerProvider>(context, listen: false)
            .get(MyPageSubRoutes.myPageSettings)
            .animateToPage(0, duration: Duration(milliseconds: 300), curve: Curves.easeOut),
        rightFunction: dummyFunction
      );
    });

    return Container(
      child: Center(
        child: Text("${pageProvider.settings}, ${pageProvider.index}"),
      ),
    );
  }
}
