import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/view/main/component/feed_component.dart';
import 'package:rf_tap_fanseem/view/main/dialog/category_edit_dialog.dart';

class FeedTabView extends StatefulWidget {
  final String pageKey;
  final bool isUserFeed;
  final bool isContent;
  final bool withEditButton;

  final List<FeedData> feedData;
  final List<ContentFeedData> contentFeedData;
  final Function getMoreFeeds;
  final Function refreshFeeds;

  final List<Widget> categoryChildren;

  const FeedTabView({
    Key key,
    this.pageKey,
    this.isUserFeed = false,
    this.isContent = false,
    this.withEditButton = false,
    this.getMoreFeeds,
    this.refreshFeeds,
    this.contentFeedData,
    this.feedData,
    this.categoryChildren
  }) : assert(!isContent || (isContent && !isUserFeed)), super(key: key);

  @override
  _FeedTabViewState createState() => _FeedTabViewState();
}

class _FeedTabViewState extends State<FeedTabView> {
//    with AutomaticKeepAliveClientMixin  {
  bool _isUserFeed;
  bool _isContent;
  List<FeedData> _feedData;
  List<ContentFeedData> _contentFeedData;
  List<Widget> _categoryChildren = [];
  ScrollController _scrollController = ScrollController();
  bool _isRefreshLoading = false;
  bool _isMoreLoading = false;

  Function _getMoreFeeds;
  Function _refreshFeeds;

  @override
  void initState() {
    super.initState();
    this._isUserFeed = widget.isUserFeed;
    this._isContent = widget.isContent;
    this._categoryChildren = widget.categoryChildren;
    this._feedData = widget.feedData;
    this._contentFeedData = widget.contentFeedData;
    this._getMoreFeeds = widget.getMoreFeeds;
    this._refreshFeeds = widget.refreshFeeds;
    _scrollController.addListener(() {
      if (!_isMoreLoading && _scrollController.position.pixels >= _scrollController.position.maxScrollExtent + 10) {
        _getMoreFeeds(_isMoreLoading, (bool flag) { setState(() { _isMoreLoading = flag; });});
      }
      else if (!_isRefreshLoading && _scrollController.position.pixels <= -10) {
        _refreshFeeds(_isRefreshLoading, (bool flag) { setState(() { _isRefreshLoading = flag; });});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    double totalLength = this._categoryChildren.length * 55.0 +
        (this._categoryChildren.length - 1) * 10 +
        (widget.withEditButton ? 76.0 : 0.0);
    return Container(
      width: double.infinity,
      child: SingleChildScrollView(
        controller: _scrollController,
        physics: BouncingScrollPhysics(),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            _isRefreshLoading ? Padding(
                padding: EdgeInsets.only(top: 30, bottom: 10),
                child: Center(child: CircularProgressIndicator(),)
            ) : Container(),
            Container(
              alignment: Alignment.centerRight,
              width: double.infinity,
              height: 67,
              padding: const EdgeInsets.symmetric(horizontal: 26, vertical: 20),
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                physics: totalLength > MediaQuery.of(context).size.width - 60
                    ? BouncingScrollPhysics()
                    : NeverScrollableScrollPhysics(),
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: _categoryChildren,
                ),
              ),
            ),
            this._isContent
                ? ContentFeedListView(
                    feedData: _contentFeedData,
                  )
                : FeedListView(
                    isMoreLoading: _isMoreLoading,
                    isRefreshLoading: _isRefreshLoading,
                    feedData: _feedData,
                    isUserFeed: _isUserFeed,
                  ),
            _isMoreLoading ? Padding(
                padding: EdgeInsets.only(top: 10, bottom: 30),
                child: Center(child: CircularProgressIndicator(),)
            ) : Container(),
          ],
        ),
      ),
    );
  }

//  @override
//  bool get wantKeepAlive => true;
}
