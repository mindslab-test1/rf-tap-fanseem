

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/util/timestamp.dart';

import '../../../../../colors.dart';

class NoticeData {
  final String celebName;
  final bool fromCeleb;
  final int count;
  final String timestamp;
  final String text;
  final NoticeType noticeType;
  final bool checked;

  const NoticeData({
    this.celebName,
    this.fromCeleb = false,
    this.count,
    this.timestamp,
    this.text,
    this.noticeType,
    this.checked = false,
  });
}

class NoticeElement extends StatelessWidget {
  final String celebName;
  final bool fromCeleb;
  final int count;
  final String timestamp;
  final String text;
  final NoticeType noticeType;
  final bool checked;

  const NoticeElement({
    Key key,
    this.celebName,
    this.fromCeleb,
    this.count,
    this.timestamp,
    this.text,
    this.noticeType,
    this.checked
  }) : super(key: key);

  static NoticeElement buildWith(NoticeData data){
    return NoticeElement(
      celebName: data.celebName,
      fromCeleb: data.fromCeleb,
      count: data.count,
      timestamp: data.timestamp,
      text: data.text,
      noticeType: data.noticeType,
      checked: data.checked,
    );
  }

  Widget _noticeIcon(){
    switch(noticeType){
      case NoticeType.comment:
        return Icon(
          Icons.comment,
          color: kFunctionButtonColor,
          size: 17.5,
        );
      case NoticeType.like:
        return Image.asset(
          "assets/icon/like_on_3x.png",
          width: 17,
          fit: BoxFit.fitWidth,
        );
      case NoticeType.scrap:
        return Image.asset(
          "assets/icon/seem_on_3x.png",
          width: 22.9,
          fit: BoxFit.fitWidth,
        );
      default:
        return Icon(
          FontAwesomeIcons.solidStar,
          color: kFunctionButtonColor,
          size: 18,
        );
    }
  }

  String _noticeSubject(){
    switch (noticeType){
      case NoticeType.comment:
        return "새 댓글이 달렸습니다.";
      case NoticeType.like:
        return "피드에 좋아요가 달렸습니다..";
      case NoticeType.scrap:
        return "내 피드가 스크랩 되었습니다.";
      case NoticeType.celebComment:
        return "셀럽이 뎃글을 남겼습니다!";
      case NoticeType.celebLike:
        return "셀럽이 좋아요를 남겼습니다!";
      case NoticeType.celebScrap:
        return "셀럽이 내 피드를 스크랩 했습니다!";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
      height: 73,
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.1),
            blurRadius: 4,
            offset: Offset(0, 2)
          )
        ]
      ),
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    celebName,
                    style: TextStyle(
                      color: this.checked ? kUnSelectedColor : Colors.black,
                      fontSize: 13,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    _noticeSubject(),
                    style: TextStyle(
                      color: this.checked ? kUnSelectedColor : (fromCeleb ? Theme.of(context).primaryColor : Colors.black),
                      fontSize: 12,
                    ),
                  ),
                  SizedBox(
                    width: 3,
                  ),
                  fromCeleb ? Container() : Text(
                    "(+$count)",
                    style: TextStyle(
                      color: this.checked ? kUnSelectedColor : Colors.black,
                      fontSize: 12,
                    ),
                  )
                ],
              ),
              // TODO refine timestamp
              Text(
                timestamp,
                style: TextStyle(
                  color: kNavigationPressableColor,
                  fontSize: 12
                ),
              )
            ],
          ),
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                text.length > 30 ? text.substring(0, 26) + "..." : text,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(
                  color: checked ? kUnSelectedColor : kSelectableTextColor,
                  fontSize: 13,
                ),
              ),
              _noticeIcon()
            ],
          )
        ],
      ),
    );
  }
}

enum NoticeType {
  comment,
  like,
  scrap,
  celebComment,
  celebLike,
  celebScrap
}

class MessageData {
  final int celebId;
  final String userName;
  final String profileImg;
  final String text;
  final String timestamp;
  final int count;

  MessageData({
    this.celebId,
    this.userName,
    this.profileImg,
    this.text,
    this.timestamp,
    this.count
  });
}

class MessageElement extends StatelessWidget {
  final int celebId;
  final String userName;
  final String profileImg;
  final String text;
  final String timestamp;
  final int count;

  const MessageElement({
    Key key,
    this.celebId,
    this.userName,
    this.profileImg,
    this.text,
    this.timestamp,
    this.count
  }) : super(key: key);

  static MessageElement buildWith(MessageData data){
    return MessageElement(
      celebId: data.celebId,
      userName: data.userName,
      profileImg: data.profileImg,
      text: data.text,
      timestamp: data.timestamp,
      count: data.count,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 30, vertical: 15),
      height: 88,
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.16),
            blurRadius: 6,
            offset: Offset(0, 2),
          )
        ]
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          CircleAvatar(
            radius: 29,
            backgroundColor: kMainColor,
            backgroundImage: profileImg == null ? AssetImage("assets/icon/seem_off_3x.png",)
              : NetworkImage(profileImg,),
          ),
          SizedBox(width: 15,),
          Expanded(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  userName,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Text(
                  text.length > 22 ? text.substring(0, 18) + "..." : text,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(
                    color: kSelectableTextColor,
                    fontSize: 13,
                  ),
                )
              ],
            ),
          ),
          Container(
            width: 70,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                // TODO refine timestamp
                Text(
                  formattedTime(timestamp),
                  style: TextStyle(
                    color: kNavigationPressableColor,
                    fontSize: 12
                  ),
                ),
                SizedBox(
                  height: 8.5,
                ),
//                Container(
//                  width: 20,
//                  height: 20,
//                  decoration: BoxDecoration(
//                    color: Theme.of(context).primaryColor,
//                    shape: BoxShape.circle,
//                  ),
//                  child: Center(
//                    child: Text(
//                      "$count",
//                      style: TextStyle(
//                        color: Theme.of(context).backgroundColor,
//                        fontSize: 10,
//                        fontWeight: FontWeight.bold
//                      ),
//                    ),
//                  ),
//                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
