import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/providers/sub_view_controller_provider.dart';
import 'my_page_celeb_sub_routes.dart';
import '../mypage_celeb/root/mypage_celeb_root_body.dart';

class MyPageCelebBody extends StatelessWidget {
  final PageController _pageController2 = PageController(
      keepPage: false
  );

  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      Provider.of<SubViewControllerProvider>(context, listen: false).add(
          MyPageCelebSubRoutes.myPageCelebRoot, _pageController2
      );
    });
    return PageView.builder(
      controller: _pageController2,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index) {
        switch (MyPageCelebSubPages.values.elementAt(index)) {
          case MyPageCelebSubPages.root:
            return MyPageCelebRootBody();
          default:
            throw UnsupportedError("unsupported view page!");
        }
      },
      itemCount: MyPageCelebSubPages.values.length,
    );
  }
}

enum MyPageCelebSubPages{
  root
}