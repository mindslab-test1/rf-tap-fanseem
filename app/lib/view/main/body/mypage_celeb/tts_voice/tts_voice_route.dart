import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage_celeb/tts_voice/tts_voice_body.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:fluttertoast/fluttertoast.dart';

class  TtsCelebAuthRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0.0,
        automaticallyImplyLeading: false,
        title: _TtsServiceAppbar(),
      ),
      body: TtsVoiceBody(),
    );
  }
}

class _TtsServiceAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 30,
                  ),
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                        child: Container(
                          child: Icon(
                            Icons.arrow_back_ios,
                            size: 30,
                            color: kAppBarButtonColor,
                          ),
                        )
                    ),
                  ),
                ]
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Text(
                "TTS 서비스",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: kAppBarButtonColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
//                  GestureDetector(
//                    onTap: () => Fluttertoast.showToast(msg: "준비중입니다..."),
//                    behavior: HitTestBehavior.translucent,
//                    child: Text(
//                      "편집",
//                      style: TextStyle(
//                        fontSize: 15,
//                        color: kAppBarButtonColor,
//                        fontWeight: FontWeight.bold,
//                      ),
//                    ),
//                  ),
//                  SizedBox(width: 30)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
