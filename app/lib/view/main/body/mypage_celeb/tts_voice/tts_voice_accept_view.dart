import 'dart:async';
import 'dart:math';
import 'dart:developer' as developer;
import 'package:audioplayers/audioplayers.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/providers/celeb_tts_provider.dart';
import 'package:rf_tap_fanseem/util/timestamp.dart';
import 'package:provider/provider.dart';

import '../../../../../routes.dart';


class TtsAcceptedList extends StatelessWidget {

  final List<AcceptedCelebTtsData> acceptedData;

  const TtsAcceptedList({
    Key key,
    this.acceptedData = const []
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
      ),
      child: Provider.of<CelebTtsProvider>(context).accepted.isEmpty ? Container(
        child: Center(
          child: Text("승인한 TTS가 없습니다."),
        ),
      ) : Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: Provider.of<CelebTtsProvider>(context).accepted.map((e) => _AcceptTtsListItem(
                  tts: e.tts,
                  subscriberName: e.subscriberName,
                  text: e.text,
                  timestamp: formattedTime(e.timestamp),
                  ttsUrl: e.ttsUrl,
                )).toList(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _AcceptTtsListItem extends StatefulWidget {
  final int tts;
  final String subscriberName;
  final String text;
  final String timestamp;
  final String ttsUrl;

  const _AcceptTtsListItem({
    Key key,
    this.tts,
    this.subscriberName = "",
    this.text = "",
    this.timestamp = "",
    this.ttsUrl
  }) : super(key: key);


  @override
  __TtsAcceptListState createState() => __TtsAcceptListState();
}

class __TtsAcceptListState extends State<_AcceptTtsListItem> {

  bool _expanded = false;

  bool _isAudioPlaying = false;
  AudioPlayer _audioPlayer;
  StreamSubscription _playerCompletionSubscription;
  StreamSubscription _playerErrorSubscription;

  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add((stopDefaultButtonEvent) {
      Navigator.popUntil(context,ModalRoute.withName(kRouteHome));
      return true;
    }, zIndex: 11, ifNotYetIntercepted: true);
    _audioPlayer = AudioPlayer(mode: PlayerMode.MEDIA_PLAYER);
    this._playerCompletionSubscription = this._audioPlayer.onPlayerCompletion.listen((event) {
      setState(() => _isAudioPlaying = false);
    });
    this._playerErrorSubscription = this._audioPlayer.onPlayerError.listen((event) {
      developer.log(event);
      Fluttertoast.showToast(msg: "TTS 듣기 중 문제가 발생했습니다...", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
      setState(() => _isAudioPlaying = false);
    });
    _initData();
  }

  void _initData() async {
    await Future.delayed(Duration(milliseconds: 500));
  }

  @override
  void dispose() {
    this._audioPlayer?.dispose();
    this._playerCompletionSubscription?.cancel();
    this._playerErrorSubscription?.cancel();
    super.dispose();
  }

  void _play(String url) async {
    await this._audioPlayer.play(url);
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 13),
      height: !_expanded ? 74 : 238,
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          boxShadow: [
            BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.1),
                blurRadius: 4,
                offset: Offset(0, 2)
            )
          ]
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width / 2 - 30,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          widget.subscriberName,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 13,
                          ),
                        ),
                        SizedBox(width: 11,),
                        Container(
                          width: 40,
                          child: Text(
                            widget.timestamp,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: kNavigationPressableColor
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 6,),
                    Text(
                      _expanded ? "" : widget.text,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: kSelectableTextColor,
                        fontSize: 13,
                      ),
                    )
                  ],
                ),
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  GestureDetector(
                    onTap: () => setState(() => _expanded = !_expanded),
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                        alignment: Alignment.centerRight,
                        width: 40,
                        height: 40,
                        child: Transform.rotate(
                          angle: 90 * pi/180,
                          child: Icon(
                            _expanded ? Icons.arrow_back_ios : Icons.arrow_forward_ios,
                            color: kBorderColor,
                            size: 25,
                          ),
                        )
                    ),
                  )
                ],
              )
            ],
          ),
          _expanded ? Container(
//            padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 32),
            height: 163,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Expanded(
                  child: SingleChildScrollView(
                    child: Text(
                      widget.text,
                      style: TextStyle(
                          fontSize: 13
                      ),
                    ),
                  ),
                ),
                Container(
                  height: 31.6,
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Container(
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(width: 5,),
                            IconButton(
                              onPressed: () async {
                                this._isAudioPlaying = true;
                                this._play(widget.ttsUrl);
                                Fluttertoast.showToast(msg: "다음이 재생됩니다: ${widget.text}", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                              },
                              icon: Icon(FontAwesomeIcons.play),
                              iconSize: 17,
                              color: Color.fromRGBO(96, 96, 96, 1),
                            )
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
//                            GestureDetector(
//                              onTap: () {
//
//                              },
//                              behavior: HitTestBehavior.translucent,
//                              child: Container(
//                                width: 38.4,
//                                height: 31.6,
//                                decoration: BoxDecoration(
//                                    color: kMainBackgroundColor,
//                                    borderRadius: BorderRadius.circular(21),
//                                    boxShadow: [
//                                      BoxShadow(
//                                        color: Color.fromRGBO(0, 0, 0, 0.16),
//                                        blurRadius: 6,
//                                        offset: const Offset(0, 3),
//                                      )
//                                    ]
//                                ),
//                                child: Center(
//                                  child: Text(
//                                    "삭제",
//                                    style: TextStyle(
//                                      fontSize: 13,
//                                      color: kNavigationPressableColor,
//                                    ),
//                                  ),
//                                ),
//                              ),
//                            )
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ) : Container()
        ],
      ),
    );
  }
}

class AcceptedCelebTtsData {
  final int tts;
  final String subscriberName;
  final String text;
  final String timestamp;
  final String ttsUrl;

  const AcceptedCelebTtsData({
    this.tts,
    this.subscriberName,
    this.text,
    this.timestamp,
    this.ttsUrl
  });
}