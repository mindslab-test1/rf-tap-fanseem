import 'dart:async';
import 'dart:developer' as developer;
import 'package:audioplayers/audioplayers.dart';
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/http/celeb_my_page/celeb_my_page_tts_approve.dart';
import 'package:rf_tap_fanseem/http/celeb_my_page/celeb_my_page_tts_decline.dart';
import 'package:rf_tap_fanseem/providers/celeb_tts_provider.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'dart:math';

import 'package:rf_tap_fanseem/util/timestamp.dart';
import 'package:rf_tap_fanseem/view/main/body/mypage_celeb/tts_voice/tts_confirm_dialog.dart';


class TtsRequestedList extends StatefulWidget {

  final List<RequestedCelebTtsData> requestData;

  const TtsRequestedList({
    Key key,
    this.requestData = const []
  }) : super(key: key);

  @override
  _TtsRequestedListState createState() => _TtsRequestedListState();
}

class _TtsRequestedListState extends State<TtsRequestedList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Theme.of(context).backgroundColor,
      ),
      child: Provider.of<CelebTtsProvider>(context).requested.isEmpty ? Container(
        child: Center(
          child: Text("요청 받은 TTS가 없습니다."),
        ),
      ) : Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
//          Container(
//            padding: const EdgeInsets.symmetric(horizontal: 30),
//            height: 63,
//            child: Row(
//              mainAxisAlignment: MainAxisAlignment.end,
//              children: <Widget>[
//                // TODO filter
//                Container(
//                  width: 97,
//                  height: 27,
//                  decoration: BoxDecoration(
//                    color: Theme.of(context).backgroundColor,
//                    borderRadius: BorderRadius.circular(14),
//                    border: Border.all(
//                        color: kUnSelectedColor,
//                        width: 1
//                    ),
//                  ),
//                  child: Center(
//                    child: Row(
//                      mainAxisSize: MainAxisSize.min,
//                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                      children: <Widget>[
//                        Text(
//                          "최신순",
//                          style: TextStyle(
//                              color: kSelectableTextColor,
//                              fontSize: 12
//                          ),
//                        ),
//                        SizedBox(
//                          width: 7.2,
//                        ),
//                        Image(
//                          image: AssetImage("assets/button/down_button.png"),
//                          fit: BoxFit.fitWidth,
//                          width: 8.9,
//                          height: 5.3,
//                          color: kUnSelectedColor,
//                          colorBlendMode: BlendMode.dst,
//                        )
//                      ],
//                    ),
//                  ),
//                )
//              ],
//            ),
//          ),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: Provider.of<CelebTtsProvider>(context).requested.map((e) => _RequestTtsListItem(
                  ttsId: e.tts,
                  timestamp: formattedTime(e.timestamp),
                  subscriberName: e.subscriberName,
                  text: e.text,
                  ttsUrl: e.ttsUrl,
                )).toList(),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class _RequestTtsListItem extends StatefulWidget {
  final int ttsId;
  final String subscriberName;
  final String text;
  final String timestamp;
  final String ttsUrl;

  const _RequestTtsListItem({
    Key key,
    this.ttsId,
    this.subscriberName = "",
    this.text = "",
    this.timestamp = "",
    this.ttsUrl
  }) : super(key: key);

  @override
  __RequestTtsListItemState createState() => __RequestTtsListItemState();
}

class __RequestTtsListItemState extends State<_RequestTtsListItem> {
  bool _expanded = false;
  bool _isAudioPlaying = false;
  AudioPlayer _audioPlayer;
  StreamSubscription _playerCompletionSubscription;
  StreamSubscription _playerErrorSubscription;

  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add((stopDefaultButtonEvent) {
      Navigator.popUntil(context, ModalRoute.withName(kRouteHome));
      return true;
    }, zIndex: 11, ifNotYetIntercepted: true);
    _audioPlayer = AudioPlayer(mode: PlayerMode.MEDIA_PLAYER);
    this._playerCompletionSubscription = this._audioPlayer.onPlayerCompletion.listen((event) {
      setState(() => _isAudioPlaying = false);
    });
    this._playerErrorSubscription = this._audioPlayer.onPlayerError.listen((event) {
      developer.log(event);
      Fluttertoast.showToast(msg: "TTS 듣기 중 문제가 발생했습니다...", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
      setState(() => _isAudioPlaying = false);
    });
    _initData();
  }

  void _initData() async {
    await Future.delayed(Duration(milliseconds: 500));
  }

  @override
  void dispose() {
    this._audioPlayer?.dispose();
    this._playerCompletionSubscription?.cancel();
    this._playerErrorSubscription?.cancel();
    super.dispose();
  }

  void _play(String url) async {
    await this._audioPlayer.play(url);
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 13),
      height: !_expanded ? 74 : 237,
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          boxShadow: [
            BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.1),
                blurRadius: 4,
                offset: Offset(0, 2)
            )
          ]
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width / 2 - 30,
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          widget.subscriberName,
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 13,
                          ),
                        ),
                        SizedBox(width: 11,),
                        Container(
                          width: 100,
                          child: Text(
                            widget.timestamp,
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(
                                color: kNavigationPressableColor
                            ),
                          ),
                        )
                      ],
                    ),
                    SizedBox(height: 6,),
                    Text(
                      _expanded ? "" : widget.text,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                        color: kSelectableTextColor,
                        fontSize: 13,
                      ),
                    )
                  ],
                ),
              ),
              Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  GestureDetector(
                    onTap: () => setState(() => _expanded = !_expanded),
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                        alignment: Alignment.centerRight,
                        width: 40,
                        height: 40,
                        child: Transform.rotate(
                          angle: 90 * pi/180,
                          child: Icon(
                            _expanded ? Icons.arrow_back_ios : Icons.arrow_forward_ios,
                            color: kBorderColor,
                            size: 25,
                          ),
                        )
                    ),
                  )
                ],
              )
            ],
          ),
          _expanded ? Container(
//            padding: const EdgeInsets.symmetric(vertical: 15, horizontal: 32),
            height: 163,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: SingleChildScrollView(
                    child: Text(
                      widget.text,
                      style: TextStyle(
                          fontSize: 13
                      ),
                    ),
                  ),
                ),
                Container(
                  height: 31.6,
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      Container(
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(width: 5,),
                            IconButton(
                                icon: Icon(FontAwesomeIcons.play),
                                iconSize: 17,
                                color: Color.fromRGBO(96, 96, 96, 1),
                                onPressed: () {
                                  if(this._isAudioPlaying){
                                    Fluttertoast.showToast(msg: "재생중 입니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                                    return;
                                  }
                                  this._isAudioPlaying = true;
                                  this._play(widget.ttsUrl);
                                  Fluttertoast.showToast(msg: "다음이 재생됩니다: ${widget.text}", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                                }
                            )
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            GestureDetector(
                              onTap: () {
                                showGeneralDialog(
                                  context: context,
                                  pageBuilder: (context, animation, secondAnimation) => TtsDeclineDialog(),
                                  barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                                  barrierDismissible: false,
                                  transitionDuration: const Duration(milliseconds: 150),
                                ).then((value) {
                                  if(value)
                                    httpCelebMyTtsDecline(context: context, ttsId: widget.ttsId).then((value){
                                      if(value){
                                        Fluttertoast.showToast(msg: "요청을 거절했습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                                        Provider.of<CelebTtsProvider>(context, listen: false).popRequested(widget.ttsId);
                                      } else {
                                        Fluttertoast.showToast(msg: "요청을 거절하는데 실패했습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                                      }
                                    });
                                });
                              },
                              behavior: HitTestBehavior.translucent,
                              child: Container(
                                width: 38.4,
                                height: 31.6,
                                decoration: BoxDecoration(
                                    color: kMainBackgroundColor,
                                    borderRadius: BorderRadius.circular(21),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color.fromRGBO(0, 0, 0, 0.16),
                                        blurRadius: 6,
                                        offset: const Offset(0, 3),
                                      )
                                    ]
                                ),
                                child: Center(
                                  child: Text(
                                    "거절",
                                    style: TextStyle(
                                      fontSize: 13,
                                      color: kNavigationPressableColor,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            SizedBox(width: 15.5,),
                            GestureDetector(
                              onTap: () {
                                showGeneralDialog(
                                  context: context,
                                  pageBuilder: (context, animation, secondAnimation) => TtsApproveDialog(),
                                  barrierColor: Color.fromRGBO(254, 254, 254, 0.01),
                                  barrierDismissible: false,
                                  transitionDuration: const Duration(milliseconds: 150),
                                ).then((value) {
                                  if(value)
                                    httpCelebMyTtsApprove(context: context, ttsId: widget.ttsId).then((value){
                                      if(value){
                                        Fluttertoast.showToast(msg: "요청을 승인했습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                                        Provider.of<CelebTtsProvider>(context, listen: false).popRequested(widget.ttsId, toAccepted: true);
                                      } else {
                                        Fluttertoast.showToast(msg: "요청을 승인하는데 실패했습니다.", toastLength: Toast.LENGTH_SHORT, gravity: ToastGravity.TOP);
                                      }
                                    });
                                });
                              },
                              behavior: HitTestBehavior.translucent,
                              child: Container(
                                width: 38.4,
                                height: 31.6,
                                decoration: BoxDecoration(
                                    color: kMainBackgroundColor,
                                    borderRadius: BorderRadius.circular(21),
                                    border: Border.all(
                                        color: kMainColor,
                                        width: 2
                                    ),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Color.fromRGBO(0, 0, 0, 0.16),
                                        blurRadius: 6,
                                        offset: const Offset(0, 3),
                                      )
                                    ]
                                ),
                                child: Center(
                                  child: Text(
                                    "승인",
                                    style: TextStyle(
                                      fontSize: 13,
                                      color: kMainColor,
                                    ),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ) : Container()
        ],
      ),
    );
  }
}

class RequestedCelebTtsData {
  final int tts;
  final String subscriberName;
  final String text;
  final String timestamp;
  final String ttsUrl;

  const RequestedCelebTtsData({
    this.tts,
    this.subscriberName,
    this.text,
    this.timestamp,
    this.ttsUrl
  });
}