
import 'package:flutter/material.dart';

class RequireLoginBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Text(
          "로그인이 필요한 서비스 입니다!"
        ),
      ),
    );
  }
}
