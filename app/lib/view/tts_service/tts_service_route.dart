
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/view/tts_service/tts_service_body.dart';

import '../../colors.dart';

class TtsServiceRoute extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0.0,
        automaticallyImplyLeading: false,
        title: _TtsServiceAppbar(),
      ),
      body: TtsServiceBody(),
      floatingActionButton: FloatingActionButton(
        backgroundColor: kFunctionButtonColor,
        onPressed: () => Navigator.pushNamed(context, kRouteTtsWrite).then((value) => value != null ? Fluttertoast.showToast(msg: "셀럽에게 요청이 들어갔습니다!", gravity: ToastGravity.TOP) : null) ,
        child: Icon(
          FontAwesomeIcons.pen,
          color: kMainBackgroundColor,
        ),
      ),
    );
  }
}

class _TtsServiceAppbar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 30,
                  ),
                  GestureDetector(
                    onTap: () => Navigator.pop(context),
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                        child: Container(
                          child: Icon(
                            Icons.arrow_back_ios,
                            size: 30,
                            color: kAppBarButtonColor,
                          ),
                        )
                    ),
                  ),
                ]
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Text(
                "TTS 서비스",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: kAppBarButtonColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(),
          ),
        ],
      ),
    );
  }
}
