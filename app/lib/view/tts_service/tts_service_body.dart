
import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/providers/tts_write_provider.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/view/tts_service/tts_service_sub_view.dart';

import '../../colors.dart';

class TtsServiceBody extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      BackButtonInterceptor.add((stopDefaultButtonEvent) {
        Navigator.popUntil(context,ModalRoute.withName(kRouteHome));
        return true;
      }, zIndex: 9, ifNotYetIntercepted: true);
      if(Provider.of<TtsWriteProvider>(context, listen: false).fromCommunity){
        Navigator.pushNamed(context, kRouteTtsWrite);
      }
    });

    return Container(
      child: DefaultTabController(
        initialIndex: 0,
        length: 2,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                color: Theme.of(context).backgroundColor,
                boxShadow: [
                  BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.05),
                    blurRadius: 11,
                    offset: Offset(0, 3)
                  )
                ]
              ),
              child: TabBar(
                indicatorColor: Theme.of(context).primaryColor,
                labelColor: Theme.of(context).primaryColor,
                labelStyle: TextStyle(
                    color: Theme.of(context).primaryColor,
                    fontSize: 17,
                    fontWeight: FontWeight.bold
                ),
                unselectedLabelColor: kNavigationPressableColor,
                unselectedLabelStyle: TextStyle(
                    fontSize: 15
                ),
                tabs: <Widget>[
                  Tab(child: Text("내 TTS 보관함")),
                  Tab(child: Text("요청한 TTS 목록")),
                ],
              ),
            ),
            Expanded(
              child: TabBarView(
                children: <Widget>[
                  TtsCelebList(),
                  TtsRequestedList()
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
