

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/colors.dart';
import 'package:rf_tap_fanseem/http/community/community_celeb_membership_list.dart';
import 'package:rf_tap_fanseem/http/community/dto/http_community_celeb_membership_list_dto.dart';
import 'package:rf_tap_fanseem/providers/selected_celeb_provider.dart';
import 'package:rf_tap_fanseem/view/membership_join/member_root_body.dart';
import 'package:rf_tap_fanseem/providers/membership_provider.dart';

class MembershipListBody extends StatefulWidget {
  final PageController pageController;

  MembershipListBody({
    Key key,
    this.pageController
  }) : super(key: key);

  @override
  _MembershipListBodyState createState() => _MembershipListBodyState();
}

class _MembershipListBodyState extends State<MembershipListBody> {

  HttpCommunityCelebMembershipListDto _data;

  Future<bool> _getData() async {
    _data = await httpCommunityCelebMembershipList(context: context, celebId: Provider.of<SelectedCelebProvider>(context, listen: false).celebId);
    if (_data != null) return true;
      return false;
  }


  @override
  Widget build(BuildContext context) {
    return FutureBuilder<bool>(
      future: _getData(),
      builder: (context, snapshot) {
        if (snapshot.hasData && snapshot.data) {
          return Container(
            child: SingleChildScrollView(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: () {
                  List<Widget> children = [Container(
                    decoration: BoxDecoration(
                        color: Theme
                            .of(context)
                            .backgroundColor,
                        boxShadow: [
                          BoxShadow(
                            color: Color.fromRGBO(0, 0, 0, 0.16),
                            blurRadius: 6,
                            offset: Offset(0, 2),
                          )
                        ]
                    ),
                    height: 52,
                    child: Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        SizedBox(width: 30,),
                        Text(
                          "${Provider
                              .of<SelectedCelebProvider>(context, listen: false)
                              .celebName}의 멤버십 목록",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 15,
                              fontWeight: FontWeight.bold
                          ),
                        ),
                      ],
                    ),
                  )];
                  children.addAll(_data.membershipList.map((e) => GestureDetector(
                    onTap: () {
                      Provider.of<MembershipProvider>(context, listen: false).setMembershipInfo(
                          e.productId,
                          e.membershipName,
                          e.price
                      );
                      widget.pageController.animateToPage(
                          1, duration: Duration(milliseconds: 300),
                          curve: Curves.easeOut);
                    },
                      behavior: HitTestBehavior.translucent,
                      child: _MembershipBar(
                        membershipName: e.membershipName,
                        membershipIcon: e.imageUrl,
                        membershipCost: e.price,
                        myMembership: e.membershipTier == _data.myTier,
                      ),
                    )).toList()
                  );
                  return children;
                }()
              ),
            ),
          );
        } else {
          return Center(child: CircularProgressIndicator(),);
        }
      }
    );
  }
}

class _MembershipBar extends StatelessWidget {
  final String membershipIcon;
  final String membershipName;
  final int membershipCost;
  final bool myMembership;

  const _MembershipBar({
    Key key,
    this.membershipIcon,
    this.membershipName,
    this.membershipCost,
    this.myMembership = false
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 89,
      decoration: BoxDecoration(
          color: Theme.of(context).backgroundColor,
          boxShadow: [
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.16),
              blurRadius: 6,
              offset: Offset(0, 2),
            )
          ]
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            flex: 6,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(width: 41,),
                Container(
                  width: 20,
                  height: 20,
                  child: CachedNetworkImage(
                    imageUrl: membershipIcon,
                    placeholder: (context, url) => Center(child: CircularProgressIndicator()),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                ),
                SizedBox(
                  width: 30,
                ),
                Text(
                  membershipName,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 4,
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Text(
                  myMembership ? "구독중" : "$membershipCost 원 / 월",
                  style: TextStyle(
                    color: kNegativeTextColor,
                    fontSize: 12,
                  ),
                ),
                SizedBox(
                  width: 24,
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
