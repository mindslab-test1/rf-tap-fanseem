

import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/view/membership_join/member_root_body.dart';

import '../../colors.dart';
import 'member_detail_body.dart';
import 'member_list_body.dart';
class MembershipRoute extends StatefulWidget {
  final PageController pageController = PageController(
    initialPage: 0,
    keepPage: false,
  );
  @override
  _MembershipRouteState createState() => _MembershipRouteState();
}

class _MembershipRouteState extends State<MembershipRoute> {

  @override
  void initState() {
    BackButtonInterceptor.add((stopDefaultButtonEvent) {
      // ignore: invalid_use_of_protected_member
      if(widget.pageController.positions.isNotEmpty)
        if(widget.pageController.page != 0) widget.pageController.animateToPage(0, duration: Duration(milliseconds: 300), curve: Curves.easeOut);
        else {
          Navigator.popUntil(context,ModalRoute.withName(kRouteHome));
        }
      return true;
    }, ifNotYetIntercepted: true, zIndex: 1, name:"popMember");
    super.initState();
  }

  @override
  void dispose(){
    BackButtonInterceptor.removeByName("popMember");
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: 0,
        automaticallyImplyLeading: false,
        title: MembershipAppbar(
          pageController: widget.pageController,
        ),
      ),
      body: PageView.builder(
        physics: NeverScrollableScrollPhysics(),
        controller: widget.pageController,
        itemBuilder: (context, index){
          switch(MembershipSubPages.values.elementAt(index)){
            case MembershipSubPages.list:
              return MembershipListBody(
                pageController: widget.pageController,
              );
            case MembershipSubPages.detail:
              return MembershipDetailBody(
              );
            default:
              throw UnimplementedError("unknown page!");
          }
        },
      ),
    );
  }
}

class MembershipAppbar extends StatelessWidget {
  final PageController pageController;

  MembershipAppbar({
    Key key,
    this.pageController
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            flex: 3,
            child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 30,
                  ),
                  GestureDetector(
                    onTap: () {
                      // ignore: invalid_use_of_protected_member
                      if(pageController.positions.isEmpty || pageController.page == 0) Navigator.pop(context);
                      else pageController.animateToPage(0, duration: Duration(milliseconds: 300), curve: Curves.easeOut);
                    },
                    behavior: HitTestBehavior.translucent,
                    child: Container(
                        child: Icon(
                          Icons.arrow_back_ios,
                          size: 27,
                          color: kAppBarButtonColor,
                        )
                    ),
                  ),
                ]
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Text(
                "멤버십 가입",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: kAppBarButtonColor,
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                ),
              ),
            ),
          ),
          Expanded(
            flex: 3,
            child: Container(
              child: Row(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Container(
                    child: Center(
                      child: Text(
                        "",
                        style: TextStyle(
                          color: Theme.of(context).backgroundColor,
                          fontSize: 15,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 24,)
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

