
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';
import 'package:rf_tap_fanseem/view/membership_join/member_detail_body.dart';
import 'package:rf_tap_fanseem/view/membership_join/member_list_body.dart';
import 'package:rf_tap_fanseem/providers/membership_provider.dart';

class MembershipRootBody extends StatelessWidget {
  final PageController _pageController = PageController(
    keepPage: true
  );

  @override
  Widget build(BuildContext context) {

    return PageView.builder(
      controller: _pageController,
      physics: NeverScrollableScrollPhysics(),
      itemBuilder: (context, index){
        switch(MembershipSubPages.values.elementAt(index)){
          case MembershipSubPages.list:
            return MembershipListBody(
            );
          case MembershipSubPages.detail:
            return MembershipDetailBody(
            );
          default:
            throw UnimplementedError("unknown page!");
        }
      },
      itemCount: MembershipSubPages.values.length,
    );
  }
}

enum MembershipSubPages {
  list, detail
}

const String kMembershipPageController = "member/page";