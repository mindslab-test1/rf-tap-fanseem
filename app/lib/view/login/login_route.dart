

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rf_tap_fanseem/data/login_data.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:rf_tap_fanseem/view/login/login_function.dart';


class LoginRoute extends StatefulWidget {
  @override
  _LoginRouteState createState() => _LoginRouteState();
}

class _LoginRouteState extends State<LoginRoute> {
  bool _autoLogin;

  @override
  void initState() {
    super.initState();
    _autoLogin = true;
  }

  @override
  Widget build(BuildContext context) {
    double buttonWidth = MediaQuery.of(context).size.width - 140;

    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 62,
            ),
            SizedBox(
              height: 41,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Image.asset("assets/icon/seem_off_3x.png", color: Theme.of(context).backgroundColor, fit: BoxFit.contain,),
                  SizedBox(width: 32,)
                ],
              ),
            ),
            SizedBox(
              height: 13,
            ),
            Padding(
              padding: EdgeInsets.only(left: MediaQuery.of(context).size.width * 0.07),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "환영합니다 :-)",
                    style: TextStyle(
                        fontSize: 20,
                        color: Theme.of(context).backgroundColor
                    ),
                  ),
                  SizedBox(
                    height: 2,
                  ),
                  Text(
                    "지금 바로 셀럽과의 만남을 시작해 보세요.",
                    style: TextStyle(
                        fontSize: 15,
                        color: Theme.of(context).backgroundColor
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 7,
            ),
            Padding(
              padding: const EdgeInsets.all(30),
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Theme.of(context).backgroundColor,
                    borderRadius: BorderRadius.all(Radius.circular(12)),
                    boxShadow: [
                      BoxShadow(
                          color: Color.fromRGBO(0, 0, 0, 0.36),
                          offset: Offset(0, 3),
                          blurRadius: 12
                      )
                    ]
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    SizedBox(
                      height: 23,
                    ),
                    _SocialLoginButton(
                      width: buttonWidth,
                      social: LoginSocial.naver,
                    ),
                    SizedBox(
                      height: 11,
                    ),
//                    _SocialLoginButton(
//                      width: buttonWidth,
//                      social: LoginSocial.google,
//                    ),
//                    SizedBox(
//                      height: 11,
//                    ),
//                    _SocialLoginButton(
//                      width: buttonWidth,
//                      social: LoginSocial.kakao,
//                    ),
                  /*
                    Row(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Checkbox(
                          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                          value: _autoLogin,
                          onChanged: (bool value) => setState(() => _autoLogin = value),
                          activeColor: Theme.of(context).primaryColor,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Text(
                          "자동 로그인",
                          style: TextStyle(
                              fontSize: 12,
                              color: kSelectableTextColor
                          ),
                        ),
                        SizedBox(
                          width: 19,
                        )
                      ],
                    ),
                    */
                    SizedBox(height: 10,)
                  ],
                ),
              ),
            ),
            Center(
              child: GestureDetector(
                onTap: () => Navigator.push(context,
                    MaterialPageRoute(builder: (context) => _PrivacyPolicy(), maintainState: true)),
                child: Text(
                  "이용 약관 및 개인정보 처리방침",
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: Theme.of(context).backgroundColor
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}

class _SocialLoginButton extends StatelessWidget {
  final double width;
  final LoginSocial social;

  const _SocialLoginButton({Key key, this.width, this.social})
      : assert(width != null),
        assert(social != null),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => loginWithSocial(context, social),
      child: Container(
        width: width,
        height: 60,
        decoration: _buttonDecoration(),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              width: 19,
            ),
            // TODO icon image per social
            Container(
              width: 17,
              height: 20,
              child: Image.asset("assets/icon/naver_icon.png", fit: BoxFit.contain,)
//              child: Icon(Icons.add),
            ),
            Expanded(
              child: Center(
                child: Text(
                  _buttonText(),
                  style: TextStyle(
                    color: social != LoginSocial.naver ? Colors.black : Theme.of(context).backgroundColor,
                    fontSize: 15,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),
            ),
            SizedBox(
              width: 36,
              height: 36,
            )
          ],
        ),
      ),
    );
  }

  String _buttonText(){
    switch(social){
      case LoginSocial.naver:
        return "네이버로 로그인";
      case LoginSocial.kakao:
        return "카카오톡으로 로그인";
      case LoginSocial.google:
        return "구글 아이디로 로그인";
      default:
        throw UnsupportedError("Unknown social login type!");
    }
  }

  BoxDecoration _buttonDecoration(){
    switch(social){
      case LoginSocial.naver:
        return BoxDecoration(
            color: Color.fromRGBO(71, 181, 72, 1),
            borderRadius: BorderRadius.all(Radius.circular(12))
        );
      case LoginSocial.kakao:
        return BoxDecoration(
            color: Color.fromRGBO(254, 205, 47, 1),
            borderRadius: BorderRadius.all(Radius.circular(12))
        );
      case LoginSocial.google:
        return BoxDecoration(
            color: Color.fromRGBO(255, 255, 255, 1),
            borderRadius: BorderRadius.all(Radius.circular(12)),
            boxShadow: [
              BoxShadow(
                offset: Offset(1, 1),
                color: Color.fromRGBO(0, 0, 0, 0.16),
                blurRadius: 2
              )
            ]
        );
      default:
        throw UnsupportedError("Unknown social login type!");
    }
  }
}

class _PrivacyPolicy extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
        centerTitle: true,
        title: Text('이용약관 및 개인정보처리방침',
              style: TextStyle(
                color: Theme.of(context).backgroundColor,
                fontFamily: 'Nanum',
                fontSize: 18,
                fontWeight: FontWeight.bold
              ),
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(18.0),
          child: Text(
              "1. 개인정보의 처리 목적 <주식회사 알에프탭>(‘fanmeet.curogom.com’이하 ‘FanMeet’) 은(는) 다음의 목적을 위하여 개인정보를 처리하고 있으며, 다음의 목적 이외의 용도로는 이용하지 않습니다.\n"
                  "- 고객 가입의사 확인, 고객에 대한 서비스 제공에 따른 본인 식별.인증, 회원자격 유지.관리, 물품 또는 서비스 공급에 따른 금액 결제, 물품 또는 서비스의 공급.배송 등\n"
                  "\n"
                  "2. 개인정보의 처리 및 보유 기간\n"
                  "\n"
                  "① <주식회사 알에프탭>(‘fanmeet.curogom.com’이하 ‘FanMeet’) 은(는) 정보주체로부터 개인정보를 수집할 때 동의 받은 개인정보 보유․이용기간 또는 법령에 따른 개인정보 보유․이용기간 내에서 개인정보를 처리․보유합니다.\n"
                  "\n"
                  "② 구체적인 개인정보 처리 및 보유 기간은 다음과 같습니다.\n"
                  "☞ 아래 예시를 참고하여 개인정보 처리업무와 개인정보 처리업무에 대한 보유기간 및 관련 법령, 근거 등을 기재합니다.\n"
                  "(예시)- 고객 가입 및 관리 : 서비스 이용계약 또는 회원가입 해지시까지, 다만 채권․채무관계 잔존시에는 해당 채권․채무관계 정산시까지\n"
                  "- 전자상거래에서의 계약․청약철회, 대금결제, 재화 등 공급기록 : 5년\n"
                  "\n"
                  "\n"
                  "\n"
                  "\n"
                  "\n"
                  "3. 개인정보의 제3자 제공에 관한 사항\n"
                  "\n"
                  "① <주식회사 알에프탭>('fanmeet.curogom.com'이하 'FanMeet')은(는) 정보주체의 동의, 법률의 특별한 규정 등 개인정보 보호법 제17조 및 제18조에 해당하는 경우에만 개인정보를 제3자에게 제공합니다.\n"
                  "\n"
                  "② <주식회사 알에프탭>('fanmeet.curogom.com')은(는) 다음과 같이 개인정보를 제3자에게 제공하고 있습니다.\n"
                  "\n"
                  "\n"
                  "1. <주식회사 알에프탭>\n"
                  "- 개인정보를 제공받는 자 : 주식회사 알에프탭\n"
                  "- 제공받는 자의 개인정보 이용목적 : 이메일, 휴대전화번호, 자택주소, 비밀번호, 로그인ID, 성별, 생년월일, 이름, 신용카드정보, 은행계좌정보, 서비스 이용 기록, 접속 로그, 쿠키, 접속 IP 정보, 결제기록, 법정대리인 이름, 법정대리인 자택 전화번호, 법정대리인 휴대전화번호\n"
                  "- 제공받는 자의 보유.이용기간: 5년\n"
                  "\n"
                  "\n"
                  "\n"
                  "4. 개인정보처리 위탁\n"
                  "\n"
                  "① <주식회사 알에프탭>('FanMeet')은(는) 원활한 개인정보 업무처리를 위하여 다음과 같이 개인정보 처리업무를 위탁하고 있습니다.\n"
                  "\n"
                  "1. <원활한 개인정보 업무처리>\n"
                  "- 위탁받는 자 (수탁자) : 주식회사 마인즈랩\n"
                  "- 위탁하는 업무의 내용 : 구매 및 요금 결제, 물품배송 또는 청구서 등 발송, 본인인증(금융거래, 금융서비스), 요금추심, 회원제 서비스 이용에 따른 본인확인, 불만처리 등 민원처리, 고지사항 전달, 신규 서비스(제품) 개발 및 맞춤 서비스 제공, 이벤트 및 광고성 정보 제공 및 참여기회 제공, 영상정보처리기기 운영\n"
                  "- 위탁기간 : 5년\n"
                  "\n"
                  "\n"
                  "\n"
                  "\n"
                  "② <주식회사 알에프탭>('fanmeet.curogom.com'이하 'FanMeet')은(는) 위탁계약 체결시 개인정보 보호법 제25조에 따라 위탁업무 수행목적 외 개인정보 처리금지, 기술적․관리적 보호조치, 재위탁 제한, 수탁자에 대한 관리․감독, 손해배상 등 책임에 관한 사항을 계약서 등 문서에 명시하고, 수탁자가 개인정보를 안전하게 처리하는지를 감독하고 있습니다.\n"
                  "\n"
                  "③ 위탁업무의 내용이나 수탁자가 변경될 경우에는 지체없이 본 개인정보 처리방침을 통하여 공개하도록 하겠습니다.\n"
                  "5. 정보주체와 법정대리인의 권리·의무 및 그 행사방법 이용자는 개인정보주체로써 다음과 같은 권리를 행사할 수 있습니다.\n"
                  "① 정보주체는 주식회사 알에프탭(‘fanmeet.curogom.com’이하 ‘FanMeet) 에 대해 언제든지 다음 각 호의 개인정보 보호 관련 권리를 행사할 수 있습니다.\n"
                  "1. 개인정보 열람요구\n"
                  "2. 오류 등이 있을 경우 정정 요구\n"
                  "3. 삭제요구\n"
                  "4. 처리정지 요구\n"
                  "\n"
                  "\n"
                  "\n"
                  "6. 처리하는 개인정보의 항목 작성\n"
                  "\n"
                  "① <주식회사 알에프탭>('fanmeet.curogom.com'이하 'FanMeet')은(는) 다음의 개인정보 항목을 처리하고 있습니다.\n"
                  "\n"
                  "1<개인정보처리 항목>\n"
                  "- 필수항목 : 이메일, 휴대전화번호, 비밀번호, 로그인ID, 성별, 생년월일, 이름, 서비스 이용 기록, 접속 로그, 쿠키, 접속 IP 정보, 결제기록\n"
                  "- 선택항목 : 자택주소, 신용카드정보, 은행계좌정보, 법정대리인 자택 주소, 법정대리인 휴대전화번호\n"
                  "\n"
                  "\n"
                  "\n"
                  "\n"
                  "7. 개인정보의 파기<주식회사 알에프탭>('FanMeet')은(는) 원칙적으로 개인정보 처리목적이 달성된 경우에는 지체없이 해당 개인정보를 파기합니다. 파기의 절차, 기한 및 방법은 다음과 같습니다.\n"
                  "\n"
                  "-파기절차\n"
                  "이용자가 입력한 정보는 목적 달성 후 별도의 DB에 옮겨져(종이의 경우 별도의 서류) 내부 방침 및 기타 관련 법령에 따라 일정기간 저장된 후 혹은 즉시 파기됩니다. 이 때, DB로 옮겨진 개인정보는 법률에 의한 경우가 아니고서는 다른 목적으로 이용되지 않습니다.\n"
                  "\n"
                  "-파기기한\n"
                  "이용자의 개인정보는 개인정보의 보유기간이 경과된 경우에는 보유기간의 종료일로부터 5일 이내에, 개인정보의 처리 목적 달성, 해당 서비스의 폐지, 사업의 종료 등 그 개인정보가 불필요하게 되었을 때에는 개인정보의 처리가 불필요한 것으로 인정되는 날로부터 5일 이내에 그 개인정보를 파기합니다.\n"
                  "\n"
                  "\n"
                  "\n"
                  "8. 개인정보 자동 수집 장치의 설치•운영 및 거부에 관한 사항\n"
                  "\n"
                  "① 주식회사 알에프탭 은 개별적인 맞춤서비스를 제공하기 위해 이용정보를 저장하고 수시로 불러오는 ‘쿠기(cookie)’를 사용합니다. ② 쿠키는 웹사이트를 운영하는데 이용되는 서버(http)가 이용자의 컴퓨터 브라우저에게 보내는 소량의 정보이며 이용자들의 PC 컴퓨터내의 하드디스크에 저장되기도 합니다. 가. 쿠키의 사용 목적 : 이용자가 방문한 각 서비스와 웹 사이트들에 대한 방문 및 이용형태, 인기 검색어, 보안접속 여부, 등을 파악하여 이용자에게 최적화된 정보 제공을 위해 사용됩니다. 나. 쿠키의 설치•운영 및 거부 : 웹브라우저 상단의 도구>인터넷 옵션>개인정보 메뉴의 옵션 설정을 통해 쿠키 저장을 거부 할 수 있습니다. 다. 쿠키 저장을 거부할 경우 맞춤형 서비스 이용에 어려움이 발생할 수 있습니다.\n"
                  "\n"
                  "\n"
                  "9. 개인정보 보호책임자 작성\n"
                  "\n"
                  "\n"
                  "① 주식회사 알에프탭(‘fanmeet.curogom.com’이하 ‘FanMeet) 은(는) 개인정보 처리에 관한 업무를 총괄해서 책임지고, 개인정보 처리와 관련한 정보주체의 불만처리 및 피해구제 등을 위하여 아래와 같이 개인정보 보호책임자를 지정하고 있습니다.\n"
                  "\n"
                  "▶ 개인정보 보호책임자\n"
                  "성명 :최정섭\n"
                  "직책 :대표이사\n"
                  "직급 :대표이사\n"
                  "연락처 :01047181194, ceo@rftap.net,\n"
                  "※ 개인정보 보호 담당부서로 연결됩니다.\n"
                  "\n"
                  "▶ 개인정보 보호 담당부서\n"
                  "부서명 :경영팀\n"
                  "담당자 :최정섭\n"
                  "연락처 :01047181194, fanmeet@rftap.net,\n"
                  "② 정보주체께서는 주식회사 알에프탭(‘fanmeet.curogom.com’이하 ‘FanMeet) 의 서비스(또는 사업)을 이용하시면서 발생한 모든 개인정보 보호 관련 문의, 불만처리, 피해구제 등에 관한 사항을 개인정보 보호책임자 및 담당부서로 문의하실 수 있습니다. 주식회사 알에프탭(‘fanmeet.curogom.com’이하 ‘FanMeet) 은(는) 정보주체의 문의에 대해 지체 없이 답변 및 처리해드릴 것입니다.\n"
                  "\n"
                  "\n"
                  "\n"
                  "10. 개인정보 처리방침 변경\n"
                  "\n"
                  "①이 개인정보처리방침은 시행일로부터 적용되며, 법령 및 방침에 따른 변경내용의 추가, 삭제 및 정정이 있는 경우에는 변경사항의 시행 7일 전부터 공지사항을 통하여 고지할 것입니다.\n"
                  "\n"
                  "\n"
                  "\n"
                  "11. 개인정보의 안전성 확보 조치 <주식회사 알에프탭>('FanMeet')은(는) 개인정보보호법 제29조에 따라 다음과 같이 안전성 확보에 필요한 기술적/관리적 및 물리적 조치를 하고 있습니다.\n"
                  "\n"
                  "1. 정기적인 자체 감사 실시\n"
                  "개인정보 취급 관련 안정성 확보를 위해 정기적(분기 1회)으로 자체 감사를 실시하고 있습니다.\n"
                  "\n"
                  "2. 개인정보 취급 직원의 최소화 및 교육\n"
                  "개인정보를 취급하는 직원을 지정하고 담당자에 한정시켜 최소화 하여 개인정보를 관리하는 대책을 시행하고 있습니다.\n"
                  "\n"
                  "3. 내부관리계획의 수립 및 시행\n"
                  "개인정보의 안전한 처리를 위하여 내부관리계획을 수립하고 시행하고 있습니다.\n"
                  "\n"
                  "4. 해킹 등에 대비한 기술적 대책\n"
                  "<주식회사 알에프탭>('FanMeet')은 해킹이나 컴퓨터 바이러스 등에 의한 개인정보 유출 및 훼손을 막기 위하여 보안프로그램을 설치하고 주기적인 갱신·점검을 하며 외부로부터 접근이 통제된 구역에 시스템을 설치하고 기술적/물리적으로 감시 및 차단하고 있습니다.\n"
                  "\n"
                  "5. 접속기록의 보관 및 위변조 방지\n"
                  "개인정보처리시스템에 접속한 기록을 최소 6개월 이상 보관, 관리하고 있으며, 접속 기록이 위변조 및 도난, 분실되지 않도록 보안기능 사용하고 있습니다.\n"
                  "\n"
                  "6. 개인정보에 대한 접근 제한\n"
                  "개인정보를 처리하는 데이터베이스시스템에 대한 접근권한의 부여,변경,말소를 통하여 개인정보에 대한 접근통제를 위하여 필요한 조치를 하고 있으며 침입차단시스템을 이용하여 외부로부터의 무단 접근을 통제하고 있습니다.\n"
                  "\n"
                  "7. 문서보안을 위한 잠금장치 사용\n"
                  "개인정보가 포함된 서류, 보조저장매체 등을 잠금장치가 있는 안전한 장소에 보관하고 있습니다.\n"
                  "\n"
                  "8. 비인가자에 대한 출입 통제\n"
                  "개인정보를 보관하고 있는 물리적 보관 장소를 별도로 두고 이에 대해 출입통제 절차를 수립, 운영하고 있습니다.\n"
                  ""
          ),
        ),
      ),
    );
  }
}
