import 'package:flutter/material.dart';
import 'package:rf_tap_fanseem/data/login_data.dart';
import 'package:rf_tap_fanseem/view/login/login_route.dart';

import 'login_function.dart';

class LoginIosRoute extends StatefulWidget {
  @override
  _LoginIosRouteState createState() => _LoginIosRouteState();
}

class _LoginIosRouteState extends State<LoginIosRoute> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      body : SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(
              height: 49,
            ),
            // back button
            Container(
              height: 28,
//              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 30,
                  ),
                  GestureDetector(
                    behavior: HitTestBehavior.translucent,
                    onTap: () => Navigator.pop(context),
                    child: Icon(
                      Icons.arrow_back_ios,
                      color: Colors.white,
                      size: 25,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Container(
              height: 82,
              child: Center(
                child: Image.asset(
                  "assets/logo/login_logo_ios.png",
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
            SizedBox(
              height: 11,
            ),
            Container(
              height: 25,
              padding: const EdgeInsets.symmetric(horizontal: 57),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "환영합니다 :-)",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                      fontWeight: FontWeight.bold
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 11,
            ),
            Container(
              height: 25,
              padding: const EdgeInsets.symmetric(horizontal: 60),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "지금 바로 셀럽과의 만남을 시작해 보세요.",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 13,
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: Container(
                width: double.infinity,
                padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 26),
                decoration: BoxDecoration(
                  color: Colors.white,
                  boxShadow: [
                    BoxShadow(
                      color: Color.fromRGBO(0, 0, 0, 0.36),
                      blurRadius: 12,
                      offset: Offset(0, 3)
                    )
                  ],
                ),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    GestureDetector(
                      child: _NaverLoginButton(),
                      behavior: HitTestBehavior.translucent,
                      onTap: () => loginWithSocial(context, LoginSocial.naver),
                    ),
                    SizedBox(
                      height: 14,
                    ),
                    GestureDetector(
                      child: _AppleLoginButton(),
                      behavior: HitTestBehavior.translucent,
                      onTap: () => loginWithSocial(context, LoginSocial.apple),
                    )
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 33,
            ),
            Container(
              height: 22,
              child: Center(
                child: Text(
                  "이용 약관 | 개인정보 처리방침",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 15,
                  ),
                ),
              ),
            ),
            SizedBox(height: 11,),
          ],
        ),
      )
    );
  }
}

class _NaverLoginButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 44,
      width: double.infinity,
      decoration: BoxDecoration(
        color: Color.fromRGBO(71, 181, 72, 1),
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.16),
            blurRadius: 6,
            offset: Offset(0, 3)
          )
        ]
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  width: 17.7,
                ),
                Container(
                  height: 16.8,
                  child: Image.asset(
                    "assets/logo/naver_login_logo.png",
                    fit: BoxFit.fitHeight,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 6,
            child: Center(
              child: Text(
                "Naver 로 로그인",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(),
          )
        ],
      ),
    );
  }
}

class _AppleLoginButton extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 44,
      width: double.infinity,
      decoration: BoxDecoration(
          color: Colors.black,
          boxShadow: [
            BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.16),
                blurRadius: 6,
                offset: Offset(0, 3)
            )
          ]
      ),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Expanded(
            flex: 2,
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  width: 10,
                ),
                Container(
                  height: 32,
                  child: Image.asset(
                    "assets/logo/apple_login_logo.png",
                    fit: BoxFit.fitHeight,
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            flex: 6,
            child: Center(
              child: Text(
                "Apple 로 로그인",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 20,
                ),
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Container(),
          )
        ],
      ),
    );
  }
}

