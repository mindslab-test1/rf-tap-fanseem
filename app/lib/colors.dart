import 'package:flutter/material.dart';

const Color kMainColor = Color.fromRGBO(216, 72, 97, 1);
const Color kMainBackgroundColor = Color.fromRGBO(255, 255, 255, 1);
const Color kBorderColor = Color.fromRGBO(228, 228, 228, 1);
const Color kAppBarButtonColor = Color.fromRGBO(255, 255, 255, 1);
const Color kFunctionButtonColor = Color.fromRGBO(254, 177, 118, 1);
const Color kNavigationPressableColor = Color.fromRGBO(155, 155, 155, 1);
const Color kUnSelectedColor = Color.fromRGBO(232, 232, 232, 1);
const Color kSelectableTextColor = Color.fromRGBO(82, 82, 82, 1);
const Color kNegativeTextColor = Color.fromRGBO(227, 55, 55, 1);