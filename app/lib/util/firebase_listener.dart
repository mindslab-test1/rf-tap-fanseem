import 'dart:convert';
import 'dart:developer' as developer;
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:rf_tap_fanseem/main.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:rf_tap_fanseem/firebase/firebase.dart';
import 'package:rf_tap_fanseem/routes.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FirebaseListener {
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  static bool isLaunch = false;
  static Map<String, dynamic> arguments = {};
  FirebaseListener(){
    getDeviceTokenId();
    run();
  }

  _processMessage(Map<String, dynamic> message) async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    // stash unchecked messages
    Map<String, dynamic> messageStash = jsonDecode(preferences.getString("fanmeet_messages"));
    Map<String, dynamic> messagePayload = message["data"];
    Map<String, dynamic> messageData = messagePayload["payload"];
    if(messagePayload["type"] == "notice"){

      int feedId = int.parse(messageData["feedId"]);
      String noticeType = messageData["noticeType"];
      if(messageStash["missedNotices"][feedId][noticeType] == null){
        messageStash["missedNotices"][feedId][noticeType] = 0;
      }
      messageStash["missedNotices"][feedId][noticeType]++;
    } else if (messagePayload["type"] == "celebMessage"){
      int celebId = messageData["celebId"];
      String celebName = messageData["celebName"];

    } else {
      throw UnimplementedError("unimplemented message type!");
    }


  }

  run() {
    final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();
    if (Platform.isIOS) {
      _firebaseMessaging.requestNotificationPermissions(
        const IosNotificationSettings(
            sound: true, badge: true, alert: true, provisional: false),
      );
    }
    _firebaseMessaging.autoInitEnabled();
    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        developer.log("onMessage: $message");
//        _processMessage(message);
      },
      onLaunch: (Map<String, dynamic> message) async {
        developer.log("onLaunch: $message");
        if(Platform.isAndroid){
          message = Map.from(message["data"]);
        }
        isLaunch=true;
        if(message["celeb"]==null){
          if(message["content"]!=null){
            FirebaseListener.arguments = {"feedId" :num.parse(message["content"]), "isContent" : true, "isUserFeed" : false};
          }else if(message["celebFeed"]!=null){
            FirebaseListener.arguments = {"feedId" :num.parse(message["celebFeed"]), "isContent" : false, "isUserFeed" : false};
          }else if(message["feed"]!=null){
            FirebaseListener.arguments = {"feedId" :num.parse(message["feed"]), "isContent" : false, "isUserFeed" : true};
          }
        }
        else if(message["celeb"]!=null)
          if(message["content"]!=null){
            FirebaseListener.arguments = {"feedId" :num.parse(message["content"]), "isContent" : true, "isUserFeed" : false, "celebId" : num.parse(message["celeb"])};
          }else if(message["celebFeed"]!=null){
            FirebaseListener.arguments = {"feedId" :num.parse(message["celebFeed"]), "isContent" : false, "isUserFeed" : false, "celebId" : num.parse(message["celeb"])};
          }else if(message["feed"]!=null){
            FirebaseListener.arguments = {"feedId" :num.parse(message["feed"]), "isContent" : false, "isUserFeed" : true, "celebId" : num.parse(message["celeb"])};
          }
          else{
            Fluttertoast.showToast(msg: "잘못된 알림 메시지 입니다.", gravity: ToastGravity.TOP);
          }
//        _processMessage(message);
      },
      onResume: (Map<String, dynamic> message) async {
        developer.log("onResume: $message");
//        _processMessage(message);
        if(Platform.isAndroid){
          message = Map.from(message["data"]);
        }
        if(message["celeb"]==null){
          if (message["content"] != null) {
            FanseemApp.navigatorKey.currentState.pushNamed(kRouteFeedDetail,
                arguments: {
                  "feedId": num.parse(message["content"]),
                  "isContent": true,
                  "isUserFeed": false
                });
          } else if (message["celebFeed"] != null) {
            FanseemApp.navigatorKey.currentState.pushNamed(kRouteFeedDetail,
                arguments: {
                  "feedId": num.parse(message["celebFeed"]),
                  "isContent": false,
                  "isUserFeed": false
                });
          } else if (message["feed"] != null) {
            FanseemApp.navigatorKey.currentState.pushNamed(kRouteFeedDetail,
                arguments: {
                  "feedId": num.parse(message["feed"]),
                  "isContent": false,
                  "isUserFeed": true
                });
          }
        }
        else if(message["celeb"]!=null){
          if (message["content"] != null) {
            FanseemApp.navigatorKey.currentState.pushNamed(kRouteFeedDetail,
                arguments: {
                  "feedId": num.parse(message["content"]),
                  "isContent": true,
                  "isUserFeed": false,
                  "celebId": num.parse(message["celeb"])
                });
          } else if (message["celebFeed"] != null) {
            FanseemApp.navigatorKey.currentState.pushNamed(kRouteFeedDetail,
                arguments: {
                  "feedId": num.parse(message["celebFeed"]),
                  "isContent": false,
                  "isUserFeed": false,
                  "celebId": num.parse(message["celeb"])
                });
          } else if (message["feed"] != null) {
            FanseemApp.navigatorKey.currentState.pushNamed(kRouteFeedDetail,
                arguments: {
                  "feedId": num.parse(message["feed"]),
                  "isContent": false,
                  "isUserFeed": true,
                  "celebId": num.parse(message["celeb"])
                });
          }
        }else{
          Fluttertoast.showToast(msg: "잘못된 알림 메시지 입니다.", gravity: ToastGravity.TOP);
        }
      },
    );
  }
}

const String kFirebaseMessageType = "firebaseMessageType";
const String kChatMessage = "chatMessage";
const String kPushNotification = "pushNotification";