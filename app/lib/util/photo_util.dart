import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
Future<File> openGallery() async {
  var imageFile = await ImagePicker().getImage(source: ImageSource.gallery);
  return File(imageFile.path);
}
Future<File> openCamera() async {
  var status = await Permission.photos.status;
  if(status.isGranted||status.isRestricted){
    var imageFile = await ImagePicker().getImage(source: ImageSource.camera);
    if(imageFile!=null) {
      return File(imageFile.path);
    }
    else{
      return null;
    }
  }
  else if(status.isDenied||status.isUndetermined) {
    if (Platform.isIOS&&status.isDenied) {
      await openAppSettings();
      status = await Permission.photos.status;
    }
    else {
      status = await Permission.photos.request();
    }
    if (status.isGranted || status.isRestricted) {
      var imageFile = await ImagePicker().getImage(source: ImageSource.camera);
      if(imageFile!=null) {
        return File(imageFile.path);
      }
      else{
        return null;
      }
    }
    else{
      return null;
    }
  }
  else{
    return null;
  }
}
Future<File> openImagePicker() async {
  var status = await Permission.photos.status;
  if(status.isGranted||status.isRestricted){
    FilePickerResult filePickerResult = await FilePicker.platform.pickFiles(
      type: FileType.image,
    );
    if(filePickerResult!=null) {
      if (filePickerResult.files.first.extension == "bmp") {
        Fluttertoast.showToast(
            msg: ".bmp 확장자 파일은 지원하지 않습니다", gravity: ToastGravity.TOP);
        return null;
      }
      return File(filePickerResult.files.first.path);
    }
    else {
      return null;
    }
  }
  else if(status.isDenied||status.isUndetermined) {
    if (Platform.isIOS&&status.isDenied) {
      await openAppSettings();
      status = await Permission.photos.status;
    }
    else {
      status = await Permission.photos.request();
    }
    if (status.isGranted || status.isRestricted) {
      FilePickerResult filePickerResult = await FilePicker.platform.pickFiles(
        type: FileType.image,
      );
      if (filePickerResult != null) {
        if (filePickerResult.files.first.extension == "bmp") {
          Fluttertoast.showToast(
              msg: ".bmp 확장자 파일은 지원하지 않습니다", gravity: ToastGravity.TOP);
          return null;
        }
        return File(filePickerResult.files.first.path);
      }
      else {
        return null;
      }
    }
    else{
      return null;
    }
  }
  else{
    return null;
  }
}
Future<List<File>> openImagesPicker() async {
  var status = await Permission.photos.status;
  if(status.isGranted||status.isRestricted){
    FilePickerResult filePickerResult = await FilePicker.platform.pickFiles(
      type: FileType.image,
      allowMultiple: true,
    );
    if(filePickerResult!=null) {
      List<File> files = [];
      for (int i = 0; i < filePickerResult.files.length; i++) {
        if (filePickerResult.files[i].extension == "bmp") {
          Fluttertoast.showToast(
              msg: ".bmp 확장자 파일은 지원하지 않습니다", gravity: ToastGravity.TOP);
          return null;
        }
        else {
          files.add(File(filePickerResult.files[i].path));
        }
      }
      return files;
    }
    else{
      return null;
    }
  }
  else if(status.isDenied||status.isUndetermined) {
    if (Platform.isIOS&&status.isDenied) {
      await openAppSettings();
      status = await Permission.photos.status;
    }
    else {
      status = await Permission.photos.request();
    }
    if (status.isGranted || status.isRestricted) {
      FilePickerResult filePickerResult = await FilePicker.platform.pickFiles(
        type: FileType.image,
        allowMultiple: true,
      );
      if(filePickerResult!=null) {
        List<File> files = [];
        for (int i = 0; i < filePickerResult.files.length; i++) {
          if (filePickerResult.files[i].extension == "bmp") {
            Fluttertoast.showToast(
                msg: ".bmp 확장자 파일은 지원하지 않습니다", gravity: ToastGravity.TOP);
            return null;
          }
          else {
            files.add(File(filePickerResult.files[i].path));
          }
        }
        return files;
      }
    }
    else {
      return null;
    }
  }
  else{
    return null;
  }
}
Future<File> openVideoPicker() async {
  var status = await Permission.photos.status;
  if(status.isGranted||status.isRestricted){
    FilePickerResult filePickerResult = await FilePicker.platform.pickFiles(
        type: FileType.video
    );
    if(filePickerResult!=null) {
      List<String> extensions = [
        'mp4',
        'avi',
        'wmv',
        'mov',
        'MP4',
        'AVI',
        'WVM',
        'MOV'
      ];
      if (!extensions.contains(filePickerResult.files.first.extension)) {
        Fluttertoast.showToast(msg: "." + filePickerResult.files.first.extension +
            " 확장자 파일은 지원하지 않습니다.", gravity: ToastGravity.TOP);
        return null;
      }
      else if(filePickerResult.files.first.size>512000){
        Fluttertoast.showToast(msg: "500 MB가 넘는 비디오 파일은 지원하지 않습니다.", gravity: ToastGravity.TOP);
        return null;
      }
      return File(filePickerResult.files.first.path);
    }
    else {
      return null;
    }
  }
  else if(status.isDenied||status.isUndetermined) {
    if (Platform.isIOS&&status.isDenied) {
      await openAppSettings();
      status = await Permission.photos.status;
    }
    else {
      status = await Permission.photos.request();
    }
    if (status.isGranted || status.isRestricted) {
      FilePickerResult filePickerResult = await FilePicker.platform.pickFiles(
          type: FileType.video
      );
      if(filePickerResult!=null) {
        List<String> extensions = [
          'mp4',
          'avi',
          'wmv',
          'mov',
          'MP4',
          'AVI',
          'WVM',
          'MOV'
        ];
        if (!extensions.contains(filePickerResult.files.first.extension)) {
          Fluttertoast.showToast(msg: "." + filePickerResult.files.first.extension +
              " 확장자 파일은 지원하지 않습니다.", gravity: ToastGravity.TOP);
          return null;
        }
        else if(filePickerResult.files.first.size>512000){
          Fluttertoast.showToast(msg: "500 MB가 넘는 비디오 파일은 지원하지 않습니다.", gravity: ToastGravity.TOP);
          return null;
        }
        return File(filePickerResult.files.first.path);
      }
      else {
        return null;
      }
    }
  }
  else{
    return null;
  }
}