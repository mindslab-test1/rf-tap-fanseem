import 'package:firebase_messaging/firebase_messaging.dart';
import 'dart:developer' as developer;

Future<String> getDeviceTokenId() async {
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  try {
    String token = await _firebaseMessaging.getToken();
    developer.log("token: " + token);
    if (token == null) throw Exception("Could not get firebase token!");
    return token;
  }
  catch (e) {
    developer.log(e);
    return null;
  }
}